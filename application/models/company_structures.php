<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Company_Structures extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function find_all(){
    	$q = $this->db->get('organizations');

    	if($q->num_rows() > 0){
    		return $q->result_array();
    	} else {
    		return 0;
    	}
    }

    function find($id){
      $this->db->where(array('id' => $id));
      $q = $this->db->get('organizations');

      if($q->num_rows() > 0){
        return $q->result_array();
      } else {
        return 0;
      }
    }

    function find_parents($id){
      $this->db->where(array('parent_id' => $id));
      $q = $this->db->get('organizations');

      if($q->num_rows() > 0){
        return $q->result_array();
      } else {
        return 0;
      }
    }

    function create($user){
      $q = $this->db->insert('organizations', $user);

      return $q;
    }

    function update($id, $data){
      $this->db->where('id', $id);
      $q = $this->db->update('organizations', $data);

      return $q;
    }

    function remove($id){
      $q = $this->db->delete('organizations', array('id' => $id));

      return $q; 
    }

    function find_distinct_parent(){
      $this->db->select('id, name');
      $q = $this->db->get('organizations');

      if($q->num_rows() > 0){
        return $q->result_array();
      } else {
        return 0;
      }
    }
}