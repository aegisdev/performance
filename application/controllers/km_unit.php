<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Km_unit extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->model->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function index(){	
		$unit_id=$this->session->userdata('organization_id')
		if($unit_id==0){
			$data['data']=$this->model->select('organizations');
		}else{
			$data['data']=$this->model->select('organizations',array('id'=>$unit_id));
		}
		$this->load->view('home/measurements/km_unit',$data);
	}
	public function realization(){	
		$data['data']=$this->model->select('organizations'); 
		$this->load->view('home/measurements/realization',$data);
	}
	public function evaluator(){	
		$data['data']=$this->model->select('organizations'); 
		$this->load->view('home/measurements/evaluator',$data);
	}	
	public function publish(){	
		$sql = "select o.name, km.quarter, km.periode, sum(km.prestasi_penilai) nilai, km.is_published from km, organizations o where quarter = (SELECT max(quarter) FROM `km` where periode = (select max(periode) from km)) and km.organization_id = o.id and km.periode = (select max(periode) from km) group by o.name";
		
		$data['data']= $this->model->query($sql);
		$this->load->view('home/measurements/publish',$data);
	}
	public function rkpp(){	
		$data['data']=$this->model->select('organizations');
		$this->load->view('home/measurements/rkpp',$data);
	}
	public function kompetitor(){	
		$sql="SELECT a.*,b.kpi FROM kompetitor a 
		JOIN kpi b ON (a.kpi_id=b.id)
		";
		$data['data']=$this->model->query($sql);
		$this->load->view('home/measurements/kompetitor',$data);
	}
	
	public function get_kpi_no_quarter(){
		$unit = $this->input->post('unit');
		$periode = $this->input->post('periode');
		
		$sql = "SELECT * FROM km_unit WHERE unit_id=". $id." AND periode=".$periode;
		echo json_encode($this->model->query($sql));
	}
	
	public function choose_kpi(){
		$kpis = $this->input->post('kpi-selected');
		
		for($j=1;$j<5;$j++){
			for($i=0;$i<count($kpis);$i++){
				$data = array(
					'kpi_id' => $kpis[$i],
					'organization_id' => $this->input->post('choose-kpi-unit'),
					'periode' => $this->input->post('choose-kpi-periode'),
					'quarter' => $j //+
				);
				
				$this->model->insert('km',$data);
			}
		}

		echo json_encode(array('status' => 'ok'));
	}
	
	public function get_selected_kpi(){
		$unit = $this->input->post('unit');
		$periode = $this->input->post('periode');	
		$quarter = $this->input->post('quarter'); //+
		
		$sql = "select km.id, km.is_published, km.pencapaian_org, km.keterangan_penilai, km.prestasi_org, km.realisasi_org, km.pencapaian_penilai, km.prestasi_penilai, km.realisasi_penilai, km.kpi_id, kpi.kpi, units.name satuan, perspectives.name perspective, km.bobot, km.target, km.order_no, kpi.formula_id, bp.maksimal maks, bp.minimal min from km, kpi, units, perspectives, batas_penilaian bp where km.kpi_id = kpi.id and kpi.unit_id = units.id and kpi.perspective_id = perspectives.id and km.organization_id = '".$unit."' and periode = '".$periode."' and quarter='".$quarter."' and kpi.batas_penilaian_id = bp.id order by order_no asc, kpi_id asc, perspective asc"; //+

		echo json_encode($this->model->query($sql));
	}
	
	public function get_selected_kpi_no_quarter(){  
		$unit_id = $this->input->post('unit');
		$periode = $this->input->post('periode');	
		$before = $periode - 1;
				
		$sql="SELECT * FROM km_unit WHERE unit_id=$unit_id AND periode=$periode";

		echo json_encode($this->model->query($sql));
	}
	
	public function save_properties_kpi(){
		$kpi =  $this->input->post('kpi-id');
		$count = count($kpi);
		$bobot = $this->input->post('kpi-bobot');
		$target = $this->input->post('kpi-target');
		$order = $this->input->post('kpi-order-no');


		for($j=1;$j<5;$j++){	
			for($i=0;$i<$count;$i++){
				$data = array(
					'bobot' => $bobot[$i],
					'target' => $target[$i],
					'order_no' => $order[$i]
				);
				
				$clause = array(
					'kpi_id' =>$kpi[$i],
					'periode' => $this->input->post('kpi-periode'),
					'organization_id' => $this->input->post('kpi-unit-id'),
					'quarter' => $j
				);
				
				$this->model->update('km',$data, $clause);
			}
		}
		
		echo json_encode(array('status' => 'ok'));
	}
	
	public function delete_kpi_selected(){
		
		$re = $this->model->delete('km', array('kpi_id' => $this->input->post('kpi-id'), 'periode' => $this->input->post('kpi-periode'), 'organization_id' => $this->input->post('kpi-unit-id')));

		if($re > 0){
			echo json_encode(array('status' => 'ok'));
		} else {
			echo json_encode(array('status' => 'error'));
		}
	}
	public function get_rkpm(){
		$unit = $this->input->post('unit');		
		$sql = "SELECT id, kpi from kpi where id NOT IN (select kpi_id from km where organization_id = '".$unit."')";
		echo json_encode($this->model->query($sql));
	}
	public function choose_rkpp(){
		$kpis = $this->input->post('kpi-selected');
		for($i=0;$i<count($kpis);$i++){
			$data = array(
				'kpi_id' => $kpis[$i],
				'rencana_pelaksanaan' => $kpis[$i],
				'keterangan' => $kpis[$i],
				'organization_id' => $this->input->post('choose-kpi-unit')
			);
			
			$this->model->insert('rkpp',$data);
		}
		
		echo json_encode(array('status' => 'ok'));
	}
	
	public function get_selected_rkpp(){
		$unit = $this->input->post('unit');
		
		$sql = "select rkpp.*, rkpp.kpi_id, kpi.kpi, units.name satuan, perspectives.name perspective, rkpp.order_no from rkpp, kpi, units, perspectives where rkpp.kpi_id = kpi.id and kpi.unit_id = units.id and kpi.perspective_id = perspectives.id and rkpp.organization_id = '".$unit."' order by order_no asc, kpi_id asc, perspective asc";

		echo json_encode($this->model->query($sql));
	}
	public function delete_rkpp_selected(){
		
		$re = $this->model->delete('rkpp', array('id' => $this->input->post('id')));

		if($re > 0){
			echo json_encode(array('status' => 'ok'));
		} else {
			echo json_encode(array('status' => 'error'));
		}
	}
	public function save_properties_rkpp(){
		$renc = $this->input->post('rkpp-rencana-pelaksanaan');
		$kpi = $this->input->post('rkpp-id');
		$count = count($kpi);
		$ket = $this->input->post('rkpp-keterangan');
		$orde = $this->input->post('rkpp-order-no');
		
		for($i=0;$i<$count;$i++){
			$data = array(
				'rencana_pelaksanaan' => $renc[$i],
				'keterangan' => $ket[$i],
				'order_no' => $orde[$i]
			);
			
			$clause = array('id' => $kpi[$i]);
			
			$this->model->update('rkpp',$data, $clause);
		}
		
		echo json_encode(array('status' => 'ok'));
	}
	
	public function save_realization_unit(){ //+
		$kpi = $this->input->post('kpi-id');
		$count = count($kpi);
		$real =  $this->input->post('kpi-realization');
		$pres = $this->input->post('kpi-prestasi');
		$penc =  $this->input->post('kpi-pencapaian');

		for($i=0;$i<$count;$i++){
			$data = array(
				'realisasi_org' =>$real[$i],
				'prestasi_org' => $pres[$i],
				'pencapaian_org' => $penc[$i]
			);
			
			$clause = array('id' => $kpi[$i]);
			
			$this->model->update('km',$data, $clause);
		}
		
		echo json_encode(array('status' => 'ok'));
	}
	
	public function save_realization_penilai(){ //+
		$kpi = $this->input->post('kpi-id');
		$count = count($kpi);
		$real =  $this->input->post('kpi-realization');
		$pres = $this->input->post('kpi-prestasi');
		$penc =  $this->input->post('kpi-pencapaian');
		$ket =  $this->input->post('kpi-keterangan');

		for($i=0;$i<$count;$i++){
			$data = array(
				'realisasi_penilai' =>$real[$i],
				'prestasi_penilai' => $pres[$i],
				'pencapaian_penilai' => $penc[$i],
				'keterangan_penilai' => $ket[$i]
			);
			
			$clause = array('id' => $kpi[$i]);
			
			$this->model->update('km',$data, $clause);
		}
		
		echo json_encode(array('status' => 'ok'));
	}
	
	public function publish_prestasi(){
		$submission = $this->model->select('submission_schedule');
		
		if($submission[0]->status == 1){
			$data = array('is_published' => 1);
			$clause = array('periode' => $this->input->post('periode'));
			$this->model->update('km', $data, $clause);
			
			echo json_encode(array('status' => 'ok'));
		} else {
			echo json_encode(array('status' => 'closed'));
		}
	}
	public function rkap(){	
		$data['data']=$this->model->select('organizations');
		$this->load->view('home/measurements/rkap',$data);
	}
	public function get_rkap(){
		$unit = $this->input->post('unit');		
		$sql = "SELECT a.id AS program_kerja_id,a.program_kerja,b.id,b.kpi from program_kerja a,kpi b	
		where a.kpi_id=b.id AND a.id NOT IN (select program_kerja_id from rkap where organization_id = '".$unit."')";
		echo json_encode($this->model->query($sql));
	}
	public function choose_rkap(){
		$kpis = $this->input->post('rkap-selected');
		for($i=0;$i<count($kpis);$i++){
			$data = array(
				'kpi_id' => $kpis[$i],
				'program_kerja_id' => $this->input->post('choose-rkap-program-kerja-id'),
				'organization_id' => $this->input->post('choose-rkap-unit')
			);
			$this->model->insert('rkap',$data);
		}
		echo json_encode(array('status' => 'ok'));
	}
	
	public function get_selected_rkap(){
		$unit = $this->input->post('unit');		
		$sql = "select rkap.*, rkap.kpi_id, kpi.kpi,perspectives.name perspective, rkap.order_no from rkap, kpi, units, perspectives where rkap.kpi_id = kpi.id and kpi.unit_id = units.id and kpi.perspective_id = perspectives.id and rkap.organization_id = '".$unit."' order by order_no asc, kpi_id asc, perspective asc";					
		echo json_encode($this->model->query($sql));
	}
	public function delete_rkap_selected(){
		
		$re = $this->model->delete('rkap', array('id' => $this->input->post('id')));
	
		if($re > 0){
			echo json_encode(array('status' => 'ok'));
		} else {
			echo json_encode(array('status' => 'error'));
		}
	}
	public function save_properties_rkap(){
		$rkap = $this->input->post('rkap-id');
		$count = count($rkap);
		$pel = $this->input->post('rkap-rencana-pelaksanaan');
		$ket = $this->input->post('rkap-keterangan');
		$orde =  $this->input->post('rkap-oder-no');
		
		for($i=0;$i<$count;$i++){
			$data = array(
				'rencana_pelaksanaan' => $pel[$i],
				'keterangan' => $ket[$i],
				'order_no' => $orde[$i]
			);
			
			$clause = array('id' => $rkap[$i]);
			
			$this->model->update('rkap',$data, $clause);
		}
		
		echo json_encode(array('status' => 'ok'));
	}
	
	public function get_published_km(){
		$periode = $this->input->post('periode');
	
		$sql = "select o.name, km.quarter, km.periode, sum(km.prestasi_penilai) nilai, km.is_published from km, organizations o where quarter = 4 and km.organization_id = o.id and km.periode = '".$periode."' group by o.name";
		
		echo json_encode($this->model->query($sql));
	}

	public function get_tingkat_penilaian(){
		json_encode($this->model->select('parameter_level'));
	}
}
