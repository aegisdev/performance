<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Monitor extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->model->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function index(){
		$sql = $sql="SELECT 
			a.id, 
			a.kpi,
			a.formula_id,
			g.name AS perspective_name,
			c.name AS unit,			
			e.name AS formula,
			i.name AS inisiative_name,
			CONCAT(h.minimal,'-',h.maksimal) AS batas_penilaian
			FROM kpi a
			JOIN units c ON (a.unit_id=c.id)
			JOIN formulas e ON (a.formula_id=e.id)
			JOIN perspectives g ON (a.perspective_id=g.id)
			JOIN batas_penilaian h ON (a.batas_penilaian_id=h.id)
			JOIN inisiatives i ON (a.inisiative_id=i.id) WHERE a.flag=1
		";

		$data['data']=$this->model->select('organizations');
		$data['kpi']=$this->db->query($sql);
		$this->load->view('home/monitor/index',$data);
	}

	
	/*public function get_monitor(){
		$periode=$this->input->post('periode');
		$unit_id=$this->input->post('unit_id');
		if(!empty($unit_id)){
			$sql_org="SELECT realisasi as realisasi_org,DATE_FORMAT(tgl_input,'%d/%m/%Y') tgl_input_org FROM log_real_unit WHERE periode='".$periode."' AND  unit_id='".$unit_id."'";
			$data_org=$this->model->query($sql);
			$data_realisasi=array();
			foreach($data_org as $do){
				$arr_org=array(
				'realisasi_org'=>$do->realisasi_org,
				'tgl_input_org'=>$do->tgl_input_org				
				);	
				array_push($data_realisasi,$arr_org);				
			}
			$sql_penilai="SELECT realisasi as realisasi_penilai,DATE_FORMAT(b.tgl_input,'%d/%m/%Y') tgl_input_penilai FROM log_real_penilai WHERE periode='".$periode."' AND  unit_id='".$unit_id."'";
			$data_penilai=$this->model->query($sql_penilai);
			foreach($data_penilai as $dp){
				$arr_penilai=array(
				'realisasi_penilai'=>$dp->realisasi_penilai,
				'tgl_input_penilai'=>$dp->tgl_input_penilai				
				);	
				array_push($data_realisasi,$arr_penilai);				
			}
		}else{
			$sql_org="SELECT realisasi as realisasi_org,DATE_FORMAT(tgl_input,'%d/%m/%Y') tgl_input_org FROM log_real_unit WHERE periode='".$periode."' AND  unit_id='0'";
			$data_org=$this->model->query($sql_org);
			$data_realisasi=array();
			foreach($data_org as $do){
				$arr_org=array(
				'realisasi_org'=>$do->realisasi_org,
				'tgl_input_org'=>$do->tgl_input_org				
				);	
				array_push($data_realisasi,$arr_org);				
			}
			$sql_penilai="SELECT realisasi as realisasi_penilai,DATE_FORMAT(tgl_input,'%d/%m/%Y') tgl_input_penilai FROM log_real_penilai WHERE periode='".$periode."' AND  unit_id='0'";
			$data_penilai=$this->model->query($sql_penilai);
			foreach($data_penilai as $dp){
				$arr_penilai=array(
				'realisasi_penilai'=>$dp->realisasi_penilai,
				'tgl_input_penilai'=>$dp->tgl_input_penilai				
				);	
				array_push($data_realisasi,$arr_penilai);				
			}
		}
		//print_r($data_realisasi);
		//echo $sql;
		
		echo json_encode($data_realisasi);
		
	}*/
	public function get_monitor(){
		$periode=$this->input->post('periode');
		$unit_id=$this->input->post('unit_id');
		$org_id	=$this->input->post('org_id');
		$kpi    =$this->input->post('kpi_id');

		$sql_kpi = ($kpi != -1) ? ' AND kpi_id = "'.$kpi.'" ' : '';

		//if(!empty($unit_id)){
		if($unit_id != 1){
			$sql="SELECT 'org',realisasi,DATE_FORMAT(tgl_input,'%d/%m/%Y') tgl_input FROM log_real_unit WHERE periode='".$periode."' AND unit_id='".$org_id."' ".$sql_kpi." UNION ALL SELECT 'penilai', realisasi,DATE_FORMAT(tgl_input,'%d/%m/%Y') tgl_input FROM log_real_penilai WHERE periode='".$periode."' AND unit_id = '".$org_id."' ".$sql_kpi;
		} else {
			$sql="SELECT 'org',realisasi,DATE_FORMAT(tgl_input,'%d/%m/%Y') tgl_input FROM log_real_unit WHERE periode='".$periode."' AND  unit_id='0' ".$sql_kpi." UNION ALL SELECT 'penilai', realisasi,DATE_FORMAT(tgl_input,'%d/%m/%Y') tgl_input FROM log_real_penilai WHERE periode='".$periode."' ".$sql_kpi;
		}

		$data=$this->model->query($sql);
		echo json_encode($data);
	}

	public function get_realisasi_org(){
		$periode=$this->input->post('periode');
		$unit_id=$this->input->post('unit_id');
		if(!empty($unit_id)){
			$sql="SELECT realisasi as realisasi_org,DATE_FORMAT(tgl_input,'%d/%m/%Y') tgl_input_org FROM log_real_penilai WHERE periode='".$periode."' AND  unit_id='".$unit_id."'";
		}else{
			$sql="SELECT realisasi as realisasi_org,DATE_FORMAT(tgl_input,'%d/%m/%Y') tgl_input_org FROM log_real_penilai WHERE periode='".$periode."'";
		}
		$data=$this->model->query($sql);
		echo json_encode($data);
	}
	public function get_realisasi_penilai(){
		$periode=$this->input->post('periode');
		$unit_id=$this->input->post('unit_id');
		if(!empty($unit_id)){
			$sql="SELECT realisasi as realisasi_penilai,DATE_FORMAT(tgl_input,'%d/%m/%Y') tgl_input_penilai FROM log_real_unit WHERE periode='".$periode."' AND  unit_id='".$unit_id."'";
		}else{
			$sql="SELECT realisasi as realisasi_penilai,DATE_FORMAT(tgl_input,'%d/%m/%Y') tgl_input_penilai FROM log_real_unit WHERE periode='".$periode."'";
		}
		$data=$this->model->query($sql);
		echo json_encode($data);
	}
}