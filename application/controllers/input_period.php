<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Input_period extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->model->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function index(){	
		$data['data']=$this->model->select('submission_schedule');
		$this->load->view('home/input_period/edit',$data);
	}
	public function update(){
		$status=($this->input->post('on_off')=="off")? 0:1;
		$this->data=array(
			'status'=>$status,
			'update_date'=>date('Y-m-d H:i:s'),
			'update_by'=>$this->session->userdata('user_id')
		);				
		echo $this->model->update('submission_schedule',$this->data) ? "1" : "0";	
	}
}
