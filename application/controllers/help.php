<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Help extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->jm->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function guide(){	
		$this->load->view('home/help/guide');
	}	
	public function faq(){	
		$this->load->view('home/help/faq');
	}
}
