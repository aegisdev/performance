<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {
	public function __construct() {
		parent::__construct();		
		$this->load->model('company_structures');			
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}					
	}
	public function index()
	{			
		$start=1;
		$length=7;
		$qKpi = "SELECT 
			a.id, 
			a.kpi,
			a.formula_id,
			g.name AS perspective_name,
			c.name AS unit,			
			e.name AS formula,
			i.name AS inisiative_name,
			CONCAT(h.minimal,'-',h.maksimal) AS batas_penilaian
			FROM kpi a
			JOIN units c ON (a.unit_id=c.id)
			JOIN formulas e ON (a.formula_id=e.id)
			JOIN perspectives g ON (a.perspective_id=g.id)
			JOIN batas_penilaian h ON (a.batas_penilaian_id=h.id)
			JOIN inisiatives i ON (a.inisiative_id=i.id) WHERE a.flag=1
		";

		$user_group_id=$this->session->userdata('user_group_id');	
		$a['tree'] = $this->traverse_tree(0, 0, 0);
		$a['dataKpi'] = $this->model->query($qKpi);
		$a['strategic'] = $this->model->select('themes');

		$this->data=array(
			'css'=>$this->load->view('layout/front/css',array('location' => $this->uri->segment(1), 'action' => $this->uri->segment(2)),true),
			'header'=>$this->load->view('layout/front/header','',true),		
			'left'=>$this->load->view('layout/front/left',array('menus' => $this->sub_menu(0, $h="", $user_group_id)),true),
			'right'=>$this->load->view('layout/front/right',$a,true),
			'footer'=>$this->load->view('layout/front/footer','',true),			
			'js'=>$this->load->view('layout/front/js',array('location' => $this->uri->segment(1), 'action' => $this->uri->segment(2)),true)
		);	
		$this->load->view('home/index',$this->data);
	}	

	public function sub_menu($parent_id = 0, $hasil, $user_group_id){
		$clause = array(
			'parent_id' => $parent_id
		);
		
		$sql="SELECT a.*, c.user_group_id FROM menu a, user_group_role c WHERE a.id = c.menu_id AND c.user_group_id = $user_group_id AND parent_id = ".$parent_id." ORDER BY a.order_no ASC";

		$w = $this->model->query($sql);
		foreach($w as $h){	
			if($h->icon == '')
				$icon = "icon-home";
			else
				$icon = $h->icon;
				
			if($h->parent_id == 0){		
				$sql2 = "SELECT * FROM menu WHERE parent_id = ".$h->id." AND id IN (SELECT menu_id FROM user_group_role WHERE user_group_id = '".$user_group_id."') ORDER BY order_no ASC";
				
				$w2 = $this->model->query($sql2);	

				if(count($w2)>0){	
					if ($h->url !='#'){
						$link="onClick=\"routes('".$h->url."','".$h->name."')\"";
					} else {
						$link="";
					}

					$hasil .= "<li class='has-sub'>";	
					$hasil .= "<a href='javascript:;' $link><i class='".$icon."'></i><span class='title'>".$h->name."</span>";
					$hasil .= "<span class='arrow'></span></a>";			
					$hasil .= "<ul class='sub'>";

					foreach($w2 as $r){	
						if($r->icon=='')
							$icon2="icon-home";
						else
							$icon2=$r->icon;
						
						$hasil .= "<li><a href='#' onClick=\"routes('".$r->url."','".$r->name."')\"><i class='".$icon2."'></i><span class='title'> ".$r->name."</a></li>";							
					}	
					$hasil .= "</ul>";					
				} else {
					$hasil .= "<li class=''>";	
					if($h->url == "home")
						$hasil .= "<a href=home><i class='".$icon."'></i><span class='title'>".$h->name."</span>";	
					else
						$hasil .= "<a href='#' onClick=\"routes('".$h->url."','".$h->name."')\"><i class='".$icon."'></i><span class='title'>".$h->name."</span>";
							
				}
				$hasil .= "</li>";
			}
		}
		return $hasil;
	}
	
	private function traverse_tree($idx, $count, $lvl){
		$d = $this->company_structures->find_parents($idx);	
		if($d == 0 || $lvl == 2){
			return;
		}

		$tree = ($count === 0) ? '<ul id="org-chart-dua">' : '<ul>';
		for($i=0;$i<count($d);$i++){
			$sub_d = $this->company_structures->find_parents($d[$i]['id']);

			$tree .= '<li data-id="'.$d[$i]['id'].'">';
			$tree .= $d[$i]['name'] . '<br/><span class="org-chart-title">'. $d[$i]['official_name'].'</span>';
			$tree .= '<br/><span>&nbsp;</span>';
        	$tree .= $this->traverse_tree($d[$i]['id'], 1, $lvl + 1);
       	 	$tree .= '</li>';
		}

		$tree .= '</ul>';
		return $tree;
	}

	public function structure($idx){
		echo $this->traverse_tree($idx, 0, 0);
	}
	
	public function get_stat_perspektif(){
		// $date = getDate();
		// $periode = $date['year'];

		// $sql = "select ps.name name, (sum(km.prestasi_penilai) / sum(km.bobot) * 100) persen from km, kpi, perspectives ps where km.organization_id = 2 and km.kpi_id = kpi.id and kpi.perspective_id = ps.id and km.quarter = (select max(quarter) from (SELECT CASE prestasi_penilai WHEN 0 THEN NULL ELSE 4 END quarter FROM km WHERE quarter = 4 and periode = '". $periode ."' and organization_id = 2 UNION SELECT CASE prestasi_penilai WHEN 0 THEN NULL ELSE 3 END FROM km WHERE quarter = 3 and periode = '". $periode ."' and organization_id = 2 UNION SELECT CASE prestasi_penilai WHEN 0 THEN NULL ELSE 2 END FROM km WHERE quarter = 2 and periode = '". $periode ."' and organization_id = 2 UNION SELECT CASE prestasi_penilai WHEN 0 THEN NULL ELSE 1 END FROM km WHERE quarter = 1 and periode = '". $periode ."' and organization_id = 2) last) group by ps.name UNION select 'Total' total, sum(km.prestasi_penilai) from km where quarter = (select max(quarter) from (SELECT CASE prestasi_penilai WHEN 0 THEN NULL ELSE 4 END quarter FROM km WHERE quarter = 4 and periode = '". $periode ."' and organization_id = 2 UNION SELECT CASE prestasi_penilai WHEN 0 THEN NULL ELSE 3 END FROM km WHERE quarter = 3 and periode = '". $periode ."' and organization_id = 2 UNION SELECT CASE prestasi_penilai WHEN 0 THEN NULL ELSE 2 END FROM km WHERE quarter = 2 and periode = '". $periode ."' and organization_id = 2 UNION SELECT CASE prestasi_penilai WHEN 0 THEN NULL ELSE 1 END FROM km WHERE quarter = 1 and periode = '". $periode ."' and organization_id = 2) last) and periode = '".$periode."' group by organization_id order by name";
		
		// echo json_encode($this->model->query($sql));
		echo '';
	}
	
	public function get_gauge_perspective(){
		$periode = $this->input->post('periode');

		$sql = "select ps.order_no, ps.id, ps.name name, sum(km.bobot) jm_bobot, sum(km.prestasi_penilai) persen, (sum(km.prestasi_penilai) / sum(km.bobot) * 100) sebelum_persen,  0 as min, ((120/100) * sum(km.bobot)) max from km, kpi, perspectives ps where km.kpi_id = kpi.id and kpi.perspective_id = ps.id and periode = '".$periode."' group by name union select 10 as order_no, 0 as id, 'Perusahaan' as name, sum(km.bobot), sum(km.prestasi_penilai), sum(km.prestasi_penilai), 0 as min, 120 as max from km, kpi, perspectives ps where km.kpi_id = kpi.id and kpi.perspective_id = ps.id and periode = '".$periode."' group by periode order by order_no";
		
		echo json_encode($this->model->query($sql));
	}

	public function get_stacked_rkm(){
		$periode = $this->input->post('periode');
		
		$direktorat = "select id, alias from organizations where id < 18";
		$dirColl = $this->model->query($direktorat);

		$perDirQuery = "";

		for($i=0;$i<count($dirColl);$i++){
			$perDirQuery .= ", (select count(unit_id) from rencana_kerja where unit_id = '".$dirColl[$i]->id."' and periode = '".$periode."' and progress_id = p.id group by progress_id) ".$dirColl[$i]->alias." ";
		}

		$sql = "select p.id ".$perDirQuery." from progress p order by p.id";

		echo json_encode($this->model->query($sql));
	}

	public function get_prestasi_perusahaan(){
		$periode = $this->input->post('periode');

		$direktorat = "select id, alias from organizations where id < 18 order by alias";
		$dirColl = $this->model->query($direktorat);

		$perDirQuery = "";

		for($i=0;$i<count($dirColl);$i++){
			$perDirQuery .= "(select sum(prestasi_penilai) from km_unit where unit_id = '".$dirColl[$i]->id."' and periode = '".$periode."' group by unit_id) ".$dirColl[$i]->alias.", ";
		}

		$sql = "select ".$perDirQuery." (select sum(prestasi_penilai) P from km where periode = '".$periode."') PU from organizations o limit 1";

		echo json_encode($this->model->query($sql));
	}

	public function get_realisasi_km(){
		$kpiId 		= $this->input->post('kpi_id');
		$periode 	= $this->input->post('periode');
		$awal 		= $periode - 9;
		$has 		= array();
		$r 			= "";
		$bp 		= "";
		$k 			= "";
		$bn 		= "";
		$ctr 		= 0;

		for($awal;$awal <= $periode;$awal++){
			$ctr++;
			$r  .= "(select realisasi_penilai from km where kpi_id = '".$kpiId."' and periode = '".$awal."') tahun_".$ctr.", ";
			$bp .= "(select best_performance from kompetitor where kpi_id = '".$kpiId."' and tahun = '".$awal."') tahun_".$ctr.", "; 
			$k  .= "(select kompetitor from kompetitor where kpi_id = '".$kpiId."' and tahun = '".$awal."') tahun_".$ctr.", "; 
			$bn .= "(select benchmark from kompetitor where kpi_id = '".$kpiId."' and tahun = '".$awal."') tahun_".$ctr.", "; 	
		}

		$sqlOne = "select " . $r . " 0 edit from DUAL";

		$sqlTwo = "select " . $bp . " 0 edit from DUAL";

		$sqlThree = "select " . $k . " 0 edit from DUAL";

		$sqlFour = "select " . $bn . " 0 edit from DUAL";

		array_push($has, $this->model->query($sqlOne),  $this->model->query($sqlTwo),  $this->model->query($sqlThree),  $this->model->query($sqlFour));

		echo json_encode($has);
	}

	public function get_calendar_data(){
		$sql = "select DATE_FORMAT(lp.tgl_input, '%Y-%m-%d') as tgl_input, u.name, u.username, 'penilai' as type, 'Verifikasi' as aksi from log_real_penilai lp left join users u on lp.user_id = u.id union select DATE_FORMAT(lu.tgl_input, '%Y-%m-%d') as tgl_input, u.name, u.username, 'unit' as type, 'Submit' as aksi from log_real_unit lu left join users u on lu.user_id = u.id ORDER BY CONVERT(tgl_input, date) ASC";
		
		echo json_encode($this->model->query($sql));
	}
	
	public function get_detail_gauge($id = 0){
		$id = $this->input->post('perspective-id');
		$periode = $this->input->post('periode');
		
		if($id != 0)
			$sql = "SELECT km.*, u.name satuan,kpi.kpi kpi_name, perspectives.name perspective FROM km, kpi, perspectives, units u WHERE kpi.perspective_id = perspectives.id and km.kpi_id = kpi.id and km.periode = '".$periode."' and kpi.unit_id = u.id and perspectives.id = '".$id."' order by perspectives.order_no, km.order_no";
		else 
			$sql = "SELECT km.*, u.name satuan,kpi.kpi kpi_name, perspectives.name perspective FROM km, kpi, perspectives, units u WHERE kpi.perspective_id = perspectives.id and km.kpi_id = kpi.id and km.periode = '".$periode."' and kpi.unit_id = u.id order by perspectives.order_no, km.order_no";

		echo json_encode($this->model->query($sql));
	}
	
	public function get_detail_pelaksanaan_rkm()
	{
		$unit=$this->input->post('kode-direktorat');
		$periode=$this->input->post('periode');
		
		$sql='SELECT a.*, o.id, o.name org_name, b.program_kerja, a.keterangan, a.value FROM rencana_kerja a JOIN program_kerja b ON (a.program_kerja_id=b.id) JOIN organizations o ON o.id = a.unit_id AND o.alias="'.$unit.'" AND a.periode="'.$periode.'"';
		echo json_encode($this->model->query($sql));
	}

	public function get_detail_gauge_kpi(){
		$periode = $this->input->post('periode');
		//$kpiId = $this->input->post('kpi-id');

		//$sql="select o.name, o.id, ku.realisasi_penilai, p.bobot, ku.kpi_id, ku.periode, ((p.bobot/100 ) *  ku.realisasi_penilai) kontribusi from organizations o right join km_unit ku on ku.unit_id = o.id join pelaksana_kpi p on o.id = p.unit_id where o.id < 18 and ku.kpi_id = '".$kpiId."' and p.kpi_id = '".$kpiId."' and ku.periode = '".$periode."'";
		//echo json_encode($this->model->query($sql));

		$kpi_id=$this->input->post('kpi-id');
		$organization_id=$this->input->post('organization_id');
		
		if(!empty($kpi_id) && !empty($organization_id)){
			$where="AND a.kpi_id='".$kpi_id."' AND a.organization_id='".$organization_id."'";
		} else if(!empty($kpi_id) && empty($organization_id)){
			$where="AND a.kpi_id='".$kpi_id."'";
		} else if(empty($kpi_id) && !empty($organization_id)){
			$where="AND a.organization_id='".$organization_id."'";
		} else {
			$where="";
		}		

		$sql="SELECT a.*, b.kpi, c.name org_name, r.progress_id, r.value as progress, r.tanggal_input as ti FROM program_kerja a LEFT JOIN kpi b ON(a.kpi_id=b.id) JOIN organizations c ON (a.organization_id=c.id) JOIN rencana_kerja r ON (r.program_kerja_id = a.id) WHERE a.flag=1 AND a.periode = ".$periode." ".$where;			
		echo json_encode($this->model->query($sql));
	}

	function get_history_rkm(){
		$periode = $this->input->post('periode');
		$thisYear = date('Y');

		if($periode >= $thisYear){
			$month = date('n');
		} else {
			$month = 12;
		}

		$seriesOne = "SELECT d.bulan, (SELECT COUNT(*) FROM (SELECT max(progress_id) as progress_id, MONTH(date) as bulan, periode FROM history_progress where periode = ".$periode." and MONTH(date) <= ".$month." group by id_rk, MONTH(date) ORDER BY bulan) as z WHERE z.bulan = d.bulan AND z.periode = ".$periode." AND z.progress_id = d.progress_id) jml FROM (SELECT * FROM (SELECT MONTH(date) bulan FROM history_progress where periode = ".$periode." group by MONTH(date)) AS a, (SELECT 1 as progress_id UNION SELECT 2 UNION SELECT 3) AS c) AS d WHERE progress_id = 1 ORDER BY d.bulan";

		$seriesTwo = "SELECT d.bulan, (SELECT COUNT(*) FROM (SELECT max(progress_id) as progress_id, MONTH(date) as bulan, periode FROM history_progress where periode = ".$periode." and MONTH(date) <= ".$month." group by id_rk, MONTH(date) ORDER BY bulan) as z WHERE z.bulan = d.bulan AND z.periode = ".$periode." AND z.progress_id = d.progress_id) jml FROM (SELECT * FROM (SELECT MONTH(date) bulan FROM history_progress where periode = ".$periode." group by MONTH(date)) AS a, (SELECT 1 as progress_id UNION SELECT 2 UNION SELECT 3) AS c) AS d WHERE progress_id = 2 ORDER BY d.bulan";

		$seriesThree = "SELECT d.bulan, (SELECT COUNT(*) FROM (SELECT max(progress_id) as progress_id, MONTH(date) as bulan, periode FROM history_progress where periode = ".$periode." and MONTH(date) <= ".$month." group by id_rk, MONTH(date) ORDER BY bulan) as z WHERE z.bulan = d.bulan AND z.periode = ".$periode." AND z.progress_id = d.progress_id) jml FROM (SELECT * FROM (SELECT MONTH(date) bulan FROM history_progress where periode = ".$periode." group by MONTH(date)) AS a, (SELECT 1 as progress_id UNION SELECT 2 UNION SELECT 3) AS c) AS d WHERE progress_id = 3 ORDER BY d.bulan";
	
		echo json_encode(array("a" => $this->model->query($seriesOne), "b" => $this->model->query($seriesTwo),"c" => $this->model->query($seriesThree)));
	}

	function get_detail_history_rkm(){
		$periode = $this->input->post('periode');
		$bln = $this->input->post('month');

		$sql = "SELECT pk.program_kerja, o.name unit_name, hp.id_rk, max(hp.value) progress, max(hp.progress_id) as progress_id, MONTH(hp.date) as bulan, hp.periode 
						FROM history_progress hp, program_kerja pk, organizations o 
						WHERE hp.periode = ".$periode." AND MONTH(hp.date) = ".$bln." AND hp.id_rk = pk.id AND o.id = hp.unit_id
						GROUP BY hp.id_rk, MONTH(hp.date) 
						ORDER BY bulan";

		echo json_encode(array("a" => $this->model->query($sql)));
	}
}
