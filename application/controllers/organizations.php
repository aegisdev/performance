<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Organizations extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		$this->load->model('company_structures');	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function all(){
		$data['location'] = $this->company_structures->find_all();
		
		$data['tree'] = $this->traverse_tree(0,0);
		$data['parent'] = $this->company_structures->find_distinct_parent();	
		$this->load->view('home/organizations/company_structure', $data);
	}

	public function id($id){
		$user = $this->company_structures->find($id);	
		if($user > 0){
			echo json_encode(array('status' => 'ok', 'data' => $user[0]));
		} else {
			echo json_encode(array('status' => 'error'));
		}
	}

	public function add(){
		$data = array(
				'name' => $this->input->post('name'),				
				'official_name' => $this->input->post('official-name'),
				'nik' => $this->input->post('nik'),
				'position' => $this->input->post('position'),
				'parent_id' => $this->input->post('parent')
			);
		$new = $this->company_structures->create($data);

		if($new > 0){
			echo json_encode(array('status' => 'ok'));
		} else {
			echo json_encode(array('status' => 'error'));
		}
	}

	public function delete(){
		$id = $this->input->post('id');
		$del = $this->company_structures->remove($id);

		if($del > 0){
			echo json_encode(array('status' => 'ok'));
		} else {
			echo json_encode(array('status' => 'error'));
		}
	}

	public function edit(){
		$id = $this->input->post('id');

		$data = array(
				'name' => $this->input->post('edit-name'),
				'official_name' => $this->input->post('edit-official-name'),
				'nik' => $this->input->post('edit-nik'),
				'position' => $this->input->post('edit-position'),
				'parent_id' => $this->input->post('edit-parent')
			); 
		$upd = $this->company_structures->update($id, $data);

		if($upd > 0){
			echo json_encode(array('status' => 'ok'));	
		} else {
			echo json_encode(array('status' => 'error'));			
		}
	}

	private function traverse_tree($idx, $count){
		$d = $this->company_structures->find_parents($idx);	
		if($d == 0){
			return;
		}

		$tree = ($count === 0) ? '<ul id="org-chart">' : '<ul>';
		for($i=0;$i<count($d);$i++){
			$sub_d = $this->company_structures->find_parents($d[$i]['id']);

			$tree .= '<li data-id="'.$d[$i]['id'].'">';
			$tree .= $d[$i]['name'] . '<br/><span class="org-chart-title">'. $d[$i]['official_name'].'</span>';
        	$tree .= $this->traverse_tree($d[$i]['id'], 1);
       	 	$tree .= '</li>';
		}

		$tree .= '</ul>';
		return $tree;
	}

	public function structure($idx){
		echo $this->traverse_tree($idx, 0);
	}
	public function get_organization_by_parent_id(){
		$parent_id=$this->input->post('parent_id');
		$data=$this->model->select('organizations',array('parent_id'=>$parent_id));
		echo json_encode($data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */