<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Main extends CI_Controller {
	public function __construct() {
		parent::__construct();			
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}		
	}

	public function index()
	{			
		$user_group_id=$this->session->userdata('user_group_id');
		$this->data=array(
		 	'css'=>$this->load->view('layout/front/css',array('location' => $this->uri->segment(1), 'action' => $this->uri->segment(2)),true),
			'header'=>$this->load->view('layout/front/header','',true),
			'right'=>$this->load->view('layout/front/right','',true),
			'footer'=>$this->load->view('layout/front/footer','',true),			
			'js'=>$this->load->view('layout/front/js',array('location' => $this->uri->segment(1), 'action' => $this->uri->segment(2)),true),
		);		

		$this->load->view('main/index',$this->data);
	}	
}
