<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Parameter_level extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->model->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function index(){	
		$data['data']=$this->model->select('parameter_level');
		$this->load->view('home/parameter_level/edit',$data);
	}
	public function update(){
		$id = $this->input->post('id');

		for($i=0;$i<count($id);$i++)
		{
			$p_min = $_POST['minimal'];
			$p_max = $_POST['maksimal'];

			if($p_min[$i]==NULL)
			{
				$minimal=NULL;
			}
			else{
				$minimal=$p_min[$i];
			}
			if($p_max[$i]==NULL)
			{
				$maksimal=NULL;
			}
			else{
				$maksimal=$p_max[$i];
			}
				
			$this->data=array(
				'minimal'=>$minimal,
				'maksimal'=>$maksimal
			);
			$this->model->update('parameter_level',$this->data,array('id'=>$id[$i]));	
		}		
		echo "1";
	}
}
