<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kpi extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->model->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function index(){	
		$sql="SELECT 
			a.id, 
			a.kpi,
			a.formula_id,
			g.name AS perspective_name,
			c.name AS unit,			
			e.name AS formula,
			i.name AS inisiative_name,
			CONCAT(h.minimal,'-',h.maksimal) AS batas_penilaian
			FROM kpi a
			JOIN units c ON (a.unit_id=c.id)
			JOIN formulas e ON (a.formula_id=e.id)
			JOIN perspectives g ON (a.perspective_id=g.id)
			JOIN batas_penilaian h ON (a.batas_penilaian_id=h.id)
			JOIN inisiatives i ON (a.inisiative_id=i.id) WHERE a.flag=1
		";
		
		$data['data']=$this->model->query($sql);	
		$data['perspectives']=$this->model->select('perspectives');	
		$data['kpi']=$this->model->query($sql);	
		//echo $this->db->last_query();
		$this->load->view('home/kpi/index',$data);
	}
	public function add(){	
		$data['perspectives']=$this->model->select('perspectives');
		$data['objectives']=$this->model->select('objectives');		
		$data['units']=$this->model->select('units');
		$data['scores']=$this->model->select('scores');
		$data['formulas']=$this->model->select('formulas');		
		$data['batas_penilaian']=$this->model->select('batas_penilaian');
		//$data['inisiatives']=$this->model->select('inisiatives');
		$data['inisiatives']=$this->db->query('SELECT a.*
								              FROM inisiatives a
											  JOIN themes b ON (a.theme_id=b.id)
								              WHERE 1=1')->result();
		//$sql="SELECT id, CONCAT(minimal,'-',maksimal) batas_penilaian FROM batas_penilaian";
		$this->load->view('home/kpi/add',$data);
	}
	public function save(){	
		$data=array(
			'kpi' => $this->input->post('kpi'),
			'batas_penilaian_id' => $this->input->post('batas_penilaian_id'),
			'perspective_id' => $this->input->post('perspective_id'),
			'inisiative_id' => $this->input->post('inisiative_id'),
			'unit_id' => $this->input->post('unit_id'),
			'formula_id' => $this->input->post('formula_id'),
			'batas_penilaian' => '2000-01-01',
			'objective_id' => 0,
			'score_id' => 0,
			'organization_id' => 0
		);

		if($this->model->insert('kpi',$data))
		{	
			echo "1";
		}else{
			echo "0";
		}
	}
	public function edit($id){	
		$data['perspectives']=$this->model->select('perspectives');
		$data['units']=$this->model->select('units');
		$data['formulas']=$this->model->select('formulas');
		$data['batas_penilaian']=$this->model->select('batas_penilaian');
		$data['inisiatives']=$this->model->select('inisiatives');
		$sql="SELECT a.*
			FROM kpi a
			JOIN units c ON (a.unit_id=c.id)
			JOIN formulas e ON (a.formula_id=e.id)
			JOIN perspectives g ON (a.perspective_id=g.id)
			JOIN batas_penilaian h ON (a.batas_penilaian_id=h.id)		
			WHERE a.id=".$id;			
	
		$data['data']=$this->model->query($sql);			
		$this->load->view('home/kpi/edit',$data);
	}
	public function update(){	
		$clause	=array('id'=>$this->input->post('id'));
		$data=array(
			'kpi'=>$this->input->post('kpi'),
			'batas_penilaian_id'=>$this->input->post('batas_penilaian_id'),
			'perspective_id'=>$this->input->post('perspective_id'),
			'objective_id'=>$this->input->post('objective_id'),
			'inisiative_id' => $this->input->post('inisiative_id'),
			'unit_id'=>$this->input->post('unit_id'),
			'formula_id'=>$this->input->post('formula_id')
		);
		
		if($this->model->update('kpi',$data,$clause))
		{	
			echo "1";
		}else{
			echo "0";
		}
	}
	public function delete(){	
		$this->model->delete('pemilik_kpi', array('kpi_id' => $this->input->post('id')));
		$this->model->delete('pendukung_kpi', array('kpi_id' => $this->input->post('id')));
		$this->model->delete('pelaksana_kpi', array('kpi_id' => $this->input->post('id')));
		$this->model->delete('km_unit', array('kpi_id' => $this->input->post('id')));
		$this->model->delete('km', array('kpi_id' => $this->input->post('id')));

		$clause=array('id'=>$this->input->post('id'));
		$data=array('flag'=>0);
		echo $this->model->update('kpi',$data,$clause)? "1":"0";
	}
	public function map(){	
		$sql="SELECT * FROM perspectives";
		$data['data']=$this->model->query($sql);	
		$this->load->view('home/kpi/map',$data);
	}
	public function program_kerja(){	
		
		$sql="SELECT a.*,b.kpi,c.name AS organization_name FROM program_kerja a
		LEFT JOIN kpi b ON(a.kpi_id=b.id)
		JOIN organizations c ON (a.organization_id=c.id)
		WHERE a.flag=1";

		$kpi="SELECT 
			a.id, 
			a.kpi,
			a.formula_id,
			g.name AS perspective_name,
			c.name AS unit,			
			e.name AS formula,
			i.name AS inisiative_name,
			CONCAT(h.minimal,'-',h.maksimal) AS batas_penilaian
			FROM kpi a
			JOIN units c ON (a.unit_id=c.id)
			JOIN formulas e ON (a.formula_id=e.id)
			JOIN perspectives g ON (a.perspective_id=g.id)
			JOIN batas_penilaian h ON (a.batas_penilaian_id=h.id)
			JOIN inisiatives i ON (a.inisiative_id=i.id) WHERE a.flag=1
		";


		$data['data']=$this->model->query($sql);
		$data['kpi']=$this->model->query($kpi);	
		$data['organizations']=$this->model->select('organizations');
					
		$this->load->view('home/program_kerja/index',$data);
	}
	public function program_kerja_add(){	
		$kpi="SELECT 
			a.id, 
			a.kpi,
			a.formula_id,
			g.name AS perspective_name,
			c.name AS unit,			
			e.name AS formula,
			i.name AS inisiative_name,
			CONCAT(h.minimal,'-',h.maksimal) AS batas_penilaian
			FROM kpi a
			JOIN units c ON (a.unit_id=c.id)
			JOIN formulas e ON (a.formula_id=e.id)
			JOIN perspectives g ON (a.perspective_id=g.id)
			JOIN batas_penilaian h ON (a.batas_penilaian_id=h.id)
			JOIN inisiatives i ON (a.inisiative_id=i.id) WHERE a.flag=1
		";

		$sql="SELECT * FROM program_kerja WHERE flag=1";
		$data['kpis']=$this->model->query($kpi);
		$data['organizations']=$this->model->select('organizations');
		$data['data']=$this->model->query($sql);	
		$this->load->view('home/program_kerja/add',$data);
	}	
	public function program_kerja_save(){	
		/*
		$this->load->library('upload');

		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'pdf';
		$config['encrypt_name'] = true;
		
		$this->upload->initialize($config);
		$upload = $this->upload->do_upload('mFile');

		if($upload)
		{
			$file = $this->upload->data();
		*/
			$data=array(
				'kpi_id'=>$this->input->post('kpi_id'),
				'organization_id'=>$this->input->post('organization_id'),
				'program_kerja'=>$this->input->post('program_kerja'),
				'keterangan'=>$this->input->post('keterangan'),
				'file_url' => $this->input->post('file'),
				'periode' => $this->input->post('periode')
			);	

			if($this->model->insert('program_kerja',$data))
			{			
				echo json_encode(array('status' => 'ok'));
			}else{
				echo json_encode(array('status' => 'error', 'note' => 'Data gagal ditambahkan.'));
			}
		/*
		}
		else
		{
			echo json_encode(array('status' => 'error','note' => $this->upload->display_errors() ));
		}
		*/
	}
	public function program_kerja_edit($id){		
		$kpi="SELECT 
			a.id, 
			a.kpi,
			a.formula_id,
			g.name AS perspective_name,
			c.name AS unit,			
			e.name AS formula,
			i.name AS inisiative_name,
			CONCAT(h.minimal,'-',h.maksimal) AS batas_penilaian
			FROM kpi a
			JOIN units c ON (a.unit_id=c.id)
			JOIN formulas e ON (a.formula_id=e.id)
			JOIN perspectives g ON (a.perspective_id=g.id)
			JOIN batas_penilaian h ON (a.batas_penilaian_id=h.id)
			JOIN inisiatives i ON (a.inisiative_id=i.id) WHERE a.flag=1
		";


		$data['kpis']=$this->model->query($kpi);
		$data['organizations']=$this->model->select('organizations');		
		$sql="SELECT * FROM program_kerja WHERE id=".$id;
		$data['data']=$this->model->query($sql);	
		$this->load->view('home/program_kerja/edit',$data);
	}


	public function program_kerja_update(){	
		$clause = array('id'=>$this->input->post('id'));

		// $this->load->library('upload');
		// $config['upload_path'] = './uploads/';
		// $config['allowed_types'] = 'pdf';
		// $config['encrypt_name'] = true;
		
		// $this->upload->initialize($config);
		// $this->upload->do_upload('mFile');
		// $file = $this->upload->data();
		
		$data=array(
			'kpi_id'=>$this->input->post('kpi_id'),
			'organization_id'=>$this->input->post('organization_id'),
			'program_kerja'=>$this->input->post('program_kerja'),
			'keterangan'=>$this->input->post('keterangan'),
			'file_url' => $this->input->post('mFile')
		);	
		if($this->model->update('program_kerja',$data,$clause))
		{
			echo json_encode(array('status' => 'ok'));
		}else{
			echo json_encode(array('status' => 'error'));
		}
	}
	
	public function program_kerja_delete(){	
		$clause=array(
			'id'=>$this->input->post('id')			
		);
		$data=$this->model->select('program_kerja',$clause);
		if(!empty($data[0]->file_url) && file_exists("./uploads/".$data[0]->file_url)){
			unlink ("./uploads/".$data[0]->file_url);
			echo $this->model->delete('program_kerja',$clause)? "1":"0";
		}else{
			echo $this->model->delete('program_kerja',$clause)? "1":"0";
		}
	}
	public function get_kpi(){		
		$perspective_id=$this->input->post('perspective_id');
		$kpi_id=$this->input->post('kpi_id');
	
		if(!empty($kpi_id) && !empty($perspective_id)){
			$where="AND a.id='".$kpi_id."' AND a.perspective_id='".$perspective_id."'";
		}else if(!empty($kpi_id) && empty($perspective_id))
		{
			$where="AND a.id='".$kpi_id."'";
		}else if(empty($kpi_id) && !empty($perspective_id))
		{
			$where="AND a.perspective_id='".$perspective_id."'";
		}else{
			$where="";
		}		
			$sql="SELECT 
				a.id, 
				a.kpi,
				a.formula_id,
				g.name AS perspective_name,
				c.name AS unit,			
				e.name AS formula,
				i.name AS inisiative_name,
				CONCAT(h.minimal,'-',h.maksimal) AS batas_penilaian
				FROM kpi a
				JOIN units c ON (a.unit_id=c.id)
				JOIN formulas e ON (a.formula_id=e.id)
				JOIN perspectives g ON (a.perspective_id=g.id)
				JOIN batas_penilaian h ON (a.batas_penilaian_id=h.id)
				JOIN inisiatives i ON (a.inisiative_id=i.id) WHERE a.flag=1 ".$where;	
		
		echo json_encode($this->model->query($sql));
	}
	
	public function get_program_kerja(){	
		$kpi_id=$this->input->post('kpi_id');
		$organization_id=$this->input->post('organization_id');
		$periode = $this->input->post('periode');
		
		if(!empty($kpi_id) && !empty($organization_id) && !empty($periode)){
			$where="AND a.kpi_id='".$kpi_id."' AND a.organization_id='".$organization_id."' AND a.periode=" . $periode;
		}else if(!empty($kpi_id) && empty($organization_id))
		{
			$where="AND a.kpi_id='".$kpi_id."'";
		}else if(empty($kpi_id) && !empty($organization_id))
		{
			$where="AND a.organization_id='".$organization_id."'";
		}else if($periode){
			$where="AND a.periode=" . $periode;
		}	else {
			$where="";
		}	 
		$sql="SELECT a.*,b.kpi,c.name AS organization_name FROM program_kerja a
		LEFT JOIN kpi b ON(a.kpi_id=b.id)
		JOIN organizations c ON (a.organization_id=c.id)
		WHERE a.flag=1 ".$where;			
		echo json_encode($this->model->query($sql));
	}

	public function unggah()
	{
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'jpg|jpeg|gif|pdf';
		$config['max_size']	= '100000';
		$config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('files'))
		{
			$msg = '{"success":false, "message": "'. $this->upload->display_errors() .'" }';
		}
		else
		{
			$uploaded_file = $this->upload->data();
			$file_name = $uploaded_file['file_name'];
			$uploadfile = $uploaded_file['orig_name'];
			$location_file = $uploaded_file['full_path'];
			$file_size = $uploaded_file['file_size'];
			$file_type = $uploaded_file['file_type'];

			$msg = '{"files":[{"name":"'.$file_name.'","files":"'.$uploadfile.'","location":"'.$location_file.'","size":'.$file_size.',"type":"'.$file_type.'"}]}';	

		}

		// $path		= 'uploads/';
		// $timestamp 	= date("Ymd")."/";
		
		// $file_name 	= $_FILES['files']['name'];
		// $file_type 	= $_FILES['files']['type'];
		// $file_tname = $_FILES['files']['tmp_name'];
		// $file_size 	= $_FILES['files']['size'];		

		// $max_size   = 10000;
		// $allow_type = array('application/pdf');
		
		// $file_size_display = number_format(($file_size / 1048576), 2, ',', '.');
		// $file_size 		   = floor($file_size / 10000);	
	
		// if($file_size >= $max_size) 
		// {
		// 	$msg = '{"success":false, "message":'.json_encode('File '.$file_name.'('.$file_size_display.' MB) yang anda upload memiliki ukuran melebihi yang diperbolehkan, yaitu '.$max_size.' MB.').'}';
		// } 
		// elseif(!in_array($file_type, $allow_type))
		// {
		// 	$msg = '{"success":false, "message":'.json_encode('Type file yang anda upload tidak diijinkan '.$file_type.' ').'}';
		// }
		// else 
		// {
		// 	//replace karakter yang tidak diperbolehkan
		// 	$vowels = array(" ", "'", "\"", ",");
		// 	$file_name = date("YmdHis") . "_" . str_replace($vowels, "_", basename($file_name));
			
		// 	/*
		// 	if(!is_dir($path . $timestamp))
		// 	{
		// 		mkdir($path . $timestamp, 0777);
		// 	}
		// 	*/ 
		
		// 	//$uploadfile   = $path . $timestamp . $file_name;
		// 	$uploadfile    = $path . $file_name;
		// 	$location_file = $file_name;

		// 	if (move_uploaded_file($_FILES['files']['tmp_name'], $uploadfile)) 
		// 	{
		// 		$msg = '{"files":[{"name":"'.$file_name.'","files":"'.$uploadfile.'","location":"'.$location_file.'","size":'.$file_size.',"type":"'.$file_type.'"}]}';	
		// 	} 
		// 	else 
		// 	{
		// 		$msg = '{"success":false, "message":'.json_encode('Proses upload file gagal. '.$file_size_display.'').'}';
		// 	}
		// }		
		echo $msg;	
	}

	function get_kpi_periode(){
		$periode = $this->input->post('periode');
		$unit_id = $this->input->post('unit');
		
		if($unit_id < 0){
			$clause = "kmu.unit_id IS NOT NULL";
		} else {
			$clause = "kmu.unit_id = " . $unit_id;
		}


		$sql = "SELECT DISTINCT kmu.kpi_id id, k.kpi text FROM km_unit kmu, kpi k WHERE k.id = kmu.kpi_id AND periode = ".$periode." AND  ".$clause." ORDER BY kmu.kpi_id ";
		
		echo json_encode($this->model->query($sql));
	}
}
