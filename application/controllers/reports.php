<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Reports extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		$this->load->model('company_structures');	
		//$this->model->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function performance(){			
		$data['data']=$this->model->select('organizations');
		$data['level']=$this->model->select('parameter_level');

		$this->load->view('home/reports/performance',$data);
	}
	public function series(){	
		$data['data']='';
		$this->load->view('home/reports/series',$data);
	}
	public function kpi(){	
		$data['perspectives'] = $this->model->select('perspectives');
		$this->load->view('home/reports/kpi',$data);
	}

	public function unit(){			
		$unit_id=$this->session->userdata('organization_id');
		//Tidak punya unit_id
		if($unit_id==0){
			$data['data']=$this->model->select('organizations');
		}else{
			$data['data']=$this->model->select('organizations',array('id'=>$unit_id));
		}
		$data['level']=$this->model->select('parameter_level');
		$this->load->view('home/reports/unit',$data);
	}
	
	public function get_report_unit(){
		$periode = $this->input->post('periode');
		$unit = $this->input->post('unit');
	
		$sql = "SELECT km.*, kpi.kpi kpi_name, units.name unit_name, perspectives.name perspective FROM `km`, kpi, units, perspectives WHERE kpi.perspective_id = perspectives.id and km.kpi_id = kpi.id and kpi.unit_id = units.id and km.`organization_id` = '".$unit."' and km.quarter = (select max(quarter) from km where organization_id = '" . $unit. "') and km.periode = '".$periode."' and is_published = 1 order by perspectives.order_no, km.order_no";
		
		echo json_encode($this->model->query($sql));
	}
		
	public function traverse_tree($idx, $prefix){
		$d = $this->company_structures->find_parents($idx);	
		if($d == 0){
			return;
		}
		$tree = '';
		for($i=0;$i<count($d);$i++){
			$sub_d = $this->company_structures->find_parents($d[$i]['id']);
			$n = ($prefix == '') ? $d[$i]['id'] : $prefix . '-' . $d[$i]['id'];

			if($idx == 0){
				$tree .= '<tr data-tt-id="'.$d[$i]['id'].'"><td>'.$d[$i]['name'].'</td></tr>';
			} else {
				$tree .= '<tr data-tt-id="'.$n.'" data-tt-parent-id="'.$prefix.'"><td>'.$d[$i]['name'].'</td></tr>';
			}
			
			$tree .= $this->traverse_tree($d[$i]['id'], $n);
		}

		return $tree;
	}
	public function tree_kpi($idx, $prefix){
		$d = $this->company_structures->find_parents($idx);	
		if($d == 0){
			return;
		}
		$tree = '';
		for($i=0;$i<count($d);$i++){
			$sub_d = $this->company_structures->find_parents($d[$i]['id']);
			$n = ($prefix == '') ? $d[$i]['id'] : $prefix . '-' . $d[$i]['id'];

			if($idx == 0){
				$tree .= '<tr data-tt-id="'.$d[$i]['id'].'"><td>'.$d[$i]['name'].'</td></tr>';
			} else {
				$tree .= '<tr data-tt-id="'.$n.'" data-tt-parent-id="'.$prefix.'"><td>'.$d[$i]['name'].'</td></tr>';
			}
			
			$tree .= $this->traverse_tree($d[$i]['id'], $n);
		}

		return $tree;
	}
	public function rkm(){
		$this->load->view("home/reports/rkm");
	}

	public function get_report_rkm(){

		$periode = $this->input->post('periode');				
		$sql = "select pk.kpi_id, perspectives.name perspective, kpi.kpi, max((select case when unit_id = pk.unit_id and pk.is_pemilik = 1 then 2 when unit_id = pk.unit_id then 1 else 0 end from pelaksana_kpi where unit_id = 2 and periode = '".$periode."' and kpi_id = pk.kpi_id)) as du, max((select case when unit_id = pk.unit_id and pk.is_pemilik = 1 then 2 when unit_id = pk.unit_id then 1 else 0 end from pelaksana_kpi where unit_id = 9 and periode = '".$periode."' and kpi_id = pk.kpi_id)) as dm, max((select case when unit_id = pk.unit_id and pk.is_pemilik = 1 then 2 when unit_id = pk.unit_id then 1 else 0 end from pelaksana_kpi where unit_id = 11 and periode = '".$periode."' and kpi_id = pk.kpi_id)) as dt, max((select case when unit_id = pk.unit_id and pk.is_pemilik = 1 then 2 when unit_id = pk.unit_id then 1 else 0 end from pelaksana_kpi where unit_id = 14 and periode = '".$periode."' and kpi_id = pk.kpi_id)) as ds, max((select case when unit_id = pk.unit_id and pk.is_pemilik = 1 then 2 when unit_id = pk.unit_id then 1 else 0 end from pelaksana_kpi where unit_id = 15 and periode = '".$periode."' and kpi_id = pk.kpi_id)) as di, max((select case when unit_id = pk.unit_id and pk.is_pemilik = 1 then 2 when unit_id = pk.unit_id then 1 else 0 end from pelaksana_kpi where unit_id = 16 and periode = '".$periode."' and kpi_id = pk.kpi_id)) as dk, max((select case when unit_id = pk.unit_id and pk.is_pemilik = 1 then 2 when unit_id = pk.unit_id then 1 else 0 end from pelaksana_kpi where unit_id = 17 and periode = '".$periode."' and kpi_id = pk.kpi_id)) as dp from pelaksana_kpi pk, perspectives, kpi where pk.periode = '".$periode."' and kpi.id = pk.kpi_id and kpi.perspective_id = perspectives.id and pk.unit_id < 18 group by pk.kpi_id order by perspectives.order_no";
		
		echo json_encode($this->model->query($sql));
	}

	public function kontribusi_direktorat(){
		$this->load->view("home/reports/kontribusi_direktorat");
	}
	public function get_kontribusi_direktorat(){
		$periode = $this->input->post('periode');
		$sql="SELECT distinct pk.kpi_id, pr.name, kpi.kpi, dk.bobot dk_bobot, dk.realisasi_penilai dk_realisasi, ROUND(((dk.bobot/100) * dk.realisasi_penilai),2) as dk_kontribusi, dm.bobot dm_bobot, dm.realisasi_penilai dm_realisasi, ROUND(((dm.bobot/100) * dm.realisasi_penilai),2) as dm_kontribusi, dt.bobot dt_bobot, dt.realisasi_penilai dt_realisasi, ROUND(((dt.bobot/100) * dt.realisasi_penilai),2) as dt_kontribusi, ds.bobot ds_bobot, ds.realisasi_penilai ds_realisasi, ROUND(((ds.bobot/100) * ds.realisasi_penilai),2) as ds_kontribusi, di.bobot di_bobot, di.realisasi_penilai di_realisasi, ROUND(((di.bobot/100) * di.realisasi_penilai),2) as di_kontribusi, dp.bobot dp_bobot, dp.realisasi_penilai dp_realisasi, ROUND(((dp.bobot/100) * dp.realisasi_penilai),2) as dp_kontribusi, du.bobot du_bobot, du.realisasi_penilai du_realisasi, ROUND(((du.bobot/100) * du.realisasi_penilai),2) as du_kontribusi, per.realisasi_penilai per_realisasi, per.pencapaian_penilai per_pencapaian,per.bobot per_bobot
			  FROM pelaksana_kpi pk join kpi on kpi.id = pk.kpi_id join perspectives pr on kpi.perspective_id = pr.id 
			  left join (select k.realisasi_penilai, k.kpi_id, p.bobot from km_unit k, pelaksana_kpi p 
			             where k.periode = '".$periode."' and k.unit_id = 16 and p.kpi_id = k.kpi_id and p.unit_id = 16) dk on pk.kpi_id = dk.kpi_id 
		      left join (select k.realisasi_penilai,  k.kpi_id, p.bobot from km_unit k, pelaksana_kpi p 
		      	         where k.periode = '".$periode."' and k.unit_id = 9 and p.kpi_id = k.kpi_id and p.unit_id = 9) dm on pk.kpi_id = dm.kpi_id 
              left join (select k.realisasi_penilai,  k.kpi_id, p.bobot from km_unit k, pelaksana_kpi p 
              	         where k.periode = '".$periode."' and k.unit_id = 11  and p.kpi_id = k.kpi_id and p.unit_id = 11) dt on pk.kpi_id = dt.kpi_id 
              left join (select k.realisasi_penilai,  k.kpi_id, p.bobot from km_unit k, pelaksana_kpi p 
              	         where k.periode = '".$periode."' and k.unit_id = 14 and p.kpi_id = k.kpi_id and p.unit_id = 14) ds on pk.kpi_id = ds.kpi_id 
              left join (select k.realisasi_penilai,  k.kpi_id, p.bobot from km_unit k, pelaksana_kpi p 
              	         where k.periode = '".$periode."' and k.unit_id = 15 and p.kpi_id = k.kpi_id and p.unit_id = 15) di on pk.kpi_id = di.kpi_id 
              left join (select k.realisasi_penilai,  k.kpi_id, p.bobot from km_unit k, pelaksana_kpi p 
              	         where k.periode = '".$periode."' and k.unit_id = 17 and p.kpi_id = k.kpi_id and p.unit_id = 17) dp on pk.kpi_id = dp.kpi_id 
              left join (select k.realisasi_penilai,  k.kpi_id, p.bobot from km_unit k, pelaksana_kpi p 
              	         where k.periode = '".$periode."' and k.unit_id = 2 and p.kpi_id = k.kpi_id and p.unit_id = 2) du on pk.kpi_id = du.kpi_id 
              join (select realisasi_penilai, prestasi_penilai pencapaian_penilai, kpi_id, bobot from km 
              	    where periode = '".$periode."') per on pk.kpi_id = per.kpi_id 
              WHERE pk.periode = '".$periode."' and pk.unit_id < 18 
              order by pr.order_no";

		//print_r($data_kontribusi_direktorat);
		echo json_encode($this->model->query($sql));
	}

	public function get_report_performance_km(){
		$periode = $this->input->post('periode');
		$sql = "SELECT km.*, u.name satuan,kpi.kpi kpi_name, perspectives.name perspective FROM `km`, kpi, perspectives, units u WHERE kpi.perspective_id = perspectives.id and km.kpi_id = kpi.id and km.periode = '".$periode."' and kpi.unit_id = u.id order by perspectives.order_no, km.order_no";

		
		echo json_encode($this->model->query($sql));
	}

	public function get_report_performance_km_unit(){
		$periode = $this->input->post('periode');
		$unit = $this->input->post('unit');

		$sql = "SELECT km_unit.*, u.name satuan, kpi.kpi kpi_name, perspectives.name perspective FROM km_unit, kpi, perspectives, units u WHERE kpi.perspective_id = perspectives.id and km_unit.kpi_id = kpi.id and km_unit.unit_id = '".$unit."' and km_unit.periode = '".$periode."' and kpi.unit_id = u.id order by perspectives.order_no, order_no";
		
		echo json_encode($this->model->query($sql));
	}
	public function report_performance($periode,$type){

		$sql = "SELECT km.*, u.name satuan,kpi.kpi kpi_name, perspectives.name perspective FROM `km`, kpi, perspectives, units u WHERE kpi.perspective_id = perspectives.id and km.kpi_id = kpi.id and km.periode = '".$periode."' and kpi.unit_id = u.id order by perspectives.order_no, km.order_no";
		$data['periode']=$periode;
		$data['data']=$this->model->query($sql);
		$data['level']=$this->model->select('parameter_level');
		
		//echo $this->db->last_query();
		//File type PDF
		if($type==1){
			$this->load->helper(array('dompdf', 'file'));
			$html = $this->load->view('home/reports/report_performance',$data, true);
			pdf_create($html,'report_rkm_perusahaan_'.$periode,TRUE,1);
		}else {//File type excel
			$this->load->view('home/reports/report_performance',$data);
		}
		
	}
	public function get_reportunit($unit,$periode,$type,$title=''){
		$data['title']=urlencode($title);
		$data['periode']=$periode;
		$sql = "SELECT km_unit.*, u.name satuan, kpi.kpi kpi_name, perspectives.name perspective FROM km_unit, kpi, perspectives, units u WHERE kpi.perspective_id = perspectives.id and km_unit.kpi_id = kpi.id and km_unit.unit_id = '".$unit."' and km_unit.periode = '".$periode."' and kpi.unit_id = u.id order by perspectives.order_no, order_no";
			
		$data['data']=$this->model->query($sql);
		$data['level']=$this->model->select('parameter_level');
		if($type==1){
			$this->load->helper(array('dompdf', 'file'));
			$html = $this->load->view('home/reports/report_unit',$data, true);
			pdf_create($html,'report_km_unit_'.$periode,TRUE,1);
		}else {//File type excel
				$this->load->view('home/reports/report_unit',$data);
		}
	
	}
	public function get_report_aligment_rkm($periode,$type){			
		$data['periode'] = $periode;
		$sql = "select pk.kpi_id, perspectives.name perspective, kpi.kpi, max((select case when unit_id = pk.unit_id and pk.bobot = 100 then 2 when unit_id = pk.unit_id then 1 else 0 end from pelaksana_kpi where unit_id = 2 and periode = '".$periode."' and kpi_id = pk.kpi_id)) as du, max((select case when unit_id = pk.unit_id and pk.bobot = 100 then 2 when unit_id = pk.unit_id then 1 else 0 end from pelaksana_kpi where unit_id = 9 and periode = '".$periode."' and kpi_id = pk.kpi_id)) as dm, max((select case when unit_id = pk.unit_id and pk.bobot = 100 then 2 when unit_id = pk.unit_id then 1 else 0 end from pelaksana_kpi where unit_id = 11 and periode = '".$periode."' and kpi_id = pk.kpi_id)) as dt, max((select case when unit_id = pk.unit_id and pk.bobot = 100 then 2 when unit_id = pk.unit_id then 1 else 0 end from pelaksana_kpi where unit_id = 14 and periode = '".$periode."' and kpi_id = pk.kpi_id)) as ds, max((select case when unit_id = pk.unit_id and pk.bobot = 100 then 2 when unit_id = pk.unit_id then 1 else 0 end from pelaksana_kpi where unit_id = 15 and periode = '".$periode."' and kpi_id = pk.kpi_id)) as di, max((select case when unit_id = pk.unit_id and pk.bobot = 100 then 2 when unit_id = pk.unit_id then 1 else 0 end from pelaksana_kpi where unit_id = 16 and periode = '".$periode."' and kpi_id = pk.kpi_id)) as dk, max((select case when unit_id = pk.unit_id and pk.bobot = 100 then 2 when unit_id = pk.unit_id then 1 else 0 end from pelaksana_kpi where unit_id = 17 and periode = '".$periode."' and kpi_id = pk.kpi_id)) as dp from pelaksana_kpi pk, perspectives, kpi where pk.periode = '".$periode."' and kpi.id = pk.kpi_id and kpi.perspective_id = perspectives.id and pk.unit_id < 18 group by pk.kpi_id order by perspectives.order_no";;

		$data['data']=$this->model->query($sql);
		
		//File type PDF
		if($type==1){
			$this->load->helper(array('dompdf', 'file'));
			$html = $this->load->view('home/reports/report_aligment_rkm',$data,true);
			pdf_create($html, 'report_aligment_rkm_'.$periode);
		}else {//File type excel
			$this->load->view('home/reports/report_aligment_rkm',$data);
		}
	}
	public function get_report_kontribusi($periode,$type){
		$data['periode']=$periode;
		$sql="SELECT distinct pk.kpi_id, pr.name, kpi.kpi, dk.bobot dk_bobot, dk.realisasi_penilai dk_realisasi, ROUND(((dk.bobot/100) * dk.realisasi_penilai),2) as dk_kontribusi, dm.bobot dm_bobot, dm.realisasi_penilai dm_realisasi, ROUND(((dm.bobot/100) * dm.realisasi_penilai),2) as dm_kontribusi, dt.bobot dt_bobot, dt.realisasi_penilai dt_realisasi, ROUND(((dt.bobot/100) * dt.realisasi_penilai),2) as dt_kontribusi, ds.bobot ds_bobot, ds.realisasi_penilai ds_realisasi, ROUND(((ds.bobot/100) * ds.realisasi_penilai),2) as ds_kontribusi, di.bobot di_bobot, di.realisasi_penilai di_realisasi, ROUND(((di.bobot/100) * di.realisasi_penilai),2) as di_kontribusi, dp.bobot dp_bobot, dp.realisasi_penilai dp_realisasi, ROUND(((dp.bobot/100) * dp.realisasi_penilai),2) as dp_kontribusi, du.bobot du_bobot, du.realisasi_penilai du_realisasi, ROUND(((du.bobot/100) * du.realisasi_penilai),2) as du_kontribusi, per.realisasi_penilai per_realisasi, per.pencapaian_penilai per_pencapaian FROM pelaksana_kpi pk join kpi on kpi.id = pk.kpi_id join perspectives pr on kpi.perspective_id = pr.id left join (select k.realisasi_penilai, k.kpi_id, p.bobot from km_unit k, pelaksana_kpi p where k.periode = '".$periode."' and k.unit_id = 16 and p.kpi_id = k.kpi_id and p.unit_id = 16) dk on pk.kpi_id = dk.kpi_id left join (select k.realisasi_penilai,  k.kpi_id, p.bobot from km_unit k, pelaksana_kpi p where k.periode = '".$periode."' and k.unit_id = 9 and p.kpi_id = k.kpi_id and p.unit_id = 9) dm on pk.kpi_id = dm.kpi_id left join (select k.realisasi_penilai,  k.kpi_id, p.bobot from km_unit k, pelaksana_kpi p where k.periode = '".$periode."' and k.unit_id = 11  and p.kpi_id = k.kpi_id and p.unit_id = 11) dt on pk.kpi_id = dt.kpi_id left join (select k.realisasi_penilai,  k.kpi_id, p.bobot from km_unit k, pelaksana_kpi p where k.periode = '".$periode."' and k.unit_id = 14 and p.kpi_id = k.kpi_id and p.unit_id = 14) ds on pk.kpi_id = ds.kpi_id left join (select k.realisasi_penilai,  k.kpi_id, p.bobot from km_unit k, pelaksana_kpi p where k.periode = '".$periode."' and k.unit_id = 15 and p.kpi_id = k.kpi_id and p.unit_id = 15) di on pk.kpi_id = di.kpi_id left join (select k.realisasi_penilai,  k.kpi_id, p.bobot from km_unit k, pelaksana_kpi p where k.periode = '".$periode."' and k.unit_id = 17 and p.kpi_id = k.kpi_id and p.unit_id = 17) dp on pk.kpi_id = dp.kpi_id left join (select k.realisasi_penilai,  k.kpi_id, p.bobot from km_unit k, pelaksana_kpi p where k.periode = '".$periode."' and k.unit_id = 2 and p.kpi_id = k.kpi_id and p.unit_id = 2) du on pk.kpi_id = du.kpi_id join (select realisasi_penilai, prestasi_penilai pencapaian_penilai, kpi_id from km where periode = '".$periode."') per on pk.kpi_id = per.kpi_id WHERE pk.periode = '".$periode."' and pk.unit_id < 18 order by pr.order_no";
		$data['data']=$this->model->query($sql);
		//File type PDF
		if($type==1){
			$this->load->helper(array('dompdf', 'file'));
			$html = $this->load->view('home/reports/report_kontribusi',$data, true);
			pdf_create($html,'report_kontribusi_'.$periode,TRUE,3);
		}else {//File type excel
			$this->load->view('home/reports/report_kontribusi',$data);
		}
		
	}
}
