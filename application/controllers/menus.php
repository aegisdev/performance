<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menus extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->jm->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function index(){		
		$sql="SELECT a.id,a.name,a.url,a.order_no,a.icon,(CASE a.parent_id WHEN 0 THEN a.name ELSE b.name END) AS parent_name FROM menu a LEFT JOIN menu b
		ON (b.id=a.parent_id) WHERE 1=1";
		$data['data']=$this->model->query($sql);
		$this->load->view('home/menus/index',$data);
	}
	
	public function menu_list()
	{	
		$aColumns = array('ID','NAME','URL','ORDER_NO','ICON','PARENT_NAME');
		$sql="SELECT a.id,a.name,a.url,a.order_no,a.icon,(CASE a.parent_id WHEN 0 THEN a.name ELSE b.name END) AS parent_name FROM menu a LEFT JOIN menu b
		ON (b.id=a.parent_id) WHERE 1=1 ORDER BY order_no ASC";
		//$sql="SELECT *  FROM MENU WHERE 1=1";
		$sql_total = "SELECT count(ID) AS jml FROM MENU";
		$this->model->datatable($sql,$aColumns,$sql_total);	
	}	
	public function add()
	{	
		$sql="SELECT a.id,a.name,a.url,(CASE a.parent_id WHEN 0 THEN a.name ELSE b.name END) AS parent_name FROM menu a LEFT JOIN menu b
		ON (b.id=a.parent_id) WHERE 1=1";
		$data['menus']=$this->model->query($sql);
	
		$this->load->view('home/menus/add',$data);		
	}
	public function save(){	
		$data=array(		
			'name'=>$this->input->post('name'),
			'url'=>$this->input->post('url'),
			'order_no'=>$this->input->post('order_no'),
			'icon'=>$this->input->post('icon'),
			'parent_id'=>$this->input->post('parent_id')	
		);
		if(!$this->check_exist($this->input->post('name'))){
			echo $this->model->insert('menu',$data) ? "1":"0";
		}else{
			echo "2";
		}		
	}
	public function edit($id){	
		$clause =array('id'=>$id);
		$sql="SELECT * FROM menu WHERE 1=1";
		$data=array(
			'menus'=>$this->model->select("menu"),
			'data'=>$this->model->select('menu',$clause)
		);		
		$this->load->view('home/menus/edit',$data);		
	}	
	public function update(){	
		$data=array(		
			'name'=>$this->input->post('name'),
			'url'=>$this->input->post('url'),
			'order_no'=>$this->input->post('order_no'),
			'icon'=>$this->input->post('icon'),
			'parent_id'=>$this->input->post('parent_id')
		);
		
		$clause=array('id'=>$this->input->post('id'));		
		echo $this->model->update('menu',$data,$clause) ? "1":"0";	
	}
	public function delete(){	
		$clause =array('id'=>$this->input->post('id'));
		echo $this->model->delete('menu',$clause) ? "1":"0";
	}	
	public function list_menu(){	
		//$data['menus']=$this->model->select("menu");
		//$data=$this->input->post('data');
		$user_group_id=$this->session->userdata('user_group_id');	
		$data['menus']=$this->sub_menu(0,$h="",$user_group_id);
	
		//echo $this->db->last_query();
		$this->load->view('home/menus/list_menu',$data);
	}		
	public function check_exist($name,$id=''){	
		$clause =array('name'=>$this->input->post('name'),'id'=>$this->input->post('id'));
		$jml=$this->model->select('menu',$clause);
		//echo $this->db->last_query();
		return (count($jml)==1) ? true : false;
	}	
	public function sub_menu($parent_id=0,$hasil,$user_group_id){
		$clause=array(
			'parent_id'=>$parent_id
		);
		$sql="SELECT * FROM menu";// a, user_group_role c
//WHERE a.id=c.menu_id AND c.user_group_id= $user_group_id AND parent_id=".$parent_id." ORDER BY a.order_no";
		//echo $sql;
		$w = $this->model->query($sql);
		foreach($w as $h){				
			if($h->parent_id==0)
			{
				$clause=array(
					'parent_id'=>$h->id
				);				
				$w2 = $this->model->select('menu',$clause);	
				//echo $this->db->last_query();
				if(count($w2)>0)
				{	
					$hasil .= "<label class=\"checkbox\"><input class=\"checkbox select_all\" type=\"checkbox\" data-menu-id=\"".$h->id."\" data-menu=\"".$h->name."\" value=\"".$h->id."\" name=\"menu[]\" id=\"menu_id-" .$h->id. "\">" . $h->name . "</label>";				
					$hasil.="<ul>";
					foreach($w2 as $r){	
						$hasil .= "<label class=\"checkbox\"><input class=\"checkbox select_item items".$h->id."\" type=\"checkbox\" data-menu-id=\"".$r->id."\" data-menu=\"".$r->name."\" value=\"".$r->id."\" name=\"menu[]\" id=\"menu_id-" .$r->id. "\">" . $r->name . "</label>";							
					}	
					$hasil .= "</ul>";					
				}else{
					$hasil .= "<label class=\"checkbox\"><input class=\"checkbox\" type=\"checkbox\" data-menu-id=\"".$h->id."\" data-menu=\"".$h->name."\" value=\"".$h->id."\" name=\"menu[]\" id=\"menu_id-" .$h->id. "\">" . $h->name . "</label>";							
				}
			}
		}
		//echo $hasil;
		return $hasil;
	}
}
