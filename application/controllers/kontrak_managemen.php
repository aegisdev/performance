<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kontrak_managemen extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->jm->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function index(){	
		$data['users']=$this->model->select('users');
		$this->load->view('home/kontrak_managemen/index',$data);
	}
}
