<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Measurements extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->model->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function forming(){	
		$data['data']=$this->model->select('organizations');
		$this->load->view('home/measurements/forming',$data);
	}

	public function realization(){	
		$unit_id=$this->session->userdata('organization_id');
		//Tidak punya unit_id
		if($unit_id==0){
			$data['data']=$this->model->select('organizations');
		}else{
			$data['data']=$this->model->select('organizations', array('id'=>$unit_id));
		}
		$this->load->view('home/measurements/realization',$data);
	}

	public function evaluator(){	
		$unit_id=$this->session->userdata('organization_id');
		//Tidak punya unit_id
		if($unit_id==0){
			$data['data']=$this->model->select('organizations');
		}else{
			$data['data']=$this->model->select('organizations',array('id'=>$unit_id));
		}
		$this->load->view('home/measurements/evaluator',$data);
	}	
	public function publish(){	
		$sql = "select o.name, km.quarter, km.periode, sum(km.prestasi_penilai) nilai, km.is_published from km, organizations o where quarter = (SELECT max(quarter) FROM `km` where periode = (select max(periode) from km)) and km.organization_id = o.id and km.periode = (select max(periode) from km) group by o.name";
		
		$data['data']= $this->model->query($sql);
		$this->load->view('home/measurements/publish',$data);
	}
	public function rkpp(){	
		$data['data']=$this->model->select('organizations');
		$this->load->view('home/measurements/rkpp',$data);
	}

	public function km_unit(){
		$unit_id=$this->session->userdata('organization_id');
		$group_type = $this->session->userdata('user_group_id');
		//Tidak punya unit_id
		if($group_type == 3 || $group_type == 1 || $unit_id==0){
			$data['data']=$this->model->select('organizations');
		}else{
			$data['data']=$this->model->select('organizations',array('id'=>$unit_id));
		}
		$this->load->view('home/measurements/km_unit',$data);
	}

	public function kompetitor(){	
		$sql="SELECT a.*,b.kpi FROM kompetitor a 
		JOIN kpi b ON (a.kpi_id=b.id)
		";
		$data['data']=$this->model->query($sql);
		$this->load->view('home/measurements/kompetitor',$data);
	}
	
	public function get_kpi_no_quarter(){
		$periode = $this->input->post('periode');
		
		//$sql = "SELECT id, kpi from kpi where id NOT IN (select kpi_id from km where periode = '".$periode."' group by km.kpi_id) and flag = 1"; // DATA KELUAR DOUBLE 
		
		// mengikuti query dari kpi/index
		$sql   = "SELECT 
					a.id, 
					a.kpi,
					a.formula_id,
					g.name AS perspective_name,
					c.name AS unit,			
					e.name AS formula,
					i.name AS inisiative_name,
					CONCAT(h.minimal,'-',h.maksimal) AS batas_penilaian
					FROM kpi a
					JOIN units c ON (a.unit_id=c.id)
					JOIN formulas e ON (a.formula_id=e.id)
					JOIN perspectives g ON (a.perspective_id=g.id)
					JOIN batas_penilaian h ON (a.batas_penilaian_id=h.id)
					JOIN inisiatives i ON (a.inisiative_id=i.id) 
					WHERE 
					a.id NOT IN (select kpi_id from km where periode = '".$periode."' group by km.kpi_id)
					and
					a.flag=1";
		
		echo json_encode($this->model->query($sql));
	}
	
	public function choose_kpi(){
		$kpis = $this->input->post('kpi-selected');
		$periode = $this->input->post('choose-kpi-periode');

		$sql = "SELECT group_concat(kpi SEPARATOR '#') r from kpi where id IN (select kpi_id from km where periode = '".$periode."' group by km.kpi_id)"; //+
		$exists = $this->db->query($sql)->row()->r;

		$not_in_kompetitor = array();
		for($i=0;$i<count($kpis);$i++){
			$cek_in_kompetitor = $this->db->query('select * from kompetitor where kpi_id = '.$kpis[$i].' and tahun = '.$periode.' ')->num_rows();
			if($cek_in_kompetitor > 0){
				$data = array(
					'kpi_id' => $kpis[$i],
					'periode' => $periode
				);

				$this->model->insert('km',$data);
				unset($kpis[$i]);
			}
		}

		//$not_in_kompetitor = (count($kpis) > 0)? implode('#',$kpis) : null;
		if(count($kpis) > 0){
			foreach($kpis as $key=>$val):
				$x[] = $this->db->where('id',$val)->get('kpi')->row()->kpi;
			endforeach;

			$not_in_kompetitor = implode('#',$x);
		} else {
			$not_in_kompetitor = null;
		}

		echo json_encode(array('status' => 'ok','exists' => $exists,'not_in_kompetitor' => $not_in_kompetitor));
	}
	
	public function get_selected_kpi(){
		$unit = $this->input->post('unit');
		$periode = $this->input->post('periode');	
		$quarter = $this->input->post('quarter'); //+
		
		$sql = "select km.id, km.is_published, km.pencapaian_org, km.keterangan_penilai, km.prestasi_org, km.realisasi_org, km.pencapaian_penilai, km.prestasi_penilai, km.realisasi_penilai, km.kpi_id, kpi.kpi, units.name satuan, perspectives.name perspective, km.bobot, km.target, km.order_no, kpi.formula_id, bp.maksimal maks, bp.minimal min from km, kpi, units, perspectives, batas_penilaian bp where km.kpi_id = kpi.id and kpi.unit_id = units.id and kpi.perspective_id = perspectives.id and km.organization_id = '".$unit."' and periode = '".$periode."' and quarter='".$quarter."' and kpi.batas_penilaian_id = bp.id order by order_no asc, kpi_id asc, perspective asc"; //+

		echo json_encode($this->model->query($sql));
	}
	
	public function get_selected_kpi_no_quarter(){  //+
		$periode = $this->input->post('periode');	
		$before = $periode - 1;
				
		$sql = "select km.id, 
		km.pencapaian_org, 
		km.keterangan_penilai, 
		km.prestasi_org, 
		km.realisasi_org, 
		km.pencapaian_penilai, 
		km.prestasi_penilai, 
		km.realisasi_penilai, 
		km.kpi_id, 
		kpi.kpi, 
		units.name satuan, 
		perspectives.name perspective, 
		km.bobot, 
		km.target, 
		km.order_no, 
		kpi.formula_id, 
		bp.maksimal maks, 
		bp.minimal min, 
		ko.best_performance, 
		ko.benchmark, 
		ko.kompetitor, 
		(select case when x.pencapaian_penilai is null then 0 else x.pencapaian_penilai end from km x where x.periode = '".$before."' and x.kpi_id = km.kpi_id) pencapaian_sebelumnya 
		from km, kpi, units, perspectives, batas_penilaian bp, kompetitor ko 
		where 
		km.kpi_id = kpi.id 
		and kpi.unit_id = units.id 
		and kpi.perspective_id = perspectives.id 
		and periode = '".$periode."' 
		and kpi.batas_penilaian_id = bp.id 
		 and km.kpi_id = ko.kpi_id 
		and km.periode = ko.tahun 
		and km.flag=1 
		group by km.kpi_id order by perspectives.order_no, km.order_no"; //+

		echo json_encode($this->model->query($sql));
	}

	public function ex_get_selected_kpi_no_quarter(){  //+
		$unit = $this->input->post('unit');
		$periode = $this->input->post('periode');	
		$before = $periode - 1;
				
		$sql = "select km.id, km.pencapaian_org, km.keterangan_penilai, km.prestasi_org, km.realisasi_org, km.pencapaian_penilai, km.prestasi_penilai, km.realisasi_penilai, km.kpi_id, kpi.kpi, units.name satuan, perspectives.name perspective, km.bobot, km.target, km.order_no, kpi.formula_id, bp.maksimal maks, bp.minimal min, ko.best_performance, ko.benchmark, ko.kompetitor, (select case when x.pencapaian_penilai is null then 0 else x.pencapaian_penilai end from km x where x.periode = '".$before."' and x.quarter = 4 and x.organization_id = km.organization_id and x.kpi_id = km.kpi_id) pencapaian_sebelumnya from km, kpi, units, perspectives, batas_penilaian bp, kompetitor ko where km.kpi_id = kpi.id and kpi.unit_id = units.id and kpi.perspective_id = perspectives.id and km.organization_id = '".$unit."' and periode = '".$periode."' and kpi.batas_penilaian_id = bp.id and km.kpi_id = ko.kpi_id and km.periode = ko.tahun AND where km.flag=1 AND kpi.id IN (SELECT kpi_id FROM pemilik_kpi WHERE periode = '".$periode."' and unit_id = '".$unit."') group by km.kpi_id order by order_no asc, kpi_id asc, perspective asc"; //+

		echo json_encode($this->model->query($sql));
	}

	public function get_selected_kpi_km_unit(){ 
		$unit = $this->input->post('unit');
		$periode = $this->input->post('periode');
		$namaUnit = $this->input->post('unit-name');	
		$before = $periode - 1;
				
		$sql = "SELECT km.id, km.pencapaian_org, km.keterangan_penilai, km.prestasi_org, km.realisasi_org, km.pencapaian_penilai, km.prestasi_penilai, km.realisasi_penilai, km.kpi_id, kpi.kpi, units.name satuan, perspectives.name perspective, km.bobot, km.target, km.order_no, kpi.formula_id, bp.maksimal maks, bp.minimal min, ko.best_performance, ko.benchmark, ko.kompetitor, (select case when x.pencapaian_penilai is null then 0 else x.pencapaian_penilai end FROM km x where x.periode = '".$before."' and x.kpi_id = km.kpi_id) pencapaian_sebelumnya FROM km_unit as km, kpi, units, perspectives, batas_penilaian bp, kompetitor ko WHERE km.kpi_id = kpi.id and kpi.unit_id = units.id and kpi.perspective_id = perspectives.id and km.unit_id = '".$unit."' and km.periode = '".$periode."' and kpi.batas_penilaian_id = bp.id and km.kpi_id = ko.kpi_id and km.periode = ko.tahun AND km.flag=1 AND kpi.id IN (SELECT kpi_id FROM pemilik_kpi WHERE periode = '".$periode."' and unit_id = '".$unit."') group by km.kpi_id order by perspectives.order_no, order_no asc, kpi_id"; //+

		$resultOne = $this->model->query($sql);

		// if(count($resultOne) > 0){
		if(1 == 0){
			echo json_encode($resultOne);	
		} else {
			$s = substr($namaUnit, 0, 1);

			if($s == 0){
				$setPelaksanaKpi = "select * from pelaksana_kpi where periode = '".$periode."' and unit_id = '".$unit."'";
				$hslSetPelaksanaKpi = $this->model->query($setPelaksanaKpi);

				for($i=0;$i<count($hslSetPelaksanaKpi);$i++){

					$targetPerKpi = "select target, order_no, bobot from km where kpi_id = '".$hslSetPelaksanaKpi[$i]->kpi_id."' and periode='".$hslSetPelaksanaKpi[$i]->periode."' limit 1";
					$hslTargetPerKpi = $this->model->query($targetPerKpi);

					$clause = array(
						'kpi_id' => $hslSetPelaksanaKpi[$i]->kpi_id,
						'periode' => $hslSetPelaksanaKpi[$i]->periode,
						'unit_id' => $hslSetPelaksanaKpi[$i]->unit_id,
					);

					$data = array(
						'bobot' => $hslTargetPerKpi[0]->bobot,
						//'target' => ($hslSetPelaksanaKpi[$i]->bobot/100) * $hslTargetPerKpi[0]->target
						'target' => $hslTargetPerKpi[0]->target,
						'order_no' => $hslTargetPerKpi[0]->order_no
					);

					$this->model->update('km_unit', $data, $clause);
				}
			} else {
				$setPendukungKpi = "select * from pendukung_kpi where periode = '".$periode."' and unit_id = '".$unit."'";
				$hslSetPendukungKpi = $this->model->query($setPendukungKpi);

				for($i=0;$i<count($hslSetPendukungKpi);$i++){

					$targetPerKpi = "select target, order_no, bobot from km_unit where kpi_id = '".$hslSetPendukungKpi[$i]->kpi_id."' and periode='".$hslSetPendukungKpi[$i]->periode."' and unit_id='".$hslSetPendukungKpi[$i]->unit_parent_id."' limit 1";
					$hslTargetPerKpi = $this->model->query($targetPerKpi);

					$clause = array(
						'kpi_id' => $hslSetPendukungKpi[$i]->kpi_id,
						'periode' => $hslSetPendukungKpi[$i]->periode,
						'unit_id' => $hslSetPendukungKpi[$i]->unit_id
					);

					$data = array(
						'bobot' => $hslTargetPerKpi[0]->bobot,
						//'target' => ($hslSetPelaksanaKpi[$i]->bobot/100) * $hslTargetPerKpi[0]->target
						'target' => $hslTargetPerKpi[0]->target,
						'order_no' => $hslTargetPerKpi[0]->order_no
					);

					$this->model->update('km_unit', $data, $clause);
				}
			}

			$abc = $this->model->query($sql);

			echo json_encode($abc);
		}
	}

	public function calculate_child_realization(){
		$unit = $this->input->post('unit');
		$periode = $this->input->post('periode');
		$type = $this->input->post('type');
		$src = 'realisasi_org';

		if($type == 1) {
			$src = 'realisasi_penilai';
		}

		$lowerDiv = "select kpi_id, sum(".$src.") total_realisasi from km_unit where unit_id IN (select unit_id from pendukung_kpi where unit_parent_id = '".$unit."' and periode='".$periode."') and periode = '".$periode."' group by kpi_id";
		$hslLowerDiv = $this->model->query($lowerDiv);

		echo json_encode($hslLowerDiv);
	}
	
	public function save_properties_kpi(){
		$kpi =  $this->input->post('kpi-id');
		$count = count($kpi);
		$bobot = str_replace(',','.',$this->input->post('kpi-bobot'));
		$target = str_replace(',','.',$this->input->post('kpi-target'));
		$order = $this->input->post('kpi-order-no');


		//for($j=1;$j<5;$j++){	
			for($i=0;$i<$count;$i++){
				$data = array(
					'bobot' => $bobot[$i],
					'target' => $target[$i],
					'order_no' => $order[$i]
				);
				
				$clause = array(
					'kpi_id' =>$kpi[$i],
					'periode' => $this->input->post('kpi-periode'),
					//'organization_id' => $this->input->post('kpi-unit-id'),
					//'quarter' => $j
				);
				
				$this->model->update('km',$data, $clause);

				$this->model->update('km_unit',$data, $clause);
			}
		//}
		
		echo json_encode(array('status' => 'ok'));
	}

	public function save_properties_kpi_unit(){
		$kpi =  $this->input->post('kpi-id');
		$count = count($kpi);
		$bobot = $this->input->post('kpi-bobot');
		$target = $this->input->post('kpi-target');
		$order = $this->input->post('kpi-order-no');


		//for($j=1;$j<5;$j++){	
			for($i=0;$i<$count;$i++){
				$data = array(
					'bobot' => $bobot[$i],
					'target' => $target[$i],
					'order_no' => $order[$i]
				);
				
				$clause = array(
					'kpi_id' =>$kpi[$i],
					'periode' => $this->input->post('kpi-periode'),
					'unit_id' => $this->input->post('kpi-unit-id'),
					//'quarter' => $j
				);
				
				$this->model->update('km_unit',$data, $clause);
			}
		//}
		
		echo json_encode(array('status' => 'ok'));
	}
	
	public function delete_kpi_selected(){

		$this->model->delete('pemilik_kpi', array('kpi_id' => $this->input->post('kpi-id'), 'periode' => $this->input->post('kpi-periode')));
		$this->model->delete('pendukung_kpi', array('kpi_id' => $this->input->post('kpi-id'), 'periode' => $this->input->post('kpi-periode')));
		$this->model->delete('pelaksana_kpi', array('kpi_id' => $this->input->post('kpi-id'), 'periode' => $this->input->post('kpi-periode')));
		$this->model->delete('km_unit', array('kpi_id' => $this->input->post('kpi-id'), 'periode' => $this->input->post('kpi-periode')));

		$re = $this->model->delete('km', array('kpi_id' => $this->input->post('kpi-id'), 'periode' => $this->input->post('kpi-periode')));


		if($re > 0){
			echo json_encode(array('status' => 'ok'));
		} else {
			echo json_encode(array('status' => 'error'));
		}
	}

	public function delete_kpi_km_unit(){
		$re = $this->model->delete('km_unit', array('kpi_id' => $this->input->post('kpi-id'), 'periode' => $this->input->post('kpi-periode'), 'unit_id' => $this->input->post('kpi-unit-id')));

		if($re > 0){
			echo json_encode(array('status' => 'ok'));
		} else {
			echo json_encode(array('status' => 'error'));
		}
	}

	public function get_rkpm(){
		$unit = $this->input->post('unit');		
		$sql = "SELECT id, kpi from kpi where id NOT IN (select kpi_id from km where organization_id = '".$unit."')";
		echo json_encode($this->model->query($sql));
	}
	public function choose_rkpp(){
		$kpis = $this->input->post('kpi-selected');
		for($i=0;$i<count($kpis);$i++){
			$data = array(
				'kpi_id' => $kpis[$i],
				'rencana_pelaksanaan' => $kpis[$i],
				'keterangan' => $kpis[$i],
				'organization_id' => $this->input->post('choose-kpi-unit')
			);
			
			$this->model->insert('rkpp',$data);
		}
		
		echo json_encode(array('status' => 'ok'));
	}
	
	public function get_selected_rkpp(){
		$unit = $this->input->post('unit');
		
		$sql = "select rkpp.*, rkpp.kpi_id, kpi.kpi, units.name satuan, perspectives.name perspective, rkpp.order_no from rkpp, kpi, units, perspectives where rkpp.kpi_id = kpi.id and kpi.unit_id = units.id and kpi.perspective_id = perspectives.id and rkpp.organization_id = '".$unit."' order by order_no asc, kpi_id asc, perspective asc";

		echo json_encode($this->model->query($sql));
	}
	public function delete_rkpp_selected(){
		
		$re = $this->model->delete('rkpp', array('id' => $this->input->post('id')));

		if($re > 0){
			echo json_encode(array('status' => 'ok'));
		} else {
			echo json_encode(array('status' => 'error'));
		}
	}
	public function save_properties_rkpp(){
		$renc = $this->input->post('rkpp-rencana-pelaksanaan');
		$kpi = $this->input->post('rkpp-id');
		$count = count($kpi);
		$ket = $this->input->post('rkpp-keterangan');
		$orde = $this->input->post('rkpp-order-no');
		
		for($i=0;$i<$count;$i++){
			$data = array(
				'rencana_pelaksanaan' => $renc[$i],
				'keterangan' => $ket[$i],
				'order_no' => $orde[$i]
			);
			
			$clause = array('id' => $kpi[$i]);
			
			$this->model->update('rkpp',$data, $clause);			
		}
		
		echo json_encode(array('status' => 'ok'));
	}
	
	public function save_realization_unit(){ //+
		$kpi = $this->input->post('kpi-id');
		$count = count($kpi);
		$real =  $this->input->post('kpi-realization');
		$pres = $this->input->post('kpi-prestasi');
		$penc =  $this->input->post('kpi-pencapaian');

		for($i=0;$i<$count;$i++){
			$data = array(
				'realisasi_org' =>$real[$i],
				'prestasi_org' => $pres[$i],
				'pencapaian_org' => $penc[$i]
			);
			
			$clause = array('id' => $kpi[$i]);
			
			$this->model->update('km',$data, $clause);
		}
		
		echo json_encode(array('status' => 'ok'));
	}
	
	public function save_realization_unit_km(){
		$this->load->library('mailman');
		$CI =& get_instance();

		$kpi = $this->input->post('kpi-id');
		$id = $this->input->post('data-id');
		$count = count($kpi);
		$real =  $this->input->post('kpi-realization');
		$pres = $this->input->post('kpi-prestasi');
		$penc =  $this->input->post('kpi-pencapaian');
		$periode = $this->input->post('periode-choosen');
		$unit = $this->input->post('unit-choosen');

		for($i=0;$i<$count;$i++){
			$data = array(
				'realisasi_org' =>$real[$i],
				'prestasi_org' => $pres[$i],
				'pencapaian_org' => $penc[$i]
			);
			
			$clause = array('id' => $id[$i]);
			
			$this->model->update('km_unit', $data, $clause);
			$this->model->update('km',$data, array('kpi_id' => $kpi[$i], 'periode' => $periode));

			$logData = array(
				'periode' => $periode,
				'unit_id' => $unit,
				'kpi_id' => $kpi[$i],
				'tgl_input' => date('YmdHis'),
				'realisasi' => $real[$i],
				'user_id' => $this->session->userdata('user_id')
 			);

 			$this->model->insert('log_real_unit', $logData);
		}

		$ser = $this->db->query("SELECT name FROM organizations WHERE id = '".$unit."'");
		$unitName = 'Unkown Unit';
		if ($ser->num_rows() > 0){
		  $row = $ser->row();
		  $unitName = $row->name;
		} 
		
		$message = 'Pengguna '.$this->session->userdata('name').' telah melakukan input/update data KPI pada Unit '.substr($unitName, 4).
			'. <br/><br/>Mohon segera lakukan pemeriksaan pada menu Verifikasi KPI oleh Penilai.';
		
		$to = $this->mailman->get_destination_email(true, $CI);
		
		$mailStatus = $this->mailman->send_email($to, 'Notifikasi Pengisian KPI', $message, $CI);

		echo json_encode(array('status' => 'ok', 'mail_status' => $mailStatus));
	}

	public function save_realization_penilai(){ //+
		$kpi = $this->input->post('kpi-id');
		$count = count($kpi);
		$real =  $this->input->post('kpi-realization');
		$pres = $this->input->post('kpi-prestasi');
		$penc =  $this->input->post('kpi-pencapaian');
		$ket =  $this->input->post('kpi-keterangan');

		for($i=0;$i<$count;$i++){
			$data = array(
				'realisasi_penilai' =>$real[$i],
				'prestasi_penilai' => $pres[$i],
				'pencapaian_penilai' => $penc[$i],
				'keterangan_penilai' => $ket[$i]
			);
			
			$clause = array('id' => $kpi[$i]);
			
			$this->model->update('km',$data, $clause);
		}
		
		echo json_encode(array('status' => 'ok'));
	}

	public function save_realization_penilai_km(){ //+
		$this->load->library('mailman');
		$CI =& get_instance();

		$kpi = $this->input->post('kpi-id');
		$count = count($kpi);
		$id = $this->input->post('data-id');
		$kpiName = $this->input->post('kpi-name');
		$realUnit = $this->input->post('kpi-real-unit');
		$real =  $this->input->post('kpi-realization');
		$pres = $this->input->post('kpi-prestasi');
		$penc =  $this->input->post('kpi-pencapaian');
		$ket =  $this->input->post('kpi-keterangan');
		$periode = $this->input->post('periode-choosen');
		$unit = $this->input->post('unit-choosen');
		$rowMsg = '';

		for($i=0;$i<$count;$i++){
			$data = array(
				'realisasi_penilai' =>$real[$i],
				'prestasi_penilai' => $pres[$i],
				'pencapaian_penilai' => $penc[$i],
				'keterangan_penilai' => $ket[$i]
			);
			
			$clause = array('id' => $id[$i]);
			
			$this->model->update('km_unit',$data, $clause);
			$this->model->update('km',$data, array('kpi_id' => $kpi[$i], 'periode' => $periode));

			$logData = array(
				'periode' => $periode,
				'unit_id' => $unit,
				'kpi_id' => $kpi[$i],
				'tgl_input' => date('YmdHis'),
				'realisasi' => $real[$i],
				'user_id' => $this->session->userdata('user_id')
 			);

 			$this->model->insert('log_real_penilai', $logData);

 			$rowMsg .= '<tr><td>'.($i+1).'</td><td>'.$kpiName[$i].'</td><td>'.$realUnit[$i].'</td><td>'.$real[$i].'</td></tr>';
		}

		$ser = $this->db->query("SELECT name FROM organizations WHERE id = '".$unit."'");
		$unitName = 'Unkown Unit';
		if ($ser->num_rows() > 0){
		  $row = $ser->row();
		  $unitName = $row->name;
		} 
		
		$message = 'Pengguna '.$this->session->userdata('name').' telah melakukan verifikasi data KPI pada Unit '.substr($unitName, 4).
			'. <br/>';

		$message .= '<p>Hasil akhir verifikasi :</p>';
		$message .= '<table border="1" cellspacing="0px" cellpading="0px">';
		$message .= '<tr><th>No</th><th>Nama KPI</th><th>Realisasi Unit</th><th>Hasil Akhir Verifikasi</th><tr>';
		$message .= $rowMsg;
		$message .= '</table>';

		
		$to = $this->mailman->get_destination_email(true, $CI);
		
		$mailStatus = $this->mailman->send_email($to, 'Notifikasi Verifikasi KPI', $message, $CI);

		echo json_encode(array('status' => 'ok', 'mail_status' => $mailStatus));
	}
	
	public function publish_prestasi(){
		$submission = $this->model->select('submission_schedule');
		
		if($submission[0]->status == 1){
			$data = array('is_published' => 1);
			$clause = array('periode' => $this->input->post('periode'));
			$this->model->update('km', $data, $clause);
			
			echo json_encode(array('status' => 'ok'));
		} else {
			echo json_encode(array('status' => 'closed'));
		}
	}
	
	public function rkap(){	
		$data['data']=$this->model->select('organizations');
		$this->load->view('home/measurements/rkap',$data);
	}
	public function get_rkap(){
		$unit = $this->input->post('unit');		
		$sql = "SELECT a.id AS program_kerja_id,a.program_kerja,b.id,b.kpi from program_kerja a,kpi b	
		where a.kpi_id=b.id AND a.id NOT IN (select program_kerja_id from rkap where organization_id = '".$unit."')";
		echo json_encode($this->model->query($sql));
	}
	public function choose_rkap(){
		$kpis = $this->input->post('rkap-selected');
		for($i=0;$i<count($kpis);$i++){
			$data = array(
				'kpi_id' => $kpis[$i],
				'program_kerja_id' => $this->input->post('choose-rkap-program-kerja-id'),
				'organization_id' => $this->input->post('choose-rkap-unit')
			);
			$this->model->insert('rkap',$data);
		}
		echo json_encode(array('status' => 'ok'));
	}
	
	public function get_selected_rkap(){
		$unit = $this->input->post('unit');		
		$sql = "select rkap.*, rkap.kpi_id, kpi.kpi,perspectives.name perspective, rkap.order_no from rkap, kpi, units, perspectives where rkap.kpi_id = kpi.id and kpi.unit_id = units.id and kpi.perspective_id = perspectives.id and rkap.organization_id = '".$unit."' order by order_no asc, kpi_id asc, perspective asc";					
		echo json_encode($this->model->query($sql));
	}
	public function delete_rkap_selected(){
		
		$re = $this->model->delete('rkap', array('id' => $this->input->post('id')));
	
		if($re > 0){
			echo json_encode(array('status' => 'ok'));
		} else {
			echo json_encode(array('status' => 'error'));
		}
	}
	public function save_properties_rkap(){
		$rkap = $this->input->post('rkap-id');
		$count = count($rkap);
		$pel = $this->input->post('rkap-rencana-pelaksanaan');
		$ket = $this->input->post('rkap-keterangan');
		$orde =  $this->input->post('rkap-oder-no');
		
		for($i=0;$i<$count;$i++){
			$data = array(
				'rencana_pelaksanaan' => $pel[$i],
				'keterangan' => $ket[$i],
				'order_no' => $orde[$i]
			);
			
			$clause = array('id' => $rkap[$i]);
			
			$this->model->update('rkap',$data, $clause);
		}
		
		echo json_encode(array('status' => 'ok'));
	}
	
	public function get_published_km(){
		$periode = $this->input->post('periode');
	
		$sql = "select o.name, km.quarter, km.periode, sum(km.prestasi_penilai) nilai, km.is_published from km, organizations o where quarter = 4 and km.organization_id = o.id and km.periode = '".$periode."' group by o.name";
		
		echo json_encode($this->model->query($sql));
	}

	public function get_tingkat_penilaian(){
		json_encode($this->model->select('parameter_level'));
	}

	public function cek_submission_schedule(){
		$stat 		 = $this->db->query("select status from submission_schedule")->row()->status;
		$status      = ($stat == 0)? 'no' : 'ok';
		
		echo json_encode(array('status' => ''.$status.''));
	}
}
