<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	var $data_array=array();
	var $user_data=array();
	var $user_name='';
	var $npp='';
	var $password='';
	var $clause=array();
	var $sql='';
	
	public function __construct() {
		parent::__construct();		
		
	}
	public function index()
	{	
		/*if($this->session->userdata('session_id')!='')
		{ 
			redirect(base_url().'home/');
		}*/
		$this->load->view('login');

	}	

	public function process() {
		$this->load->library('user_agent');				
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));	
		
		$clause = array('username'=>$username,'password'=>$password);
	
		if($username=="developer" && $password==md5("aegis102")){
			$this->user_data=array(		
				'user_id'=>99999,
				'username'=>$username,
				'user_group_id'=>1,	
				'name'=>'Developer',					
				'session_id'=>date('YmdHis'),					
				'browser'=>$this->agent->browser(),
				'log_addr'=>$this->input->ip_address(),
				'login_time'=>date('YmdHis'),
				'last_act_time'=>date('YmdHis'),
				'logged_in'=>true,				
				'organization_id'=>0
			);	
			$this->session->set_userdata($this->user_data);			
			echo "1";		
		} else {
			$selected_user = $this->model->select('users',$clause);
			
			//echo $this->db->last_query();
			if (count($selected_user) == 0) { //User not exist
				$data = array(
					'status_code' => 1, //Status checked ini view login
					'message' => 'Incorrect Email/Password' //Message display ini view login
				); 
				//redirect(base_url().'login');
				echo "no";
			} else { //User exist						
					//TODO is admin 				
					$this->data_array=array(
						'user_id'=>$selected_user[0]->id,					
						'username'=>$username,					
						'session_id'=>date('YmdHis'),
						'browser'=>$this->agent->browser(),
						'log_addr'=>$this->input->ip_address(),					
						'login_time'=>date('YmdHis'),
						'last_act_time'=>date('YmdHis')
					);	

				if($this->model->insert('user_logs', $this->data_array)){
						
					$this->user_data=array(
						'user_id'=>$selected_user[0]->id,
						'username'=>$username,	
						'user_group_id'=>$selected_user[0]->user_group_id,					
						'name'=>$selected_user[0]->name,					
						'session_id'=>date('YmdHis'),					
						'browser'=>$this->agent->browser(),
						'log_addr'=>$this->input->ip_address(),
						'login_time'=>date('YmdHis'),
						'last_act_time'=>date('YmdHis'),
						'logged_in'=>true,
						'organization_id'=>$selected_user[0]->organization_id
					);	
					$this->session->set_userdata($this->user_data);		
					echo "1";	
				}
				else{
					echo "Gagal menyimpan data di LOGIN_SESSION_LOG";								
				}
			}		
		}// End if
	}
	
	public function logout(){
		//catat waktu logout		
		$session_id=$this->session->userdata('session_id');
		$data=array('logout_time'=>date('YmdHis'),'session_id'=>$session_id);
		$clause=array('session_id'=>$session_id);
		if($this->model->update('user_logs',$data,$clause)){				
			$this->session->sess_destroy();	
			redirect(base_url().'login');
		}
	}	
} 