<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Luas extends CI_Controller {
	protected $panjang;
	protected $lebar;
	protected $id;
	
	public function __construct() {
		parent::__construct();	
		$this->panjang=0;
		$this->lebar=0;
	}
	public function getPanjang()
	{
		return $this->panjang;
	}
	public function getLebar()
	{
		return $this->lebar;
	}
	public function getID()
	{
		return $this->id;
	}
	public function setPanjang($p)
	{	
		if($p<0)
		{
			echo "Panjang tidak boleh kurang dari 0";
			break;
		}
		$this->panjang=$p;
	}
	public function setLebar($l)
	{
		if($l<0)
		{
			echo "Lebar tidak boleh kurang dari 0";
			break;
		}
		$this->lebar=$l;
	}
	public function setID($id)
	{
		$this->id=$id;
	}
	public function getLuas()
	{ 	
		$luas=$this->panjang*$this->lebar;
		return $luas;
	}
	public function index(){	
		$this->setID(6);
		$this->setPanjang(18);
		$this->setLebar(90);
		$this->update();
		
		//$this->delete();
		$this->view();
	}
	public function save()
	{		
		echo "DATA DISIMPAN <br/>";
		$data=array(
			'panjang'=>$this->getPanjang(),
			'lebar'=>$this->getLebar()		
		);		
		$this->model->insert('luas',$data);
	}
	public function update()
	{		
		echo "DATA DIUPDATE <br/>";
		$clause=array('id'=>$this->getID());
		$data=array(
			'panjang'=>$this->getPanjang(),
			'lebar'=>$this->getLebar()		
		);		
		$this->model->update('luas',$data,$clause);
	}
	public function view()
	{		
		echo "DATA DITAMPILKAN <br/>";
		$data=$this->model->select('luas');
		foreach($data as $d)
		{
			echo $d->id. ": ".$d->panjang." : ".$d->lebar."<br/>";
		}
	}
	public function delete()
	{
		$clause=array('id'=>$this->getID());
		$data=$this->model->delete('luas',$clause);
	}
}
