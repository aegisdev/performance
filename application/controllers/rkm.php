<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Rkm extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function index(){	
		$sql="SELECT a.*, kpi, c.name AS units FROM forming a JOIN kpi b ON (a.kpi_id=b.id) JOIN units c ON (b.unit_id=c.id) LEFT JOIN pemilik_kpi d ON (b.id=d.kpi_id)  LEFT JOIN pelaksana_kpi e ON (b.id=e.kpi_id)";
		
		$data['data']=$this->model->select('organizations');
		$this->load->view('home/measurements/rkm',$data);
	}
	
	public function get_rkm_by_periode(){  
		$periode = $this->input->post('periode');
		
		//RKM		
		$sql="SELECT a.*,b .kpi, c.name AS satuan, d.name perspective		
			FROM km a
			JOIN kpi b ON (a.kpi_id=b.id)
			JOIN units c ON(b.unit_id=c.id)
			JOIN perspectives d ON(b.perspective_id=d.id)
			WHERE a.periode='".$periode."' and a.flag=1 ORDER BY d.order_no, a.order_no ASC";
		
		$rkm=$this->model->query($sql);
		//RKM
		$data=array();
		foreach($rkm as $r){
			$array_rkm=array(
					'id'=>$r->id,
					'perspective'=>$r->perspective,
					'kpi'=>$r->kpi,
					'satuan'=>$r->satuan,
					'periode'=>$r->periode,
					'kpi_id'=>$r->kpi_id,
					'bobot'=>$r->bobot,
					'target'=>$r->target
				);
			
			//Pemilik KPI
			$sql2="SELECT a.*, b.name pemilik_kpi			
				FROM  pemilik_kpi a
				JOIN organizations b ON (a.unit_id=b.id)
				WHERE a.periode=".$periode." AND a.kpi_id=".$r->kpi_id;
			
			$pemilik_kpi=$this->model->query($sql2);
			$str_pemilik='';
			$periode_input = 1;
			foreach($pemilik_kpi as $pemilik){
				$str_pemilik .= $pemilik->pemilik_kpi."<br/>";
				$periode_input = $pemilik->periode_input;
			}
			//if($str_pemilik !="")
			array_push($array_rkm,$str_pemilik);

			$array_rkm['periode_input'] = $periode_input;
			//Pelaksana KPI
			$sql3="SELECT a.*, b.name pelaksana_kpi			
				FROM  pelaksana_kpi a
				JOIN organizations b ON (a.unit_id=b.id)
				WHERE a.periode=".$periode." AND a.kpi_id=".$r->kpi_id;
				
			$pelaksana_kpi=$this->model->query($sql3);
			$str_pelaksana='';
			$filterOwnerIterator = 0;
			foreach($pelaksana_kpi as $pelaksana){		
				if($filterOwnerIterator > 0)	$str_pelaksana .= $pelaksana->pelaksana_kpi."<br/>";	
				$filterOwnerIterator++;
			}
			//if($str_pelaksana !="")
			array_push($array_rkm,$str_pelaksana);
			
			array_push($data,$array_rkm);
		}
		echo json_encode($data);
	}
	
	public function save_properties_rkpp(){
		$count = count($this->input->post('rkpp-id'));
		$aaa = $this->input->post('rkpp-rencana-pelaksanaan');
		$bbb = $this->input->post('rkpp-keterangan');
		$ccc = $this->input->post('rkpp-order-no');
		$ddd = $this->input->post('rkpp-id');
		
		for($i=0;$i<$count;$i++){
			$data = array(
				'rencana_pelaksanaan' => $aaa[$i],
				'keterangan' => $bbb[$i],
				'order_no' => $ccc[$i]
			);
			
			$clause = array('id' => $ddd[$i]);
			
			$this->model->update('rkpp',$data, $clause);
		}
		
		echo json_encode(array('status' => 'ok'));
	}
	
	public function get_organization(){
		$sql = "select * from organizations where parent_id = 2 or id = 2";
		echo json_encode($this->model->query($sql));
	}

	public function get_all_unit(){
		$sql = "select * from organizations where parent_id <> 2 and id <> 2";
		echo json_encode($this->model->query($sql));
	}

	public function get_tree(){
		$data = $this->tree_rkm(0,0);

		echo json_encode($data);
	}
	
	public function tree_rkm($idx, $prefix){
		$this->load->model('company_structures');
		$d = $this->company_structures->find_parents($idx);	
		if($d == 0){
			return;
		}
		/*$tree = '';
		for($i=0;$i<count($d);$i++){
			$sub_d = $this->company_structures->find_parents($d[$i]['id']);
			$n = ($prefix == '') ? $d[$i]['id'] : $prefix . '-' . $d[$i]['id'];
			$tree .= '<tr><td>'.$d[$i]['name'].'<input type="hidden" name="organization_id[]" class="span6" value="'.$d[$i]['id'].'"></td><td class="center-column"><input type="text" name="bobot-unit[]" class="span6"></td></tr>';
			$tree .= $this->tree_rkm($d[$i]['id'], $n);
		}
		$tree.="<tr>"; */
		$tree = '';
		for($i=0;$i<count($d);$i++){
			$sub_d = $this->company_structures->find_parents($d[$i]['id']);
			$n = ($prefix == '') ? $d[$i]['id'] : $prefix . '-' . $d[$i]['id'];

			if($idx == 0){
				$tree .= '<tr data-tt-id="'.$d[$i]['id'].'"><td>'.$d[$i]['name'].'</td><td class="center-column"><input type="text" name="bobot-unit[]" class="span6"></td></tr>';
			} else {
				$tree .= '<tr data-tt-id="'.$n.'" data-tt-parent-id="'.$prefix.'"><td>'.$d[$i]['name'].'</td><td class="center-column"><input type="text" name="bobot-unit[]" class="span6"></td></tr>';
			}
			
			$tree .= $this->tree_rkm($d[$i]['id'], $n);
		}
		return $tree;
	}

	public function save_rkm_detail(){
		$periode = $this->input->post('periode');
		$kpiId = $this->input->post('kpi-id');
		$periodeInput = $this->input->post('periode-input');

		//cleanup
		$clause = array('periode' => $periode, 'kpi_id' => $kpiId);

		$this->db->delete('pemilik_kpi', $clause);
		$this->db->delete('pendukung_kpi', $clause);
		$this->db->delete('pelaksana_kpi', $clause);

		$xxx = $this->input->post('kpi-option-direktorat');
		$yyy = $this->input->post('kpi-status-direktorat');
		$zzz = $this->input->post('kpi-bobot-direktorat');

		for($i=0;$i<count($xxx);$i++){
			$is_pemilik = 0;
			if($yyy[$i] == '1'){
				$is_pemilik = 1;
				$pemilik = array(
					'periode'=>$periode,
					'kpi_id'=>$kpiId,
					'unit_id'=>$xxx[$i],
					'periode_input'=> $periodeInput
				);	

				$this->model->insert('pemilik_kpi',$pemilik);
			}

			$kontributor = array(
				'periode'=>$periode,
				'kpi_id'=>$kpiId,
				'unit_id'=> $xxx[$i],
				'bobot'=> 0,
				'is_pemilik' => $is_pemilik //$zzz[$i]
			);
			
			$this->model->insert('pelaksana_kpi',$kontributor);	
			

			//insert to km_unit
			$data = array(
				'kpi_id' => $kpiId,
				'periode' => $periode,
				'unit_id' => $xxx[$i]
			);
			//if exist, clear
			$this->model->delete('km_unit', $data);


			$this->model->insert('km_unit', $data);
		}

		$aaa = $this->input->post('kpi-option-unit');
		$bbb = $this->input->post('kpi-bobot-unit');
		$ccc = $this->input->post('kpi-parent-unit');

		for($i=0;$i<count($aaa);$i++){
			$pendukung = array(
				'periode'=>$periode,
				'kpi_id'=>$kpiId,
				'unit_id'=>$aaa[$i],
				'unit_parent_id'=>$ccc[$i],
				'bobot'=> 0 //$bbb[$i]
			);

			$this->model->insert('pendukung_kpi',$pendukung);	
		}

		// $clause	=array('kpi_id'=>$this->input->post('kpi_id'));
		// $total_rkm_detail=$this->input->post('total-rkm-detail');

		// if($total_rkm_detail > 0){
		// 	for($i=0;$i<$total_rkm_detail;$i++)
		// 	{
		// 		//Pemilik KPI
		// 		if($_POST['peranan'][$i]=="1"){	
		// 			$data=array(
		// 				'periode'=>$this->input->post('periode'),
		// 				'kpi_id'=>$this->input->post('kpi_id'),
		// 				'unit_id'=>$_POST['direktorat'][$i],
		// 				'bobot'=>$_POST['bobot'][$i]			
		// 			);
		// 			$this->model->insert('pelaksana_kpi',$data);
					
		// 			$data2=array(
		// 				'periode'=>$this->input->post('periode'),
		// 				'kpi_id'=>$this->input->post('kpi_id'),
		// 				'unit_id'=>$_POST['direktorat'][$i]	
		// 			);					
		// 			$this->model->insert('pemilik_kpi',$data2);
		// 		}else{//Direktorat pelaksana
		// 			$data=array(
		// 				'periode'=>$this->input->post('periode'),
		// 				'kpi_id'=>$this->input->post('kpi_id'),
		// 				'unit_id'=>$_POST['direktorat'][$i],
		// 				'bobot'=>$_POST['bobot'][$i]			
		// 			);
		// 			$this->model->insert('pelaksana_kpi',$data);
		// 		}		
		// 	}
		// }		
		echo json_encode(array('status' => 'ok'));
	}

	public function get_detail_rkm(){
		$periode = $this->input->post('periode');
		$kpiId = $this->input->post('kpi-id');

		$sql = "SELECT pk.id, 
		pk.periode, 
		pk.kpi_id, 
		o.name, 
		pk.unit_id, 
		pk.bobot, 
		0 as parent_unit_id, 
		(select periode_input from pemilik_kpi where periode = '".$periode."' and kpi_id = '".$kpiId."' ) as periode_input,
		(select case unit_id when pk.unit_id then 1 else 2 end from pemilik_kpi where periode = '".$periode."' and kpi_id = '".$kpiId."' limit 1) as role FROM pelaksana_kpi pk, organizations o where pk.kpi_id = '".$kpiId."' and pk.periode = '".$periode."' and  pk.unit_id = o.id union SELECT dk.id, dk.periode, dk.kpi_id, o.name, dk.unit_id, dk.bobot, dk.unit_parent_id, 0 as periode_input, 'pendukung' as role from pendukung_kpi dk, organizations o where dk.kpi_id = '".$kpiId."' and dk.periode='".$periode."' and dk.unit_id = o.id";

		$sql2 = "select id, name from organizations";

		echo json_encode(array('a' => $this->model->query($sql), 'b' => $this->model->query($sql2)));
	}

	public function get_rkm_by_kpi_id(){
		$kpi_id=$this->input->post('kpi_id');
		$periode=$this->input->post('periode');
		$sql="SELECT a.* FROM  pemilik_kpi a WHERE periode=".$periode." AND kpi_id=".$kpi_id;			
		$pemilik_kpi=$this->model->query($sql);
		
		foreach($pemilik_kpi as $pemilik){
			
		}					
		//Pelaksana KPI
		$sql2="SELECT a.*, b.name pelaksana_kpi			
			FROM  pelaksana_kpi a
			JOIN organizations b ON (a.unit_id=b.id)
			WHERE a.periode=".$periode." AND a.kpi_id=".$kpi_id;
			
		$pelaksana_kpi=$this->model->query($sql2);

		
		$organizations=$this->model->select('organizations');
		
		echo json_encode($pemilik_kpi);
	}
}
