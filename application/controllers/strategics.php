<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Strategic References
class Strategics extends CI_Controller {
	var $data=array();
	public function __construct() {
		parent::__construct();				
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}	
	}	
	public function perspectives(){	
		$data['data']=$this->model->select('perspectives');
		$this->load->view('strategics/perspectives/index',$data);
	}
	public function perspective_add(){			
		$this->load->view('strategics/perspectives/add');		
    }
	public function perspective_save(){	
		$data=array('name'=>$this->input->post('name'));	
		echo $this->model->insert('perspectives',$data)? "1" : "0";	
	}
	public function perspective_edit($id){		
		$clause=array('id'=>$id);
		$data['data']=$this->model->select('perspectives',$clause);
		$this->load->view('strategics/perspectives/edit',$data);		
    }
	public function perspective_update(){	
		$data=array('name'=>$this->input->post('name'));	
		$clause=array('id'=>$this->input->post('id'));
		echo $this->model->update('perspectives',$data,$clause)? "1" : "0";	
	}
	public function perspective_delete($id){	
		$clause=array('id'=>$id);	
		echo $this->model->delete('perspectives',$clause)? "1" : "0";	
	}
	public function themes(){	
		$sql="SELECT a.id AS theme_id,a.name AS theme_name,a.target,b.name AS objective_name
        FROM themes a        
        JOIN objectives b ON (a.objective_id=b.id)       
        WHERE 1=1";
		//$sql="SELECT * FROM themes";
		$data['data']=$this->model->query($sql);
		$this->load->view('strategics/themes/index',$data);
	
	}
	public function theme_add(){			
		$data['data']=$this->model->select('objectives');
		$this->load->view('strategics/themes/add',$data);		
    }
	public function theme_save(){		
		$data=array(
			'name'=>$this->input->post('name'),
			'sasaran'=>$this->input->post('sasaran'),
			'target'=>$this->input->post('target'),
			'objective_id'=>$this->input->post('objective_id')
		);	
		echo $this->model->insert('themes',$data)? "1" : "0";	
	}
	public function theme_edit($id){		
		$sql="SELECT a.id theme_id,a.name theme_name,a.target,b.id objective_id, b.name objective_name 
			FROM themes a
			JOIN objectives b ON (a.objective_id=b.id)
			WHERE a.id=$id";
		//$sql="SELECT * FROM themes WHERE id=".$id;
		$data['data']=$this->model->query($sql);
		$data['data2']=$this->model->select('objectives');
		$this->load->view('strategics/themes/edit',$data);		
    }
	public function theme_update(){	
		$data=array(
			'name'=>$this->input->post('name'),
			'sasaran'=>$this->input->post('sasaran'),
			'target'=>$this->input->post('target'),
			'objective_id'=>$this->input->post('objective_id')
		);	
		$clause=array('id'=>$this->input->post('id'));
		echo $this->model->update('themes',$data,$clause)? "1" : "0";	
	}
	public function theme_delete(){	
		$clause=array('id'=>$this->input->post('id'));	
		echo $this->model->delete('themes',$clause)? "1" : "0";	
	}
	public function theme_auto($name=''){	
		//$name=$this->input->post('name');
		$sql="SELECT id,name FROM perspectives WHERE name LIKE '%$name%'";
		$data=$this->model->query($sql);
		
		$name=array();
		if(count($data)>0){
			foreach($data as $r){
				$name[]=$r->id."|".$r->name;
			}
			//print_r($name);
			//echo $this->db->last_query();
			echo json_encode($name);
		}else {
			echo "0";
		
		}
		//$this->load->view('strategics/objectives/index');	
	}
	public function objectives(){	
		$sql="SELECT * FROM objectives WHERE 1=1";	 		
		$data['data']=$this->model->query($sql);			
		$this->load->view('strategics/objectives/index',$data);	
	}
	public function objective_add(){		
		$data['data']="";	
		$this->load->view('strategics/objectives/add',$data);		
    }
	public function objective_save(){	
		$data=array('name'=>$this->input->post('name'));	
		echo $this->model->insert('objectives',$data)? "1" : "0";	
	}
	public function objective_edit($id){	
		$sql="SELECT *
			FROM objectives			
			WHERE id=$id";
		$data['data']=$this->model->query($sql);
		$this->load->view('strategics/objectives/edit',$data);		
    }
	public function objective_update(){	
		$data=array('name'=>$this->input->post('name'));	
		$clause=array('id'=>$this->input->post('id'));
		echo $this->model->update('objectives',$data,$clause)? "1" : "0";	
	}
	public function objective_delete(){	
		$clause=array('id'=>$this->input->post('id'));	
		echo $this->model->delete('objectives',$clause)? "1" : "0";	
	}
	public function inisiatives(){	
		$sql="SELECT a.id AS inisiative_id,a.name AS inisiative_name,b.name AS theme_name
              FROM inisiatives a
			  JOIN themes b ON (a.theme_id=b.id)
              WHERE 1=1";	 
		
		$data['data']=$this->model->query($sql);
		$this->load->view('strategics/inisiatives/index',$data);	
	}
	public function inisiative_add(){		
		$data['data']=$this->model->select('themes');	
		$this->load->view('strategics/inisiatives/add',	$data);		
    }
	public function inisiative_save(){	
		$data=array('name'=>$this->input->post('name'),'theme_id'=>$this->input->post('theme_id'));	
		echo $this->model->insert('inisiatives',$data) ? "1" : "0";	
		
	}
	public function inisiative_edit($id){		
		$sql="SELECT a.id AS inisiative_id,b.id theme_id,a.name AS inisiative_name,a.name AS theme_name
              FROM inisiatives a
			  JOIN themes b ON (a.theme_id=b.id)
              WHERE a.id=$id";
		$data['data']=$this->model->query($sql);	
		$data['data2']=$this->model->select('themes');	
		$this->load->view('strategics/inisiatives/edit',$data);		
    }
	public function inisiative_update(){	
		$data=array('name'=>$this->input->post('name'),'theme_id'=>$this->input->post('theme_id'));	
		$clause=array('id'=>$this->input->post('id'));
		echo $this->model->update('inisiatives',$data,$clause)? "1" : "0";	
	}
	public function inisiative_delete(){	
		$clause=array('id'=>$this->input->post('id'));	
		echo $this->model->delete('inisiatives',$clause)? "1" : "0";	
	}
	public function maps(){	
		$data=$this->model->select('objectives');
		$tree='';
		foreach($data as $row){			
			$tree.='<tr data-tt-id="'.$row->id.'"><td>'.$row->name.'</td></tr>';
			$tree.=$this->get_themes($row->id);
		}
		$data['tree']=$tree;
		$this->load->view('strategics/maps/index',$data);
	}
	public function get_themes($id){	
		$sql="SELECT * FROM themes WHERE objective_id=".$id;
		$data=$this->model->query($sql);
		$tree='';
		foreach($data as $row)
		{
			$tree.='<tr data-tt-id="'.$id.$row->id.'" data-tt-parent-id="'.$id.'"><td>'.$row->name.'</td></tr>';
			$tree.=$this->get_inisiatives($id,$row->id);
		}
		return $tree; 
	}
	public function get_inisiatives($id,$id2){	
		$sql="SELECT * FROM inisiatives WHERE theme_id=".$id2;
		$data=$this->model->query($sql);
		$tree='';
		foreach($data as $row)
		{
			$tree.='<tr data-tt-id="'.$row->id.'" data-tt-parent-id="'.$id.$id2.'"><td>'.$row->name.'</td></tr>';
		}
		return $tree; 
	}
}
