<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Types extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->model->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function index(){	
		$data['data']=$this->model->select('types');
		$this->load->view('home/types/index',$data);
	}
	public function add(){	
		$this->load->view('home/types/add');
	}
	public function save(){	
		$data=array(
			'name'=>$this->input->post('name'),
			'description'=>$this->input->post('description')
		);
		echo $this->model->insert('types',$data)? "1":"0";
	}
	public function edit($id){	
		$clause=array(
			'id'=>$id			
		);
		$data['data']=$this->model->select('types',$clause);		
		$this->load->view('home/types/edit',$data);
	}
	public function update(){			
		$data=array(
			'name'=>$this->input->post('name'),
			'description'=>$this->input->post('description')
		);
		$clause=array(
			'id'=>$this->input->post('id')			
		);
		echo $this->model->update('types',$data,$clause)? "1":"0";
	}
	public function delete(){	
		$clause=array(
			'id'=>$this->input->post('id')			
		);
		echo $this->model->delete('types',$clause)? "1":"0";
	}
}
