<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pelaksanaan_rkm extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->model->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}

	public function index(){	
		$unit_id=$this->session->userdata('organization_id');
		if($unit_id==0){
			$data['data']=$this->model->select('organizations');
		}else{
			$data['data']=$this->model->select('organizations',array('id'=>$unit_id));
		}
		$this->load->view('home/pelaksanaan_rkm/index',$data);
	}

	public function get_progaram_kerja(){
		$unit=$this->input->post('unit');
		$periode=$this->input->post('periode');
		
		$sql = "SELECT a.*, a.keterangan_tobe_verify keterangan_rencana, b.program_kerja FROM rencana_kerja a JOIN program_kerja b ON (a.program_kerja_id = b.id) AND a.unit_id = '".$unit."' AND a.periode = '".$periode."'";
		echo json_encode($this->model->query($sql));
	}

	public function get_keterangan($id){	
		$ket  = $this->db->query('SELECT keterangan FROM rencana_kerja  where id="'.$id.'"')->row()->keterangan;
		$echo = ($ket=='')? 'Belum ada keterangan' : $ket;
		echo $echo;
	}
	
	public function save_rencana_kerja(){
		$unit=$this->input->post('choose_program_kerja_unit');
		$periode=$this->input->post('choose_program_kerja_periode');
		$program_kerja = $this->input->post('program_kerja_selected');
		
	
		for($i=0;$i<count($program_kerja);$i++){
			$data = array(
				'program_kerja_id' => $program_kerja[$i],
				'unit_id' => $unit,
				'periode' => $periode
			);
			//print_r($data);
			$this->model->insert('rencana_kerja',$data);
		}
		

		echo json_encode(array('status' => 'ok'));
	}

	public function update_rencana_kerja(){		
		$this->load->library('mailman');
		$CI =& get_instance();

		$program_kerja_id = $this->input->post('program_kerja_id');	
		$keterangan = $this->input->post('keterangan_progress');		
		$value = $this->input->post('persentase_progress');
		$id_rk = $this->input->post('id_rk');

		for($i=0;$i<count($program_kerja_id);$i++){
			if ($this->input->post('progress_id_'.$i)==1){
				$progress_id = 1;
			}elseif($this->input->post('progress_id_'.$i)==2){
				$progress_id = 2;
			}elseif($this->input->post('progress_id_'.$i)==3){
				$progress_id = 3;
			}else{
				$progress_id = '';
			}

			$data = array(
				//'progress_id' => $progress_id,
				//'value' => $value[$i],
				//'keterangan' => $keterangan[$i]
				'value_tobe_verify' => $value[$i],
				'status_tobe_verify' => $progress_id,
				'keterangan_tobe_verify' => $keterangan[$i],
				'status_verifikasi' => 0 
			);

			$clause = array('id'=>$program_kerja_id[$i]);

			$this->model->update('rencana_kerja', $data, $clause);

			//insert history
			// $history = array(
			// 	'id_rk' => $id_rk[$i],
			// 	'value' =>  $value[$i],
			// 	'date' => date("Y-m-d H:i:s"),
			// 	'userid' => $this->session->userdata('user_id'),
			// 	'periode' => $this->input->post('choose_periode'),
			// 	'unit_id' => $this->input->post('choose_unit'),
			// 	'keterangan' => $keterangan[$i],
			// 	'progress_id' => $progress_id,
			// 	'info_tambahan' => 'Realisasi oleh Unit',
			// 	'include_in_history' => 0
  	// 	);

			// $this->model->insert('history_progress', $history);
		}	

		$unit = $this->input->post('choose_unit');
		$ser = $this->db->query("SELECT name FROM organizations WHERE id = '".$unit."'");
		$unitName = 'Unkown Unit';

		if ($ser->num_rows() > 0){
		  $row = $ser->row();
		  $unitName = $row->name;
		} 
		
		$message = 'Pengguna '.$this->session->userdata('name').' telah melakukan input/update data RKM pada Unit '.substr($unitName, 4).
			'. <br/><br/>Mohon segera lakukan pemeriksaan pada menu Verifikasi RKM oleh Penilai.';
		
		$to = $this->mailman->get_destination_email(true, $CI);
		$mailStatus = $this->mailman->send_email($to, 'Notifikasi Pengisian RKM', $message, $CI);

		echo json_encode(array('status' => 'ok', 'mail_status' => null));
	}
}