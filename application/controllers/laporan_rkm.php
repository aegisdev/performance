<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Laporan_rkm extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->model->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}

	public function index(){	
		$unit_id=$this->session->userdata('organization_id');
		//Tidak punya unit_id
		if(TRUE/*$unit_id==0*/){
			$data['data']=$this->model->select('organizations');
		}else{
			$data['data']=$this->model->select('organizations',array('id'=>$unit_id));
		}		
		$this->load->view('home/laporan_rkm/index',$data);
	}

	public function get_progaram_kerja(){
		$unit=$this->input->post('unit');
		$periode=$this->input->post('periode');
		
		$sql='SELECT a.*,b.*
		FROM rencana_kerja a 
		JOIN program_kerja b ON (a.program_kerja_id=b.id)
		AND a.unit_id="'.$unit.'" AND a.periode="'.$periode.'"';
		echo json_encode($this->model->query($sql));
	}

	public function get_report_rkm($unit,$periode,$type,$title=''){
		$data['title']=rawurldecode($title);
		$data['periode']=$periode;
		$sql='SELECT a.*,b.*
		FROM rencana_kerja a 
		JOIN program_kerja b ON (a.program_kerja_id=b.id)
		AND a.unit_id="'.$unit.'" AND a.periode="'.$periode.'"';
		$data['data']=$this->model->query($sql);
		if($type==1){
			$this->load->helper(array('dompdf', 'file'));
			$html = $this->load->view('home/laporan_rkm/report_rkm',$data, true);
			pdf_create($html,'report_rkm_'.$periode,TRUE,1);
		}else {//File type excel			
		$this->load->view('home/laporan_rkm/report_rkm',$data);	
		}
	}

	public function get_progress(){
		$id = $this->input->post('id-rkm');
		$periode = $this->input->post('periode');
		$unit = $this->input->post('unit');

		$q1 = "SELECT DATE_FORMAT(tanggal_input,'%d/%m/%Y') as date, status_verifikasi, keterangan_verifikasi, DATE_FORMAT(tanggal_verifikasi, '%d/%m/%Y') as tanggal_verifikasi FROM rencana_kerja WHERE periode = ".$periode." AND unit_id = ".$unit." AND program_kerja_id = ".$id;
		$sq1 = $this->model->query($q1);

		$q2 = "SELECT MIN(id), DATE_FORMAT(date,'%d/%m/%Y') as date FROM history_progress WHERE periode = ".$periode." AND unit_id = ".$unit." AND id_rk = ".$id." AND include_in_history = 1 AND value > 0 GROUP BY id, date";
		$sq2 = $this->model->query($q2);

		$q3 = "SELECT MAX(id), DATE_FORMAT(date,'%d/%m/%Y') as date FROM history_progress WHERE periode = ".$periode." AND unit_id = ".$unit." AND id_rk = ".$id." AND value = 100 GROUP BY id, date";
		$sq3 = $this->model->query($q3);

		$sql = "SELECT DATE_FORMAT(date,'%d/%m/%Y') as date, value, keterangan FROM history_progress WHERE id_rk = ".$id." AND periode = ".$periode." AND include_in_history = 1 ORDER BY CONVERT(DATE, date) ASC";
		
		$data = array('data' => $this->model->query($sql), 'a' => $sq1, 'b' => $sq2, 'c' => $sq3);
		
		echo json_encode($data);
		
	}

	public function get_tanggal(){
		$id_program_kerja = $this->input->post('id_program_kerja');
		$unit = $this->input->post('unit_id');
		$periode = $this->input->post('periode');

		$q1 = "SELECT tanggal_input as date FROM rencana_kerja WHERE periode = ".$periode." AND unit_id = ".$unit." AND program_kerja_id = ".$id_program_kerja;
		$sq1 = $this->model->query($q1);

		$q2 = "SELECT MIN(id), date FROM history_progress WHERE periode = ".$periode." AND unit_id = ".$unit." AND id_rk = ".$id_program_kerja." GROUP BY id, date";
		$sq2 = $this->model->query($q2);

		$q3 = "SELECT MAX(id), date FROM history_progress WHERE periode = ".$periode." AND unit_id = ".$unit." AND id_rk = ".$id_program_kerja." AND value = 100 GROUP BY id, date";
		$sq3 = $this->model->query($q3);

		echo json_encode(array('a' => $sq1, 'b' => $sq2, 'c' => $sq3));
	}

	public function report_detail_rkm($unit,$periode,$id,$title=''){
		$q1 = "SELECT DATE_FORMAT(tanggal_input,'%d/%m/%Y') as date, status_verifikasi, keterangan_verifikasi, DATE_FORMAT(tanggal_verifikasi, '%d/%m/%Y') as tanggal_verifikasi FROM rencana_kerja WHERE periode = ".$periode." AND unit_id = ".$unit." AND program_kerja_id = ".$id;
		$sq1 = $this->model->query($q1);

		$q2 = "SELECT MIN(id), DATE_FORMAT(date,'%d/%m/%Y') as date FROM history_progress WHERE periode = ".$periode." AND unit_id = ".$unit." AND id_rk = ".$id." AND include_in_history = 1 AND value > 0 GROUP BY id, date";
		$sq2 = $this->model->query($q2);

		$q3 = "SELECT MAX(id), DATE_FORMAT(date,'%d/%m/%Y') as date FROM history_progress WHERE periode = ".$periode." AND unit_id = ".$unit." AND id_rk = ".$id." AND value = 100 GROUP BY id, date";
		$sq3 = $this->model->query($q3);

		$sql = "SELECT DATE_FORMAT(date,'%d/%m/%Y') as date, value, keterangan FROM history_progress WHERE id_rk = ".$id." AND periode = ".$periode." AND include_in_history = 1 ORDER BY CONVERT(DATE, date) ASC";
		
		$title = $this->model->query("SELECT program_kerja FROM program_kerja WHERE id = " . $id);


		$data = array('data' => $this->model->query($sql), 'a' => $sq1, 'b' => $sq2, 'c' => $sq3, 'periode' => $periode, 'title' => $title);

		$this->load->helper(array('dompdf', 'file'));
		$html = $this->load->view('home/laporan_rkm/detail_report_rkm', $data, true);
		pdf_create($html,'report_progress_rkm_'.$periode,TRUE,1);
		//$this->load->view('home/laporan_rkm/detail_report_rkm', $data);
	}
}
