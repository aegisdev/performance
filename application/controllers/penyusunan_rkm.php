<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Penyusunan_rkm extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->model->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}

	public function index(){	
		$unit_id=$this->session->userdata('organization_id');
		if($unit_id==0){
			$data['data']=$this->model->select('organizations');
		}else{
			$data['data']=$this->model->select('organizations',array('id'=>$unit_id));
		}
		$this->load->view('home/penyusunan_rkm/index',$data);
	}
	public function get_progaram_kerja()
	{
		$unit = $this->input->post('unit');
		$periode = $this->input->post('periode');
		$sql = 'SELECT a.id, b.keterangan, b.program_kerja, a.program_kerja_id 
		FROM rencana_kerja a 
		JOIN program_kerja b ON (a.program_kerja_id = b.id)
		AND a.unit_id="'.$unit.'" AND a.periode="'.$periode.'"';
		echo json_encode($this->model->query($sql));
	}

	public function view_progaram_kerja(){
		$periode=$this->input->post('periode');
		$unit_id=$this->input->post('unit');
		$sql='SELECT b.id,b.keterangan,b.program_kerja 
		FROM program_kerja b 	
		WHERE b.id NOT IN (SELECT program_kerja_id FROM rencana_kerja WHERE periode="'.$periode.'" AND unit_id="'.$unit_id.'")';
		echo json_encode($this->model->query($sql));
	}
	
	public function save_rencana_kerja(){
		$unit=$this->input->post('choose_program_kerja_unit');
		$periode=$this->input->post('choose_program_kerja_periode');
		$program_kerja = $this->input->post('program_kerja_selected');		
	
		for($i=0;$i<count($program_kerja);$i++){
			$data = array(
				'program_kerja_id' => $program_kerja[$i],
				'unit_id' => $unit,
				'periode' => $periode,
				'tanggal_input' => date("Y-m-d H:i:s"),
				//added
				'progress_id' => 1,
				'value' => 0,
				'status_verifikasi' => 1,
				'keterangan_verifikasi' => 'Approved by System',
				'tanggal_verifikasi' =>  date("Y-m-d H:i:s"),
				'status_tobe_verify' => 1,
				'value_tobe_verify' => 0
			);

			$this->model->insert('rencana_kerja',$data);

			//history
			$thisMonth = date('n');

			for($j = 1;$j<13;$j++){
				if($j != $thisMonth){
					$includeInHistory = 0;
				} else {
					$includeInHistory = 1;
				}

				$date = date("Y-m-d H:i:s", mktime(date('H'), date('i'), date('s'), $j, date('j'), date('Y')));
				$history = array(
					'id_rk' => $program_kerja[$i],
					'value' =>  0,
					'date' => $date,
					'userid' => $this->session->userdata('user_id'),
					'periode' => $periode,
					'unit_id' => $unit,
					'keterangan' => 'Inisialisasi Pembuatan RKM',
					'progress_id' => 1,
					'include_in_history' => $includeInHistory
	 			);

				$this->model->insert('history_progress', $history);
			}
		}
		echo json_encode(array('status' => 'ok'));
	}
	public function delete(){	
		$histClause = array('id_rk' => $this->input->post('id-rencana-kerja'), 'unit_id' => $this->input->post('unit'), 'periode' => $this->input->post('periode'));
		$this->model->delete('history_progress', $histClause);

		$clause = array('id'=>$this->input->post('id'));
		echo $this->model->delete('rencana_kerja',$clause)? "1":"0";
	}
}
