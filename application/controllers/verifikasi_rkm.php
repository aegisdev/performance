<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Verifikasi_rkm extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}

	public function index(){
		$data['data']=$this->model->select('organizations'); 
		$this->load->view('home/verifikasi_rkm/index', $data);
	}

	public function verifikasi(){
		$this->load->library('mailman');
		$CI =& get_instance();

		$rencanaKerja = $this->input->post('program_kerja');
		$arrRencanaKerja = $this->input->post('program_kerja_id');
		$statusVerifikasi = $this->input->post('status_verifikasi');
		$keteranganVerifikasi = $this->input->post('keterangan_verifikasi');
		$unit = $this->input->post('choose_unit');
		$periode = $this->input->post('choose_periode');
		$idRkm = $this->input->post('id_rkm');
		$keterangan = $this->input->post('keterangan');
		$valueTobeVerify = $this->input->post('value_tobe_verify');
		$statusTobeVerify = $this->input->post('status_tobe_verify');
		$keteranganTobeVerify = $this->input->post('keterangan_tobe_verify');
		$value = $this->input->post('value');
		$progressId = $this->input->post('progress_id');
		$rowMsg = '';

		for ($i=0; $i < count($arrRencanaKerja); $i++){ 
			$data = array();

			if($statusVerifikasi[$i] == 1){
				//approved, update value etc
				$data['value'] = $valueTobeVerify[$i];
				$data['progress_id'] = $statusTobeVerify[$i];
				$data['keterangan'] = $keteranganTobeVerify[$i]; 
			} else {
				$data['value'] = $value[$i];
				$data['progress_id'] = $progressId[$i];
				$data['value_tobe_verify'] = $value[$i];
				$data['status_tobe_verify'] = $statusTobeVerify[$i]; 				
			}

			$data['status_verifikasi'] = $statusVerifikasi[$i];
			$data['keterangan_verifikasi'] = $keteranganVerifikasi[$i];
			$data['tanggal_verifikasi'] = date("Y-m-d H:i:s");

			$lblStatusVerifikasi = ($statusVerifikasi[$i] == 1) ? 'Sesuai' : 'Belum Sesuai';

			$rowMsg .= '<tr><td>'.($i+1).'</td><td>'.$rencanaKerja[$i].'</td><td>'.$lblStatusVerifikasi.'</td><td>'.$keteranganVerifikasi[$i].'</td></tr>';
			$clause = array('id'=>$arrRencanaKerja[$i]);
			$this->model->update('rencana_kerja', $data, $clause);

			$history = array(
				'id_rk' => $idRkm[$i],
				'value' =>  $data['value'],
				'date' => date("Y-m-d H:i:s"),
				'userid' => $this->session->userdata('user_id'),
				'periode' => $this->input->post('choose_periode'),
				'unit_id' => $this->input->post('choose_unit'),
				'keterangan' => $data['keterangan'],
				'progress_id' => $data['progress_id'],
				'info_tambahan' => 'Verifikasi oleh Auditor'
  		);

			$this->model->insert('history_progress', $history);

			//update default
			$this->db->query("update history_progress set value = ".$data['value'].", progress_id = ".$data['progress_id']." where id_rk = ".$idRkm[$i]." and unit_id = ".$unit." and periode = ".$periode." and date > '".date('Y-m-d H:i:s')."'");
		}

		$ser = $this->db->query("SELECT name FROM organizations WHERE id = '".$unit."'");
		$unitName = 'Unkown Unit';
		if ($ser->num_rows() > 0){
		  $row = $ser->row();
		  $unitName = $row->name;
		} 

		$message = 'Pengguna '.$this->session->userdata('name').' telah melakukan verifikasi data RKM untuk Unit '.substr($unitName, 4).
			' pada '.date("d-m-Y H:i:s").'. <br/>';

		$message .= '<p>Daftar RKM yang telah diverifikasi : </p>';
		$message .= '<table border="1px" cellpadding="1px" cellspacing="1px">';
		$message .= '<tr><th>No</th><th>Program Kerja</th><th>Status Verfikasi</th><th>Keterangan</th></tr>';
		$message .= $rowMsg;
		$message .= '</table>';

		$to = $this->mailman->get_destination_email(false, $CI, $unit);		
		$mailStatus = $this->mailman->send_email($to, 'Notifikasi Verifikasi RKM', $message, $CI);

		echo json_encode(array('status' => 'ok', 'mailStatus' => null));
	}
}