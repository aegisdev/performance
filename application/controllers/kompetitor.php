<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kompetitor extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->model->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	
	public function index(){	
		$sql="SELECT a.*,b.kpi FROM kompetitor a 
		JOIN kpi b ON (a.kpi_id=b.id)
		";
		$data['data']=$this->model->query($sql);
		$this->load->view('home/kompetitor/index',$data);
	}
	public function add($tahun){
		$sql="SELECT * FROM kpi 
			WHERE id NOT IN (
			SELECT a.id FROM kpi a, kompetitor b
			WHERE b.kpi_id=a.id AND b.tahun=".$tahun.")";
			
		$data['data']=$this->model->query($sql);
		$data['tahun']=$tahun;
		$this->load->view('home/kompetitor/add',$data);
	}
	public function save(){	
		$data=array(		
			'tahun'=>$this->input->post('tahun'),
			'kpi_id'=>$this->input->post('kpi_id'),
			'best_performance'=>$this->input->post('best_performance'),
			'kompetitor'=>$this->input->post('kompetitor'),
			'benchmark'=>$this->input->post('benchmark')	
		);
		echo $this->model->insert('kompetitor',$data) ? "1":"0";
	
	}
	public function edit($id){	
		$clause =array('id'=>$id);
		$sql="SELECT * FROM kompetitor WHERE id=".$id;
		$data['kpis']=$this->model->select("kpi");		
		$data['data']=$this->model->query($sql);		
		$this->load->view('home/kompetitor/edit',$data);		
	}	
	public function update(){	
		$data=array(		
			'tahun'=>$this->input->post('tahun'),
			'kpi_id'=>$this->input->post('kpi_id'),
			'best_performance'=>$this->input->post('best_performance'),
			'kompetitor'=>$this->input->post('kompetitor'),
			'benchmark'=>$this->input->post('benchmark')	
		);
		$clause=array('id'=>$this->input->post('id'));		
		echo $this->model->update('kompetitor',$data,$clause) ? "1":"0";	
	}
	public function delete(){	
		$clause =array('id'=>$this->input->post('id'));
		echo $this->model->delete('kompetitor',$clause) ? "1":"0";
	}	
	public function get_pembanding($tahun){
		$sql="SELECT a.*,b.kpi FROM kompetitor a 
		JOIN kpi b ON (a.kpi_id=b.id) WHERE tahun='".$tahun."'";
		echo json_encode($this->model->query($sql));	
	}
}
