<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Batas_penilaian extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		$this->model->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function index(){	
		$data['data']=$this->model->select('batas_penilaian');
		$this->load->view('home/batas_penilaian/index',$data);
	}
	public function add(){	
		$this->load->view('home/batas_penilaian/add');
	}
	public function save(){	
		$data=array(
			'maksimal'=>$this->input->post('maksimal'),
			'minimal'=>$this->input->post('minimal')
		);
		echo $this->model->insert('batas_penilaian',$data)? "1":"0";
	}
	public function edit($id){	
		$clause=array(
			'id'=>$id			
		);
		$data['data']=$this->model->select('batas_penilaian',$clause);		
		$this->load->view('home/batas_penilaian/edit',$data);
	}
	public function update(){			
		$data=array(
			'maksimal'=>$this->input->post('maksimal'),
			'minimal'=>$this->input->post('minimal')
		);
		$clause=array(
			'id'=>$this->input->post('id')			
		);
		echo $this->model->update('batas_penilaian',$data,$clause)? "1":"0";
	}
	public function delete(){	
		$clause=array(
			'id'=>$this->input->post('id')			
		);
		echo $this->model->delete('batas_penilaian',$clause)? "1":"0";
	}
}
