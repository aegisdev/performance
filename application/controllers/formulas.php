<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Formulas extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->model->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function index(){	
		$data['data']=$this->model->select('formulas');
		$this->load->view('home/formulas/index',$data);
	}
}
