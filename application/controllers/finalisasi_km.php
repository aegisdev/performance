<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Finalisasi_km extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->model->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}				
	}
	public function index(){	
		$this->load->view('home/measurements/finalisasi_km');
	}
	public function get_finalisasi_km(){	
		$periode=$this->input->post('periode');

		$sql="SELECT a.*, kpi.kpi as kpi_name, kpi.formula_id, e.maksimal maks, e.minimal min, units.name as satuan, d.name as perspective			
			FROM km as a, kpi, units, pelaksana_kpi c, perspectives d, batas_penilaian e 
			WHERE a.kpi_id = kpi.id 
			AND kpi.unit_id = units.id 
			AND kpi.perspective_id = d.id 
			AND kpi.batas_penilaian_id = e.id 
			AND a.periode ='".$periode."' 
			GROUP BY a.kpi_id,d.id ORDER BY order_no";			
		
		$data=$this->model->query($sql);
		echo json_encode($data);
	}

	public function calculate_finalisasi_km(){	
		$periode = $this->input->post('periode');
		
		$lowerDiv = "select kpi_id, sum(realisasi_org) realisasi_org,sum(realisasi_penilai) realisasi_penilai from km_unit where unit_id IN (select unit_id from pelaksana_kpi where periode='".$periode."') and periode = '".$periode."' group by kpi_id";
		$hslLowerDiv = $this->model->query($lowerDiv);

		echo json_encode($hslLowerDiv);				
	}

	public function save_finalization(){
		$periode = $this->input->post('periode-choosen');

		$data = $this->model->select('km', array('periode' => $periode, 'is_published' => 0));

		if(count($data) > 0){			
			$this->model->update('km', array('is_published'=> 1), array('periode' => $periode));
			foreach($data as $d){
				$data_log_unit = array(
					'periode' => $periode,
					'unit_id' => 0,
					'kpi_id' => $d->kpi_id,
					'tgl_input' => date('YmdHis'),
					'realisasi' => $d->realisasi_org,
					'user_id' =>$this->session->userdata('user_id')
				);

				$data_log_penilai = array(
					'periode' => $periode,
					'unit_id'=> 0,
					'kpi_id'=> $d->kpi_id,
					'tgl_input' => date('YmdHis'),
					'realisasi' => $d->realisasi_penilai,
					'user_id' => $this->session->userdata('user_id')
				);				

				$this->model->insert('log_real_unit',$data_log_unit);
				$this->model->insert('log_real_penilai',$data_log_penilai);
			}	
		}

		echo json_encode(array('status'=>'ok', 'data' => $data));
	}

	public function simpan_finalisasi($periode){
		$user_id=$this->session->userdata('user_id');
		$data=$this->model->select('finalisasi',array('periode'=>$periode,'user_id'=>$user_id));
		//Jika baru
		if(count($data)==0){
			$data=array(
					'user_id'=>$user_id,
					'periode'=>$periode,
					'tanggal'=>date('YmdHis')
				);
			$this->model->insert('finalisasi',$data);	
		} else {
			$data=array(
					'tanggal'=>date('YmdHis')
				);
			$this->model->update('finalisasi',$data,array('user_id'=>$user_id,'periode'=>$periode));				
		}
		$data2=$this->model->select('km',array('periode'=>$periode,'is_published'=>0));
		if(count($data2) > 0){		
			$this->model->update('km',array('is_published'=>1),array('periode'=>$periode));			
		} 
		echo json_encode(array('status'=>'ok'));
	}


	public function set_finalisasi(){
		$user_id=$this->session->userdata('user_id');
		$periode = $this->input->post('periode_choosen');
		$kpi = $this->input->post('kpi_id');
		$real_kpi_id = $this->input->post('real_kpi_id');
		$count = count($kpi);
		$realisasi_org =  $this->input->post('realisasi_org');
		$pencapaian_org =  $this->input->post('pencapaian_org');
		$prestasi_org = $this->input->post('prestasi_org');		
		
		$realisasi_penilai =  $this->input->post('realisasi_penilai');
		$pencapaian_penilai =  $this->input->post('pencapaian_penilai');
		$prestasi_penilai = $this->input->post('prestasi_penilai');	

		$data=$this->model->select('finalisasi',array('periode'=>$periode, 'user_id'=>$user_id));

		if(count($data) == 0){	
			$data=array(
				'user_id'=>$user_id,
				'periode'=>$periode,
				'tanggal'=>date('YmdHis')
			);

			$this->model->insert('finalisasi',$data);	

			for($i=0;$i<$count;$i++){
				$data = array(
					'realisasi_org' =>$realisasi_org[$i],
					'pencapaian_org' => $pencapaian_org[$i],
					'prestasi_org' => $prestasi_org[$i],
					'realisasi_penilai' => $realisasi_penilai[$i],
					'pencapaian_penilai' => $pencapaian_penilai[$i],
					'prestasi_penilai' => $prestasi_penilai[$i],
					'is_published' => 1
				);

				$clause = array('id' => $kpi[$i]);		
				$this->model->update('km',$data, $clause);

				$clauseKmUnit = array('periode' => $periode, 'kpi_id' => $real_kpi_id[$i]);
				$this->model->update('km_unit', $data, $clauseKmUnit);
			}

			echo json_encode(array('status' => 'ok'));
		} else {
			echo json_encode(array('status' => 'not ok', 'msg' => 'Sudah difinalisasi!'));
		}

	}
	
	public function check_finalisasi(){
		$user_id	= $this->session->userdata('user_id');
		$periode 	= $this->input->post('periode');
		$data 		= $this->model->select('finalisasi', array('periode'=>$periode));
		//$data 	= $this->model->select('finalisasi',array('periode'=>$periode,'user_id'=>$user_id));
		if(count($data) > 0){	
			echo json_encode(array('status' => 'ok'));
		} else {
			echo json_encode(array('status' => 'no'));
		}
	
	}
}



