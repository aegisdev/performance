<?php

class Mailman {

	public function send_email($to, $subject, $message, $CI){
		$config = Array(
	    'protocol' => 'smtp',
	    'smtp_host' => 'ssl://smtp.googlemail.com',
	    'smtp_port' => 465,
	    'smtp_user' => 'support@aegis.co.id',
	    'smtp_pass' => 'aegis10201',
	    'mailtype'  => 'html', 
	    'charset'   => 'iso-8859-1'
		);

		$CI->load->library('email', $config);
		$CI->email->set_newline("\r\n");
		$CI->email->from('support@aegis.co.id', 'Support Performance Dashboard');

		if($to){
			foreach($to as $row){
				$CI->email->to($row->email); 
			}
			//$CI->email->cc('irfan@aegis.co.id'); 
			$CI->email->subject($subject);
			$CI->email->message($message);	
			$CI->email->send();
			return $CI->email->print_debugger();
		} else {
			return 'no mail receiver registered';
		}
	}

	public function get_destination_email($isAuditor, $CI, $organization_id = 0){
		$email = null;
		if($isAuditor){
			$email = $CI->db->query("SELECT email FROM users WHERE user_group_id = 4");
		} else {
			$email = $CI->db->query("SELECT email FROM users WHERE organization_id = '" .$organization_id. "'");
		}

		if ($email->num_rows() > 0){
	   	return $email->result();
		} else {
			return null;
		}
	}
}