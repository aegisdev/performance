<!DOCTYPE html>
<html lang="en"> 
<head>
  <meta charset="utf-8" />
	<title>Performance - Login</title>  
	<meta content="" name="description" />
	<meta content="" name="abakhrun@aegis.co.id" />
	<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/css/metro.css" rel="stylesheet" />	
	<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" />
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/img/logo.png" width="50px" height="50px" />  
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login blue">
<br/>
  <!-- BEGIN LOGO --> 
<center><h1><b>ENTERPRISE PERFORMANCE DASHBOARD</b></h1><center>
<br/>
  <!-- END LOGO -->
  <!-- BEGIN LOGIN -->
  <div class="content">
    <center>
		<img src="<?php echo base_url();?>assets/img/logo.jpg" width="100px" height="100px" alt="" /> 
	</center>
	<br/>
    <form name="flogin" id="flogin" class="form-vertical login-form">		
      <div class="alert alert-error hide">
        <button class="close" data-dismiss="alert"></button>
        <span>Username/password salah</span>
      </div>
      <div class="control-group">        
        <label class="control-label visible-ie8 visible-ie9">Username</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-user"></i>
            <input class="m-wrap placeholder-no-fix" type="text" placeholder="Username" name="username"/>
          </div>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-lock"></i>
            <input class="m-wrap placeholder-no-fix" type="password" placeholder="Password" name="password"/>
          </div>
        </div>
      </div>
      <div style="margin-bottom:10px;text-align:left;">
        <a href="#" onclick="lupaPassword();">Lupa Password?</a>
      </div>
      <!--<div class="form-actions">-->
        <button type="button" class="btn blue btn-block" onClick="login()"><i class="icon-lock"></i> Login</button>         
        <!-- <button type="button" class="btn red btn-block"><i class="icon-key"></i> Lupa Password</button> -->          
		<br/>
    <center>© 2015</center><center>PT. Bina Performa Ekselen</center>
	 </div>
    <!--</form> -->
    <!-- END LOGIN FORM -->  
  </div>  
  
  <center><img src="<?php echo base_url();?>assets/img/foto airnav.png" style="margin-top:-45px"/><center>
  <!-- END LOGIN -->
  <!-- BEGIN COPYRIGHT -->
 <!-- <div class="copyright"> <?php //echo $footer;?> </div> -->
  <!-- END COPYRIGHT -->
  <!-- BEGIN JAVASCRIPTS -->
  <script src="<?php echo base_url();?>assets/js/jquery-1.8.3.min.js"></script>
  <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>  
  <script src="<?php echo base_url();?>assets/uniform/jquery.uniform.min.js"></script> 
  <script src="<?php echo base_url();?>assets/js/jquery.blockui.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/app.js"></script>
  <script>
    jQuery(document).ready(function() {     
		App.initLogin();
    });
	
	function login() {		
		var str = $("#flogin").serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>login/process",
			data: str,
			success: function(msg){
				if(msg=="no"){
					$(".alert").show(); 						
				}else if(msg=="1") {	
					window.open('<?php echo base_url();?>home','_self');
				}
				else if(msg=="2") {						
					window.open('<?php echo base_url();?>main','_self');
				}
			},
			error: function(fnc,msg){
				alert("Tidak dapat terhubung ke server untuk malakukan proses login");
			}
		});		
	};	

  function lupaPassword(){
    alert('Silahkan hubungi admin via email it@airnavindonesia.co.id');
  }
  </script> 
  <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>