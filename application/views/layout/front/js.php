<script src="<?php echo base_url();?>assets/js/jquery-1.8.3.min.js"></script>	
<script src="<?php echo base_url();?>assets/breakpoints/breakpoints.js"></script>		
<script src="<?php echo base_url();?>assets/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>	
<script src="<?php echo base_url();?>assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url();?>assets/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-modal.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-transition.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.blockui.js"></script>	
<script src="<?php echo base_url();?>assets/js/jquery.cookie.js"></script>
<script src="<?php echo base_url();?>assets/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>	
<script src="<?php echo base_url();?>assets/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>	
<script src="<?php echo base_url();?>assets/flot/jquery.flot.js"></script>
<script src="<?php echo base_url();?>assets/flot/jquery.flot.pie.js"></script>
<script src="<?php echo base_url();?>assets/flot/jquery.flot.resize.js"></script>
<script src="<?php echo base_url();?>assets/flot/jquery.flot.orderBars.js"></script>
<script src="<?php echo base_url();?>assets/flot/jquery.flot.axislabels.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/gritter/js/jquery.gritter.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/uniform/jquery.uniform.min.js"></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.pulsate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-daterangepicker/date.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>	
<script src="<?php echo base_url();?>assets/js/app.js"></script>				
<script src="<?php echo base_url();?>assets/js/messi.js"></script>	
<script src="<?php echo base_url();?>assets/data-tables/jquery.dataTables.js"></script>	
<script src="<?php echo base_url();?>assets/data-tables/DT_bootstrap.js"></script>	
<script src="<?php echo base_url();?>assets/js/TableTools.js"></script>	
<script src="<?php echo base_url();?>assets/js/ZeroClipboard.js"></script>	
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.easyModal.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap-tree/bootstrap-tree/js/bootstrap-tree.js"></script>
<script src="<?php echo base_url();?>assets/js/tree.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.orgchart.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.treetable.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/data-tables/ColVis.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.growl.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/highcharts.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/highcharts-more.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/FixedColumns.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-fileupload.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery.form.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/upload/js/jquery.fileupload.js"></script>
<script>
	jQuery(document).ready(function() {		
		App.setPage("index");  // set current page
		App.init(); // init the rest of plugins and elements
	});
</script>