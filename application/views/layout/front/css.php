<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/metro.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />	
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/dt.css"/>  		
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/TableTools.css"/>  	
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/style_responsive.css" rel="stylesheet" />
<link href="<?php echo base_url();?>assets/css/style_light.css" rel="stylesheet" id="style_color" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/gritter/css/jquery.gritter.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/uniform/css/uniform.default.css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap-daterangepicker/daterangepicker.css" />
<link href="<?php echo base_url();?>assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/jqvmap/jqvmap/jqvmap.css" media="screen" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/css/messi.css" media="screen" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url();?>assets/data-tables/DT_bootstrap.css" media="screen" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap-tree/bootstrap-tree/css/bootstrap-tree.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.orgchart.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.treetable.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.treetable.theme.default.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/data-tables/ColVis.css" />
<link rel="stylesheet" type="text/css" href="<?php base_url();?>assets/css/jquery.growl.css" />
<link rel="shortcut icon" href="favicon.ico" />