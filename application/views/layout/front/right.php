<style type="text/css">
	.portlet-title{
		text-overflow:ellipsis;
		overflow: hidden;
		white-space: nowrap;
	}
	.table thead tr th{
		font-size:inherit;
		font-weight:inherit;
	}
</style>
<!-- BEGIN PAGE HEADER-->
<div class="container-fluid" id="content-right">
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN STYLE CUSTOMIZER 
			<div class="color-panel hidden-phone">
				<div class="color-mode-icons icon-color"></div>
				<div class="color-mode-icons icon-color-close"></div>
				<div class="color-mode">
					<p>THEME COLOR</p>
					<ul class="inline">
						<li class="color-black current color-default" data-style="default"></li>
						<li class="color-blue" data-style="blue"></li>
						<li class="color-brown" data-style="brown"></li>
						<li class="color-purple" data-style="purple"></li>
						<li class="color-white color-light" data-style="light"></li>
					</ul>
					<label class="hidden-phone">
					<input type="checkbox" class="header" checked value="" />
					<span class="color-mode-label">Fixed Header</span>
					</label>							
				</div>
			</div>
			END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Dashboard				
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Dashboard</a></li>
				<li class="pull-right no-text-shadow">
					<div data-placement="top" data-desktop="tooltips" data-tablet="" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive"  style="display: block;">
						<i class="icon-calendar"></i>
						<span>
						<?php 
							$date = getDate();
							echo $date['weekday'] .", ". $date['mday'] . " / " . $date['month'] . " / " . $date['year'];
						?>
						</span>
					</div>
				</li>
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<!-- END PAGE HEADER-->
	<div id="dashboard">
	<!-- BEGIN DASHBOARD STATS -->
		<div style="margin-bottom:25px">
			<table>
				<tr>
					<td width="200px"><strong>Pilih Tahun Periode</strong></td>
					<td>
						<select id="select-periode-dashboard" width="300px">
							<?php 
								$yyy= date('Y')-2;
								$selected = ($date['year']==date('Y'))? 'selected' : '';
								for($yyy;$yyy<= $date['year'];$yyy++){
									echo "<option value=".$yyy." ".$selected.">".$yyy."</option>";
									//echo "<option value=".$yyy." >".$yyy."</option>";
								}
							?>
						</select>
					</td>
				</tr>
			</table>
		</div>
		<div class="row-fluid">
			<div class="span12 responsive">
				<div class="portlet box green">
					<div class="portlet-title">
						<h4><i class="icon-bar-chart"></i> Tema Strategis</h4>
					</div>
					<div class="portlet-body">
						<div>
							<ul>
							<?php
							for($i=0;$i<count($strategic);$i++){
								echo '<li>'. $strategic[$i]->name  .'</li>'; 
							}
							?>
							</ul>
						</div>
					</div>
				</div>
			</div>	
		</div>
		<div class="row-fluid">
			<div class="span4 responsive" data-tablet="span4" data-desktop="span4">
				<div class="portlet box brown">
					<div class="portlet-title" id="title-gauge-1"></div>
					<div class="portlet-body">
						<div id="gauge-1" style="height:200px"></div>
					</div>
				</div>
			</div>
			<div class="span4 responsive" data-tablet="span4" data-desktop="span4">
				<div class="portlet box green">
					<div class="portlet-title" id="title-gauge-2"></div>
					<div class="portlet-body">
						<div id="gauge-2" style="height:200px"></div>
					</div>
				</div>
			</div>
			<div class="span4 responsive" data-tablet="span4" data-desktop="span4">
				<div class="portlet box purple">
					<div class="portlet-title" id="title-gauge-3"></div>
					<div class="portlet-body">
						<div id="gauge-3" style="height:200px"></div>
					</div>
				</div>
			</div>
			
		</div>
		<div class="row-fluid">
			<div class="span4 responsive" data-tablet="span4" data-desktop="span4">
				<div class="portlet box yellow">
					<div class="portlet-title" id="title-gauge-4"></div>
					<div class="portlet-body">
						<div id="gauge-4" style="height:200px"></div>
					</div>
				</div>
			</div>
			<div class="span4 responsive" data-tablet="span4" data-desktop="span4">
				<div class="portlet box red">
					<div class="portlet-title" id="title-gauge-5"></div>
					<div class="portlet-body">
						<div id="gauge-5" style="height:200px"></div>
					</div>
				</div>
			</div>
			<div class="span4 responsive" data-tablet="span4" data-desktop="span4">
				<div class="portlet box blue">
					<div class="portlet-title" id="title-gauge-6">Perusahaan</div>
					<div class="portlet-body">
						<div id="gauge-6" style="height:200px"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row-fluid" style="display:none;">
			<div class="span6 responsive">
				<div class="portlet box green">
					<div class="portlet-title">
						<h4><i class="icon-bar-chart"></i> Grafik Realisasi Per-KPI</h4>
					</div>
					<div class="portlet-body">
						<div>
							<table border="0">
								<tr>
									<td width="100px">Pilih KPI</td>
									<td>
										<select name="year" id="pilih-tampilan-kpi">
											<?php for($i=0;$i<count($dataKpi);$i++) { ?>
												<option value="<?php echo $dataKpi[$i]->id; ?>"><?php echo $dataKpi[$i]->kpi;?></option>
											<?php } ?>
										</select>
									</td>
								</tr>
							</table>
						</div>
						<div class="clearfix"></div>
						<div id="chart-pencapaian-kpi" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
					</div>
				</div>
			</div>	
			<div class="span6 responsive">
				<div class="portlet box green">
					<div class="portlet-title">
						<h4><i class="icon-bar-chart"></i> Grafik Prestasi Perusahaan</h4>
					</div>
					<div class="portlet-body">
						<div style="height:42px">
							<div class="clearfix"></div>
						</div>
						<div id="prestasi-perusahaan-bar" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
					</div>
				</div>
			</div>
		</div>		
		<div class="row-fluid">
			<div class="span12 responsive">
				<div class="portlet box green">
					<div class="portlet-title"><h4><i class="icon-signal"></i> Trend Pelaksanaan RKM</h4></div>
					<div class="portlet-body">
						<div id="chart-history-rkm" style="height:350px;"></div>
<!-- 						<center><strong>Bulan</strong></center>
 -->					</div>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span6 responsive" data-table="span6" data-desktop="span6">
				<div class="portlet box green">
					<div class="portlet-title"><h4><i class="icon-bar-chart"></i> Rekap Pelaksanaan RKM</h4></div>
					<div class="portlet-body">
						<div id="pie-rekap-rkm" style="height:350px;"></div>
					</div>
				</div>
			</div>
			<div class="span6 responsive">
				<div class="portlet box green">
					<div class="portlet-title"><h4><i class="icon-bar-chart"></i> Grafik Pelaksanaan RKM</h4></div>
					<div class="portlet-body">
						<div id="rkm-stacked" style="min-width: 310px; height: 350px; margin: 0 auto"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="row-fluid" style="display:none;">
			<div class="span12 responsive">
				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>Kontribusi Direktorat
						</h4>												
					</div>
					<div class="portlet-body">
						<div>					
						<table class="table table-bordered table-striped table-hover" id="table_kontribusi_direktorat" style="font-size:9px;">
							<thead>
								<tr>
									<th rowspan="2" width="100px">KPI</th>						
									<th colspan="3"><center>DM</center></th>	
									<th colspan="3"><center>DT</center></th>
									<th colspan="3"><center>DS</center></th>
									<th colspan="3"><center>DI</center></th>
									<th colspan="3"><center>DK</center></th>
									<th colspan="3"><center>DP</center></th>
									<th colspan="3"><center>DU</center></th>
									<th colspan="3"><center>PER.</center></th>
								</tr>
								<tr>
									<th width="20px">B (%)</th>								
									<th width="20px">R</th>								
									<th width="20px">K (%)</th>
									<th width="20px">B (%)</th>
									<th width="20px">R</th>
									<th width="20px">K (%)</th>
									<th width="20px">B (%)</th>
									<th width="20px">R</th>
									<th width="20px">K (%)</th>
									<th width="20px">B (%)</th>
									<th width="20px">R</th>
									<th width="20px">K (%)</th>
									<th width="20px">B (%)</th>
									<th width="20px">R</th>
									<th width="20px">K (%)</th>	
									<th width="20px">B (%)</th>
									<th width="20px">R</th>
									<th width="20px">K (%)</th>
									<th width="20px">B (%)</th>
									<th width="20px">R</th>
									<th width="20px">K (%)</th>	
									<th width="20px">B (%)</th>		
									<th width="20px">R</th>
									<th width="20px">P (%)</th>		
								</tr>
							</thead>
							<tbody></tbody>
						</table>
						<div class="clearfix"></div>
						<div>
							Ket : <br/>
							B = Bobot <br/>
							R = Realisasi <br/>
							K = Kontribusi <br/>
							P = Prestasi <br/>
							PER. = Perusahaan <br/>
						</div>
						</div>
					</div>
				</div>	
		</div>
	</div>
	<div class="clearfix"></div>
	<div class="row-fluid">
		<div class="span12 responsive">
			<a href="#" id="btn-show-calendar">Perlihatkan Kalender</a>
		</div>
	</div>
	<div class="row-fluid" id="calendar-display">
		<div class="span12 responsive">
			<div class="portlet box green calendar">
				<div class="portlet-title">
					<h4><i class="icon-calendar"></i>Calendar History</h4>												
				</div>
				<div class="portlet-body light-grey">	
					<div id="calendar-history" class="has-toolbar"></div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="modal-detail-gauge" style="width:850px!important;margin-left:-425px!important;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" id="title-detail-gauge"></h4>
      </div>
      <div class="modal-body" id="body-detail-gauge">
		<table class="table table-bordered table-striped table-hover" style="font-size:11px;" id="tbl-selected-kpi">
			<thead>
				<tr>
					<td rowspan="2">NO</td>
					<td rowspan="2">RESPONSIBILITY</td>
					<td rowspan="2">SATUAN</td>
					<td rowspan="2">TARGET</td>
					<td rowspan="2">BOBOT</td>
					<td colspan="3" class="center-column">UNIT</td>
					<td colspan="3" class="center-column">PENILAI</td>
					<td rowspan="2">KET.</td>
				</tr>
				<tr>
					<td>REALISASI</td>
					<td>PENCAPAIAN</td>
					<td>PRESTASI</td>
					<td>REALISASI</td>
					<td>PENCAPAIAN</td>
					<td>PRESTASI</td>
				</tr>								  
			</thead>
			<tbody>								
			</tbody>
		</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn blue" data-dismiss="modal"></i> Ok</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal hide" id="modal-detail-gauge-kpi" style="width:850px!important;margin-left:-425px!important;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Informasi Pelaksanaan RKM</h4>
      </div>
      <div class="modal-body">
				<table class="table table-bordered table-striped table-hover" id="table-kontribusi-direktorat-detail">
					<thead>
						<tr style="font-weight:bold;">
							<td width="350px" style="text-align:center;">Nama Program Kerja</td>
							<td style="text-align:center;">Unit</td>
							<td style="text-align:center;">Status</td>
							<td style="text-align:center;">Keterangan</td>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn blue" data-dismiss="modal"></i> Ok</button>
        <button type="button" class="btn blue btn-detail-gauge" data-dismiss="modal" id="btn-back-perspektif"></i> Kembali</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal hide" id="modal-detail-pelaksanaan-rkm" style="width:850px!important;margin-left:-425px!important;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" id="title-detail-pelaksanaan-rkm"></h4>
      </div>
      <div class="modal-body" id="body-detail-gauge">
		<table class="table table-bordered table-striped table-hover" id="tabel_program_kerja">
			<thead>
				<tr>
					<th width="5px">NO</th>
					<th>PROGRAM KERJA</th>													
					<th>NOT STARTED</th>													
					<th>IN PROGRESS</th>													
					<th>DONE</th>
					<th>STATUS VERIFIKASI</th>													
					<th>KET.</th>													
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn blue" data-dismiss="modal"></i> Ok</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal hide" id="modal-detail-history-rkm" style="width:850px!important;margin-left:-425px!important;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title" id="title-detail-history-rkm"></h4>
      </div>
      <div class="modal-body" id="body-detail-gauge">
		<table class="table table-bordered table-striped table-hover" id="table_detail_history_rkm">
			<thead>
				<tr>
					<th width="5px">NO</th>
					<th>NAMA UNIT</th>													
					<th>PROGRAM KERJA</th>													
					<th>NOT STARTED</th>													
					<th>IN PROGRESS</th>													
					<th>DONE</th>
<!-- 					<th>STATUS VERIFIKASI</th>													
					<th>KET.</th>			 -->										
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn blue" data-dismiss="modal"></i> Ok</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


