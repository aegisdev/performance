	<ul class="menu">
		<li class="active"><a href="<?php echo base_url();?>home"><span>Home</span></a></li>
		<li>
			<a href="#" class="parent"><span>Parameters</span></a>
			<div>
				<ul>
					<li><a href="#" onClick="javascript:module('parameters/document_types','Document Types');"><span>Document Types</span></a></li>
					<li><a href="#" onClick="javascript:module('parameters/usage_recomendation','Usage Recomendation');"><span>Usage Recomendation</span></a></li>
					<li><a href="#" class="parent"><span>Document Categories</span></a>				
						<div>
							<ul>
								<li><a href="#" onClick="javascript:module('parameters/categories','Categories');"><span>Categories</span></a></li>
								<li><a href="#" onClick="javascript:module('parameters/sub_categories','Sub Categories');"><span>Sub Categories</span></a></li>
							</ul>
						</div>
					</li>
					<li><a href="#" onClick="javascript:module('parameters/experts','Experts');"><span>Experts</span></a></li>				
					<li><a href="#" onClick="javascript:module('parameters/size_upload','Size Upload');"><span>Size Upload</span></a></li>
					
				</ul>
			</div>
		</li>
		<li>
			<a href="#" class="parent"><span>Content</span></a>
			<div>
				<ul>
					<li><a href="#" onClick="javascript:module('contents/bod','BOD Message');"><span>BOD Message</span></a></li>
					<li><a href="#" onClick="javascript:module('contents/video','Video');"><span>Video</span></a></li>
					<li><a href="#" onClick="javascript:module('contents/running_text','Running Text');"><span>Running Text</span></a></li>
					<li><a href="#" onClick="javascript:module('contents/disclaime','Syarat dan Ketentuan');"><span>Syarat dan Ketentuan</span></a></li>
				</ul>
			</div>
		</li>
		<li>
			<a href="#" class="parent"><span>Tools</span></a>
			<div>
				<ul>
					<!--<li><a href="#" onClick="javascript:module('tools/reset_document_counter','Reset Documents Counter');"><span>Reset Documents Counter</span></a></li>
					<li><a href="#" onClick="javascript:module('tools/reset_subscription','Reset Subscription');"><span>Reset Subscription</span></a></li>-->
					<li><a href="#" onClick="javascript:module('tools/dss','Document Submission Schedule');"><span>Document Submission Schedule</span></a></li>
					<li><a href="#" onClick="javascript:module('tools/cbhrm','CBHRM Document Submission Schedule');"><span>CBHRM Document Submission Schedule</span></a></li>
				</ul>
			</div>
		</li>
		<li>
			<a href="#" class="parent"><span>Document Control and Monitoring</span></a>
			<div>
				<ul>
					<li><a href="#" onClick="javascript:module('dcm/documents','Document List');"><span>Document List</span></a></li>
					<li><a href="#" onClick="javascript:module('dcm/workflow','Monitoring Workflow');"><span>Monitoring Workflow</span></a></li>
					<li><a href="#" onClick="javascript:module('dcm/escalation','Escalation');"><span>Escalation</span></a></li>
				</ul>
			</div>
		</li>	
		<li>
			<a href="#" class="parent"><span>Users</span></a>
			<div>
				<ul>
					<li><a href="#" onClick="javascript:module('accounts/employees','Employee Data Management');"><span>Employee Data Management</span></a></li>
					<!--<li><a href="#" onClick="javascript:module('accounts/users','Users Management');"><span>Users Management</span></a></li>-->
					<li><a href="#" onClick="javascript:module('accounts/admins','User Admin Management');"><span>User Admin Management</span></a></li>
					<!-- <li><a href="#" onClick="javascript:module('accounts/cop','User COP Management');"><span>User COP Management</span></a></li> -->
				</ul>
			</div>
		</li>					
		<li>
			<a href="#" class="parent"><span>Help</span></a>
			<div>
				<ul>
					<li><a href="#" onClick="javascript:module('helps/faq','FAQ Content Management');"><span>FAQ Content Management</span></a></li>
					<li><a href="#" onClick="javascript:module('helps/help','HELP Content Management');"><span>HELP Content Management</span></a></li>
				</ul>
			</div>
		</li>
	</ul>
