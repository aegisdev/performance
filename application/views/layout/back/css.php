	<meta charset="utf-8">
	<title>.: SIPAKAR :.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="abakhrun@aegis.co.id">
    <meta name="author" content="Akhmad Bakhrun">
    <link rel="stylesheet" type="text/css"href="<?php echo base_url();?>assets/css/jquery-ui.min.css">
		<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">	
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style_cp.css"/>  
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/messi.css"/>  		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/promptumenu/promptumenu.css"/>  		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/TableTools.css"/>  		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/dt.css" rel="stylesheet">	
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap-combobox.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/ticker-style.css" />  
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/menu.css" />  
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/datepicker.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.uix.multiselect.css" />

		<link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.ico">	
   