<!DOCTYPE html>
<html>
  <head>
    <meta content="text/html; charset=utf-8" http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ASSET TRACT.</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/messi.css"/>  
    <script src="<?php echo base_url();?>assets/js/jquery-1.7.2.min.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/js/messi.js"></script>	
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/img/favicon.ico">
   <script type="text/javascript">
		
		function validateLogin() {
			if (document.forms[0].username.value=='') {
				alert("Username harus diisi");			
				return false;
			}		
			if (document.forms[0].password.value==0) {
				alert("Password harus diisi");	
				
				return false;
			}
			return true;
			
		};
		
		//submit form
		function login() {				
			if (validateLogin()==false) {
				return false;
			} else {
				window.open('<?php echo base_url();?>home','_self');
				/*var str = $("#flogin").serialize();
				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>login/process",
					data: str,
					success: function(msg){
						if(msg=="unreg") {
							$("#alert_login").html("<p class='alert'>Data pengguna tidak terdaftar</p>");
							
						}else if(msg=="nopass") {							
							$("#alert_login").html("<p class='alert'>Password tidak sesuai !</p>");							
						} else if(msg=="ok") {						
							window.open('<?php echo base_url();?>home','_self');
						}
					},
					error: function(fnc,msg){
						new Messi('Tidak dapat terhubung ke server untuk malakukan proses login !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ $('#btn_submit').removeAttr("disabled", "").html("Login"); }});
					}
				});*/
			}
		};	
	
		function enter(evt) 
		{   
			if ((evt.keyCode == 13))  
			{
				login();
				return false;
			}
		}
		document.onkeypress = enter;
	</script>
  </head>
  <body>
    <div class="container">
	<form name="flogin" id="flogin" class="form-signin">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input name ="username" id="username" type="text" class="input-block-level" placeholder="Username">
        <input name="password" id="password" type="password" class="input-block-level" placeholder="Password">
        <button class="btn btn-large btn-primary" type="button" onClick="login()">Login</button>
      </form>

    </div> <!-- /container -->
   
  </body>
</html>
