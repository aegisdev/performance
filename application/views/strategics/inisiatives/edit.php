<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">Managemen Inisiative</h3>	
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li>			
					<a href="#" onClick="javascript:routes('strategics/inisiatives','Managemen Inisiative')">Strategik Inisiative</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li>			
					<a href="#">Edit</a> 
				</li>		
			</ul>
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-edit"></i>
						<span class="hidden-480">Edit Inisiative</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">
									<div class="control-group">
										  <label class="control-label">Inisiatif</label>
										  <div class="controls">
											 <input name="name" id="name" type="text" value="<?php echo $data[0]->inisiative_name;?>" class="span6 m-wrap" />
										  </div>
									 </div>				 
									<div class="control-group">
										  <label class="control-label">Tema</label>
										  <div class="controls">
											  <select name="theme_id" id="theme_id" class="span6">
												<option value="">.: Pilih Tema :.</option>
												<?php foreach($data2 as $row) { 
													$selected=($data[0]->theme_id==$row->id) ? "selected" : " ";
												?>										
													<option value="<?php echo $row->id;?>" <?php echo $selected;?>><?php echo $row->name;?></option>
												<?php } ?>												
											  </select>
										  </div>
									 </div>			  
									 <div class="control-group">
										<div class="controls">		
											<input name="id" id="id" type="hidden" value="<?php echo $data[0]->inisiative_id;?>" class="span5 m-wrap" />											
											<button class="btn blue" type="button" onClick="save()"><i class="icon-save"></i> Simpan</button>
											<button class="btn blue" type="button" onClick="routes('strategics/inisiatives','Managemen Inisiative')"><i class="icon-share"></i> Kembali</button>											
										</div>
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		

<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>
<script language="javascript">
	$(document).ready(function() { $("#theme_id").select2(); });	
	function validate() {
		if ($('#name').val() =='') {
			new Messi('Inisiative masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("name").focus(); }});
			return false;
		}	
		if ($('#theme_id').val() =='') {
			new Messi('Theme masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("theme_id").focus(); }});
			return false;
		}	
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>strategics/inisiative_update",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil disimpan !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('strategics/inisiatives','Managemen Inisiatif'); }});
					}else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
	$("#btn_view").click(function(){
			$("#view_data").modal({show:true,backdrop:false});
		});	
</script>