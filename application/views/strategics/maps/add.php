<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">Managemen Theme</h3>	
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li>			
					<a href="#" onClick="javascript:routes('strategics/themes','Managemen Theme')">Strategik Theme</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li>			
					<a href="#">Tambah</a> 					
				</li>		
			</ul>
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-plus"></i>
						<span class="hidden-480">Tambah Theme</span>
					</h4>
					
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">
									<div class="control-group">
										  <label class="control-label">Theme</label>
										  <div class="controls">
											 <input name="name" id="name" type="text" value="" class="span6 m-wrap" />
										  </div>
									 </div>				 
									<div class="control-group">
										  <label class="control-label">Perspective</label>
										  <div class="controls">	
											<select name="perspective_id" id="perspective_id" class="span6">
												<option value="">.: Pilih Perspektive :.</option>
												<?php foreach($data as $row) { ?>
													<option value="<?php echo $row->id;?>"><?php echo $row->name;?></option>
												<?php } ?>												
											</select>
										  </div>
									 </div>			  
									 <div class="control-group">
										<div class="controls">		
											<!--<input name="perspective_id" id="perspective_id" type="hidden" value="" class="span5 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="Name" />-->
											<button class="btn blue" type="button" onClick="save()"><i class="icon-save"></i> Simpan</button>
											<button class="btn blue" type="button" onClick="routes('strategics/themes','Managemen Theme')"><i class="icon-share"></i> Kembali</button>											
										</div>
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>
<script language="javascript">
	$(document).ready(function() { $("#perspective_id").select2(); });
	function validate() {
		if ($('#name').val() =='') {
			new Messi('Theme masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("name").focus(); }});
			return false;
		}	
		if ($('#perspective_id').val() =='') {
			new Messi('Perspective masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("perspective_id").focus(); }});
			return false;
		}	
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>strategics/theme_save",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil disimpan !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('strategics/themes','Managemen Theme'); }});
					}else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
</script>