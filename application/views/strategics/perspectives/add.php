<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">	Perspective Management</h3>	
			<ul class="breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="<?php echo base_url();?>home">Beranda</a> 
						<i class="icon-angle-right"></i>
					</li>
					<li>			
						<a href="#">Strategic References</a> 
						<i class="icon-angle-right"></i>
					</li>	
					<li>			
						<a href="#" onClick="javascript:routes('strategics/perspectives','Strategic Perspective')">Strategic Perspective</a> 
						<i class="icon-angle-right"></i>
					</li>	
					<li>			
						<a href="#">Add</a>		
					</li>		
				</ul>
			<div class="portlet box red tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-reorder"></i>
						<span class="hidden-480">Add Perspective</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">
									<div class="control-group">
										  <label class="control-label">Name</label>
										  <div class="controls">
											 <input name="name" id="name" type="text" value="" class="span6 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="Name" />
										  </div>
									 </div>				 
												  
									 <div class="control-group">
										<div class="controls">							
											<button class="btn red" type="button" onClick="save()">Save</button>
											<button class="btn blue" type="button" onClick="routes('strategics/perspectives','List Perspective Management')">Back</button>											
										</div>
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
<script language="javascript">
	function validate() {
		if ($('#name').val() =='') {
			new Messi('Description masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("name").focus(); }});
			return false;
		}	
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>strategics/perspective_save",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil disimpan !', {title: 'Menu Message', titleClass: 'anim info', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('strategics/perspectives','Perspective Management'); }});
					}else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Menu Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
</script>
<script src="http://localhost/performance/assets/js/jquery-1.8.3.min.js"></script>	