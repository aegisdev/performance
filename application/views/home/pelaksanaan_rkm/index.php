	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Realisasi RKM	oleh Unit	
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('penyusunan_rkm/index','Pelaksanaan RKM')">Realisasi RKM oleh Unit</a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>Realisasi RKM oleh Unit
						</h4>
					</div>
					<div class="portlet-body">						
						<table border="0">
							<tr>
								<td width="100px">Unit</td>
								<td colspan="2" width="90%">
									<select name="organization_id" id="organization_id" class="span6 m-wrap">										
										<option value="-1">-- Pilih Unit --</value>
										<?php foreach($data as $row){ ?>
										<option value="<?php echo $row->id;?>"><?php echo $row->name;?></value>
										<?php } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>Periode</td>
								<td>
									<select name="year" id="year" class="span2 m-wrap">
										<option value="-1">-- Pilih Tahun --</value>
										<?php for($year=2013;$year <=(date('Y')+1);$year++){ ?>
										<option value="<?php echo $year;?>"><?php echo $year;?></option>
										<?php } ?>
									</select>
								</td>
								<td>
								</td>
							</tr>
						</table>
						<br/>
						<div class="clearfix">						
							<div class="pull-left">
								<p>
									<button class="btn blue" type="button" id="btn_save"><i class="icon-save"></i> Simpan</button>
								</p> 
							</div>
						</div>
						<form id="form_update_program_kerja">
						<input type="hidden" id="choose_unit" name="choose_unit" />
						<input type="hidden" id="choose_periode" name="choose_periode" />
						<table class="table table-bordered table-striped table-hover" id="tabel_program_kerja">
							<thead>
								<tr>
									<th width="5px">NO</th>
									<th>PROGRAM KERJA</th>													
									<th width="50px">NOT STARTED</th>													
									<th width="50px">ON PROGRESS</th>
									<!-- <th width="100px"></th>	 -->		
									<th width="50px">DONE</th>		
									<th width="50px">PERSENTASE</th>															
									<th width="300px">KETERANGAN</th>													
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						</form>
						<center>
						<button class="btn blue" id="btn-save-realization-unit" style="display:none"><i class="icon-save"></i> Simpan</button>
						</center>
						<div class="clearfix"></div> 
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->


<div class="modal" id="modal_tampilkan_keterangan">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Keterangan</h4>
      </div>
      <div class="modal-body keterangan-view">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn blue" data-dismiss="modal" aria-hidden="true" onclick="$('#modal_tampilkan_keterangan').trigger('closeModal');">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script language="javascript">
	$(document).ready(function(){
		$('#modal_tampilkan_keterangan').easyModal();
		$("#organization_id").select2(); 
		$("#year").select2(); 	
		$('#modal_tampilkan_keterangan').trigger('closeModal');
	});

	function tampilkan_keterangan(id){
		$.ajax({
			dataType:"html",  		
			url : '<?php echo base_url();?>pelaksanaan_rkm/get_keterangan/'+id,
			success: function(response){
			  $('.keterangan-view').html(response);

			  $('#modal_tampilkan_keterangan').trigger('openModal');
			}
	 	});

		return false;
	}
	
	$('#tabel_program_kerja').dataTable({		
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ baris per halaman",
			"oPaginate": {
				"sPrevious": "Prev",
				"sNext": "Next"
			}
		},
		"bSort": false,
		"bFilter": false,
		"bInfo": false,
		"bPaginate": false
	});

	(function(){
		function loadRkm(unit, periode){
			$.ajax({
				type: 'post',
				data: {'unit' : unit, 'periode' : periode},
				url : '<?php echo base_url();?>pelaksanaan_rkm/get_progaram_kerja',
				dataType: 'json',
				beforeSend: function(){
					$('#tabel_program_kerja > tbody:last').empty();
					//$('#btn-save-realization-unit').hide();
				}
			})
			.done(function(response, textStatus, jqhr){
				if(response){			
					for(var i=0;i<response.length;i++){		
						var percentage_readonly = 'readonly="readonly"';
						if(response[i].status_tobe_verify==1){
							var status_tobe_verify=1;
							var check_1='checked=checked';
							var check_2='';
							var check_3='';
						} else if(response[i].status_tobe_verify==2) {
							var status_tobe_verify=2;
							var check_1='disabled=disabled';
							var check_2='checked=checked';
							var check_3='';
							percentage_readonly = '';
						} else if(response[i].status_tobe_verify==3){
							var status_tobe_verify=3;
							var check_1='disabled=disabled';
							var check_2='disabled=disabled';
							var check_3='checked=checked';
						} else {
							var status_tobe_verify='';
							var check_1='';
							var check_2='';
							var check_3='';
						}

						if(response[i].keterangan != ""){
							var keterangan=response[i].keterangan;
						} else {
							var keterangan='';
						}

						var inputPersentase = '<input type="text" name="persentase_progress[]" class="span6 input_presentase" value="'+response[i].value_tobe_verify+'" '+percentage_readonly+'/>% <input type="hidden" name="before_presentase[]" value="'+response[i].value_tobe_verify+'" class="before_presentase" /><input type="hidden" name="id_rk[]" value="'+response[i].program_kerja_id+'" />';
						var el = '<tr valign="middle">' + 
							'<td width="30px">'+(i+1)+'<input type="hidden" name="program_kerja_id[]" value="'+response[i].id+'"/></td>' + 
							'<td>'+response[i].program_kerja+'</td>'+ 
							'<td class="center-column" width="100px"><input type="radio" name="progress_id_'+i+'" class="check_1" value="1" '+check_1+' /></td>' + 
							'<td class="center-column"><input type="radio" name="progress_id_'+i+'" class="check_2" value="2" '+check_2+' /></td>'+ 
						  // '<td class="center-column"><div id="bar" style="margin-bottom:0px"; class="progress progress-success progress-striped active"><div class="bar" style="width: '+response[i].value+'%;">'+response[i].value+'%</div></div></td>' + 
						  '<td class="center-column" width="60px"><input type="radio" name="progress_id_'+i+'"class="check_3" value="3" '+check_3+' /></td>' + 
						  '<td class="center-column input_progress">'+inputPersentase+'</td>' +
						  //+ '<td width="20px"><center><button class="btn blue" onclick="tampilkan_keterangan('+response[i].id+')"><i class="icon-eye-open" title="Lihat Keterangan"></i></button></center></td>' + 
						  '<td><textarea name="keterangan_progress[]" rows="10" style="width:280px;">' + ((response[i].keterangan_rencana) ?  response[i].keterangan_rencana : '') + '</textarea></td>'
						  '</tr>';
						$('#tabel_program_kerja > tbody:last').append(el);				
					}
				}
			})
			.fail(function(){
			
			});
		}

		$('#tabel_program_kerja').delegate('.check_1', 'click', function(){
			$(this).parent().siblings('.input_progress').children('.input_presentase').attr('readonly','true');
			$(this).parent().siblings('.input_progress').children('.input_presentase').val(0);
		});

		$('#tabel_program_kerja').delegate('.check_2', 'click', function(){
			$(this).parent().siblings('.input_progress').children('.input_presentase').removeAttr('readonly');
			$(this).parent().siblings('.input_progress').children('.input_presentase').focus();
		});

		$('#tabel_program_kerja').delegate('.check_3', 'click', function(){
			$(this).parent().siblings('.input_progress').children('.input_presentase').attr('readonly','true');
			$(this).parent().siblings('.input_progress').children('.input_presentase').val(100);
		});

		$('#tabel_program_kerja').delegate('.input_presentase','focusout', function(){
			var nilai = $(this).val();
			var nilai_before = $(this).next('.before_presentase').val();
			if(nilai < 1 || nilai > 99){
				alert('Nilai harus lebih besar dari 0 dan lebih kecil dari 100!');
				$(this).val(99);
			}

			if((nilai*1) < (nilai_before*1)){
				alert('Nilai harus lebih besar dari '+ nilai_before+'!');
				$(this).val(nilai_before);
			}

		});
	
		$('#btn_program_kerja').click(function(){
		  var unit = $('#organization_id').val();
		  var periode = $('#year').val();
		  
		  $.ajax({
				url: '<?php echo base_url();?>pelaksanaan_rkm/get_progaram_kerja',
				dataType: 'json',
				data: {'unit' : unit, 'periode': periode},
				type: 'post',
				beforeSend: function(){
					listTabelProgramKerja.fnClearTable();
				}
		  })
		  .done(function(response, textStatus, jqhr){
				if(response){
					$('#choose_program_kerja_unit').val(unit);
					$('#choose_program_kerja_periode').val(periode);
					
					for(var i=0;i<response.length;i++){
						listTabelProgramKerja.fnAddData([response[i].program_kerja, '<input type="checkbox" name="program_kerja_selected[]" value="'+ response[i].id +'" />']);
					}
					$('#modal_program_kerja').trigger('openModal');
				}		
		  })
		  .fail(function(){
		  });
		  
		});
	
		$('#btn_save').click(function(){	
			new Messi('Apakah anda yakin data yang anda masukkan sudah benar dan akan melakukan realisasi?', {
				title: 'Confirmation', 
				titleClass: 'anim info', 
				closeButton: true,
				buttons: [
				{
					id: 0, 
					label: 'YES', 
					val: '1'
				},{
					id: 1, 
					label: 'NO', 
					val: '0'
				}], 
				callback:function(val){ 
					if(val == 0) {
						return false;
					} else if(val == 1) {
						$.ajax({
							type: 'post',
							url: '<?php echo base_url();?>pelaksanaan_rkm/update_rencana_kerja',
							data: $('#form_update_program_kerja').serialize(),
							dataType: 'json'
						})
						.done(function(response, textStatus, jqhr){
							if(response.status == 'ok'){
								$('#modal_porgam_kerja').trigger('closeModal');
								$.growl.notice({ title: "Informasi", message: "Data berhasil disimpan ke database." });
							} else {
								$.growl.error({ title: "Informasi", message: "Terjadi kesalahan, silahkan kontak admin!" });
							}
							var unit=$('#choose_unit').val();
							var periode=$('#choose_periode').val();
							loadRkm(unit, periode);
						})
						.fail(function(){
						})
					} 
				}
			});
		});

		$('#organization_id').change(function(){
			var optionSelected = $(this).find("option:selected");
			$('#choose_unit').val(optionSelected.val());		
			if($('#year').val() !=-1)
				loadRkm(optionSelected.val(), $('#year').val());
		});

		$('#year').change(function(){
			var optionSelected = $(this).find("option:selected");
			$('#choose_periode').val(optionSelected.val());	
			if($('#organization_id').val() !=-1)
				loadRkm($('#organization_id').val(), optionSelected.val());
		});

		$('#tabel_program_kerja').delegate('.click_1', 'click', function(){

		});
	}());

</script>
	
	