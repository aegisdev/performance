	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Penyusunan RKM		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('penyusunan_rkm/index','Penyusunan RKM')">Penyusunan RKM</a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>Penyusunan RKM
						</h4>
					</div>
					<div class="portlet-body">						
						<table border="0">
							<tr>
								<td width="100px">Unit</td>
								<td colspan="2" width="90%">
									<select name="organization_id" id="organization_id" class="span6 m-wrap">										
										<option value="-1">-- Pilih Unit --</value>
										<?php foreach($data as $row){ ?>
										<option value="<?php echo $row->id;?>"><?php echo $row->name;?></value>
										<?php } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>Periode</td>
								<td>
									<select name="year" id="year" class="span2 m-wrap">
										<option value="-1">-- Pilih Tahun --</value>
										<?php for($year=2013;$year <=(date('Y')+1);$year++){ ?>
										<option value="<?php echo $year;?>"><?php echo $year;?></option>
										<?php } ?>
									</select>
									<input type="hidden" name="unit-name" id="forming-kpi-unit-name" />
								</td>
								<td>
								</td>
							</tr>
							
						</table><br/>
						<div class="clearfix">
							<div class="pull-left">								
									<button class="btn blue" type="button" id="btn_program_kerja"><i class="icon-ok"></i> Pilih Program Kerja</button>							
							</div>
							<!--<div class="pull-right">
								<p>
									<button class="btn blue" type="button" id="btn_save">Simpan</button>
								</p> 
							</div>-->
						</div>
						<form id="form-save-kpi">
						<table class="table table-bordered table-striped table-hover" id="tabel_selected_program_kerja">
							<thead>
								<tr>
									<th width="5px">NO</th>
									<th>PROGRAM KERJA</th>											
									<th width="150px" class="center-column">ACTION</th>											
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						</form>
						<center>
						<button class="btn blue" id="btn-save-realization-unit" style="display:none"><i class="icon-save"></i> Simpan</button>
						</center>
						<div class="clearfix"></div> 
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->

<div class="modal" id="modal_program_kerja" style="width:80%">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Program Kerja</h4>
      </div>
      <div class="modal-body">
		<form id="form_choose_porgam_kerja">
		<input type="hidden" id="choose_program_kerja_unit" name="choose_program_kerja_unit" />
		<input type="hidden" id="choose_program_kerja_periode" name="choose_program_kerja_periode" />
		<table id="tabel_list_program_kerja" class="table table-bordered table-striped table-hover" >
			<thead>
				<tr>
					<td class="center-column">PROGRAM KERJA</td>
					<td width="300px" class="center-column">KETERANGAN</td>
					<td width="100px" class="center-column">ACTION</td>
				</tr>
			</thead>
		</table>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn blue" id="btn_save_choose_program_kerja"><i class="icon-save"></i> Simpan</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	

<script language="javascript">
	$(document).ready(function(){
		$("#organization_id").select2(); 
		$("#year").select2(); 	
		$('#modal_program_kerja').easyModal();

		$('#tabel_selected_program_kerja').dataTable({	
			"sPaginationType": "bootstrap",
			"oLanguage": {
				"sLengthMenu": "_MENU_ baris per halaman",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next"
				}
			},
			"bSort": false,
			"bFilter": false,
			"bInfo": false,
			"bPaginate": false
		});
	});

	(function(){
		var listTabelProgramKerja =  $('#tabel_list_program_kerja').dataTable({
		  "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
		  "sPaginationType": "bootstrap",
		  "iDisplayLength": 5,
		  "aLengthMenu": [[5, 10, 25, 50, 100, 200, -1], [5, 10, 25, 50, 100, 200, "Semuanya"]],
		  "oLanguage": {
			  "sLengthMenu": "_MENU_ baris per halaman",
			  "oPaginate": {
				  "sPrevious": "Prev",
				  "sNext": "Next"
			  }
		  },
		  "bSort": false,
		});

		function loadRkm(unit, periode){
			$.ajax({
				type: 'post',
				data: {'unit' : unit, 'periode' : periode},
				url : '<?php echo base_url();?>penyusunan_rkm/get_progaram_kerja',
				dataType: 'json',
				beforeSend: function(){
					$('#tabel_selected_program_kerja > tbody:last').empty();
				}
			})
			.done(function(response, textStatus, jqhr){
				if(response){			
					for(var i=0;i<response.length;i++){
						
						var el = '<tr>' + 
							'<td width="30px">'+(i+1)+'</td>' + 
							'<td>'+response[i].program_kerja+'</td>' + 
							'<td class="center-column"><button class="btn delete_data blue" data-id="'+response[i].id+'" data-unit="'+unit+'" data-periode="'+periode+'" data-rencana-kerja-id="'+response[i].program_kerja_id+'" data-label="'+response[i].program_kerja+'"><i class="icon-trash" title="Hapus data"></i></button></td>' + 
							'</tr>'
						$('#tabel_selected_program_kerja > tbody:last').append(el);				
					}
				}
			})
			.fail(function(){
			
			});
		}

		$('#btn_program_kerja').click(function(){
		  var unit = $('#organization_id').val();
		  var periode = $('#year').val();
		  if(unit =='-1' && periode =='-1'){
				alert("Unit dan periode harus dipilih");
				return false;
		  }

		  $.ajax({
				url: '<?php echo base_url();?>penyusunan_rkm/view_progaram_kerja',
				dataType: 'json',
				data: {'unit' : unit, 'periode': periode},
				type: 'post',
				beforeSend: function(){
					listTabelProgramKerja.fnClearTable();
				}
		  })
		  .done(function(response, textStatus, jqhr){
				if(response){
					$('#choose_program_kerja_unit').val(unit);
					$('#choose_program_kerja_periode').val(periode);
					
					for(var i=0;i<response.length;i++){
						listTabelProgramKerja.fnAddData([response[i].program_kerja, response[i].keterangan,'<center><input type="checkbox" name="program_kerja_selected[]" value="'+ response[i].id +'" /></center>']);
					}

					$('#modal_program_kerja').trigger('openModal');
				}		
		  })
		  .fail(function(){
		  
		  });
		});
		
		$('#btn_save_choose_program_kerja').click(function(){
			var unit = $('#choose_program_kerja_unit').val();
			var periode = $('#choose_program_kerja_periode').val();
			
			$.ajax({
				type: 'post',
				url: '<?php echo base_url();?>penyusunan_rkm/save_rencana_kerja',
				data: $('#form_choose_porgam_kerja').serialize(),
				dataType: 'json'
			})
			.done(function(response, textStatus, jqhr){
				if(response.status == 'ok'){
					$('#modal_program_kerja').trigger('closeModal');
				}		
				loadRkm(unit, periode);
			})
			.fail(function(){
			})
		});

		$('#organization_id').change(function(){
			var optionSelected = $(this).find("option:selected");
			if($('#year').val() !=-1)
				loadRkm(optionSelected.val(), $('#year').val());
		});

		$('#year').change(function(){
			var optionSelected = $(this).find("option:selected");
			if($('#organization_id').val() !=-1)
				loadRkm($('#organization_id').val(), optionSelected.val());
		});
		
		$('#tabel_selected_program_kerja').delegate('.delete_data','click',function () { 
			var id = $(this).attr('data-id');
			var unit = $(this).attr('data-unit');
			var periode = $(this).attr('data-periode');
			var idRk = $(this).attr('data-rencana-kerja-id');
			var label = $(this).attr('data-label');
			new Messi('Yakin ingin menghapus rencana kerja <strong>'+label+'</strong>?', {
				title: 'Confirmation', 
				titleClass: 'anim warning', 
				closeButton: true,
				buttons: [
				{
					id: 0, 
					label: 'YES', 
					val: '1'
				},{
					id: 1, 
					label: 'NO', 
					val: '0'
				}], 
				callback:function(val)
				{ 
					if(val == 0) {
						return false;
					} else if(val == 1) {
						$.ajax({
							type: "POST",
							url: "<?php echo base_url();?>penyusunan_rkm/delete",
							data: {'id' : id, 'unit' : unit, 'periode' : periode, 'id-rencana-kerja' : idRk },
							success: function(msg){
								if(msg=='1') {
									loadRkm(unit, periode);
								} else {
									new Messi('Data gagal dihapus !<br />Debug : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
								} 
							},
							error: function(fnc,msg){
								new Messi('Tidak dapat terhubung ke server untuk malakukan proses hapus data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
							}
						});
					} 
				}
			});
			return false;
		});
	}());
</script>
	
	