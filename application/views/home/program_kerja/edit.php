<link rel="stylesheet" href="<?=base_url();?>assets/upload/css/jquery.fileupload.css">
<style>
  .progress-bar-success 
  {
    background-color: #5CB85C;
  }
  .progress-bar 
  {
    float: left;
    width: 0;
    height: 100%;
    font-size: 12px;
    line-height: 20px;
    color: #FFF;
    text-align: center;
    background-color: #428BCA;
    -webkit-box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
    box-shadow: inset 0 -1px 0 rgba(0, 0, 0, 0.15);
    -webkit-transition: width .6s ease;
    transition: width .6s ease;
  }
  .progress {
    height: 20px;
    margin-bottom: 20px;
    overflow: hidden;
    background-image: none;
    background-image: none;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
  }
  h1 small
  {
    font-size: 19px;
  }

  form {
    margin: 0;
  }
</style>
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Managemen Program Kerja		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Program Kerja </a> 	
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('kpi/program_kerja','Managemen Program Kerja')">Daftar Program Kerja </a> 	
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('kpi/program_kerja_add','Managemen Program Kerja')"> Tambah</a> </li>						
			</ul>
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-edit"></i>
						<span class="hidden-480">Tambah Program Kerja	</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo base_url();?>kpi/program_kerja_update">
									<div class="control-group">
										  <label class="control-label">Nama KPI</label>
										  <div class="controls">										  
											 <select name="kpi_id" id="kpi_id" class="span6 m-wrap">
												<option value="-1">Pilih KPI</option>
												<?php foreach ($kpis as $kpi) { $selected = ($kpi->id == $data[0]->kpi_id) ? "selected" : ""; ?>
												<option value="<?php echo $kpi->id;?>" <?php echo $selected;?> ><?php echo $kpi->kpi;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>										 
									  <div class="control-group">
										  <label class="control-label">Organisasi</label>
										  <div class="controls">										  
											 <select name="organization_id" id="organization_id" class="span6 m-wrap">
												<option value="">Pilih Organisasi</option>
												<?php foreach ($organizations as $organization) { 
													$selected=($organization->id==$data[0]->organization_id) ? "selected":"";
												?>
													<option value="<?php echo $organization->id;?>" <?php echo $selected;?>><?php echo $organization->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									 <div class="control-group">
										  <label class="control-label">Program Kerja</label>
										  <div class="controls">										  
												<textarea name="program_kerja" id="program_kerja" class="span6 m-wrap" rows="3"><?php echo $data[0]->program_kerja;?></textarea>
										  </div>
									 </div>								 
									  <div class="control-group">
										  <label class="control-label">Keterangan</label>
										  <div class="controls">										  
											<textarea name="keterangan" id="keterangan" class="span6 m-wrap" rows="3"><?php echo $data[0]->keterangan;?></textarea>
										  </div>
									 </div>		
									 <input type="hidden" id="file" name="mFile" />
							 
								  	<fieldset style="border:none;padding:0px !important;"> 
						          <div class="control-group">
						            <label class="control-label">File</label>
						            <div class="controls">
						              <span class="btn btn-primary fileinput-button" id="button_img">
						                <i class="icon-upload icon-white"></i>
						                <span>Pilih File...</span>
						                <input id="fileupload" type="file" name="files">
						              </span>
						              <span>Tipe file yang diizinkan : <strong>pdf<strong></span>    
						              <div id="progress" class="progress progress-info progress-striped" style="width:30%;position:relative;display:none;margin-top:3px;">
						                <div class="progress-bar progress-bar-success"></div>
						              </div>
						            </div>   
						          </div> 
						          <div class="control-group">
						          	<label class="control-label">&nbsp;</label>
						          	<div class="controls">
	      									<div class="alert preview" style="width:350px;display:inline;"><?php echo (($data[0]->file_url) ?  '<a href="' . base_url() . 'uploads/' . $data[0]->file_url .'">'.$data[0]->file_url.'</a>' : "Belum ada file");?></div>    
						          	</div>
						          </div>
						        </fieldset>								 
									  		
									 <div class="control-group">
										<div class="controls">		
											<input type="hidden" name="id" id="id" value="<?php echo $data[0]->id;?>"/>
											<button class="btn blue" type="submit"><i class="icon-save"> Ubah</i></button>
											<button class="btn red" type="button" onClick="routes('kpi/program_kerja','Daftar Program Kerja')"> <i class="icon-share"> Kembali</i></button>
										</div>
									 </div>							 
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->	
<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script language="javascript">
	$(document).ready(function(){
		$("#kpi_id").select2(); 
		$("#organization_id").select2(); 
	});

	function validate() {
		if ($('#kpi_id').val() =='') {
			new Messi('KPI masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("kpi_id").focus(); }});
			return false;
		}
		if ($('#organization_id').val() =='') {
			new Messi('Organisasi masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("organization_id").focus(); }});
			return false;
		}
		if ($('#program_kerja').val() =='') {
			new Messi('Program kerja masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("program_kerja").focus(); }});
			return false;
		}			
	}
			
	$('#fform').ajaxForm({
	  dataType: 'json',
	  success: function(response, statusText, xhr){
			if(response.status == "ok"){
		  	$('#fform')[0].reset();
		  	$.growl.notice({title: "Infomasi", message: "Data berhasil disimpan" });
				routes("kpi/program_kerja","Daftar Program Kerja");
			} else {
		  	$.growl.error({ title: "Peringatan", message: "Data gagal disimpan" });
			}
	  }
	});


	$(function () {
	    'use strict';
	    // Change this to the location of your server-side upload handler:
	    var path = '<?=base_url();?>';
	    var url  = '<?=base_url();?>kpi/unggah';

	    $('#fileupload').fileupload({
	      url: url,
	      dataType: 'json',
	      maxFileSize: 90000000, // 40 MB
	      done: function (e, data) {
	        if(data.result.success == false) {
	        	alert(data.result.message);
	          $('#progress').hide();
	        } else {
	          $.each(data.result.files, function (index, file) {
	              $('#progress').hide();
	              $('.preview').show().html('<i class="icon-file icon-white"></i> &nbsp;' + file.files + '&nbsp; (<a href="#" class="delete-attachment">Hapus</a>)');
	              $('#file').val(file.name);
	          });
	        }
	      },
	      progressall: function (e, data) {
	        $('#progress').show();
	          var progress = parseInt(data.loaded / data.total * 100, 10);
	          $('#progress .progress-bar').css(
	              'width',
	              progress + '%'
	          );
	      }
	    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
	});

	$('.preview').delegate('.delete-attachment', 'click', function(){
		$('.preview').hide();
	  $('#file').val('');
	  return false;
	});

</script>