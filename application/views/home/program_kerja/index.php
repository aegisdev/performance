
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Daftar Program Kerja				
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('kpi/program_kerja','Program Kerja')">Daftar Program Kerja</a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>Daftar Program Kerja
						</h4>
					</div>
					<div class="portlet-body">	
						<table border="0">	
							<tr>
								<td width="100px">Periode</td>
								<td width="90%">
									<select name="periode" id="periode" class="span6 m-wrap">
										<option value="-1">-- Pilih Tahun --</value>
										<?php for($year=2013;$year <=(date('Y')+1);$year++){ ?>
										<option value="<?php echo $year;?>"><?php echo $year;?></option>
										<?php } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td width="100px">KPI</td>
								<td width="90%">
									<select name="kpi_id" id="kpi_id" class="span6 m-wrap">
										<option value="">--Pilih KPI--</option>
										<?php foreach ($kpi as $k) { ?>
											<option value="<?php echo $k->id;?>"><?php echo $k->kpi;?></option>
										<?php } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td width="100px">Organisasi</td>
								<td>
									<select name="organization_id" id="organization_id" class="span6 m-wrap">
										<option value="">--Pilih Organisasi--</option>
										<?php foreach ($organizations as $o) { ?>
											<option value="<?php echo $o->id;?>"><?php echo $o->name;?></option>
										<?php } ?>
									</select>
								</td>
							</tr>
							
						</table> <br/>
						<div class="clearfix">
							<div class="btn-group">
								<button class="btn blue" onClick="javascript:routes('kpi/program_kerja_add','Program Kerja')">
								<i class="icon-plus"></i> Tambah
								</button>
							</div>							
						</div>
						<table class="table table-bordered table-striped table-hover" id="data_table">
							
							<thead>
								<tr>
									<th width="3%">ID#</th>
									<th>KPI</th>														
									<th>ORGANISASI</th>								
									<th>PROGRAM KERJA</th>
									<th>KETERANGAN</th>								
									<th width="150px">AKSI</th>									
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data as $row) {
									
								?>
									<tr>
										<td><?php echo $row->id;?></td>
										<td><?php echo $row->kpi;?></td>
										<td><?php echo $row->organization_name;?></td>
										<td><?php echo $row->program_kerja;?></td>
										<td><?php echo $row->keterangan;?></td>
										<td class="center-column">
											<?php if(!empty($row->file_url)){?>
											<a href="<?php echo base_url();?>uploads/<?php echo $row->file_url;?>" target="_blank" class="btn green"><i class="icon-book" title="Buka Program Kerja"></i></a>
											<?php } else {?>
												<a href="#" class="btn green"><i class="icon-book" onClick="alert('File program kerja tidak tersedia')" title="Buka Program Kerja"></i></a>
											<?php } ?>
											<button class="btn blue" onClick="javascript:routes('kpi/program_kerja_edit/<?php echo $row->id;?>','Program Kerja')"><i class="icon-edit" title="Edit data"></i></button>
											<button class="btn delete_data red" data-kpi='<?php echo $row->kpi;?>' data-id="<?php echo $row->id;?>"><i class="icon-trash" title="Hapus data"></i></button>
										</td>
									</tr>
									<?php } ?>			
							</tbody>
						</table>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->

<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script language="javascript">
$(document).ready( function () {	
	$("#kpi_id").select2();
	$("#organization_id").select2(); 
	$("#periode").select2();
	
	$('#data_table').dataTable({		
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ baris per halaman",
			"oPaginate": {
				"sPrevious": "Prev",
				"sNext": "Next"
			}
		},
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': [0,-2,-1]
		}]
	});
});

	$('#data_table').delegate('.delete_data', 'click', function(){
		var id =$(this).attr('data-id');
		var kpi = $(this).attr('data-kpi')
		new Messi('Yakin ingin menghapus KPI <strong>'+kpi+'</strong>?', {
						title: 'Confirmation', 
						titleClass: 'anim info', 
						closeButton: true,
						buttons: [
						{
							id: 0, 
							label: 'YES', 
							val: '1'
						},{
							id: 1, 
							label: 'NO', 
							val: '0'
						}
						], 
						callback:function(val)
						{ 
							if(val == 0) {
								return false;
							} else if(val == 1) {
								$.ajax({
									type: "POST",
									url: "<?php echo base_url();?>kpi/program_kerja_delete",
									data: "id="+id,
									success: function(msg){
										if(msg=='1') {
											new Messi('Data berhasil dihapus !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('kpi/program_kerja','Program Kerja'); }});
										} else {
											new Messi('Data gagal dihapus !<br />Debug : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
										} 
									},
									error: function(fnc,msg){
										new Messi('Tidak dapat terhubung ke server untuk malakukan proses hapus data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
									}
								});
							} 
						}
					}
				);
		});


	$("#kpi_id").change(function(){
		var optionSelected = $(this).find("option:selected");
		loadKpiForTable($('#organization_id').val(), optionSelected.val(), $('#periode').val());
	});
	$("#organization_id").change(function(){
		var optionSelected = $(this).find("option:selected");
		loadKpiForTable(optionSelected.val(), $('#kpi_id').val(), $('#periode').val());
	});

	$('#periode').change(function(){
		var optionSelected = $(this).find("option:selected");
		loadKpiForTable($('#organization_id').val(), $('#kpi_id').val(), optionSelected.val());
	});

	function loadKpiForTable(organization_id, kpi_id, periode){
		$.ajax({
			type: 'post',
			data: {'organization_id' : organization_id, 'kpi_id' : kpi_id, 'periode' : periode },
			url : '<?php echo base_url();?>kpi/get_program_kerja',
			dataType: 'json',
			beforeSend: function(){
				$('#data_table > tbody:last').empty();
			}
		})
		.done(function(response, textStatus, jqhr){
			if(response){
				for(var i=0;i<response.length;i++){			
					if(response[i].file_url !=""){
						var href='<a href="uploads/'+response[i].file_url+'" target="_blank" class="btn green"><i class="icon-book" title="Buka Program Kerja"></i></a>';
					}else
					{
						var href='<a href="#" class="btn green"><i class="icon-book" onClick=\'alert("File program kerja tidak tersedia")\' title="Buka Program Kerja"></i></a>';
					}				
					var el ='<tr><td>'+response[i].id+'</td>' +
							'<td>'+response[i].kpi+'</td>' + 
							'<td>'+response[i].organization_name+'</td>' +
							'<td>'+response[i].program_kerja+'</td>' +
							'<td>'+response[i].keterangan+'</td>' +
							'<td class="center-column">'+
								href+
								'&nbsp<button class="btn blue" onClick=\'javascript:routes("kpi/program_kerja_edit/'+response[i].id+'","Program Kerja")\'><i class="icon-edit" title="Edit data"></i></button>'+
								'&nbsp<button class="btn delete_data blue" data-id="'+response[i].id+'"><i class="icon-trash" title="Hapus data"></i></button>'+
							'</td>'+
							'</tr>';						
						$('#data_table > tbody:last').append(el);
				}
			}
		})
		.fail(function(){
		
		});
	}
</script>
	