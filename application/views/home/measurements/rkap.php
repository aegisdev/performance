<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Penyusunan RKAP				
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('measurements/forming','Penyusunan')">RKAP</a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>RKAP
						</h4>
					</div>
					<div class="portlet-body">	
						<div class="clearfix">
							<div class="btn-group">
								
							</div>
							<div class="btn-group pull-right">
								<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Print</a></li>
									<li><a href="#">Save as PDF</a></li>
									<li><a href="#">Export to Excel</a></li>
								</ul>
							</div>
						</div>							
							Unit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <select name="organization_id" id="forming-rkap-unit">										
										<option value="-1">--Pilih Unit--</value>
										<?php
											foreach($data as $row){	?>
												<option value="<?php echo $row->id;?>"><?php echo $row->name;?></value>
										<?php }
										?>
										
									</select><br/>
							<button type="button" id ="btn_rkap" class="btn blue span4"> Pilih Program Kerja </button> <br/><br/>
							<form id="form-save-rkap">
							<table class="table table-bordered table-striped table-hover" id="tbl-selected-rkap">
								<thead>
									<tr>
										<th>ORDER NO</th>
										<th>INDIKATOR</th>
										<th>RENCANA PELAKSANAAN</th>								
										<th>KETERANGAN</th>						
										<th>AKSI</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
							</form>
						<center>
							<button class="btn blue" id="btn-save-selected-rkap" style="display:none"><i class="icon-save"></i> Simpan</button>
						</center>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->
</div>
<div class="modal" id="modal_rkap">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">PROGRAM KERJA</h4>
      </div>
      <div class="modal-body">
		<form id="form-choose-rkap">
		<input type="hidden" id="choose-rkap-unit" name="choose-rkap-unit" />
		<table id="tbl-list-rkap" class="table table-bordered table-striped table-hover" >
			<thead>
				<tr>
					<td>NAMA KPI</td>
					<td>PROGRAM KERJA</td>
					<td>AKSI</td>
				</tr>
			</thead>
		</table>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn blue" id="btn-save-choose-rkap"><i class="icon-save"></i> Simpan</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- END PAGE CONTAINER-->		
<script type="text/javascript" charset="utf-8">			
	$('#data_table').dataTable({
		"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ baris per halaman",
			"oPaginate": {
				"sPrevious": "Prev",
				"sNext": "Next"
			}
		},
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': [0]
		}]
	});
	
	$('.delete_data').click(function () { 
		var id =$(this).attr('data-id');
		new Messi('Yakin ingin menghapus data dengan ID '+id+' ?', {
						title: 'Confirmation', 
						titleClass: 'anim info', 
						closeButton: true,
						buttons: [
						{
							id: 0, 
							label: 'YES', 
							val: '1'
						},{
							id: 1, 
							label: 'NO', 
							val: '0'
						}
						], 
						callback:function(val)
						{ 
							if(val == 0) {
								return false;
							} else if(val == 1) {
								$.ajax({
									type: "POST",
									url: "<?php echo base_url();?>rkap/delete",
									data: "id="+id,
									success: function(msg){
										if(msg=='1') {
											new Messi('Data berhasil dihapus !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('rkap','Managemen RKAP'); }});
										} else {
											new Messi('Data gagal dihapus !<br />Debug : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
										} 
									},
									error: function(fnc,msg){
										new Messi('Tidak dapat terhubung ke server untuk malakukan proses hapus data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
									}
								});
							} 
						}
					}
				);
	});
	
	var listTblRkap =  $('#tbl-list-rkap').dataTable({
      "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
      "sPaginationType": "bootstrap",
      "oLanguage": {
          "sLengthMenu": "_MENU_ baris per halaman",
          "oPaginate": {
              "sPrevious": "Prev",
              "sNext": "Next"
          }
      }
    });
	
$(function() {
  $('#modal_rkap').easyModal();
});	

$('#btn_rkap').click(function(){
  var unit = $('#forming-rkap-unit').val();  

  if(unit =='-1'){
	alert("Pilih unit terlebih dahulu");
	return false;
  }
  $.ajax({
	url: '<?php echo base_url();?>measurements/get_rkap',
	dataType: 'json',
	data: {'unit' : unit},
	type: 'post',
	beforeSend: function(){
		listTblRkap.fnClearTable();
	}
  })
  .done(function(response, textStatus, jqhr){
	if(response){
		$('#choose-rkap-unit').val(unit);
		for(var i=0;i<response.length;i++){
			listTblRkap.fnAddData([response[i].kpi, response[i].program_kerja, '<input type="checkbox" name="rkap-selected[]" value="'+ response[i].id +'" />']);
		}
		$('#modal_rkap').trigger('openModal');
	}		
  })
  .fail(function(){
  });
});


$('#btn-save-choose-rkap').click(function(){
	var unit = $('#choose-rkap-unit').val();
	
	$.ajax({
		type: 'post',
		url: '<?php echo base_url();?>measurements/choose_rkap',
		data: $('#form-choose-rkap').serialize(),
		dataType: 'json'
	})
	.done(function(response, textStatus, jqhr){
		if(response.status == 'ok'){
			$('#modal_rkap').trigger('closeModal');
		}
		loadRkapForTable(unit);
	})
	.fail(function(){
	})
});

function loadRkapForTable(unit){	
	$.ajax({
		type: 'post',
		data: {'unit' : unit},
		url : '<?php echo base_url();?>measurements/get_selected_rkap',
		dataType: 'json',
		beforeSend: function(){
			$('#tbl-selected-rkap > tbody:last').empty();
			$('#btn-save-selected-rkap').hide();
		}
	})
	.done(function(response, textStatus, jqhr){
		if(response){
			for(var i=0;i<response.length;i++){
				var order_no = (response[i].order_no) ? response[i].order_no : i;
				var rowbefore = (response[i-1]) ? response[i-1].perspective : '';
				var el = (response[i].perspective != rowbefore) ? '<tr><td colspan="6">'+response[i].perspective+'</td></tr>' : '';
				el += '<tr><td width="50px"><input class="span12" type="text" value="'+order_no+'" name="rkap-order-no[]"/></td><td>'+response[i].kpi+' <input type="hidden" name="rkap-id[]" value="'+response[i].id+'" /> <input type="hidden" name="program-kerja-id[]" value="'+response[i].program_kerja_id+'" /></td><td width="50px"><div class="input-prepend"><span class="add-on"><i class="icon-calendar"></i></span><input type="text" name="rkap-rencana-pelaksanaan[]" value="'+response[i].rencana_pelaksanaan+'" class="m-wrap m-ctrl-medium date-range"/></div></td><td width="50px"><textarea rows="3" cols="6" class="input-rkap-keterangan" name="rkap-keterangan[]">'+response[i].keterangan+'</textarea></td><td width="50px"><button class="btn red btn-delete-rkap-selected" object="'+response[i].id+'"><i class="icon-trash"></i></button></td></tr>'
				$('#tbl-selected-rkap > tbody:last').append(el);
				$('#btn-save-selected-rkap').show();
			}
			//var total = '<tr><td colspan="3">Total</td><td><span id="input-total-bobot"></span></td><td id="input-total-target"></td><td></td></tr>'
			//$('#tbl-selected-kpi > tbody:last').append(total);
			//calculateTotal();
			
		}
		$('.date-range').daterangepicker();
	})
	.fail(function(){
	
	});
}

$('#forming-rkap-unit').change(function(){
    var optionSelected = $(this).find("option:selected");
	loadRkapForTable(optionSelected.val());
	
});

$('#btn-save-selected-rkap').click(function(e){
	$.ajax({
		url: '<?php echo base_url();?>measurements/save_properties_rkap',
		dataType: 'json',
		data: $('#form-save-rkap').serialize(),
		type: 'post'
	})
	.done(function(response, textStatus, jqhr){
		if(response.status == 'ok'){
			loadRkapForTable($('#forming-rkap-unit').val());
		}
	})
	.fail(function(){
	
	});
	
	e.preventDefault();
});

$('#tbl-selected-rkap').delegate('.btn-delete-rkap-selected', 'click', function(e){
	var r=confirm("Delete this item?");
	if (r==true){
		var id = $(this).attr('object');
		$.ajax({
			type: 'post',
			url: '<?php echo base_url();?>measurements/delete_rkap_selected',
			data: {'id' : id},
			dataType: 'json'
		})
		.done(function(response, textStatus, jqhr){
			if(response.status == 'ok'){
				loadRkapForTable($('#forming-rkap-unit').val());
			} else {
				alert('Terjadi Kesalahan, Refresh Halaman!');
			}
		})
		.fail(function(){
		
		});
	}

	e.preventDefault();
});
/*
$('input').delegate('.date-range', 'change', function(e){
	alert("HHH");
	//delegate$('.date-range').daterangepicker();
 });*/
</script>
	
	