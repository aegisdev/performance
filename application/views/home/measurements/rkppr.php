<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Penyusunan				
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('measurements/forming','Penyusunan')">Penyusunan</a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>Penyusunan
						</h4>
					</div>
					<div class="portlet-body">	
						<div class="clearfix">
							<div class="btn-group">
								
							</div>
							<div class="btn-group pull-right">
								<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Print</a></li>
									<li><a href="#">Save as PDF</a></li>
									<li><a href="#">Export to Excel</a></li>
								</ul>
							</div>
						</div>
						<form name="fform" id="fform" class="form form-horizontal">
						<table class="table table-bordered table-striped table-hover" id="data_table2">						
							Unit : <select name="organization_id" id="organization_id">										
										<option value="">--Pilih Unit--</value>
										<?php
											foreach($data as $row){	?>
												<option value="<?php echo $row->id;?>"><?php echo $row->name;?></value>
										<?php }
										?>
										
									</select><br/>							
							<button type="button" id ="btn_kpi" class="btn blue span4"> Pilih KPI </button> <br/><br/>
							<thead>
								<tr>
									<th>NO</th>
									<th>INDIKATOR</th>								
									<th>TANGGAL AWAL</th>								
									<th>TANGGAL AKHIR</th>								
									<th>DESKRIPSI</th>								
																
								</tr>
							</thead>
							<tbody>
								<?php //foreach ($data as $row) { ?>
									<tr>
										<td style="background-color:blue"></td>
										<td style="background-color:blue">Keuangan dan Pasar</td>
										<td style="background-color:blue"></td>
										<td style="background-color:blue"></td>						
										<td style="background-color:blue"></td>						
									</tr>
									<tr>
										<td>1</td>
										<td>Earning Before Interest and Tax (EBIT)</td>
										<td><input type="text" name="date_start[]" class="span9"/></td>
										<td><input type="text" name="date_end[]" class="span9"/></td>
										<td><textarea name="description[]"></textarea></td>							
									</tr>
									<tr>
										<td>2</td>
										<td>Collection ratio</td>
										<td><input type="text" name="date_start[]" class="span9"/></td>
										<td><input type="text" name="date_end[]" class="span9"/></td>
										<td><textarea name="description[]"></textarea></td>																		
									</tr>
									<tr>
										<td>3</td>
										<td>Cost per route unit</td>
										<td><input type="text" name="date_start[]" class="span9"/></td>
										<td><input type="text" name="date_end[]" class="span9"/></td>
										<td><textarea name="description[]"></textarea></td>		
																		
									</tr>
									<tr>
										<td style="background-color:blue"></td>
										<td style="background-color:blue">Fokus Pelanggan</td>
										<td style="background-color:blue"></td>
										<td style="background-color:blue"></td>
										<td style="background-color:blue" class="center-column"></td>
																	
									</tr>
									<tr>
										<td>4</td>
										<td>Pengelolaan ekspektasi pelanggan</td>
										<td><input type="text" name="date_start[]" class="span9"/></td>
										<td><input type="text" name="date_end[]" class="span9"/></td>
										<td><textarea name="description[]"></textarea></td>	
																	
									</tr>
									<tr>
										<td>5</td>
										<td>Acceptable level of safeft</td>
										<td><input type="text" name="date_start[]" class="span9"/></td>
										<td><input type="text" name="date_end[]" class="span9"/></td>
										<td><textarea name="description[]"></textarea></td>		
																			
									</tr>
									<tr>
										<td style="background-color:blue"></td>
										<td style="background-color:blue">Efektivitas Produk dan Proses</td>
										<td style="background-color:blue"></td>
										<td style="background-color:blue"></td>
										<td style="background-color:blue" class="center-column"></td>
																
									</tr>
									<tr>
										<td>6</td>
										<td>Implementasi pedoman-pedoman kerja dan pemenuhan persyaratan penting</td>
										<td><input type="text" name="date_start[]" class="span9"/></td>
										<td><input type="text" name="date_end[]" class="span9"/></td>
										<td><textarea name="description[]"></textarea></td>									
									</tr>
									<tr>
										<td>7</td>
										<td>Realisasi investasi</td>
										<td><input type="text" name="date_start[]" class="span9"/></td>
										<td><input type="text" name="date_end[]" class="span9"/></td>
										<td><textarea name="description[]"></textarea></td>										
									</tr>
									<tr>
										<td>8</td>
										<td>Facility Readiness</td>
										<td><input type="text" name="date_start[]" class="span9"/></td>
										<td><input type="text" name="date_end[]" class="span9"/></td>
										<td><textarea name="description[]"></textarea></td>										
									</tr>
									<tr>
										<td style="background-color:blue"></td>
										<td style="background-color:blue">Fokus Tenaga Kerja</td>
										<td style="background-color:blue"></td>
										<td style="background-color:blue"></td>
										<td style="background-color:blue" class="center-column"></td>
																			
									</tr>
									<tr>
									<td>9</td>
										<td>Human capital readiness</td>
										<td><input type="text" name="date_start[]" class="span9"/></td>
										<td><input type="text" name="date_end[]" class="span9"/></td>
										<td><textarea name="description[]"></textarea></td>									
									</tr>
									<tr>
										<td>10</td>
										<td>Mandatory certification level</td>
										<td><input type="text" name="date_start[]" class="span9"/></td>
										<td><input type="text" name="date_end[]" class="span9"/></td>
										<td><textarea name="description[]"></textarea></td>											
									</tr>
									<tr>
										<td style="background-color:blue"></td>
										<td style="background-color:blue">Kepemimpinan, Tata kelola dan Tanggung Jawab Kemasyarakatan</td>
										<td style="background-color:blue"></td>
										<td style="background-color:blue"></td>																
										<td style="background-color:blue"></td>																
									</tr>
									<tr>
										<td>11</td>
										<td>Peyusunan pedoman dan kebijakan Risk Management</td>
										<td><input type="text" name="date_start[]" class="span9"/></td>
										<td><input type="text" name="date_end[]" class="span9"/></td>
										<td><textarea name="description[]"></textarea></td>									
									</tr>
									<tr>
										<td>12</td>
										<td>Comply to GCG Indicators</td>
										<td><input type="text" name="date_start[]" class="span9"/></td>
										<td><input type="text" name="date_end[]" class="span9"/></td>
										<td><textarea name="description[]"></textarea></td>								
									</tr>
									<tr>
										<td>13</td>
										<td>Comply to KPKU Requirement</td>
										<td><input type="text" name="date_start[]" class="span9"/></td>
										<td><input type="text" name="date_end[]" class="span9"/></td>
										<td><textarea name="description[]"></textarea></td>											
									</tr>									
									<tr>																		
										<td  class="center-column" colspan="5">
										<button type="type" class="btn blue"><i class="icon-save"></i> Simpan</button> <button type="type" class="btn blue"><i class="icon-share"></i> Batal</button></td>										
										
									</tr>		
																
									<?php //} ?>			
							</tbody>
						</table>
						</form>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->
</div>
<div class="modal" id="modal_kpi">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">KPI</h4>
      </div>
      <div class="modal-body">
			LIST KPI
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn blue" id="btn-save-changes-company-structure"><i class="icon-save"></i> Simpan</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- END PAGE CONTAINER-->		
<script type="text/javascript" charset="utf-8">			
	$('#data_table').dataTable({
		"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ baris per halaman",
			"oPaginate": {
				"sPrevious": "Prev",
				"sNext": "Next"
			}
		},
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': [0]
		}]
	});
	
	$('.delete_data').click(function () { 
		var id =$(this).attr('data-id');
		new Messi('Yakin ingin menghapus data dengan ID '+id+' ?', {
						title: 'Confirmation', 
						titleClass: 'anim info', 
						closeButton: true,
						buttons: [
						{
							id: 0, 
							label: 'YES', 
							val: '1'
						},{
							id: 1, 
							label: 'NO', 
							val: '0'
						}
						], 
						callback:function(val)
						{ 
							if(val == 0) {
								return false;
							} else if(val == 1) {
								$.ajax({
									type: "POST",
									url: "<?php echo base_url();?>kpi/delete",
									data: "id="+id,
									success: function(msg){
										if(msg=='1') {
											new Messi('Data berhasil dihapus !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('kpi','Managemen KPI'); }});
										} else {
											new Messi('Data gagal dihapus !<br />Debug : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
										} 
									},
									error: function(fnc,msg){
										new Messi('Tidak dapat terhubung ke server untuk malakukan proses hapus data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
									}
								});
							} 
						}
					}
				);
	});
$(function() {
  $('#modal_kpi').easyModal();
});	
$('#btn_kpi').click(function(){
	
  $('#modal_kpi').trigger('openModal');
});
	//$('#modal-kpi').trigger('openModal');
</script>
	
	