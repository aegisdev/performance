
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Finalisasi Penilaian		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Finalisasi Penilaian	</a> </li>				
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->		
				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>Finalisasi Penilaian	
						</h4>												
					</div>
					<div class="portlet-body">					
						<table border="0">							
							<tr>
								<td width="100px">Periode</td>
								<td>
									<select name="year" id="periode">
										<option value="-1">-- Pilih Tahun --</value>
										<?php for($year=2013;$year <=(date('Y')+1);$year++){ ?>
										<option value="<?php echo $year;?>"><?php echo $year;?></option>
										<?php } ?>
									</select>
								</td>
							</tr>
						</table> 
						<br/>
						<div class="pull-right">							
								<!-- <button class="btn blue" type="button" id="btn_kalkulasi">Kalkulasi</button> -->
								<!-- <button class="btn blue" type="button" id="btn_simpan">Simpan</button> -->
								<button class="btn blue" type="button" id="btn_finalisasi">Finalisasi</button>
							
						</div>
						<form id="form_save_finalization">
							<input type="hidden" name="periode_choosen" id="periode_choosen" />
							<table class="table table-bordered table-striped table-hover" id="table_finalisasi_km">
								<thead>
									<tr>
										<td rowspan="2" width="10px">NO</td>
										<td rowspan="2">INDIKATOR</td>
										<td rowspan="2">SATUAN</td>						
										<td rowspan="2">TARGET</td>						
										<td rowspan="2">BOBOT</td>													
										<td colspan="3"><center>UNIT</center></td>
										<td colspan="3"><center>PENILAI</center></td>
										<td rowspan="2">KETERANGAN</td>								
									</tr>
									<tr>
										<td>Realisasi</td>
										<td>Pencapaian</td>
										<td>Prestasi</td>	
										<td>Realisasi</td>
										<td>Pencapaian</td>
										<td>Prestasi</td>										
									</tr>
								</thead>
								<tbody>		
								</tbody>
							</table>
						</form>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->
	
<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script type="text/javascript" charset="utf-8">	

	$(document).ready(function(){
		$('#periode').select2();
		$('#table_finalisasi_km').dataTable({	
			"sPaginationType": "bootstrap",
			"oLanguage": {
				"sLengthMenu": "_MENU_ baris per halaman",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next"
				}
			},
			"bSort": false,
			"bFilter": false,
			"bInfo": false,
			"bPaginate": false
		});	
	});
		
	(function(){
		$('#periode').change(function(){		
			var optionSelected = $(this).find("option:selected");
			
			$("#periode_choosen").val(optionSelected.val());
			loadKontributorDirektorat(optionSelected.val());

			$.ajax({
				url: '<?php echo base_url();?>measurements/cek_submission_schedule',
				dataType: 'json',
				type: 'post'
			})
		  .done(function(response, textStatus, jqhr){
			if(optionSelected.val() == -1 || response.status == 'ok')
			{
				$('#btn_kalkulasi').removeAttr('disabled'); 
				$('#btn_simpan').removeAttr('disabled'); 
				$('#btn_finalisasi').removeAttr('disabled'); 
			}

			if(response.status == 'no'){
				$('#btn-kalkulasi').attr('disabled','disabled');; 
				$('#btn-simpan').attr('disabled','disabled');; 
				$('#btn-finalisasi').attr('disabled','disabled');;
			}
	
		  })
		  .fail(function(){

		  });

			$.ajax({
				type: 'post',
				data:{'periode':optionSelected.val()},
				url : '<?php echo base_url();?>finalisasi_km/check_finalisasi',
				dataType: 'json'
			})
			.done(function(response, textStatus, jqhr){	
				if(optionSelected.val() == -1 || response.status == 'no')
				{
					$('#btn_kalkulasi').removeAttr('disabled'); 
					$('#btn_simpan').removeAttr('disabled'); 
					$('#btn_finalisasi').removeAttr('disabled'); 
				}

				if(response.status=='ok'){	
					$('#btn_kalkulasi').attr('disabled','disabled');; 
					$('#btn_simpan').attr('disabled','disabled');; 
				}
			})
			.fail(function(){
			
			});
			
		});
		
		function loadKontributorDirektorat(periode){		
			$.ajax({
				type: 'post',
				data: {'periode' : periode},
				url : '<?php echo base_url();?>finalisasi_km/get_finalisasi_km',
				dataType: 'json',
				beforeSend: function(){
					$('#table_finalisasi_km > tbody:last').empty();
				}
			})
			.done(function(response, textStatus, jqhr){	
				console.log(response);
				if(response){
					var total_bobot=0;
					var total_prestasi_org=0;
					var total_prestasi_penilai=0;
					for(var i=0;i<response.length;i++){
						//published(response[i].is_published);
						total_bobot+=parseFloat(response[i].bobot);					
						total_prestasi_org+=parseFloat(response[i].prestasi_org);					
						total_prestasi_penilai+=parseFloat(response[i].prestasi_penilai);					
						var order_no = (response[i].order_no) ? response[i].order_no : i;
						var rowbefore = (response[i-1]) ? response[i-1].perspective : '';
						var el = (response[i].perspective != rowbefore) ? '<tr><td colspan="12"><strong>'+response[i].perspective+'</strong></td></tr>' : '';
						
						var realisasi_org = (response[i].realisasi_org != '' || response[i].realisasi_org != null)? response[i].realisasi_org : 0,
							pencapaian_org = (response[i].pencapaian_org != '' || response[i].pencapaian_org != null)? parseFloat(response[i].pencapaian_org).toFixed(2) : 0,
							prestasi_org = (response[i].prestasi_org != '' || response[i].prestasi_org != null)? parseFloat(response[i].prestasi_org).toFixed(2) : 0,
							realisasi_penilai = (response[i].realisasi_penilai != '' || response[i].realisasi_penilai != null)? response[i].realisasi_penilai : 0,
							pencapaian_penilai = (response[i].pencapaian_penilai != '' || response[i].pencapaian_penilai != null)? parseFloat(response[i].pencapaian_penilai).toFixed(2) : 0,
							prestasi_penilai = (response[i].prestasi_penilai != '' || response[i].prestasi_penilai != null)? parseFloat(response[i].prestasi_penilai).toFixed(2) : 0,
							keterangan = (response[i].keterangan_penilai != null)? response[i].keterangan_penilai : '';

						el += '<tr><td width="50px">'+(i+1)+'</td>'+
								'<td>'+response[i].kpi_name+' <input type="hidden" name="kpi_id[]" value="'+response[i].id+'" /> <input type="hidden" name="real_kpi_id[]" value="'+response[i].kpi_id+'" /> </td>'+
								'<td width="50px">'+response[i].satuan+'</td>'+
								'<td width="50px"><span id="target_'+response[i].id+'">'+response[i].target+'</span></td>'+
								'<td width="50px"><span class="bobot" id="bobot_'+response[i].id+'">'+response[i].bobot+'</span></td>'+
								'<td width="50px"><input type="text" class="span12 input_realisasi_org" name="realisasi_org[]" id="realisasi_org_'+response[i].kpi_id+'" value="'+realisasi_org+'" object="'+response[i].id+'" formula_id="'+response[i].formula_id+'" maks="'+response[i].maks+'" min="'+response[i].min+'" pencapaian_penilai="'+response[i].pencapaian_penilai+'"/></td>'+
								'<td width="50px"><input type="text" class="span12 input_pencapaian_org" name="pencapaian_org[]" value="'+pencapaian_org+'" readonly="true" id="pencapaian_org_'+response[i].id+'"/></td>'+
								'<td width="50px"><input type="text" class="span12 input_prestasi_org" name="prestasi_org[]" value="'+prestasi_org+'" readonly="true" id="prestasi_org_'+response[i].id+'"/></td>'+
								'<td width="50px"><input type="text" class="span12 input_realisasi_penilai" name="realisasi_penilai[]" id="realisasi_penilai_'+response[i].kpi_id+'" value="'+realisasi_penilai+'" object="'+response[i].id+'" formula_id="'+response[i].formula_id+'" maks="'+response[i].maks+'" min="'+response[i].min+'" pencapaian_penilai="'+response[i].pencapaian_penilai+'"></td>'+
								'<td width="50px"><input type="text" class="span12 input_pencapaian_penilai" name="pencapaian_penilai[]" value="'+pencapaian_penilai+'" readonly="true" id="pencapaian_penilai_'+response[i].id+'"/></td>'+
								'<td width="50px"><input type="text" class="span12 input_prestasi_penilai" name="prestasi_penilai[]" value="'+prestasi_penilai+'" readonly="true" id="prestasi_penilai_'+response[i].id+'"/></td>'+
								'<td><input type="text" class="span12 input_keterangan" name="keterangan[]" value="'+keterangan+'" readonly="true" id="keterangan_penilai_'+response[i].id+'"/></td></tr>'
						$('#table_finalisasi_km > tbody:last').append(el);									
					}
						var total = '<tr><td colspan="4">Total</td><td><span id="input_total_bobot">'+total_bobot+'</span></td><td></td><td></td><td><span id="input_total_prestasi_org">'+parseFloat(total_prestasi_org).toFixed(2)+'</span></td><td></td><td></td><td><span id="input_total_prestasi_penilai">'+parseFloat(total_prestasi_penilai).toFixed(2)+'</span></td><td></td></tr>';					
						total += '<input type="hidden" name="finaslis_periode" value="'+periode+'" />';
						$('#table_finalisasi_km > tbody:last').append(total);			
				}
			})
			.fail(function(){
			
			});
		}
		function published(is_published){	
			if(is_published==0){
				 $("#btn_simpan").removeAttr("disabled");
				 $("#btn_finalisasi").removeAttr("disabled");
			}else{
				$("#btn_simpan").attr("disabled", true);
				 $("#btn_finalisasi").attr("disabled", true);
			}
		
		}

		$("#btn_kalkulasi").click(function(){
			var periode = $('#periode').val();
			
			if(periode == -1){
				alert('Pilih periode terlebih dahulu');
				return false;
			} else {
				$.ajax({
					type: 'post',
					data:{'periode':periode},
					url : '<?php echo base_url();?>finalisasi_km/calculate_finalisasi_km',
					dataType: 'json'
				})
				.done(function(response, textStatus, jqhr){	
					if(response){
						for(var i=0;i<response.length;i++){
							$('#realisasi_org_' + response[i].kpi_id).val(response[i].realisasi_org);
							$('#realisasi_penilai_' + response[i].kpi_id).val(response[i].realisasi_penilai);
							calculateRealisasiOrg('#realisasi_org_' + response[i].kpi_id);
							calculateRealisasiPenilai('#realisasi_penilai_' + response[i].kpi_id);
						}
					}else {
						alert('Tidak ada hasil realisasi unit, silahkan isi manual.');	
					}
				})
				.fail(function(){
				
				});
			}
		});

		$('#table_finalisasi_km').delegate('.input_realisasi_org', 'keyup change', function(){
			calculateRealisasiOrg(this);
		});
		$('#table_finalisasi_km').delegate('.input_realisasi_penilai', 'keyup change', function(){	
			calculateRealisasiPenilai(this);
		});

		function calculateRealisasiOrg(ctx){
			var id = $(ctx).attr('object');
			var formulaId = $(ctx).attr('formula_id');
			var realisasi_org = $(ctx).val();	
			var target = $('#target_' + id).html();
			var maks = parseInt($(ctx).attr('maks'));
			var min = parseInt($(ctx).attr('min'));
		
			var a = calculatePencapaian(parseInt(formulaId), parseFloat(realisasi_org), parseFloat(target), 0, 0);
			var pencapaian_org = thresholdPencapaian(a, maks, min);
			var prestasi_org = parseInt($('#bobot_' + id).html()) * pencapaian_org / 100;
			$('#pencapaian_org_' + id).val(parseFloat(pencapaian_org).toFixed(2));
			$('#prestasi_org_' + id).val(parseFloat(prestasi_org).toFixed(2));
			calculateTotalOrg();
		}

		function calculateRealisasiPenilai(ctx){
			var id = $(ctx).attr('object');
			var formulaId = $(ctx).attr('formula_id');		
			var pencapaian_penilai = $(ctx).val();	
			var target = $('#target_' + id).html();	
			var maks = parseInt($(ctx).attr('maks'));
			var min = parseInt($(ctx).attr('min'));	
			var a = calculatePencapaian(parseInt(formulaId), parseFloat(pencapaian_penilai), parseFloat(target), 0, 0);	
			var pencapaian_penilai = thresholdPencapaian(a, maks, min);
			var prestasi_penilai = parseInt($('#bobot_' + id).html()) * pencapaian_penilai / 100;
			$('#pencapaian_penilai_' + id).val(parseFloat(pencapaian_penilai).toFixed(2));
			$('#prestasi_penilai_' + id).val(parseFloat(prestasi_penilai).toFixed(2));
			calculateTotalPenilai();
		}

		function thresholdPencapaian(a, b, c){
			if(a > b){
				a = b;
			} else if(a < c){
				a = c;
			}
			return a;
		}

		$("#btn_finalisasi").click(function(){
			var periode = $('#periode').val();
			if(periode==-1)
			{
				alert('Pilih periode terlebih dahulu');
				return false;
			} else {
				new Messi('Apakah anda yakin akan melakukan finalisasi penilaian? Karena proses ini tidak bisa diulangi kembali!', {
					title: 'Confirmation', 
					titleClass: 'anim info', 
					closeButton: true,
					buttons: [
					{
						id: 0, 
						label: 'YES', 
						val: '1'
					},{
						id: 1, 
						label: 'NO', 
						val: '0'
					}], 
					callback:function(val){ 
						if(val == 0) {
							return false;
						} else if(val == 1) {
							$.ajax({
								type: 'post',
									data: $('#form_save_finalization').serialize(),
								url : '<?php echo base_url();?>finalisasi_km/set_finalisasi',
								dataType: 'json'
							})
							.done(function(response, textStatus, jqhr){	
								if(response){
									loadKontributorDirektorat(periode);
								}
							})
							.fail(function(){
							
							});
						} 
					}
				});
			}
		});
		
		$("#btn_simpan").click(function(){
			var periode = $('#periode').val();
			if(periode == -1){
				alert('Pilih periode terlebih dahulu');
				return false;
			} else {
				$.ajax({
					type: 'post',
					data: $('#form_save_finalization').serialize(),
					url : '<?php echo base_url();?>finalisasi_km/save_finalization',
					dataType: 'json'
				})
				.done(function(response, textStatus, jqhr){	
					if(response){					
						loadKontributorDirektorat(periode);					
					}
				})
				.fail(function(){
				
				});
			}	
		});

		function calculatePencapaian(formulaId, realisasi, target, konstanta, konstanta_tanggal){
			var result = 0;
			konstanta = konstanta || 0;
			konstanta_tanggal = konstanta_tanggal || 0;
			switch (formulaId){
				case 1 :
				result = 100 + ((( realisasi - target ) / target ) * 100 );
				break;
				case 2 :
				result = 100 + ((( target - realisasi ) / target ) * 100 );
				break;
				case 3 :
				result = 100 + ((( target - realisasi ) / konstanta_tanggal ) * 100 );
				break;
				case 4 :
				result = 100 + (( realisasi - target ) * konstanta );
				break;
				case 5 :
				result = 100 + (( target - realisasi ) * konstanta );
				break;
				default :
				result = 0;
				break;
			}
			return result;
		}

		function calculateTotalOrg(){
			var bob = 0;
			var prestasi_org = 0;
			var prestasi_penilai = 0;

			$('.bobot').each(function(){
				bob += parseInt($(this).html());
			});	
			$('.input_prestasi_org').each(function(){
				prestasi_org += parseFloat($(this).val());
			});
			
			$('#input_total_bobot').html(bob);
			$('#input_total_prestasi_org').html(prestasi_org.toFixed(2));
		}

		function calculateTotalPenilai(){
			var bob = 0;
			var prestasi_penilai = 0;

			$('.bobot').each(function(){
				bob += parseInt($(this).html());
			});	
		
			$('.input_prestasi_penilai').each(function(){
				prestasi_penilai += parseFloat($(this).val());
			});
			$('#input_total_bobot').html(bob);
			$('#input_total_prestasi_penilai').html(prestasi_penilai.toFixed(2));
		}
	}());
</script>