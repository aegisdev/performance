	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Verifikasi Oleh Penilai				
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('measurements/evaluator','Realisasi Oleh Penilai')">Verifikasi Oleh Penilai</a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>Verifikasi Oleh Penilai
						</h4>
					</div>
					<div class="portlet-body">	
						<!-- <div class="clearfix">
							<div class="btn-group">
								
							</div>
							<div class="btn-group pull-right">
								<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Print</a></li>
									<li><a href="#">Save as PDF</a></li>
									<li><a href="#">Export to Excel</a></li>
								</ul>
							</div>
						</div> -->
						<table border="0">
							<tr>
								<td width="100px">Unit</td>
								<td colspan="2" width="90%">
									<select name="organization_id" id="forming-kpi-unit" class="span6 m-wrap">										
										<option value="-1">-- Pilih Unit --</value>
										<?php foreach($data as $row){ ?>
										<option value="<?php echo $row->id;?>"><?php echo $row->name;?></value>
										<?php } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td width="100px">Periode</td>
								<td>
									<select name="year" id="forming-kpi-periode" class="span2 m-wrap">
										<option value="-1">-- Pilih Tahun --</value>
										<?php for($year=2013;$year <=(date('Y')+1);$year++){ ?>
										<option value="<?php echo $year;?>"><?php echo $year;?></option>
										<?php } ?>
									</select>
								</td>
								<td>
									<!-- <select name="triwulan" id="forming-kpi-quarter">
										<option value="-1">-- Pilih Triwulan -- </option>
										<option value="1">Triwulan 1</option>
										<option value="2">Triwulan 2</option>
										<option value="3">Triwulan 3</option>
										<option value="4">Triwulan 4</option>
									</select> -->
								</td>
							</tr>
						</table>
						<div class="clearfix">
							<div class="pull-right">
								<p>
									<button class="btn blue" type="button" id="btn-kalkulasi-realisasi" style="display:none" >Kalkulasi</button>
								</p>
							</div>
						</div>						
						<form id="form-save-kpi">
						<input type="hidden" name="unit-choosen" id="unit-choosen" />
						<input type="hidden" name="periode-choosen" id="periode-choosen" />
						<table class="table table-bordered table-striped table-hover" id="tbl-selected-kpi">
							<thead>
								<tr>
									<th>NO</th>
									<th>INDIKATOR</th>
									<th>SATUAN</th>								
									<th>TARGET</th>								
									<th>BOBOT</th>		
									<th>REALISASI UNIT</th>					
									<th>REALISASI</th>								
									<th>PENCAPAIAN</th>								
									<th>PRESTASI</th>
									<th>KETERANGAN</th>
								</tr>
							</thead>
							<tbody>		
							</tbody>
						</table>
						</form>
						<center>
						<button class="btn blue" id="btn-save-realization-unit" style="display:none"><i class="icon-save"></i> Simpan</button>
						</center>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->

<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script language="javascript">
$(document).ready(function(){
	$("#forming-kpi-unit").select2(); 
	$("#forming-kpi-periode").select2(); 	

	$('#tbl-selected-kpi2').dataTable({		
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ baris per halaman",
			"oPaginate": {
				"sPrevious": "Prev",
				"sNext": "Next"
			}
		},
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': [0]
		}]
	});		
});
		
		
(function(){
	$('#forming-kpi-unit').change(function(){
		var optionSelected = $(this).find("option:selected");
		$('#forming-kpi-unit-name').val((optionSelected.html().replace(/\s+/g, ' ')));
		$('#unit-choosen').val(optionSelected.val());
		loadKpiForTable(optionSelected.val(), $('#forming-kpi-periode').val(), $('#forming-kpi-unit-name').val());
	});

	$('#forming-kpi-periode').change(function(){
		var optionSelected = $(this).find("option:selected"),
			periode		   = optionSelected.val();

		$('#periode-choosen').val(optionSelected.val());
		loadKpiForTable($('#forming-kpi-unit').val(), optionSelected.val(), $('#forming-kpi-unit-name').val());

		$.ajax({
			url: '<?php echo base_url();?>measurements/cek_submission_schedule',
			dataType: 'json',
			type: 'post'
		  })
		  .done(function(response, textStatus, jqhr){
			if(periode == -1 || response.status == 'ok'){
				document.getElementById('btn-kalkulasi-realisasi').disabled = false; 
				document.getElementById('btn-save-realization-unit').disabled = false; 
			}

			if(response.status == 'no'){
				document.getElementById('btn-kalkulasi-realisasi').disabled = true; 
				document.getElementById('btn-save-realization-unit').disabled = true; 
			}		
		  })
		  .fail(function(){

		  });

		  $.ajax({
			url: '<?php echo base_url();?>finalisasi_km/check_finalisasi',
			dataType: 'json',
			data: {'periode': periode},
			type: 'post'
		  })
		  .done(function(response, textStatus, jqhr){
			if(periode == -1 || response.status == 'no'){
				document.getElementById('btn-kalkulasi-realisasi').disabled = false; 
				document.getElementById('btn-save-realization-unit').disabled = false; 
			}	

			if(response.status == 'ok'){
				document.getElementById('btn-kalkulasi-realisasi').disabled = true; 
				document.getElementById('btn-save-realization-unit').disabled = true; 
			}		
		  })
		  .fail(function(){

		  });
	});

	$('#forming-kpi-quarter').change(function(){
		var optionSelected = $(this).find("option:selected");
		loadKpiForTable($('#forming-kpi-unit').val(), $('#forming-kpi-periode').val(), optionSelected.val());
	});
	
	function loadKpiForTable(unit, periode, unitName){
		$.ajax({
			type: 'post',
			data: {'unit' : unit, 'periode' : periode, 'unit-name': unitName},
			url : '<?php echo base_url();?>measurements/get_selected_kpi_km_unit',
			dataType: 'json',
			beforeSend: function(){
				$('#tbl-selected-kpi > tbody:last').empty();
				$('#btn-save-realization-unit').hide();
			}
		})
		.done(function(response, textStatus, jqhr){
			if(response){
				var isPublished = 0;
				for(var i=0;i<response.length;i++){
					var order_no = (response[i].order_no) ? response[i].order_no : i;
					var rowbefore = (response[i-1]) ? response[i-1].perspective : '';
					var ket = (response[i].keterangan_penilai != null) ? response[i].keterangan_penilai : ''; 
					var el = (response[i].perspective != rowbefore) ? '<tr><td colspan="10"><strong>'+response[i].perspective+'</strong></td></tr>' : '';
					el += '<tr>' +
					'<td width="50px">'+(i+1)+'</td>'+
					'<td>'+response[i].kpi+' <input type="hidden" name="kpi-id[]" value="'+response[i].kpi_id+'" /> <input type="hidden" value="'+response[i].id+'" name="data-id[]" /><input type="hidden" name="kpi-name[]" value="'+response[i].kpi+'"/></td>' + 
					'<td width="50px">'+response[i].satuan+'</td>' + 
					'<td width="50px"><span id="kpi-target-'+response[i].id+'">'+response[i].target+'</span></td>' + 
					'<td width="50px"><span class="span-kpi-bobot" id="kpi-bobot-'+response[i].id+'">'+response[i].bobot+'</span></td>' +
					'<td><input type="hidden" name="kpi-real-unit[]" value="'+response[i].realisasi_org+'" />' + response[i].realisasi_org + '</td>' +  
					'<td width="50px"><input type="text" class="span12 input-kpi-realization" name="kpi-realization[]" id="kpi-'+response[i].kpi_id+'" value="'+response[i].realisasi_penilai+'" object="'+response[i].id+'" formula-id="'+response[i].formula_id+'" maks="'+response[i].maks+'" min="'+response[i].min+'"/></td>' + 
					'<td width="50px"><input type="text" class="span12 input-kpi-pencapaian" name="kpi-pencapaian[]" value="'+parseFloat(response[i].pencapaian_penilai).toFixed(2)+'" readonly="true" id="kpi-pencapaian-'+response[i].id+'"/></td>' + 
					'<td width="50px"><input type="text" class="span12 input-kpi-prestasi" name="kpi-prestasi[]" value="'+parseFloat(response[i].prestasi_penilai).toFixed(2)+'" readonly="true" id="kpi-prestasi-'+response[i].id+'"/></td>' + 
					'<td><textarea name="kpi-keterangan[]">'+ket+'</textarea></td>' + 
					'</tr>'
					$('#tbl-selected-kpi > tbody:last').append(el);
					isPublished = response[i].is_published;
				}

				var total = '<tr><td colspan="4">Total</td><td><span id="input-total-bobot"></span></td><td colspan="3">&nbsp;</td><td><span id="input-total-prestasi"></span></td><td></td></tr>';
				$('#tbl-selected-kpi > tbody:last').append(total);
				if(isPublished == 1){
					$('#btn-save-realization-unit').hide();
				} else {
					$('#btn-save-realization-unit').show();
				}
				calculateTotal();
			}
		})
		.fail(function(){
		
		});
	}
	
	$('#tbl-selected-kpi').delegate('.input-kpi-realization', 'keyup change', function(){
		calculatePrestasi(this);
	});

	function calculatePrestasi(ctx){
		var id = $(ctx).attr('object');
		var formulaId = $(ctx).attr('formula-id');
		var realisasi = $(ctx).val();
		var target = $('#kpi-target-' + id).html();
		var maks = parseInt($(ctx).attr('maks'));
		var min = parseInt($(ctx).attr('min'));
		var a = calculatePencapaian(parseInt(formulaId), parseFloat(realisasi), parseFloat(target), 0, 0);
		var pencapaian = thresholdPencapaian(a, maks, min);
		var prestasi = parseInt($('#kpi-bobot-' + id).html()) * pencapaian / 100;
		$('#kpi-pencapaian-' + id).val(parseFloat(pencapaian).toFixed(2));
		$('#kpi-prestasi-' + id).val(parseFloat(prestasi).toFixed(2));
		calculateTotal();
	}
	
	
	function thresholdPencapaian(a, b, c){
		if(a > b){
			a = b;
		} else if(a < c){
			a = c;
		}
		return a;
	}

	function calculatePencapaian(formulaId, realisasi, target, konstanta, konstanta_tanggal){
		var result = 0;
		konstanta = konstanta || 0;
		konstanta_tanggal = konstanta_tanggal || 0;
		switch (formulaId){
			case 1 :
			result = 100 + ((( realisasi - target ) / target ) * 100 );
			break;
			case 2 :
			result = 100 + ((( target - realisasi ) / target ) * 100 );
			break;
			case 3 :
			result = 100 + ((( target - realisasi ) / konstanta_tanggal ) * 100 );
			break;
			case 4 :
			result = 100 + (( realisasi - target ) * konstanta );
			break;
			case 5 :
			result = 100 + (( target - realisasi ) * konstanta );
			break;
			default :
			result = 0;
			break;
		}
		
		return result;
	}

	function calculateTotal(){
		var bob = 0, prestasi = 0;
		$('.span-kpi-bobot').each(function(){
			bob += parseFloat($(this).html());
		});
		
		$('.input-kpi-prestasi').each(function(){
			prestasi += parseFloat($(this).val());
		});
		
		$('#input-total-bobot').html(bob);
		$('#input-total-prestasi').html(prestasi.toFixed(2));
	}

	$('#btn-save-realization-unit').click(function(e){
		new Messi('Apakah anda yakin data yang anda masukkan sudah benar dan akan melakukan verifikasi?', {
			title: 'Confirmation', 
			titleClass: 'anim info', 
			closeButton: true,
			buttons: [
			{
				id: 0, 
				label: 'YES', 
				val: '1'
			},{
				id: 1, 
				label: 'NO', 
				val: '0'
			}], 
			callback:function(val){ 
				if(val == 0) {
					return false;
				} else if(val == 1) {
					$.ajax({
						url: '<?php echo base_url();?>measurements/save_realization_penilai_km',
						dataType: 'json',
						data: $('#form-save-kpi').serialize(),
						type: 'post'
					})
					.done(function(response, textStatus, jqhr){
						if(response.status == 'ok'){
							$.growl.notice({ title: "Informasi", message: "Data berhasil disimpan ke database." });
						} else {
							$.growl.error({ title: "Informasi", message: "Terjadi kesalahan, silahkan kontak admin!" });
						}

						loadKpiForTable($('#forming-kpi-unit').val(), $('#forming-kpi-periode').val(),  $('#forming-kpi-quarter').val());
					})
					.fail(function(){
					
					});
				} 
			}
		});
		
		e.preventDefault();
	});

	$('#btn-kalkulasi-realisasi').click(function(){
		var unit = $('#forming-kpi-unit').val();
		var periode = $('#forming-kpi-periode').val();

		$.ajax({
			url: '<?php echo base_url();?>measurements/calculate_child_realization',
			data: {'unit' : unit, 'periode': periode , 'type' : 1},
			type: 'post',
			dataType: 'json'
		})
		.done(function(response, textStatus, jqhr){
			if(response.length > 0){
				for(var i=0;i<response.length;i++){
					$('#kpi-' + response[i].kpi_id).val(response[i].total_realisasi);
					calculatePrestasi('#kpi-' + response[i].kpi_id);
				}
			} else {
				alert('Tidak ada hasil realisasi unit, silahkan isi manual.');	
			}
		})
		.fail(function(){

		});
	});
}());
</script>
	
	