
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				KM Perusahaan			
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('measurements/forming','KM Perusahaan')">KM Perusahaan</a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>KM Perusahaan
						</h4>
					</div>
					<div class="portlet-body">						
						<table border="0">		
							<tr>
								<td width="100px">Periode</td>
								<td>
									<select name="year" id="forming-kpi-periode">
										<option value="-1">-- Pilih Tahun --</value>
										<?php for($year=2013;$year <=(date('Y')+1);$year++){ ?>
										<option value="<?php echo $year;?>"><?php echo $year;?></option>
										<?php } ?>
									</select>
								</td>
							</tr>
						</table>	
						<div class="pull-right">								
							<button type="button" id ="btn_kpi" class="btn blue"><i class="icon-ok"></i> Pilih KPI </button>	
						</div>
						<form id="form-save-kpi">
							<table class="table table-bordered table-striped table-hover" id="tbl-selected-kpi">
								<thead>
									<tr>
										<th width="40px">ORDER NO</th>
										<th>INDIKATOR</th>
										<th width="40px">SATUAN</th>
										<th width="40px">BOBOT</th>
										<th width="40px">PENC. SBELUMNYA</th>										
										<th width="40px">BEST PERFORMANCE</th>
<!-- 										<th width="40px">COMPE- TITOR</th>
										<th width="40px">BENCH- MARK</th>		 -->				
										<th width="40px">TARGET</th>	
										<th width="40px">AKSI</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</form>
						<center>
							<button class="btn blue" id="btn-save-selected-kpi" style="display:none"><i class="icon-save"></i> Simpan</button>
						</center>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->
<div class="modal" id="modal_kpi">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Silahkan Pilih KPI</h4>
      </div>
      <div class="modal-body">
		<form id="form-choose-kpi">
		<input type="hidden" id="choose-kpi-unit" name="choose-kpi-unit" />
		<input type="hidden" id="choose-kpi-periode" name="choose-kpi-periode" />
		<input type="hidden" id="choose-kpi-quarter" name="choose-kpi-quarter" />
		<table id="tbl-list-kpi" class="table table-bordered table-striped table-hover" >
			<thead>
				<tr>
					<td style="text-align:center">NAMA KPI</td>
					<td style="text-align:center" width="60px">PILIH KPI</td>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
		</form>
      </div>
      <div class="modal-footer">
	  	<button type="button" class="btn blue" id="btn-save-choose-kpi"><i class="icon-save"></i> Simpan</button>
	  </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<!-- END PAGE CONTAINER-->		
<script type="text/javascript" charset="utf-8">		
	$(document).ready(function(){
		$('#modal_kpi').easyModal();
		$('#modal_kpi').trigger('closeModal');
		$('#forming-kpi-periode').select2();
		$('#tbl-selected-kpi').dataTable({
			"bSort": false,
			"bFilter": false,
			"bInfo": false,
			"bPaginate": false
		});	
	});

	(function(){
		var listTblKpi =  $('#tbl-list-kpi').dataTable({
			"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
			"sPaginationType": "bootstrap",
			"bSort": false,
			"bPaginate": false
		});

		$('#btn_kpi').click(function(){
		  var unit = $('#forming-kpi-unit').val();
		  var periode = $('#forming-kpi-periode').val();
		  
		  if(periode == -1){
				$.growl.error({ title: "Peringatan", message: "Silahkan Pilih Periode Dahulu !!" });
				return false;
			}

		  $.ajax({
				url: '<?php echo base_url();?>measurements/get_kpi_no_quarter',
				dataType: 'json',
				data: {'unit' : unit, 'periode': periode},
				type: 'post',
				beforeSend: function(){
					listTblKpi.fnClearTable();
				}
		  })
		  .done(function(response, textStatus, jqhr){
				if(response){
					$('#choose-kpi-unit').val(unit);
					$('#choose-kpi-periode').val(periode);
					
					for(var i=0;i<response.length;i++){
						listTblKpi.fnAddData([response[i].kpi, '<center><input type="checkbox" name="kpi-selected[]" value="'+ response[i].id +'" /></center>']);
					}
					$('#modal_kpi').trigger('openModal');
				}		
		  })
		  .fail(function(){

		  });
		});

		$('#btn-save-choose-kpi').click(function(){
			var unit    = $('#forming-kpi-unit').val();
			var periode = $('#forming-kpi-periode').val();

			if(periode == -1){
				$.growl.error({ title: "Peringatan", message: "Silahkan Pilih Periode Dahulu !!" });
			} else {	
				$.ajax({	
					type: 'post',
					url: '<?php echo base_url();?>measurements/choose_kpi',
					data: $('#form-choose-kpi').serialize(),
					dataType: 'json'
				})
				.done(function(response, textStatus, jqhr){
					if(response.status == 'ok'){
						$('#modal_kpi').trigger('closeModal');
						//$.growl.notice({ title: "Informasi", message: "Data berhasil disimpan ke database." });

						if(response.exists != null)
						{
							var arr     = response.exists,
								explode   = arr.split("#"),
								c         = '<ul>';

								for(i = 0; i < explode.length; i++){ c +='<li> '+explode[i]+'</li>'; }
								c += '</ul>';
							   $.growl.warning({ title: "Data exists", message: c, 'static': true });
						}	

						if(response.not_in_kompetitor != null)
						{
							var arr       = response.not_in_kompetitor,
								explode   = arr.split("#"),
								c         = '<ul>';

								for(i = 0; i < explode.length; i++){ c +='<li> '+explode[i]+'</li>'; }
								c += '</ul>';
							   $.growl.warning({ title: "Data Belum Memiliki Pembanding", message: c, 'static': true });
						}	

					} else {
						$.growl.error( { title: "Peringatan", message: "Data gagal disimpan!" });
					}
					loadKpiForTable(unit, periode);
					$('#form-choose-kpi')[0].reset();
				})
				.fail(function(){
				})
			}
		});

		function loadKpiForTable(unit, periode){
			$.ajax({
				type: 'post',
				data: {'unit' : unit, 'periode' : periode },
				url : '<?php echo base_url();?>measurements/get_selected_kpi_no_quarter',
				dataType: 'json',
				beforeSend: function(){
					$('#tbl-selected-kpi > tbody:last').empty();
					$('#btn-save-selected-kpi').hide();
				}
			})
			.done(function(response, textStatus, jqhr){
				if(response){
					for(var i=0;i<response.length;i++){
						var order_no = (response[i].order_no) ? response[i].order_no : i;
						var rowbefore = (response[i-1]) ? response[i-1].perspective : '';
						var pencapaian_sebelumnya = (response[i].pencapaian_sebelumnya) ? response[i].pencapaian_sebelumnya : 0;
						var el = (response[i].perspective != rowbefore) ? '<tr><td colspan="10"><strong>'+response[i].perspective+'</strong></td></tr>' : '';
						el += '<tr><td><input class="span12" type="text" value="'+order_no+'" name="kpi-order-no[]"/></td><td>'+response[i].kpi+' <input type="hidden" name="kpi-id[]" value="'+response[i].kpi_id+'" /></td><td>'+response[i].satuan+'</td ><td><input type="text" class="span12 input-kpi-bobot" name="kpi-bobot[]" value="'+response[i].bobot+'" /></td><td>'+pencapaian_sebelumnya+'</td><td>'+response[i].best_performance+'</td>' + 
							// '<td>'+response[i].kompetitor+'</td><td>'+response[i].benchmark+'</td>' + 
							'<td><input type="text" class="span12 input-kpi-target" name="kpi-target[]" value="'+response[i].target+'" /></td><td><button class="btn red btn-delete-kpi-selected" kpi-id="'+response[i].kpi_id+'" kpi-periode="'+ periode +'" kpi-unit-id="'+ unit +'"><i class="icon-trash"></i></button></td></tr>';
						$('#tbl-selected-kpi > tbody:last').append(el);
						$('#btn-save-selected-kpi').show();
					}
					var total = '<tr><td colspan="3">Total</td><td><span id="input-total-bobot"></span></td><td colspan="6"></td></tr>';
					total += '<tr><td colspan="3">Sisa</td><td><span id="input-sisa-total-bobot">100</span></td><td colspan="6"></td></tr>'
					total += '<input type="hidden" name="kpi-periode" value="'+periode+'" />';
					total += '<input type="hidden" name="kpi-unit-id" value="'+unit+'" />';
					$('#tbl-selected-kpi > tbody:last').append(total);
					
					calculateTotal();
				}
			})
			.fail(function(){
			
			});
		}
			
		$('#forming-kpi-unit').change(function(){
			var optionSelected = $(this).find("option:selected");
			loadKpiForTable(optionSelected.val(), $('#forming-kpi-periode').val());
		});

		$('#forming-kpi-periode').change(function(){
			var optionSelected = $(this).find("option:selected");
			loadKpiForTable($('#forming-kpi-unit').val(), optionSelected.val());
		});

		$('#btn-save-selected-kpi').click(function(e){

			if($('#input-total-bobot').html() == 100){
				$.ajax({
					url: '<?php echo base_url();?>measurements/save_properties_kpi',
					dataType: 'json',
					data: $('#form-save-kpi').serialize(),
					type: 'post'
				})
				.done(function(response, textStatus, jqhr){
					if(response.status == 'ok'){
						$.growl.notice({ title: "Informasi", message: "Data berhasil disimpan ke database." });
						loadKpiForTable($('#forming-kpi-unit').val(), $('#forming-kpi-periode').val());
					} else {
						$.growl.error({ title: "Informasi", message: "Terjadi kesalahan, silahkan kontak admin!" });
					}
				})
				.fail(function(){
				
				});
			} else {
				alert('Pastikan jumlah bobot 100!');
			}			
			
			e.preventDefault();
		});

		$('#tbl-selected-kpi').delegate('.btn-delete-kpi-selected', 'click', function(e){
			var r=confirm("Delete this item?");
			if (r==true){
				var kpi_id = $(this).attr('kpi-id');
				var kpi_unit_id = $(this).attr('kpi-unit-id');
				var kpi_periode = $(this).attr('kpi-periode');

				$.ajax({
					type: 'post',
					url: '<?php echo base_url();?>measurements/delete_kpi_selected',
					data: {'kpi-id' : kpi_id, 'kpi-unit-id' : kpi_unit_id, 'kpi-periode' : kpi_periode },
					dataType: 'json'
				})
				.done(function(response, textStatus, jqhr){
					if(response.status == 'ok'){
						loadKpiForTable($('#forming-kpi-unit').val(), $('#forming-kpi-periode').val());
					} else {
						alert('Terjadi Kesalahan, Refresh Halaman!');
					}
				})
				.fail(function(){
				
				});
			}

			e.preventDefault();
		});

		function calculateTotal(){
			var bob = 0;
			$('.input-kpi-bobot').each(function(){
				bob += parseFloat($(this).val().replace(",", "."));
			});

			var sisa = 100 - bob.toFixed(12);
			
			$('#input-total-bobot').html(bob.toFixed(1));
			$('#input-sisa-total-bobot').html(sisa.toFixed(1));
		}

		$('#tbl-selected-kpi').delegate('.input-kpi-bobot', 'keyup', function(){
			calculateTotal();
		});

		$('#tbl-selected-kpi').delegate('.input-kpi-target', 'keyup', function(){
			calculateTotal();
		});
	}());
</script>
	
	