<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Publish KM				
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('measurements/publish','Publish KM')">Publish KM</a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			
			<div class="portlet box green">
				<div class="portlet-title">
					<h4>
						<i class="icon-table"></i>Publish KM
					</h4>
				</div>
				<div class="portlet-body">
				<table border="0">
					<tr>
						<td width="100px">Periode</td>
						<td>:
							<select name="year" id="forming-kpi-periode">
								<option value="-1">-- Pilih Tahun --</value>
								<?php for($year=2013;$year <=(date('Y')+1);$year++){ ?>
								<option value="<?php echo $year;?>"><?php echo $year;?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
				</table>
				<table class="table table-bordered table-striped table-hover" id="tbl-selected-kpi">
					<thead>
						<tr>
							<th>NO</th>
							<th>UNIT</th>
							<th>PERIODE</th>
							<th>PRESTASI</th>								
							<th>AKSI</th>								
						</tr>
					</thead>
					<tbody>
					<!--	<?php //for($i=0;$i<count($data);$i++){ ?>
						<tr>
							<td><?php //echo $i+1;?></td>
							<td><?php //echo $data[$i]->name;?></td>
							<td><?php //echo $data[$i]->periode;?></td>
							<td><?php //echo number_format($data[$i]->nilai, 2);?></td>
							<td>
								<?php 
									// if($data[$i]->is_published == 0){
									// 	echo '<button class="btn btn-publish-nilai" periode="' . $data[$i]->periode . '" quarter="'. $data[$i]->quarter .'"><i class=" icon-bullhorn"></i></button>';
									// } else {
									// 	echo 'Published';
									// }
								?>
							</td>
						</tr>
						<?php //} ?> -->
					</tbody>
				</table>	
				</div>
			</div>	
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
<script type="text/javascript" charset="utf-8">			

(function(){

	$('#forming-kpi-periode').change(function(){
		var optionSelected = $(this).find("option:selected");
		loadKpiForTable($('#forming-kpi-periode').val());
	});
	
	function loadKpiForTable(periode){
		$.ajax({
			type: 'post',
			data: {'periode' : periode },
			url : '<?php echo base_url();?>measurements/get_published_km',
			dataType: 'json',
			beforeSend: function(){
				$('#tbl-selected-kpi > tbody:last').empty();
			}
		})
		.done(function(response, textStatus, jqhr){
			if(response){
				var el = '';
				for(var i=0;i<response.length;i++){
					var btn = (response[i].is_published == 1) ? 'Penilaian Sudah Final' : '<button class="btn btn-publish-nilai" periode="'+response[i].periode+'" quarter="'+response[i].quarter+'"><i class=" icon-bullhorn"></i></button>';
					el += '<tr><td>'+ 1+i +'</td>' +
							 '<td>'+response[i].name+'</td>' +
							 '<td>'+response[i].periode+'</td>' +
							 '<td>'+parseFloat(response[i].nilai).toFixed(2)+'</td>' +
							 '<td>'+btn+'</td></tr>';
				}
				$('#tbl-selected-kpi > tbody:last').append(el);
			}
		})
		.fail(function(){
		
		});
	}
	
	$('#tbl-selected-kpi').delegate('.btn-publish-nilai', 'click', function(e){
		var r=confirm("Finalisasi Penilaian?");
		if (r==true){
			var quarter = $(this).attr('quarter');
			var periode = $(this).attr('periode');
			$.ajax({
				url: '<?php echo base_url();?>measurements/publish_prestasi',
				type: 'post',
				data: { 'quarter' : quarter, 'periode': periode}
			})
			.done(function(response, textStatus, jqhr){
				if(response.status == 'ok'){
					alert('Penilaian telah dipublish');
				} else if(response.status == 'closed'){
					alert('Periode input ditutup!');
				} 
				routes('measurements/publish','Finalisasi Penilaian');
			})
			.fail(function(){
			
			});
		}

		e.preventDefault();
	});	
}());

</script>
	
	