<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Penyusunan RKPP				
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('measurements/forming','Penyusunan')">RKPP</a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>RKPP
						</h4>
					</div>
					<div class="portlet-body">	
						<div class="clearfix">
							<div class="btn-group">
								
							</div>
							<div class="btn-group pull-right">
								<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="#">Print</a></li>
									<li><a href="#">Save as PDF</a></li>
									<li><a href="#">Export to Excel</a></li>
								</ul>
							</div>
						</div>
							Unit &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <select name="organization_id" id="forming-kpi-unit">										
										<option value="-1">--Pilih Unit--</value>
										<?php
											foreach($data as $row){	?>
												<option value="<?php echo $row->id;?>"><?php echo $row->name;?></value>
										<?php }
										?>
										
									</select><br/>
							<button type="button" id ="btn_kpi" class="btn blue span4"> Pilih KPI </button> <br/><br/>
							<form id="form-save-kpi">
							<table class="table table-bordered table-striped table-hover" id="tbl-selected-kpi">
								<thead>
									<tr>
										<th>ORDER NO</th>
										<th>INDIKATOR</th>
										<th>RENCANA PELAKSANAAN</th>								
										<th>KETERANGAN</th>						
										<th>AKSI</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
							</form>
						<center>
							<button class="btn blue" id="btn-save-selected-kpi" style="display:none"><i class="icon-save"></i> Simpan</button>
						</center>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->
</div>
<div class="modal" id="modal_kpi">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">KPI</h4>
      </div>
      <div class="modal-body">
		<form id="form-choose-kpi">
		<input type="hidden" id="choose-kpi-unit" name="choose-kpi-unit" />
		<table id="tbl-list-kpi" class="table table-bordered table-striped table-hover" >
			<thead>
				<tr>
					<td>Nama KPI</td>
					<td>Action</td>
				</tr>
			</thead>
		</table>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn blue" id="btn-save-choose-kpi"><i class="icon-save"></i> Simpan</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- END PAGE CONTAINER-->		
<script type="text/javascript" charset="utf-8">			
	$('#data_table').dataTable({
		"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ baris per halaman",
			"oPaginate": {
				"sPrevious": "Prev",
				"sNext": "Next"
			}
		},
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': [0]
		}]
	});
	
	$('.delete_data').click(function () { 
		var id =$(this).attr('data-id');
		new Messi('Yakin ingin menghapus data dengan ID '+id+' ?', {
						title: 'Confirmation', 
						titleClass: 'anim info', 
						closeButton: true,
						buttons: [
						{
							id: 0, 
							label: 'YES', 
							val: '1'
						},{
							id: 1, 
							label: 'NO', 
							val: '0'
						}
						], 
						callback:function(val)
						{ 
							if(val == 0) {
								return false;
							} else if(val == 1) {
								$.ajax({
									type: "POST",
									url: "<?php echo base_url();?>kpi/delete",
									data: "id="+id,
									success: function(msg){
										if(msg=='1') {
											new Messi('Data berhasil dihapus !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('kpi','Managemen KPI'); }});
										} else {
											new Messi('Data gagal dihapus !<br />Debug : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
										} 
									},
									error: function(fnc,msg){
										new Messi('Tidak dapat terhubung ke server untuk malakukan proses hapus data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
									}
								});
							} 
						}
					}
				);
	});
	
	var listTblKpi =  $('#tbl-list-kpi').dataTable({
      "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
      "sPaginationType": "bootstrap",
      "oLanguage": {
          "sLengthMenu": "_MENU_ baris per halaman",
          "oPaginate": {
              "sPrevious": "Prev",
              "sNext": "Next"
          }
      }
    });
	
$(function() {
  $('#modal_kpi').easyModal();
});	

$('#btn_kpi').click(function(){
  var unit = $('#forming-kpi-unit').val();
  
  $.ajax({
	url: '<?php echo base_url();?>measurements/get_rkpm',
	dataType: 'json',
	data: {'unit' : unit},
	type: 'post',
	beforeSend: function(){
		listTblKpi.fnClearTable();
	}
  })
  .done(function(response, textStatus, jqhr){
	if(response){
		$('#choose-kpi-unit').val(unit);
		for(var i=0;i<response.length;i++){
			listTblKpi.fnAddData([response[i].kpi, '<input type="checkbox" name="kpi-selected[]" value="'+ response[i].id +'" />']);
		}
		$('#modal_kpi').trigger('openModal');
	}		
  })
  .fail(function(){
  });
  
});


$('#btn-save-choose-kpi').click(function(){
	var unit = $('#forming-kpi-unit').val();
	var periode = $('#forming-kpi-periode').val();
	
	$.ajax({
		type: 'post',
		url: '<?php echo base_url();?>measurements/choose_rkpp',
		data: $('#form-choose-kpi').serialize(),
		dataType: 'json'
	})
	.done(function(response, textStatus, jqhr){
		if(response.status == 'ok'){
			$('#modal_kpi').trigger('closeModal');
		}
		loadKpiForTable(unit, periode);
	})
	.fail(function(){
	})
});

function loadKpiForTable(unit, periode){
	$.ajax({
		type: 'post',
		data: {'unit' : unit, 'periode' : periode},
		url : '<?php echo base_url();?>measurements/get_selected_rkpp',
		dataType: 'json',
		beforeSend: function(){
			$('#tbl-selected-kpi > tbody:last').empty();
			$('#btn-save-selected-kpi').hide();
		}
	})
	.done(function(response, textStatus, jqhr){
		if(response){
			for(var i=0;i<response.length;i++){
				var order_no = (response[i].order_no) ? response[i].order_no : i;
				var rowbefore = (response[i-1]) ? response[i-1].perspective : '';
				var el = (response[i].perspective != rowbefore) ? '<tr><td colspan="6">'+response[i].perspective+'</td></tr>' : '';
				el += '<tr><td width="50px"><input class="span12" type="text" value="'+order_no+'" name="rkpp-order-no[]"/></td><td>'+response[i].kpi+' <input type="hidden" name="rkpp-id[]" value="'+response[i].id+'" /></td><td width="50px"><input type="text" name="rkpp-rencana-pelaksanaan[]" value="'+response[i].rencana_pelaksanaan+'" /></td><td width="50px"><textarea rows="3" cols="6" class="input-kpi-keterangan" name="rkpp-keterangan[]">'+response[i].keterangan+'</textarea></td><td width="50px"><button class="btn red btn-delete-rkpp-selected" object="'+response[i].id+'"><i class="icon-trash"></i></button></td></tr>'
				$('#tbl-selected-kpi > tbody:last').append(el);
				$('#btn-save-selected-kpi').show();
			}
			//var total = '<tr><td colspan="3">Total</td><td><span id="input-total-bobot"></span></td><td id="input-total-target"></td><td></td></tr>'
			//$('#tbl-selected-kpi > tbody:last').append(total);
			//calculateTotal();
		}
	})
	.fail(function(){
	
	});
}

$('#forming-kpi-unit').change(function(){
    var optionSelected = $(this).find("option:selected");
	loadKpiForTable(optionSelected.val(), $('#forming-kpi-periode').val());
});

$('#forming-kpi-periode').change(function(){
	var optionSelected = $(this).find("option:selected");
	loadKpiForTable($('#forming-kpi-unit').val(), optionSelected.val());
});

$('#btn-save-selected-kpi').click(function(e){
	$.ajax({
		url: '<?php echo base_url();?>measurements/save_properties_rkpp',
		dataType: 'json',
		data: $('#form-save-kpi').serialize(),
		type: 'post'
	})
	.done(function(response, textStatus, jqhr){
		if(response.status == 'ok'){
			loadKpiForTable($('#forming-kpi-unit').val(), $('#forming-kpi-periode').val());
		}
	})
	.fail(function(){
	
	});
	
	e.preventDefault();
});

$('#tbl-selected-kpi').delegate('.btn-delete-kpi-selected', 'click', function(e){
	var r=confirm("Delete this item?");
	if (r==true){
		var id = $(this).attr('object');
		$.ajax({
			type: 'post',
			url: '<?php echo base_url();?>measurements/delete_rkpp_selected',
			data: {'id' : id},
			dataType: 'json'
		})
		.done(function(response, textStatus, jqhr){
			if(response.status == 'ok'){
				loadKpiForTable($('#forming-kpi-unit').val(), $('#forming-kpi-periode').val());
			} else {
				alert('Terjadi Kesalahan, Refresh Halaman!');
			}
		})
		.fail(function(){
		
		});
	}

	e.preventDefault();
});

function calculateTotal(){
	var bob = 0, tar = 0;
	$('.input-kpi-bobot').each(function(){
		bob += parseInt($(this).val());
	});
	
	$('.input-kpi-target').each(function(){
		tar += parseInt($(this).val());
	});
	
	$('#input-total-bobot').html(bob);
	$('#input-total-target').html(tar);
}

$('#tbl-selected-kpi').delegate('.input-kpi-bobot', 'keyup', function(){
	calculateTotal();
});

$('#tbl-selected-kpi').delegate('.input-kpi-target', 'keyup', function(){
	calculateTotal();
});

	//$('#modal-kpi').trigger('openModal');
</script>
	
	