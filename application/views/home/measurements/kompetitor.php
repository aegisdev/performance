<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Kompetitor	
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('measurements/kompetitor','Kompetitor')">Kompetitor</a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>
							Kompetitor
						</h4>
					</div>
					<div class="portlet-body">		
						<!-- <div class="clearfix">
							<div class="btn-group">
								<button class="btn blue" onClick="javascript:routes('menus/add','Tambah Menu')">
								<i class="icon-plus"></i> Tambah
								</button>
							</div>						
						</div> -->
						<table class="table table-bordered table-striped table-hover" id="data_table">
							<thead>
								<tr>
									<th width="3%">ID#</th>
									<th>TAHUN</th>
									<th>KPI</th>											
									<th>PERUSAHAAN TERBAIK</th>											
									<th>NILAI</th>											
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data as $row) { ?>
									<tr>
										<td><?php echo $row->id;?></td>
										<td><?php echo $row->tahun;?></td>
										<td><?php echo $row->kpi;?></td>
										<td><?php echo $row->perusahaan_terbaik;?></td>				
										<td class="center-column"><?php echo $row->nilai;?></td>				
									</tr>
									<?php } ?>			
							</tbody>
						</table>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
<script type="text/javascript" charset="utf-8">			
	$('#data_table').dataTable({
		"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ baris per halaman",
			"oPaginate": {
				"sPrevious": "Prev",
				"sNext": "Next"
			}
		},
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': [0]
		}]
	});
	
	$('.delete_data').click(function () { 
		var id =$(this).attr('data-id');
		new Messi('Yakin ingin menghapus data dengan ID '+id+' ?', {
						title: 'Confirmation', 
						titleClass: 'anim info', 
						closeButton: true,
						buttons: [
						{
							id: 0, 
							label: 'YES', 
							val: '1'
						},{
							id: 1, 
							label: 'NO', 
							val: '0'
						}
						], 
						callback:function(val)
						{ 
							if(val == 0) {
								return false;
							} else if(val == 1) {
								$.ajax({
									type: "POST",
									url: "<?php echo base_url();?>measurements/kompetitor_delete",
									data: "id="+id,
									success: function(msg){
										if(msg=='1') {
											new Messi('Data berhasil dihapus !', {title: 'Menu Message', titleClass: 'anim info', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('measurements/kompetitor','Kompetitor'); }});
										} else {
											new Messi('Data gagal dihapus !<br />Debug : '+msg, {title: 'Menu Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
										} 
									},
									error: function(fnc,msg){
										new Messi('Tidak dapat terhubung ke server untuk malakukan proses hapus data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
									}
								});
							} 
						}
					}
				);
	});
</script>
	
	