<style type="text/css">
	#modal_rkm_detail{
		top: 30%!important;
	}
</style>
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
		<!-- END BEGIN STYLE CUSTOMIZER -->   	
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
		<h3 class="page-title">
			Distribusi KM	
		</h3>
		<ul class="breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="<?php echo base_url();?>home">Beranda</a> 
				<i class="icon-angle-right"></i>
			</li>
			<li><a href="#" onClick="routes('measurements/rkm','Distribusi RKM')">Distribusi KM</a>					
			</li>			
		</ul>
		<!-- END PAGE TITLE & BREADCRUMB-->			
		<div class="portlet box green">
			<div class="portlet-title">
				<h4>
					<i class="icon-table"></i>Distribusi KM
				</h4>
			</div>
			<div class="portlet-body">	
				<table border="0">						
					<tr>
						<td width="100px">Periode</td>
						<td>
							<select name="year" id="rkm-periode">
								<option value="-1">-- Pilih Tahun --</value>
								<?php for($year=2013;$year <=(date('Y')+1);$year++){ ?>
								<option value="<?php echo $year;?>"><?php echo $year;?></option>
								<?php } ?>
							</select>
						</td>
					</tr>
				</table>
				<br/>	
				<form id="form-save-kpi">
					<table class="table table-bordered table-striped table-hover" id="table-rkm">
						<thead>
							<tr>
								<th width="40px">ORDER NO</th>
								<th width="200px">KPI</th>
								<th width="40px">SATUAN</th>
								<th width="40px">BOBOT</th>				
								<th width="40px">TARGET</th>	
								<th width="40px">PERIODE INPUT</th>
								<th>PEMILIK KPI</th>	
								<th>DIR. KONTRIBUTOR</th>	
								<th width="40px">AKSI</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</form>
				<center>
					<button class="btn blue" id="btn-save-selected-kpi" style="display:none"><i class="icon-save"></i> Simpan</button>
				</center>
				<div class="clearfix"></div>
			</div>
		</div>	
	</div>
	<!-- END PAGE HEADER-->

<div class="modal hide" id="modal_rkm_detail" style="width:800px;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			<h4 class="modal-title">Detail RKM</h4>
			</div>
			<div class="modal-body">
				<div>
					<button class="btn blue" id="btn-add-kontributor"><i class="icon-plus"></i> Unit</button>
					<span>Klik <strong>+ Unit</strong> untuk menambahkan unit pemilik/pendukung KPI.</span>
				</div>
				<div class="clear-fix"><br/></div>
				<form id="form-rkm-detail" style="border:1px solid #eee;padding:10px 0;">
					<input type="hidden" name="kpi-id" id="kpi_id"/>
					<input type="hidden" name="periode" id="periode"/>
					<ul id="root-detail-kontributor" style="list-style:none;">
					</ul>
					<div style="margin-left:25px">Periode Input Realisasi :	</br>		
						<select name="periode-input" id="periode-input">
							<option value="1">Per-Bulan</option>
							<option value="2">Per-Triwulan</option>
							<option value="3">Per-Semester</option>
							<option value="4">Per-Tahun</option>
						</select>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn blue" id="btn-rkm-save"><i class="icon-save"></i> Simpan</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal hide" id="modal_rkm_unit_pelaksana">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
				<form id="form-rkm-unit-pelaksana">		
					<table class="table table-bordered" id="table-rkm-unit-pelaksana">
						<thead>
							<tr>
								<td width="300px" class="center-column">Unit</td>
								<td width="80px" class="center-column">Bobot</td>					
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn blue" id="btn-save-unit-pelaksana"><i class="icon-save"></i> Simpan</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- END PAGE CONTAINER-->		
<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script type="text/javascript" charset="utf-8">		

	$(document).ready(function(){
		$('#rkm-periode').select2();
		// $('#table-rkm').dataTable({
		// 	"sPaginationType": "bootstrap",
		// 	"oLanguage": {
		// 		"sLengthMenu": "_MENU_ baris per halaman",
		// 		"oPaginate": {
		// 			"sPrevious": "Prev",
		// 			"sNext": "Next"
		// 		}
		// 	},
		// 	"aoColumnDefs": [{
		// 		'bSortable': false,
		// 		'aTargets': [0,1,2,3,4,5,6,7]
		// 	}]
		// });	
	});

	(function(){

		var str_global = '';
		var optionAllUnit = '';	
		$('#modal_rkm_detail').easyModal();
		$('#modal_rkm_unit_pelaksana').easyModal();

		$.ajax({
			type: 'get',
			url: "<?php echo base_url();?>rkm/get_organization",
			dataType: 'json'
		})
		.done(function(response, textStatus, jqhr){
			if(response){			
				var str='';
				for(var i=0;i<response.length;i++){					
					str+='<option value="'+response[i].id+'">'+response[i].name+'</option>';					
				}			
				str_global=str;
			}
		})
		.fail(function(){
		
		});

		$.ajax({
			type: 'get',
			url: "<?php echo base_url();?>rkm/get_all_unit",
			dataType: 'json'
		})
		.done(function(response, textStatus, jqhr){
			if(response){			
				var str='';
				for(var i=0;i<response.length;i++){					
					str+='<option value="'+response[i].id+'">'+response[i].name+'</option>';					
				}			
				optionAllUnit=str;
			}
		})
		.fail(function(){
		
		});

		$('#btn-add-kontributor').click(function(){
			var newLi = '<li>' +
						'<table><tr valign="top">' +
						'<td><select name="kpi-option-direktorat[]">'+ str_global +'</select></td>' +
						'<td width="100px">' + 
						'<select name="kpi-status-direktorat[]" class="kpi-status-direktorat">' + 
						'<option value="1">Pemilik KPI</option>' + 
						'<option value="2">Dir. Kontributor</option>' + 
						'</select></td>' +
						// '<td width="80"><input type="text" class="span12" placeholder="Bobot" name="kpi-bobot-direktorat[]"/></td>' +
						//'<td width="100px"><button class="btn blue btn-add-sub-unit"><i class="icon-plus"></i> Sub Unit</button></td>' +
						'<td><button class="btn red btn-delete-kontributor"><i class="icon-trash"> </i></button></td>' +
						'</tr></table>' + 
						'<ul style="list-style:none;"></ul></li>'; 
			$('#root-detail-kontributor').append(newLi);
		});

		$('#root-detail-kontributor').delegate('.btn-add-sub-unit', 'click', function(e){
			var parentId = $(this).parent().siblings(':first').children().find('option:selected').val();
			$(this).parent().siblings(':first').children().find('option:not(:selected)').attr('disabled', true);

			var newLi = '<li>' + 
						'<table><tr valign="top">' +
						'<td><select name="kpi-option-unit[]">'+optionAllUnit+'</select></td>' +
						// '<td width="80"><input type="text" class="span12" placeholder="Bobot" name="kpi-bobot-unit[]" /></td>' +
						//'<td width="100px"><button class="btn blue btn-add-sub-unit"><i class="icon-plus"></i> Sub Unit</button></td>' +
						'<td><input type="hidden" name="kpi-parent-unit[]" value="'+parentId+'" />' + 
						'<button class="btn red btn-delete-kontributor"><i class="icon-trash"> </i></button></td>' +
						'</tr></table><ul style="list-style:none;"></ul></li>';

			$(this).parent().parent().parent().parent().next().append(newLi);

			e.preventDefault();
		});

		$('#root-detail-kontributor').delegate('.btn-delete-kontributor', 'click', function(e){
			$(this).parent().parent().parent().parent().parent().remove();

			e.preventDefault();
		});

		function loadKontributor(kpiId, unit, periode){
			$.ajax({
				url: '<?php echo base_url();?>measurements/get_unit_kontributor',
				method: 'post',
				dataType: 'json',
				data: {'kpi-id' : kpiId, 'unit' : unit, 'periode' : periode}
			})
			.done(function(response, textStatus, jqhr){
				if(response){
					var el = '';
					for(var i=0;i<response.length;i++){

					}
				}
			})
			.fail(function(){

			});
		}


		$('#rkm-periode').change(function(){
			var optionSelected = $(this).find("option:selected");
			loadRkmTable(optionSelected.val());
		});

		$('#btn-rkm-save').click(function(e){
			var periode = $("#periode").val();

			$.ajax({
				type: 'post',
				url: '<?php echo base_url();?>rkm/save_rkm_detail',
				data: $('#form-rkm-detail').serialize(),
				dataType: 'json',
				beforeSend: function(){
					//check only one directorate should be pemilik kpi
					if(checkIsPemilikIsOne()){
						return true;
					} else {
						alert('Data gagal disimpan, Pastikan Pemilik KPI hanya satu Direktorat!');
						return false;
					}
				}
			})
			.done(function(response, textStatus, jqhr){
				if(response.status == 'ok'){				
					loadRkmTable(periode);
				} else {
					$.growl.error({ title: "Peringatan", message: "Data gagal disimpan!" });
				}
				$('#modal_rkm_detail').trigger('closeModal');
			})
			.fail(function(){

			});

			e.preventDefault();
		});

		function loadRkmTable(periode){
			$.ajax({
				type: 'post',
				data: {'periode' : periode },
				url : '<?php echo base_url();?>rkm/get_rkm_by_periode',
				dataType: 'json',
				beforeSend: function(){				
					$('#table-rkm > tbody').empty();
					$('#btn-save-selected-kpi').hide();
				}
			})
			.done(function(response, textStatus, jqhr){
				if(response.length > 0){
					var totalBobot = 0;
					var labelPeriode = '';
					for(var i=0;i<response.length;i++){				
						if(typeof response[i][0]=='undefined'){
							var pemilik_kpi="";
						}else{
							var pemilik_kpi=response[i][0];
						}
						if(typeof response[i][1]=='undefined'){
							var pelaksana_kpi="";
						}else{
							var pelaksana_kpi=response[i][1];
						}
						
						switch(response[i].periode_input){
							case '1':
								labelPeriode = 'Per-Bulan';
							break;
							case '2':
								labelPeriode = 'Per-Triwulan';
							break;
							case '3':
								labelPeriode = 'Per-Semester';
							break;
							default:
								labelPeriode = 'Per-Tahun';
							break;
						}

						var rowbefore = (response[i-1]) ? response[i-1].perspective : '';
						var pencapaian_sebelumnya = (response[i].pencapaian_sebelumnya) ? response[i].pencapaian_sebelumnya : 0;
						var el = (response[i].perspective != rowbefore) ? '<tr><td colspan="10"><strong>'+response[i].perspective+'</strong></td></tr>' : '';
						
						el += '<tr><td>'+(i+1)+'</td>'+
						'<td>'+response[i].kpi+'<input type="hidden" name="kpi-id[]" value="'+response[i].kpi_id+'" /></td>' + 
						'<td>'+response[i].satuan+'</td>' + 
						'<td>'+response[i].bobot+'</td>' + 
						'<td>'+response[i].target+'</td>' +
						'<td>'+labelPeriode+'</td>'+ 
						'<td>'+pemilik_kpi+'</td>' + 
						'<td>'+pelaksana_kpi+'</td>' + 
						'<td><button class="btn blue btn-rkm-detail" rkm-id="'+response[i].id+'" rkm-periode="'+ periode +'" rkm-kpi-id="'+ response[i].kpi_id +'">Detail</button></td></tr>';
						
						$('#table-rkm > tbody:last').append(el);		

						totalBobot += response[i].bobot * 1;	
					}

					var total = '<tr><td colspan="3">Total</td><td><span id="input-total-bobot">'+totalBobot+'</span></td><td colspan="6"></td></tr>';
					total += '<input type="hidden" name="kpi-periode" value="'+periode+'" />';

					$('#table-rkm > tbody:last').append(total);
				} else {
					$('#table-rkm > tbody:last').empty('');
				}
			})
			.fail(function(){
			
			});
		}
			
		$('#table-rkm').delegate('.btn-rkm-detail', 'click', function(e){	
			var kpi_id=$(this).attr('rkm-kpi-id');
			var periode=$(this).attr('rkm-periode');
			
			$("#kpi_id").val(kpi_id);
			$("#periode").val(periode);
			
			var el='<button type="button" name="btn-rkm-tambah" id="btn-rkm-tambah" class="btn blue btn-rkm-tambah">Tambah</button>';		
			
			$.ajax({
				url: '<?php echo base_url();?>rkm/get_detail_rkm',
				type: 'post',
				dataType: 'json',
				data: {'kpi-id': kpi_id, 'periode': periode},
				beforeSend: function(){
					$('#root-detail-kontributor').empty();
				} 
			})
			.done(function(response, textStatus, jqhr){
				if(response){
					var per = (response.a[0]) ? response.a[0] : 1;

					var eskimo = build_tree(response.a, 0, true);

					$('#periode-input').val(per);
					$('#modal_rkm_detail').trigger('openModal');	
					$('#root-detail-kontributor').append(eskimo);
				} 
			})
			.fail(function(){

			});

			e.preventDefault();
		});

		function build_tree(collections, idx, isRoot){
			var ch = findChilds(idx, collections);
			var el = '';


			if(ch.length == 0){
				return ''; 
			} else {
				for(var i=0;i<ch.length;i++){
					el += '<li>';
					var sel1 = "";
					var sel2 = "";
					if(ch[i].role == "1"){
						sel1 = "selected";
					} else {
						sel2 = "selected";
					}

					if(isRoot){
						el += '<table><tr valign="top">' +
							'<td><select name="kpi-option-direktorat[]">'+
							'<option value="'+ch[i].unit_id+'">'+ch[i].name+'</option>' +
							'</select></td>' +
							'<td width="100px"><select name="kpi-status-direktorat[]" class="kpi-status-direktorat">' + 
							'<option value="1" '+sel1+' >Pemilik KPI</option>' + 
							'<option value="2" '+sel2+'>Dir. Kontributor</option></select></td>' +
							'<td width="80">' + 
							// '<input type="text" value="'+ch[i].bobot+'" class="span12" placeholder="Bobot" name="kpi-bobot-direktorat[]"/></td>' +
							//'<td width="100px"><button class="btn blue btn-add-sub-unit"><i class="icon-plus"></i> Sub Unit</button></td>' +
							'<td><button class="btn red btn-delete-kontributor"><i class="icon-trash"> </i></button></td>' +
							'</tr></table>' +
						 	'<ul style="list-style:none;">' + build_tree(collections, ch[i].unit_id, false) + '</ul>';
					} else {
						el += '<table><tr valign="top">' +
							'<td><select name="kpi-option-unit[]">' + 
							'<option value="'+ch[i].unit_id+'">'+ch[i].name+'</option>' +
							'</select></td>' +
							'<td width="80">' + 
							// '<input type="text" value="'+ch[i].bobot +'" class="span12" placeholder="Bobot" name="kpi-bobot-unit[]" /></td>' +
							//'<td width="100px"><button class="btn blue btn-add-sub-unit"><i class="icon-plus"></i> Sub Unit</button></td>' +
							'<td><input type="hidden" name="kpi-parent-unit[]" value="'+ch[i].parent_unit_id+'" />' + 
							'<button class="btn red btn-delete-kontributor"><i class="icon-trash"> </i></button></td>' +
							'</tr></table><ul style="list-style:none;">' + build_tree(collections, ch[i].unit_id, false) + 
							'</ul>';
					} 
					el += '</li>';	
				}
				
				return el;		
			}			
	 	}

	 	function findChilds(idx, collections){
	 		var newColl = [];
			for(var i=0;i<collections.length;i++){
				if(collections[i].parent_unit_id == idx){
					newColl.push(collections[i]);
				}
			}
			return newColl;
	 	}

		function loadOrganization(){
			$.ajax({
				type: 'get',
				data: {parent_id:parent_id},
				url: "<?php echo base_url();?>rkm/get_organization",
				dataType: 'json'
			})
			.done(function(response, textStatus, jqhr){
				if(response){			
					var str='';
					for(var i=0;i<response.length;i++){					
						str+='<option value="'+response[i].id+'">'+response[i].name+'</option>';					
					}			
					str_global = str;
				}
			})
			.fail(function(){
			
			});
		}

		// $('#table-rkm-detail').delegate('.btn-rkm-unit-pelaksana', 'click', function(e){	
		// 	//$('#table-rkm-unit-pelaksana').treetable({ expandable: true })
		// 	$('#modal_rkm_unit_pelaksana').trigger('openModal');
		// 	$.ajax({
		// 		type: 'get',
		// 		url: "<?php echo base_url();?>rkm/get_tree",
		// 		dataType: 'json'
		// 	})
		// 	.done(function(response, textStatus, jqhr){
		// 		if(response){		
		// 			$('#table-rkm-unit-pelaksana > tbody:last').append(response);	
		// 		}
		// 	})
		// 	.fail(function(){
			
		// 	});
		// 	e.preventDefault();
			
			
		// 	e.preventDefault();
		// });

		// var index=1;

		// $('#form-rkm-detail').delegate('#btn-rkm-tambah','click',function(){			
		// 	var el=	'<tr id="div-rkm-tambah-'+index+'">'+
		// 			'<td><button class="btn red btn-pemilik-delete" index="'+index+'"><i class="icon-trash"></i> </button></td>'+
		// 			'<td><select name="direktorat[]" id="direktorat-'+index+'" class="span12 m-wrap direktorat"></select></td>'+
		// 			'<td><select name="peranan[]" id="peranan-'+index+'" class="span12 m-wrap peranan">'+
		// 			'<option value="1">Pemilik KPI</option>'+
		// 			'<option value="2">Direktorat Pelaksana</option>'+
		// 			'</select></td>'+
		// 			'<td class="center-column"><input type="text" name="bobot[]" class="span8 m-wrap"></td>'+
		// 			'<td><button class="btn blue btn-rkm-unit-pelaksana"><i class="icon-plus"></i> Unit</button></td>'+
		// 			'</tr>';
		// 	$('#table-rkm-detail > tbody:last').append(el);
		// 	$("#total-rkm-detail").val(index);	
		// 	$("#direktorat-"+index).html(str_global);
		// 	index++;
		// });
		
		// $('#table-rkm-detail').delegate('.btn-pemilik-delete','click',function(){
		// 	var index=$(this).attr('index');
		// 	$("#div-rkm-tambah-"+index).fadeOut(100).remove();
		// });

	}());

	// $('#table-rkm-unit-pelaksana').treetable({ expandable: true });
	// $("#table-rkm-unit-pelaksana tbody").on("mousedown", "tr", function() {
	//   $(".selected").not(this).removeClass("selected");
	//   $(this).toggleClass("selected");
	// });

	function checkIsPemilikIsOne(){
		isPemilik = 0;

		$('.kpi-status-direktorat').each(function(i,obj){
			if($(this).val() == 1) isPemilik++;
		});

		return (isPemilik > 1) ? false : true;
	}

</script>
	
	