<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Managemen Organisasi		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('organizations','Daftar Organisasi')">Organisasi</a> 		
					<i class="icon-angle-right"></i>	
				</li>
				<li><a href="#" onClick="routes('organization/add','Tambah Organisasi ')"> Tambah</a> </li>						
			</ul>
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-edit"></i>
						<span class="hidden-480">Tambah Organisasi</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">
									<div class="control-group">
										  <label class="control-label">Parent Organisasi</label>
										  <div class="controls">
											 <select name="parent_id" id="parent_id">
												<option value="0">Root</option>
												<?php foreach($data as $row) {?>
													<option value="<?php echo $row->id;?>"><?php echo $row->name;?></option>
												<?php }?>
											 </select>
										  </div>
									 </div>
									  <div class="control-group">
										  <label class="control-label">Nama Organisasi</label>
										  <div class="controls">
											 <input name="organization" id="organization" type="text" value="" class="span6 m-wrap"/>
										  </div>
									 </div> 
									 <div class="control-group">
										  <label class="control-label">No. Order</label>
										  <div class="controls">
											 <input name="order_no" id="order_no" type="text" value="" class="span6 m-wrap"/>
										  </div>
									 </div> 
									 <div class="control-group">
										  <label class="control-label">Nama Pejabat</label>
										  <div class="controls">
											 <input name="official_name" id="official_name" type="text" value="" class="span6 m-wrap"/>
										  </div>
									 </div> 
									 <div class="control-group">
										  <label class="control-label">NIK</label>
										  <div class="controls">
											 <input name="nik" id="nik" type="text" value="" class="span6 m-wrap"/>
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Jabatan</label>
										  <div class="controls">
											 <input name="position" id="position" type="text" value="" class="span6 m-wrap"/>
										  </div>
									 </div>
									 
									 <div class="control-group">
										<div class="controls">							
											<button class="btn blue" type="button" onClick="save()"><i class="icon-save"></i> Simpan</button>
											<button class="btn blue" type="button" onClick="routes('organizations','Daftar Organisasi')"><i class="icon-share"></i> Kembali</button>
										</div>
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
<script language="javascript">
	function validate() {
		if ($('#organization').val() =='') {
			new Messi('Organization masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("organization").focus(); }});
			return false;
		}
		if ($('#order_no').val() =='') {
			new Messi('No order masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("order_no").focus(); }});
			return false;
		}
		if ($('#official_name').val() =='') {
			new Messi('Nama pejabat masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("official_name").focus(); }});
			return false;
		}
		if ($('#nik').val() =='') {
			new Messi('NIK masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("nik").focus();  }});
			return false;
		}
		if ($('#position').val() =='') {
			new Messi('Jabatan masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("position").focus();  }});
			return false;
		}
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>organizations/save",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil disimpan !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('organizations','Managemen Organisasi'); }});
					} else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
</script>