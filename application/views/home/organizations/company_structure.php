 <link rel="stylesheet" href="<?=base_url();?>assets/autocomplete/docsupport/style.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/autocomplete/docsupport/prism.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/autocomplete/chosen.css">
  <style type="text/css" media="all">
    .chosen-rtl .chosen-drop { left: -9000px; }
  </style>

  <div class="row-fluid">
    <div class="span12">
		<h3 class="page-title">
				Struktur Organisasi			
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="#" onClick="routes('organizations/all','Struktur Organisasi')">Struktur Organisasi</a> 	
				</li>										
			</ul>
        <!-- BEGIN BASIC PORTLET-->
        <div class="portlet box green">
			<div class="portlet-title">
					<h4>
						<i class="icon-open-eye"></i>
						<span class="hidden-480">Struktur Organisasi</span>
					</h4>
				</div>        
            <div class="portlet-body">
                <div class="clearfix">
                  <div class="btn-group">
                      <button id="btn-add-company-structure" class="btn blue">
                        <i class="icon-plus"></i> Tambah
                      </button>
                  </div>
                </div>
                <div class="space15"></div>
                <div style="max-width:100%;overflow-x:scroll;"><div class="hide"><?php echo $tree; ?></div>
                <div id="orgchart-container" style="max-width:100%"></div></div>
            </div>
        </div>
        <!-- END BASIC PORTLET-->
    </div>
</div>

<div class="modal" id="modal-add-company-structure">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Tambah Struktur Organisasi</h4>
      </div>
      <div class="modal-body" style="height:800px;">
        <form id="form-add-company-structure" class="form-horizontal">
          <div class="control-group">
              <label class="control-label">Nama</label>
              <div class="controls">
                  <input type="text" class="span6 " name="name"/>
                  <span class="help-inline"></span>
              </div>
          </div>			 
          <div class="control-group">
              <label class="control-label">Pimpinan</label>
              <div class="controls">
                  <input type="text" class="span6 " name="official-name" />
                  <span class="help-inline"></span>
              </div>
          </div>
		  <div class="control-group">
			  <label class="control-label">NIK</label>
			  <div class="controls">
				 <input name="nik" id="nik" type="text" value="" class="span6 m-wrap"/>
			  </div>
		 </div>
		 <div class="control-group">
			  <label class="control-label">Jabatan</label>
			  <div class="controls">
				 <input name="position" id="position" type="text" value="" class="span6 m-wrap"/>
			  </div>
		 </div>
          <div class="control-group">
              <label class="control-label">Parent Name</label>
              <div class="controls">
                  <select name="parent" class="span8" id="parent">
                    <?php foreach($parent as $b){
                      echo "<option value=\"".$b['id']."\">". $b['name'] ."</option>";
                      }
                    ?>
                  </select>
                  <span class="help-inline"></span>
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn blue" id="btn-save-changes-company-structure"><i class="icon-save"></i> Simpan</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal" id="modal-edit-company-structure">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Ubah Struktur Organisasi</h4>
      </div>
      <div class="modal-body" style="height:800px;">
        <form class="form-horizontal" id="form-edit-company-structure">
          <div class="control-group">
              <label class="control-label">Nama</label>
              <div class="controls">
                  <input type="text" class="span6 " id="edit-company-structure-name" name="edit-name" />
                  <span class="help-inline"></span>
              </div>
          </div>
          <div class="control-group">
              <label class="control-label">Pimpinan</label>
              <div class="controls">
                  <input type="text" class="span6 " id="edit-company-structure-official-name" name="edit-official-name" />
                  <span class="help-inline"></span>
              </div>
          </div>
		   <div class="control-group">
			  <label class="control-label">NIK</label>
			  <div class="controls">
				 <input name="edit-nik" id="edit-nik" type="text" class="span6 m-wrap"/>
			  </div>
		 </div>
		 <div class="control-group">
			  <label class="control-label">Jabatan</label>
			  <div class="controls">
				 <input name="edit-position" id="edit-position" type="text" class="span6 m-wrap"/>
			  </div>
		 </div>
          <div class="control-group">
              <label class="control-label">Parent Name</label>
              <div class="controls">
                  <select name="edit-parent" class="span8" id="edit-parent">
                    <option value="0">Root</option>
                    <?php foreach($parent as $b){
                      echo "<option value=\"".$b['id']."\">". $b['name'] ."</option>";
                      }
                    ?>
                  </select>
                  <span class="help-inline"></span>
              </div>
          </div>
          <input type="hidden" name="id" id="edit-company-structure-id" />
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn blue" id="btn-edit-changes-company-structure"><i class="icon-save"></i> Simpan</button>
        <button type="button" class="btn blue" id="btn-delete-changes-company-structure"><i class="icon-trash"></i> Hapus</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!--<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>-->
<!--<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>-->

<script src="<?php echo base_url();?>assets/autocomplete/chosen.jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/autocomplete/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>	
<script language="javascript">
$(document).ready(function(){
	$("#parent").chosen();
  $("#edit-parent").chosen();

  //$("#parent").select2();  
	//$("#edit-parent").select2();  
});

$('#org-chart').orgChart({
  container: $('#orgchart-container'),
  nodeClicked: function($node){
    var id = $node.data('id');
    $.ajax({
      url: '<?php echo base_url();?>organizations/id/' + id,
      dataType: 'json'
    })
    .done(function(response, textStatus, jqhr){
      if(response.status == "ok"){
        $('#edit-company-structure-id').val(response.data.id);
        $('#edit-company-structure-name').val(response.data.name);
        $('#edit-company-structure-official-name').val(response.data.official_name);
        $('#edit-nik').val(response.data.nik);
        $('#edit-position').val(response.data.position);
        $('#edit-parent').val(response.data.parent_id);
        $('#btn-delete-changes-company-structure').attr('object',response.data.id);
      }
    })
    .fail(function(){

    });

    $('#modal-edit-company-structure').trigger('openModal');
  }
});



$(function() {
  $('#modal-add-company-structure').easyModal();
  $('#modal-edit-company-structure').easyModal();
});

$('#btn-add-company-structure').click(function(){
  $('#modal-add-company-structure').trigger('openModal');
});


$('#btn-save-changes-company-structure').click(function(){
  $.ajax({
    url: '<?php echo base_url();?>organizations/add',
    type: 'POST',
    data: $('#form-add-company-structure').serialize(),
    dataType: 'json'
  })
  .done(function(response, textStatus, jqhr){
    listCompanyStructure();
    $('#modal-add-company-structure').trigger('closeModal');
  })
  .fail(function(){

  });
});

$('#btn-edit-changes-company-structure').click(function(){
  $.ajax({
    url: '<?php base_url();?>organizations/edit',
    type: 'POST',
    dataType: 'json',
    data: $('#form-edit-company-structure').serialize()
  })
  .done(function(response, textStatus, jqhr){
    listCompanyStructure();
    $('#modal-edit-company-structure').trigger('closeModal');
     $('.select2-drop,.select2-drop-above,.select2-drop-active').css('z-index','999999');
  })
  .fail(function(){

  });
});

$('#btn-delete-changes-company-structure').click(function(){
  var r=confirm("Delete this item?");
  if (r==true){
    var id = $(this).attr('object');
    $.ajax({
      url: '<?php echo base_url();?>organizations/delete',
      data: {'id' : id},
      type: 'POST'
    })
    .done(function(response, textStatus, jqhr){
      $('#modal-edit-company-structure').trigger('closeModal');
      listCompanyStructure();
    })
    .fail(function(){

    });
  }  
});
 function listCompanyStructure(){
	$.ajax({
	  url: '<?php echo base_url();?>organizations/all'
	})
	.done(function(response, textStatus, jqhr){
	  routes('organizations/all','Company Struture');
	  $('#parameter-company-structure').html(response);
	}) 
	.fail(function(){

	}); 
  }
  function initView(idToShow, docTitle){
	$('.page-content').hide();
	$('#' + idToShow).show();
	document.title = docTitle;
  }
</script>