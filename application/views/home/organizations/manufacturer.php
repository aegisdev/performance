<div class="row-fluid">
    <div class="span12">
        <!-- BEGIN BASIC PORTLET-->
        <div class="widget blue">
            <div class="widget-title">
                <h4>List Manufacturer</h4>
            </div>
            <div class="widget-body">
                <div class="clearfix">
                  <div class="btn-group">
                      <button id="btn-add-manufacturer" class="btn green">
                        Add New <i class="icon-plus"></i>
                      </button>
                  </div>
                  <div class="btn-group pull-right">
                      <button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
                      </button>
                      <ul class="dropdown-menu pull-right">
                          <li><a href="#">Print</a></li>
                          <li><a href="#">Save as PDF</a></li>
                          <li><a href="#">Export to Excel</a></li>
                      </ul>
                  </div>
                </div>
                <div class="space15"></div>
                <table class="table table-striped table-bordered" id="tbl-manufacturer">
                    <thead>
                    <tr>
                        <th width="100px">Id</th>
                        <th width="100px">Name</th>
                        <th>Description</th>
                        <th width="200px">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($manufacturer as $row) { ?>
                    <tr>
                        <td><?php echo $row['id'];?></td>
                        <td><?php echo $row['name'];?></td>
                        <td><?php echo $row['description'];?></td>
                        <td class="center-column"> 
                            <button class="btn btn-primary btn-edit-item" object="<?php echo $row['id'];?>"><i class="icon-pencil"></i></button>
                            <button class="btn btn-danger btn-delete-item" object="<?php echo $row['id'];?>"><i class="icon-trash "></i></button>
                        </td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END BASIC PORTLET-->
    </div>
</div>

<div class="modal" id="modal-add-manufacturer">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Add Manufacturer</h4>
      </div>
      <div class="modal-body">
        <form id="form-add-manufacturer" class="form-horizontal">
          <div class="control-group">
              <label class="control-label">Name</label>
              <div class="controls">
                  <input type="text" class="span6 " name="name"/>
                  <span class="help-inline"></span>
              </div>
          </div>
          <div class="control-group">
              <label class="control-label">Description</label>
              <div class="controls">
                  <input type="text" class="span6 " name="description"/>
                  <span class="help-inline"></span>
              </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn-save-changes-manufacturer">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal" id="modal-edit-manufacturer">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Edit Manufacturer</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="form-edit-manufacturer">
          <div class="control-group">
              <label class="control-label">Name</label>
              <div class="controls">
                  <input type="text" class="span6 " id="edit-manufacturer-name" name="edit-name" />
                  <span class="help-inline"></span>
              </div>
          </div>
          <div class="control-group">
              <label class="control-label">Description</label>
              <div class="controls">
                  <input type="text" class="span6 " id="edit-manufacturer-description" name="edit-description" />
                  <span class="help-inline"></span>
              </div>
          </div>
          <input type="hidden" name="id" id="edit-manufacturer-id" />
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn-edit-changes-manufacturer">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

$('#tbl-manufacturer').dataTable({
  "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
  "sPaginationType": "bootstrap",
  "oLanguage": {
      "sLengthMenu": "_MENU_ baris per halaman",
      "oPaginate": {
          "sPrevious": "Prev",
          "sNext": "Next"
      }
  },
  "aoColumnDefs": [{
      'bSortable': false,
      'aTargets': [3]
  }]
});


$(function() {
  $('#modal-add-manufacturer').easyModal();
  $('#modal-edit-manufacturer').easyModal();
});

$('#btn-add-manufacturer').click(function(){
  $('#modal-add-manufacturer').trigger('openModal');
});


$('#btn-save-changes-manufacturer').click(function(){
  $.ajax({
    url: '<?php echo base_url();?>manufacturer/add',
    type: 'POST',
    data: $('#form-add-manufacturer').serialize(),
    dataType: 'json'
  })
  .done(function(response, textStatus, jqhr){
    listManufacturer();
    $('#modal-add-manufacturer').trigger('closeModal');
  })
  .fail(function(){

  });
});

$('.btn-edit-item').click(function(){
  var id = $(this).attr('object');
  $.ajax({
    url: '<?php echo base_url();?>manufacturer/id/' + id,
    dataType: 'json'
  })
  .done(function(response, textStatus, jqhr){
    if(response.status == "ok"){
      $('#edit-manufacturer-id').val(response.data.id);
      $('#edit-manufacturer-name').val(response.data.name);
      $("#edit-manufacturer-description").val(response.data.description);
    }
  })
  .fail(function(){

  });

  $('#modal-edit-manufacturer').trigger('openModal');
});

$('#btn-edit-changes-manufacturer').click(function(){
  $.ajax({
    url: '<?php base_url();?>manufacturer/edit/',
    type: 'POST',
    dataType: 'json',
    data: $('#form-edit-manufacturer').serialize()
  })
  .done(function(response, textStatus, jqhr){
    listManufacturer();
    $('#modal-edit-manufacturer').trigger('closeModal');
  })
  .fail(function(){

  });
});

$('.btn-delete-item').click(function(){
  var r=confirm("Delete this item?");
  if (r==true){
    var id = $(this).attr('object');
    $.ajax({
      url: '<?php echo base_url();?>manufacturer/delete',
      data: {'id' : id},
      type: 'POST'
    })
    .done(function(response, textStatus, jqhr){
      listManufacturer();
    })
    .fail(function(){

    });
  }  
});



</script>