<div id="cont-right">
	<div id="box-detaildoc">
		<div class="doctopnav">
			<a href="<?php echo base_url();?>home"> &nbsp;Kembali ke halaman sebelumnya</a>
		</div>
		<div class="doctitle"><?php echo $document[0]->TITLE;?></div>
		<div class="userauthor">
			<div class="userpic">
				<?php
					//Punya foto
					if(!empty($document_foto[0]->FOTO))
					{
						$image="archieves/".$document_foto[0]->FOTO;
					}
					else //Foto default
					{
						$image="assets/img/th-user.png";				
					}		
				?>
				<img src="<?php echo base_url().$image;?>" style="width:47px;height:47px">
			</div>
			<div class="userdocinfo">				
				<p>Penulis: <a class="author" href="#" id="popup_employee" rel="popover" data-content="<?php echo $employee;?>" data-original-title="Detail Data Pegawai">
					<?php echo $document[0]->COMPOSER_NAMA." / ". $document[0]->COMPOSER_NPP;?></a></p>
				<p>Posisi: <?php echo $document[0]->COMPOSER_POSITION_NAME;?></p>
				<p>Organisasi: <?php echo $document[0]->COMPOSER_ORG_NAME;?></p>
			</div>
			<div class="userdocinfo2">					
				<p>Tanggal publikasi: <?php echo $document[0]->TGL_PUBLISH;?></p>
				<p>Jenis dokumen: <?php echo $document[0]->DOCUMENT_TYPE;?></p>
				<p>Kategori kompetensi: <?php echo $document[0]->SUB_CATEGORY;?></p>
				<p>Tipe Kontribusi: <?php echo $document[0]->CONTRIBUTION_TYPE;?></p>				
			</div>
			<div class="wipe"></div>
		</div>                    
		<div class="detaildoc">
			<p><?php echo $document[0]->ABSTRACT;?></p>
		</div>
		
		<div class="doc-stat">
			<ul>
				<li>
					<a href="#">
						<span class="count">
							<?php echo $total_download;?>
						</span>
						<br>DIUNDUH
					</a>
				</li>
				<li>
					<a href="#">
						<span class="count">
							<?php echo $total_feedback;?>
						</span>
						<br>KOMENTAR
					</a>
				</li>
				<li>
					<a href="#">
						<span class="count">
							<?php echo $total_rate;?>
						</span>
						<br>FAVORIT
					</a>
				</li>			
			</ul>		
			<div class="wipe"></div>
		</div>
						
			<div class="doc-download">
				<table width="100px">
					<tr>
					<?php if(!empty($document[0]->ATTACHMENT))
						{ 
							$path="archieves/".$document[0]->ATTACHMENT;
							$exp=explode('.',$path);
							$type=end($exp);
						?>
						<td>
							<?php if($type=='mp4'){ ?>		
								<a class="btn_download open-modal" href="#" id="btn_download_video" path="<?php echo $path; ?>">									
								<span class="btnicon_download"></span>Unduh				
								</a>
							<?php } else {?>
								<a class="btn_download" id="btn_download" href="<?php echo base_url().$path;?>" download-document-id="<?php echo $document[0]->ID;?>" target="_blank">									
								<span class="btnicon_download"></span>Unduh				
								</a>
							<?php } ?>
						</td>
					<?php } ?>
					<?php 
						//Draft atau return
						if($document[0]->STATUS==1 || $document[0]->STATUS==3)
						{ 
							$path="archieves/".$document[0]->ATTACHMENT;
						?>
						
							<td>						
							<button type="button" class="btn btn-warning" id="btn_edit" edit-document-id="<?php echo $document[0]->ID;?>" style="width:100px" ><i class="icon-edit icon-white"></i> Ubah</button>	
							</td>
						</td>
					<?php }?>
					</tr>
				</table>
				</div>
				
		
				
			
		<div class="doc-comms">
			<div class="doccomm-user">			
				DOKUMEN HISTORY
			</div>
			<div class="wipe"></div>
		</div>
		<?php foreach($history as $h)
		{
			
		?>
		<div class="doc-comms userdocinfo">
			<div class="doccomm-user">		
			
				<div class="commdatetime">
					<?php echo $h->TGL_HISTORY;?>
				</div>
				<div class="wipe"></div>
				<p><?php echo $h->PESAN;?></p>							
			</div>
			<div class="wipe"></div>
		</div>
		<?php } ?>                   
		
		<div class="wipe"></div>
		<div class="doc-allcomm">
			<!-- <a href="#">Lihat semua komentar</a> -->
		</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$("[rel='tooltip']").tooltip();
	$("#popup_employee").popover({ width:800,html : true });
});
	
	$('#btn_download').click(function(ev){
		var id=$(this).attr('download-document-id');
		var path=$(this).attr('href');
		ev.preventDefault();		
		$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>documents/download",
				data: {document_id:id},
				success: function(msg)
				{
					loadRightContent('<?php echo base_url();?>documents/show/'+id);
					window.open(path,'_blank');
				},
				error: function(fnc,msg)
				{
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
	});
	
	$('#btn_edit').click(function(ev){
		var id=$(this).attr('edit-document-id');	
		ev.preventDefault();	
		$.ajax({				
			url: '<?php echo base_url(); ?>' + 'documents/edit/' + id,
			success: function(response){
				$('#cont-right').html(response);
			},
			error: function(e){

			},
			complete: function(){

			}
		});
	})
	$('#btn_download_video').click(function(ev){
		ev.preventDefault();
		var path=$(this).attr('path');
		$.ajax({
			type: "POST",
			data:{path:path},
			url: '<?php echo base_url(); ?>' + 'documents/download_video/',
			success: function(response){
				$('#myModalLabel').html('');
				$('#main-modal-body').html(response);
			},
			error: function(e){
				alert(e);
			},
			complete: function(){

			}
		});	
	});
	
</script>