<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap-tree/bootstrap-tree/css/bootstrap-tree.css" />
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Struktur Organisasi			
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('organizations','Struktur Organisasi')">Struktur Organisasi</a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->		

							
					<div class="portlet-body">						
						<div class="portlet box green">
							<div class="portlet-title">
								<h4><i class="icon-cloud-download"></i>Struktur Organisasi</h4>
								<!-- <div class="actions">
									<a href="javascript:;" id="tree_2_collapse" class="btn blue"> Collapse All</a>
									<a href="javascript:;" id="tree_2_expand" class="btn yellow"> Expand All</a>
								</div> -->
							</div>
							<div class="portlet-body fuelux">
								<div class="clearfix">
									<div class="btn-group">
										<button class="btn blue" onClick="javascript:routes('organizations/add','Tambah Organisasi')">
										<i class="icon-plus"></i> Tambah
										</button>
									</div>
									<div class="btn-group pull-right">
										<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
										</button>
										<ul class="dropdown-menu">
											<li><a href="#">Print</a></li>
											<li><a href="#">Save as PDF</a></li>
											<li><a href="#">Export to Excel</a></li>
										</ul>
									</div>
								</div>
								<ul class="tree" id="tree_2">
									<li>
										<a href="#" data-role="branch" class="tree-toggle" data-toggle="branch" data-value="Bootstrap_Tree">Root
										</a>
										<ul class="branch in">
											<li><a href="#" data-role="leaf" id="nut"><i class="icon-suitcase"></i> Sejarah Organisasi </a></li>
											<li><a href="#" data-role="leaf"><i class="icon-plane"></i> Visi dan Misi</a></li>
											<li><a href="#" data-role="leaf"><i class="icon-tasks"></i> Struktur Organisasi</a></li>													
											<li><a href="assets/bootstrap-tree/jsonexample.json" data-role="branch" class="tree-toggle closed" data-toggle="branch" data-value="JSON_Example">Informasi Lain</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</div>
					
						<div class="clearfix"></div>
					</div>
			
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
<script src="<?php echo base_url();?>assets/bootstrap-tree/bootstrap-tree/js/bootstrap-tree.js"></script>	