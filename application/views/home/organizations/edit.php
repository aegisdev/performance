<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				User Management		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" data-toggle="tooltip" rel="tooltip" data-placement="top" title="Klik untuk melihat menu management" onClick="routes('users','List Users')">Users </a> </li>		
				<i class="icon-angle-right"></i>	
				<li><a href="#" data-toggle="tooltip" rel="tooltip" data-placement="top" title="Klik untuk melihat menu management" onClick="routes('users/edit','Edit User')"> Edit</a> </li>						
			</ul>
			<div class="portlet box red tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-reorder"></i>
						<span class="hidden-480">Edit User</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">
									<div class="control-group">
										  <label class="control-label">NIK</label>
										  <div class="controls">
											 <input name="nik" id="nik" type="text" value="<?php echo $users[0]->nik;?>" class="span6 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="NIK" />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Name</label>
										  <div class="controls">
											 <input name="name" id="name" type="text" value="<?php echo $users[0]->name;?>" class="span6 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="Nama" />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Password</label>
										  <div class="controls">
											 <input name="password" id="password" type="password" value="<?php echo $users[0]->password;?>" class="span6 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="Password" />
										  </div>
									 </div>
									  <div class="control-group">
										  <label class="control-label">User Group</label>
										  <div class="controls">
											<select class="large m-wrap" tabindex="1" name="user_group_id" id="user_group_id">
													<option value="">Pilih Group</option>
													<?php foreach($data as $row){?>
														<option value="<?php echo $row->id;?>"><?php echo $row->name?></option>
													<?php } ?>
											  </select>											
										  </div>
									 </div>
									 <div class="control-group">
										<div class="controls">	
											<input name="id" id="id" type="hidden" value="<?php echo $users[0]->id;?>">
											<button class="btn" type="button" onClick="save()">Update</button>
										</div>
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
<script language="javascript">
	function validate() {
		if ($('#nik').val() =='') {
			new Messi('Nik masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("nik").focus(); }});
			return false;
		}
		if ($('#name').val() =='') {
			new Messi('Name masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("name").focus(); }});
			return false;
		}
		if ($('#password').val() =='') {
			new Messi('Password masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("password").focus();  }});
			return false;
		}
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>users/update",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil disimpan !', {title: 'User Message', titleClass: 'anim info', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('users','List User'); }});
					} else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'User Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
</script>