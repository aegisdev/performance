<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Managemen Group		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" data-toggle="tooltip" rel="tooltip" data-placement="top" onClick="routes('groups','List Group')">Groups </a> </li>		
				<i class="icon-angle-right"></i>	
				<li><a href="#" data-toggle="tooltip" rel="tooltip" data-placement="top"> Tambah</a> </li>						
			</ul>
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-plus"></i>
						<span class="hidden-480">Tambah Group</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">								
									 <div class="control-group">
										  <label class="control-label">Nama Group</label>
										  <div class="controls">
											 <input name="name" id="name" type="text" value="" class="span6 m-wrap" />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Keterangan</label>
										  <div class="controls">
											 <input name="description" id="description" type="text" value="" class="span6 m-wrap"/>
										  </div>
									 </div>	 
									<!-- <div class="control-group">
										  <label class="control-label">Menu</label>
										  <div class="controls">
											 <input type="text" id="menu_name" name="menu_name" class="span6" disabled>																																														 
											 <button class="btn  blue open-modal btn_list_menu" type="button" id="btn_list_menu"><i class="icon-eye-open"></i> Lihat Menu</button>
										  </div>
									 </div>	-->								  
									 <div class="control-group">
										<div class="controls">		
											<input type="hidden" id="menu_id" name="menu_id">		
											<button class="btn blue" type="button" onClick="save()"><i class="icon-save"></i> Simpan</button>
											<button class="btn blue" type="button" onClick="routes('groups','Daftar Group')"><i class="icon-share"></i> Kembali</button>
											
										</div>
									 </div>
									 <div class="control-group ">
										<!--<div class="controls">	
											<div class="span5">
												<table class="table table-striped table-bordered">
													<tr><td style="text-align:center"><b>GROUP NAME</b></td></tr>
												<?php foreach($groups as $group){?>
													<tr><td><?php //echo $group->name;?></td></tr>																	
												<?php } ?>
												</table>
											</div>
										</div> -->
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		<h3 id="myModalLabel"></h3>
	</div>
	<div class="modal-body" id="main-modal-body"></div>
	<div class="modal-footer">
		<button type="button" class="btn red" id="btn_save_list_group" data-dismiss="modal">Simpan</button> 
		<button type="button" class="btn blue" data-dismiss="modal">Batal</button>	
	</div>
</div>
<!-- END PAGE CONTAINER-->		
<script language="javascript">
	function validate() {
		if ($('#name').val() =='') {
			new Messi('Name masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("name").focus(); }});
			return false;
		}
		if ($('#description').val() =='') {
			new Messi('Description masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("password").focus();  }});
			return false;
		}
	/*	if ($('#menu_name').val() =='') {
			new Messi('Menu masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("menu_name").focus();  }});
			return false;
		}*/
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>groups/save",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil disimpan !', {title: 'Message', titleClass: 'anim info', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('groups','Group Management'); }});
					} else if (msg=='2'){
						new Messi('Group sudah terdaftar!<br />Pesan : '+msg, {title: 'Message', titleClass: 'anim warning', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
	$('#btn_list_menu').click(function(ev){			
		ev.preventDefault();		
		if($('#menu_name').val()!='');
			var data=$('#menu_name').val();
		$.ajax({
			type: "POST",		
			url: '<?php echo base_url(); ?>' + 'menus/list_menu/',
			data:{data:data},
			success: function(response){
				$('#main-modal-body').html(response);
			},
			error: function(e){
				alert(e);
			},
			complete: function(){

			}
		});
	});
	
</script>