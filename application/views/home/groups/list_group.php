<form style="margin: 10px;" id="form-subscription">
	<div class="span5">
			<?php foreach($groups as $group){?>
				 <label class="checkbox">
						<input type="checkbox" id="group_id-<?php echo $group->id;?>" name="group_id[]" value="<?php echo $group->id;?>"> <?php echo $group->name;?>
				  </label>
			<?php } ?>
	</div>
</form>
<script type="text/javascript">
  $.ajax({
    url: '<?php echo base_url();?>documents/mysubscription',
    dataType: 'json',
    success: function(response){
      if(response.length > 0){
        for(var i=0;i<response.length;i++){
          $('#cat-' + response[i].SUB_CATEGORY_ID).attr('checked','checked');
        }
      }
    },
    error: function(e){

    },
    complete: function(){

    }
  });
</script>