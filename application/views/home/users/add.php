<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Pengguna		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('users','Pengguna')">Pengguna </a> 	
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('users/add','Pengguna')">Tambah</a> </li>						
			</ul>
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-plus"></i>
						<span class="hidden-480">Tambah</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">
									<div class="control-group">
										  <label class="control-label">Username</label>
										  <div class="controls">
											 <input name="username" id="username" type="text" value="" class="span6 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="NIK" />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Nama</label>
										  <div class="controls">
											 <input name="name" id="name" type="text" value="" class="span6 m-wrap"/>
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Password</label>
										  <div class="controls">
											 <input name="password" id="password" type="password" value="" class="span6 m-wrap"/>
										  </div>
									 </div> 
									 <div class="control-group">
										  <label class="control-label">Ulangi Password</label>
										  <div class="controls">
											 <input name="re_password" id="re_password" type="password" value="" class="span6 m-wrap" />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Email</label>
										  <div class="controls">
											 <input name="email" id="email" type="text" value="" class="span6 m-wrap" />
										  </div>
									 </div>
									  <div class="control-group">
										  <label class="control-label">Group User</label>
										  <div class="controls">
											 <select class="large m-wrap" tabindex="1" name="user_group_id" id="user_group_id">
													<option value="">Pilih Group</option>
													<?php foreach($data as $row){?>
														<option value="<?php echo $row->id;?>"><?php echo $row->name?></option>
													<?php } ?>
											  </select>
										  </div>
									 </div>
									  <div class="control-group">
										  <label class="control-label">Unit</label>
										  <div class="controls">
											 <select class="large m-wrap" tabindex="1" name="organization_id" id="organization_id">
													<option value="0">Pilih Unit</option>
													<?php foreach($organizations as $organization){?>
														<option value="<?php echo $organization->id;?>"><?php echo $organization->name?></option>
													<?php } ?>
											  </select>
										  </div>
									 </div>
									 <div class="control-group">
										<div class="controls">							
											<button class="btn blue" type="button" onClick="save()"><i class="icon-save"></i> Simpan</button>
											<button class="btn red" type="button" onClick="routes('users','Daftar User')"><i class="icon-share"></i> Kembali</button>
										</div>
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script language="javascript">
$(document).ready(function(){
	$("#organization_id").select2(); 
});
	function validate() {
		if ($('#username').val() =='') {
			new Messi('Username masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("username").focus(); }});
			return false;
		}
		if ($('#name').val() =='') {
			new Messi('Name masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("name").focus(); }});
			return false;
		}
		if ($('#password').val() =='') {
			new Messi('Password masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("password").focus();  }});
			return false;
		}
		if ($('#re_password').val() =='') {
			new Messi('Ulangi password masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("re_password").focus();  }});
			return false;
		}if ($('#email').val() =='') {
			new Messi('Email masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("email").focus();  }});
			return false;
		}
		if ($('#password').val() != $('#re_password').val()) {
			new Messi('Password dan Re Password tidak cocok !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("password").focus();  $('#password').val('');$('#re_password').val('');}});
			return false;
		}
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>users/save",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil disimpan !', {title: 'Message', titleClass: 'anim info', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('users','User Management'); }});
					} else if(msg=='2') {
						new Messi('Username/email sudah terdaftar !', {title: 'Message', titleClass: 'anim warning', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ document.getElementById("username").focus();}});
					}
					else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
</script>