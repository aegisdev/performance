<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Managemen User		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="#" data-toggle="tooltip" rel="tooltip" data-placement="top" title="Klik untuk melihat menu management" onClick="routes('users','Daftar User')">Users</a> 	
					<i class="icon-angle-right"></i>	
				</li>	
				<li><a href="#" data-toggle="tooltip" rel="tooltip" data-placement="top" title="Klik untuk melihat menu management"> Edit</a> </li>						
			</ul>
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-edit"></i>
						<span class="hidden-480">Edit User</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">
									<div class="control-group">
										  <label class="control-label">Username</label>
										  <div class="controls">
											 <input name="username" id="username" type="text" readonly="true" value="<?php echo $user[0]->username;?>" class="span6 m-wrap" />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Nama</label>
										  <div class="controls">
											 <input name="name" id="name" type="text" value="<?php echo $user[0]->name;?>" class="span6 m-wrap"/>
										  </div>
									 </div>									
									 <!-- jika account milik sendiri -->
									 <?php if($this->session->userdata('id') == $user[0]->id){ ?>
									 <div class="control-group">
										  <label class="control-label">Old Password</label>
										  <div class="controls">
											 <input name="old_password" id="old_password" type="password" class="span6 m-wrap" />
										  </div>
									 </div> 
									 <div class="control-group">
										  <label class="control-label">New Password</label>
										  <div class="controls">
											 <input name="new_password" id="new_password" type="password"  class="span6 m-wrap" />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Re New Password</label>
										  <div class="controls">
											 <input name="re_new_password" id="re_new_password" type="password"  class="span6 m-wrap"/>
										  </div>
									 </div>
									  <div class="control-group">
										  <label class="control-label">Email</label>
										  <div class="controls">
											 <input name="email" id="email" type="text" value="<?php echo $user[0]->email;?>" class="span6 m-wrap" />
										  </div>
									 </div>
									 <?php } else { ?>
									  <div class="control-group">
										  <label class="control-label">User Group</label>
										  <div class="controls">
										   <select class="large m-wrap" tabindex="1" name="user_group_id" id="user_group_id">
													<option value="">Pilih Group</option>
													<?php foreach($data as $row){
														$selected=($row->id==$user[0]->user_group_id)? "selected":"";
													?>
														<option value="<?php echo $row->id;?>" <?php echo $selected;?> ><?php echo $row->name?></option>
													<?php } ?>
											</select>																			
										  </div>
									 </div>
									 <?php } ?>
									 <div class="control-group">
										  <label class="control-label">Unit</label>
										  <div class="controls">
											 <select class="large m-wrap" tabindex="1" name="organization_id" id="organization_id">
													<option value="">Pilih Unit</option>
													<option value="0">Non-Unit</option>
													<?php foreach($organizations as $organization){
														$selected=($organization->id==$user[0]->organization_id)? "selected":"";
													?>
													<option value="<?php echo $organization->id;?>" <?php echo $selected;?>><?php echo $organization->name?></option>
													<?php } ?>
											  </select>
										  </div>
									 </div>
									 <div class="control-group">
										<div class="controls">	
											<input name="id" id="id" type="hidden" value="<?php echo $user[0]->id;?>">										
											<button class="btn blue" type="button" onClick="save()"><i class="icon-save"></i> Ubah</button>
											<button class="btn red" type="button" onClick="routes('users','Daftar Users')"><i class="icon-share"></i> Kembali</button>
										</div>
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script language="javascript">
$(document).ready(function(){
	$("#organization_id").select2(); 
});
	function validate() {
		if ($('#username').val() =='') {
			new Messi('Nik masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("username").focus(); }});
			return false;
		}
		if ($('#name').val() =='') {
			new Messi('Name masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("name").focus(); }});
			return false;
		}
		if ($('#old_password').val() =='') {
			new Messi('Old password masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("old_password").focus();  }});
			return false;
		}
		if ($('#new_password').val() =='') {
			new Messi('New Password masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("new_password").focus();  }});
			return false;
		}
		if ($('#re_new_password').val() =='') {
			new Messi('Re New Password masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("re_new_password").focus();  }});
			return false;
		}
		if($('#new_password').val() !=$('#re_new_password').val())
		{
			new Messi('New Password dan Re New Password tidak cocok !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("new_password").focus();  }});
			return false;
		}
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>users/update",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil diupdate !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('users','Managemen User'); }});
					}
					else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Profile Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
	
	$('#old_password').change(function(){
		var password=$('#old_password').val();
		$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>users/IsExist",
				data: {password:password},
				success: function(msg){
					if(msg=='0') {
						new Messi('Password tidak terdaftar !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("old_password").focus();$('#old_password').val(''); }});
						return false;
					} 					
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
	
	});
</script>