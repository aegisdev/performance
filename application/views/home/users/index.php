
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Pengguna			
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Pengguna</a> </li>				
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>
							Daftar Pengguna
						</h4>
					</div>
					<div class="portlet-body">	
						<div class="clearfix">
							<div class="btn-group">
								<button class="btn blue" onClick="javascript:routes('users/add','Pengguna')">
								<i class="icon-plus"></i> Tambah
								</button>
							</div>						
						</div>
						<table class="table table-bordered table-striped" id="data_table">
							<thead>
								<tr>
									<th width="3%">ID#</th>
									<th>USERNAME</th>
									<th>NAMA</th>
									<th>EMAIL</th>
									<th width="150px">AKSI</th>									
								</tr>
							</thead>
							<tbody>
								<?php foreach ($users as $row) { ?>
									<tr>
										<td><?php echo $row->id;?></td>
										<td><?php echo $row->username;?></td>
										<td><?php echo $row->name;?></td>
										<td><?php echo $row->email;?></td>
										<td class="center-column">
											<button class="btn blue" onClick="javascript:routes('users/edit/<?php echo $row->id;?>','Edit User')" title="Edit User"><i class="icon-edit"></i></button>
											<button class="btn yellow" title="Reset Password" id="btn-reset-password" data-id="<?php echo $row->id;?>" data-label="<?php echo $row->username;?>"><i class="icon-key"></i></button>
											<button class="btn delete_data red" data-id="<?php echo $row->id;?>" data-label="<?php echo $row->username;?>" ><i class="icon-trash" title="Hapus User"></i></button>
										</td>
									</tr>
									<?php } ?>			
							</tbody>
						</table>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->
		
<script type="text/javascript" charset="utf-8">	
	$(document).ready(function(){
		$('#data_table').dataTable({
			"sPaginationType": "bootstrap",
			"oLanguage": {
				"sLengthMenu": "_MENU_ baris per halaman",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next"
				}
			},
			"aoColumnDefs": [{
				'bSortable': false,
				'aTargets': [0]
			}]
		});

		$('.delete_data').click(function(){ 
			var id =$(this).attr('data-id');
			var label = $(this).attr('data-label');

			new Messi('Apakah anda yakin ingin menghapus data dengan username "'+label+'" ?', {
				title: 'Confirmation', 
				titleClass: 'anim info', 
				closeButton: true,
				buttons: [
				{
					id: 0, 
					label: 'YES', 
					val: '1'
				},{
					id: 1, 
					label: 'NO', 
					val: '0'
				}], 
				callback:function(val){ 
					if(val == 0) {
						return false;
					} else if(val == 1) {
						$.ajax({
							type: "POST",
							url: "<?php echo base_url();?>users/delete",
							data: "id="+id,
							success: function(msg){
								if(msg=='1') {
									new Messi('Data berhasil dihapus !', {title: 'Menu Message', titleClass: 'anim info', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('users','User Management'); }});
								} else {
									new Messi('Data gagal dihapus !<br />Debug : '+msg, {title: 'Menu Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
								} 
							},
							error: function(fnc,msg){
								new Messi('Tidak dapat terhubung ke server untuk malakukan proses hapus data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
							}
						});
					} 
				}
			});
		});

		$('#data_table').delegate('#btn-reset-password','click', function(){
			var id = $(this).attr('data-id');
			var label = $(this).attr('data-label');
			
			new Messi('Apakah anda yakin ingin mereset password untuk user "'+label+'" ?', {
				title: 'Confirmation', 
				titleClass: 'anim info', 
				closeButton: true,
				buttons: [
				{
					id: 0, 
					label: 'YES', 
					val: '1'
				},{
					id: 1, 
					label: 'NO', 
					val: '0'
				}], 
				callback:function(val){ 
					if(val == 0) {
						return false;
					} else if(val == 1) {
						$.ajax({
							type: "POST",
							url: "<?php echo base_url();?>users/reset_password",
							data: "id="+id,
							dataType: 'json',
							success: function(response){
								msg = response.status;
								if(msg=='1') {
									new Messi('Password berhasil diubah! Passwrod baru : ' + response.new_password, {title: 'Menu Message', titleClass: 'anim info', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('users','User Management'); }});
								} else {
									new Messi('Password gagal untuk diubah!<br />Info : '+msg, {title: 'Menu Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
								} 
							},
							error: function(fnc,msg){
								new Messi('Tidak dapat terhubung ke server untuk malakukan proses reset password!', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
							}
						});
					} 
				}
			});
		});
	});		
</script>
	
	