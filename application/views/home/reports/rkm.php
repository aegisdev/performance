
<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">					
		<!-- END BEGIN STYLE CUSTOMIZER -->   	
		<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
		<h3 class="page-title">
			Distribusi KPI	
		</h3>
		<ul class="breadcrumb">
			<li>
				<i class="icon-home"></i>
				<a href="<?php echo base_url();?>home">Beranda</a> 
				<i class="icon-angle-right"></i>
			</li>
			<li><a href="#">Distribusi KPI	</a> </li>				
		</ul>
		<!-- END PAGE TITLE & BREADCRUMB-->		
			<div class="portlet box green">
				<div class="portlet-title">
					<h4>
						<i class="icon-table"></i>Distribusi KPI	
					</h4>												
				</div>
				<div class="portlet-body">						
					<table border="0">							
						<tr>
							<td width="100px">Periode</td>
							<td>
								<select name="year" id="forming-kpi-periode">
									<option value="-1">-- Pilih Tahun --</value>
									<?php for($year=2013;$year <=(date('Y')+1);$year++){ ?>
									<option value="<?php echo $year;?>"><?php echo $year;?></option>
									<?php } ?>
								</select>
							</td>
						</tr>
					</table>
					<div class="pull-right">
						<button class="btn btn_report"  data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="1">.PDF</button>
						<button class="btn btn_report"  data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="2">.XLS</button>
						<!--
						<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
						</button>
						<ul class="dropdown-menu">
							 <li><a href="#" data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="1" class="btn_report">Export to PDF</a></li>
							<li><a href="#" data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="2" class="btn_report">Export to Excel</a> </li>
						</ul>
						-->
					</div>
					<table class="table table-bordered table-striped table-hover" id="tbl-selected-kpi" style="font-size:11px">
						<thead>
							<tr>
								<td width="10px">NO</td>
								<td width="300px">KPI</td>								
								<td width="30px" class="center-column">DU</td>								
								<td width="30px" class="center-column">DM</td>							
								<td width="30px" class="center-column">DT</td>	
								<td width="30px" class="center-column">DS</td>
								<td width="30px" class="center-column">DI</td>
								<td width="30px" class="center-column">DK</td>
								<td width="30px" class="center-column">DP</td>
							</tr>								
						</thead>
						<tbody>		
						</tbody>
					</table>
					<div class="clearfix"></div>
					<div>
						Ket : <br/>
						&#10004; : Pemilik KPI <br/>
						o : Kontributor 
					</div>
				</div>
			</div>	
</div>
<!-- END PAGE HEADER-->	
<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script type="text/javascript" charset="utf-8">	

	$(document).ready(function(){
		$('#forming-kpi-periode').select2();
		$('#tbl-selected-kpi').dataTable({		
			"sPaginationType": "bootstrap",
			"oLanguage": {
				"sLengthMenu": "_MENU_ baris per halaman",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next"
				}
			},
			"bSort": false,
			"bFilter": false,
			"bInfo": false,
			"bPaginate": false
		});
	});

	(function(){
		$('#forming-kpi-periode').change(function(){
			var optionSelected = $(this).find("option:selected");
			var periode =optionSelected.val();	
			$('.btn_report').attr('data-periode',optionSelected.val());
			var	href="<?php echo base_url();?>reports/get_report_aligment_rkm/"+periode; 	
			$('.btn_report').attr('data-href',href);
			loadKpiForTable(optionSelected.val());
		});
		
		function loadKpiForTable(periode){
			$.ajax({
				type: 'post',
				data: {'periode' : periode },
				url : '<?php echo base_url();?>reports/get_report_rkm',
				dataType: 'json',
				beforeSend: function(){
					$('#tbl-selected-kpi > tbody:last').empty();
				}
			})
			.done(function(response, textStatus, jqhr){
				if(response){
					for(var i=0;i<response.length;i++){
						var rowbefore = (response[i-1]) ? response[i-1].perspective : '';
						var el = (response[i].perspective != rowbefore) ? '<tr><td colspan="9"><strong>'+response[i].perspective+'</strong></td></tr>' : '';
						
					 	el += '<tr><td>'+(i+1)+'</td>' +
								'<td>'+response[i].kpi+'</td>' +
								'<td class="center-column">' + buatCentang(response[i].du) + '</td>' +
								'<td class="center-column">' + buatCentang(response[i].dm) + '</td>' +
								'<td class="center-column">' + buatCentang(response[i].dt) + '</td>' +
								'<td class="center-column">' + buatCentang(response[i].ds) + '</td>' +
								'<td class="center-column">' + buatCentang(response[i].di) + '</td>' +
								'<td class="center-column">' + buatCentang(response[i].dk) + '</td>' +
								'<td class="center-column">' + buatCentang(response[i].dp) + '</td>' +
								'</tr>';
						$('#tbl-selected-kpi > tbody:last').append(el);					
					}
				}
			})
			.fail(function(){
			
			});
		}
		
		function buatCentang(nilai){
			if(nilai == null || nilai == 'NULL' || nilai == undefined || nilai == 0){		
				return "-";
			} else if(nilai == 2){
				return "&#10004;";
			} else {
				return "o";
			}
		}

		$(".btn_report").click(function(){
			var periode=$(this).attr('data-periode');		
			var file_type=$(this).attr('data-file');
			var href=$(this).attr('data-href');
			if (periode =='-1'){
				alert("Unit dan periode harus dipilih");
			} else{
				window.location.href=href+"/"+file_type;
			}
		});
	}());
</script>