<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">					
	<!-- END BEGIN STYLE CUSTOMIZER -->   	
	<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
	<h3 class="page-title">
		Laporan KM Unit				
	</h3>
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo base_url();?>home">Beranda</a> 
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="#">Laporan KM Unit</a> </li>				
	</ul>
	<!-- END PAGE TITLE & BREADCRUMB-->		
	<div class="portlet box green">
		<div class="portlet-title">
			<h4>
				<i class="icon-table"></i>Laporan KM Unit
			</h4>												
		</div>
		<div class="portlet-body">							
			<table border="0">
				<tr>
					<td width="100px">Unit</td>
					<td colspan="2" width="90%">
						<select name="organization_id" id="forming-kpi-unit" class="span6 m-wrap">										
							<option value="-1">-- Pilih Unit --</value>
							<?php foreach($data as $row){ ?>
							<option value="<?php echo $row->id;?>"><?php echo $row->name;?></value>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td width="100px">Periode</td>
					<td>
						<select name="year" id="forming-kpi-periode"  class="span2 m-wrap">
							<option value="-1">-- Pilih Tahun --</value>
							<?php for($year=2013;$year <=(date('Y')+1);$year++){ ?>
							<option value="<?php echo $year;?>"><?php echo $year;?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
			<div class="pull-right">
				<button class="btn btn_report"  data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="1">.PDF</button>
				<button class="btn btn_report"  data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="2">.XLS</button>
				<!--
				<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
				</button>
				<ul class="dropdown-menu">
					 <li><a href="#" data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="1" class="btn_report">Export to PDF</a></li>
					<li><a href="#" data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="2" class="btn_report">Export to Excel</a> </li>
				</ul>
				-->
			</div>
			<table class="table table-bordered table-striped table-hover" id="tbl-selected-kpi" style="font-size:9px">
				<thead>
					<tr>
						<td rowspan="2" width="10px">NO</td>
						<td rowspan="2">RESPONSIBILITY</td>
						<td rowspan="2" width="30px">Satuan</td>								
						<td rowspan="2" width="30px">Target</td>								
						<td rowspan="2" width="30px">Bobot</td>							
						<td colspan="3"><center>UNIT</center></td>	
						<td colspan="3"><center>PENILAI</center></td>
						<td rowspan="2">KET.</td>
						<td rowspan="2" width="30px">LEVEL</td>
					</tr>
					<tr>
						<td width="30px">Realisasi</td>								
						<td width="30px">Pencapaian</td>
						<td width="30px">Prestasi</td>
						<td width="30px">Realisasi</td>
						<td width="30px">Pencapaian</td>
						<td width="30px">Prestasi</td>
					</tr>
				</thead>
				<tbody>		
				</tbody>
			</table>
			<div class="clearfix"></div>
		</div>
	</div>	
</div>
<!-- END PAGE HEADER-->	
<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script language="javascript">
$(document).ready(function(){
	$("#forming-kpi-unit").select2(); 
	$("#forming-kpi-periode").select2(); 	

	$('#tbl-selected-kpi').dataTable({		
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ baris per halaman",
			"oPaginate": {
				"sPrevious": "Prev",
				"sNext": "Next"
			}
		},
		"bSort": false,
		"bFilter": false,
		"bInfo": false,
		"bPaginate": false
	});
});


(function(){
	$('#forming-kpi-unit').change(function(){
		var optionSelected = $(this).find("option:selected");
		var unit = optionSelected.val();
		var title = $('option:selected').html();
		
		$('.btn_report').attr('data-title',title);
		$('.btn_report').attr('data-unit',unit);	
		
		$('#choose_unit').val(optionSelected.val());		
		if($('#forming-kpi-periode').val() !=-1){
			var	href="<?php echo base_url();?>reports/get_reportunit/"+unit+"/"+$('#forming-kpi-periode').val(); 	
			$('.btn_report').attr('data-href',href);
			loadKpiForTable(optionSelected.val(), $('#forming-kpi-periode').val());
		}
		//loadKpiForTable(optionSelected.val(), $('#forming-kpi-periode').val());
	});

	$('#forming-kpi-periode').change(function(){
		var optionSelected = $(this).find("option:selected");
		var periode =optionSelected.val();	
		$('.btn_report').attr('data-periode',optionSelected.val());
		$('#choose_periode').val(optionSelected.val());	
		if($('#forming-kpi-unit').val() !=-1){		
			var unit=$('#forming-kpi-unit').find("option:selected");
			var title = $('#forming-kpi-unit').find("option:selected").html();
			var	href="<?php echo base_url();?>reports/get_reportunit/"+$('#forming-kpi-unit').val()+"/"+periode; 	
			$('.btn_report').attr('data-href',href);
			loadKpiForTable($('#forming-kpi-unit').val(), optionSelected.val());
		}
		//loadKpiForTable($('#forming-kpi-unit').val(), optionSelected.val());
	});
	
	function loadKpiForTable(unit, periode){
		$.ajax({
			type: 'post',
			data: {'unit' : unit, 'periode' : periode },
			url : '<?php echo base_url();?>reports/get_report_performance_km_unit',
			dataType: 'json',
			beforeSend: function(){
				$('#tbl-selected-kpi > tbody:last').empty();
			}
		})
		.done(function(response, textStatus, jqhr){
			if(response){
				var t1=0, t2=0, t3=0, t4=0, t5=0, t6=0, t7=0, t8=0;
				for(var i=0;i<response.length;i++){
					t1 += parseFloat(response[i].target) * 1;
					t2 += parseFloat(response[i].bobot) * 1;
					t3 += parseFloat(response[i].realisasi_org) * 1;
					t4 += parseFloat(response[i].pencapaian_org) * 1;
					t5 += parseFloat(response[i].prestasi_org) * 1;
					t6 += parseFloat(response[i].realisasi_penilai) * 1;
					t7 += parseFloat(response[i].pencapaian_penilai) * 1;
					t8 += parseFloat(response[i].prestasi_penilai) * 1;
					 
					var rowbefore = (response[i-1]) ? response[i-1].perspective : '';
					var el = (response[i].perspective != rowbefore) ? '<tr><td colspan="13"><strong>'+response[i].perspective+'</strong></td></tr>' : '';
					el += '<tr><td>'+(i+1*1)+'</td>' +
							'<td>'+response[i].kpi_name+'</td>' + 
							'<td>'+response[i].satuan+'</td>' +
							'<td>'+response[i].target+'</td>' +
							'<td>'+response[i].bobot+'</td>' +
							'<td>'+response[i].realisasi_org+'</td>' +
							'<td>'+parseFloat(response[i].pencapaian_org).toFixed(2)+'</td>' +
							'<td>'+parseFloat(response[i].prestasi_org).toFixed(2)+'</td>' +
							'<td>'+response[i].realisasi_penilai+'</td>' +
							'<td>'+parseFloat(response[i].pencapaian_penilai).toFixed(2)+'</td>' +
							'<td>'+parseFloat(response[i].prestasi_penilai).toFixed(2)+'</td>' +
							'<td>'+response[i].keterangan_penilai+'</td>' +
							calculateLevel(response[i].pencapaian_penilai) + '</tr>';

						$('#tbl-selected-kpi > tbody:last').append(el);
				}
				
				//add total rows 
				var htmlTemp = '<tr><td colspan="4">Total</td>' + 
								'<td>'+ t2.toFixed(2) +'</td>' +
								'<td colspan="2"></td>' + 
								'<td>'+ t5.toFixed(2) +'</td>' + 
								'<td colspan="2"></td>' + 
								'<td>'+ t8.toFixed(2) +'</td>' + 
								'<td colspan="2"></td>' + 
								'</tr>';
				$('#tbl-selected-kpi > tbody:last').append(htmlTemp);
			}
		})
		.fail(function(){
		
		});
	}
	
	function calculateLevel(a){
		var e = '<td style="background:#0000FF;color:#FFF">EXCELLENT</td>';
		if(a <= <?php echo $level[0]->maksimal;?>){
			e = '<td style="background:#FF0000;color:#FFF">BAD</td>';
		} else if(a > <?php echo $level[0]->maksimal;?> && a <= <?php echo $level[1]->maksimal;?>){
			e = '<td style="background:yellow;color:#000">POOR</td>';
		} else if(a > 90 && a <= <?php echo $level[2]->maksimal;?>){
			e = '<td style="background:green;color:#FFF">GOOD</td>';		
		}
		
		return e;
	}
	$(".btn_report").click(function(){
		var title=$(this).attr('data-title');
		var unit=$(this).attr('data-unit');
		var periode=$(this).attr('data-periode');
		var file_type=$(this).attr('data-file');
		var href=$(this).attr('data-href');
		 if (unit =='-1' || periode =='-1'){
			alert("Unit dan periode harus dipilih");
		} else{
			//console.log(href+"/"+file_type);
			window.location.href=href+"/"+file_type;
		}

	});
}());
</script>