<?php
 	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment;filename=report_km_perusahaan_".$periode.".xls"); 
	header("Content-Transfer-Encoding: binary"); 
?>
<h2 align="center">LAPORAN KM PERUSAHAAN <?php echo $periode;?></h2>		
<table border="1" width="100%" cellpadding="2" cellspacing="0">
	<tr>
		<td rowspan="2" width="1%">NO</td>
		<td rowspan="2">RESPONSIBILITY</td>
		<td rowspan="2"><center>SATUAN</center></td>
		<td rowspan="2"><center>TARGET</center></td>
		<td rowspan="2"><center>BOBOT</center></td>
		<td colspan="3"><center>UNIT</center></td>
		<td colspan="3"><center>PENILAI</center></td>
		<td rowspan="2" width="10%"><center>KET.</center></td>
		<td rowspan="2"><center>LEVEL</center></td>
	  </tr>
	  <tr>
		<td>REALISASI</td>
		<td>PENCAPAIAN</td>
		<td>PRESTASI</td>
		<td>REALISASI</td>
		<td>PENCAPAIAN</td>
		<td>PRESTASI</td>
	  </tr>	

	  <?php 
			$count=0;
			$t1=0;$t2=0;$t3=0;$t4=0;$t5=0;$t6=0;$t7=0;$t8=0;
			for($i=0;$i<count($data);$i++)
			{										
				$count++;	
				$t1 += (float)($data[$i]->target) * 1;
				$t2 += (float)($data[$i]->bobot) * 1;
				$t3 += (float)($data[$i]->realisasi_org) * 1;
				$t4 += (float)($data[$i]->pencapaian_org) * 1;
				$t5 += (float)($data[$i]->prestasi_org) * 1;
				$t6 += (float)($data[$i]->realisasi_penilai) * 1;
				$t7 += (float)($data[$i]->pencapaian_penilai) * 1;
				$t8 += (float)($data[$i]->prestasi_penilai) * 1;
				
				if($i>0){
					$rowbefore = ($data[$i-1]) ? $data[$i-1]->perspective : '';
				}else{
					$rowbefore='';
				}
				
				$e = '<td style="background:#0000FF;color:#FFF">EXCELLENT</td>';
				$a=$data[$i]->pencapaian_penilai;
				if($a <=$level[0]->maksimal){
					$e = '<td style="background:#FF0000;color:#FFF">BAD</td>';
				} else if($a > $level[0]->maksimal && $a <= $level[1]->maksimal){
					$e = '<td style="background:yellow;color:#000">POOR</td>';
				} else if($a > 90 && $a <= $level[2]->maksimal){
					$e = '<td style="background:green;color:#FFF">GOOD</td>';		
				}
				$data_report = ($data[$i]->perspective != $rowbefore) ? '<tr><td colspan="13"><strong>'. $data[$i]->perspective.'</strong></td></tr>' : '';							
				$data_report .= '<tr><td><center>'.($count).'</center></td>'.
						'<td>'.$data[$i]->kpi_name.'</td>'. 
						'<td><center>'.$data[$i]->satuan.'</center></td>'.
						'<td align=right>'.$data[$i]->target.'</td>'.
						'<td><center>'.$data[$i]->bobot.'</center></td>'.
						'<td align=right>'.number_format($data[$i]->realisasi_org,2).'</td>'.
						'<td align=right>'.number_format($data[$i]->pencapaian_org,2).'</td>'.
						'<td align=right>'.number_format($data[$i]->prestasi_org,2).'</td>'.
						'<td align=right>'.number_format($data[$i]->realisasi_penilai,2).'</td>'.
						'<td align=right>'.number_format($data[$i]->pencapaian_penilai,2).'</td>'.
						'<td align=right>'.number_format($data[$i]->prestasi_penilai,2).'</td>'.
						'<td align=right>'.(($data[$i]->keterangan_penilai) ? ($data[$i]->keterangan_penilai) : '').'</td>'.$e.
						'</tr>';								 
				echo $data_report;           
			}							
	  ?>
	</table>					
	