<?php
 	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment;filename=report_km_unit_".$periode.".xls"); 
	header("Content-Transfer-Encoding: binary");
?>
<h2 align="center">LAPORAN KM UNIT <?php echo $periode;?></h2>		
<!--<link href="<?php echo base_url();?>assets/data-tables/DT_bootstrap.css" media="screen" rel="stylesheet" type="text/css" />
	<table border="0" class="table" align="center" width="100%">
			<tr><th colspan="12">LAPORAN KM UNIT</th></tr>
			<tr><th colspan="12">UNIT : <?php //echo rawurldecode($title);?></th></tr>
			<tr><th colspan="12">TAHUN: <?php //echo $periode;?></th></tr>
	</table>	
-->			
<table border="1" width="100%" cellpadding="2" cellspacing="0">
	<tr>
		<td rowspan="2" width="1%">NO</td>
		<td rowspan="2">RESPONSIBILITY</td>
		<td rowspan="2" width="30px">Satuan</td>								
		<td rowspan="2" width="30px">Target</td>								
		<td rowspan="2" width="30px">Bobot</td>							
		<td colspan="3"><center>UNIT</center></td>	
		<td colspan="3"><center>PENILAI</center></td>
		<td rowspan="2"><center>KET.</center></td>
		<td rowspan="2" width="30px"><center>LEVEL</center></td>
	</tr>
	<tr>
		<td width="30px">Realisasi</td>								
		<td width="30px">Pencapaian</td>
		<td width="30px">Prestasi</td>
		<td width="30px">Realisasi</td>
		<td width="30px">Pencapaian</td>
		<td width="30px">Prestasi</td>
	</tr>
	  <?php 
			$count=0;
			$t1=0;$t2=0;$t3=0;$t4=0;$t5=0;$t6=0;$t7=0;$t8=0;	
			$data_report='';			
			for($i=0;$i<count($data);$i++)
			{										
				$count++;	
				$t1 += (float)($data[$i]->target) * 1;
				$t2 += (float)($data[$i]->bobot) * 1;
				$t3 += (float)($data[$i]->realisasi_org) * 1;
				$t4 += (float)($data[$i]->pencapaian_org) * 1;
				$t5 += (float)($data[$i]->prestasi_org) * 1;
				$t6 += (float)($data[$i]->realisasi_penilai) * 1;
				$t7 += (float)($data[$i]->pencapaian_penilai) * 1;
				$t8 += (float)($data[$i]->prestasi_penilai) * 1;
				
				if($i>0){
					$rowbefore = ($data[$i-1]) ? $data[$i-1]->perspective : '';
				}else{
					$rowbefore ='';
				}
				
				$e = '<td style="background:#0000FF;color:#FFF">EXCELLENT</td>';
				$a=$data[$i]->pencapaian_penilai;
				if($a <=$level[0]->maksimal){
					$e = '<td style="background:#FF0000;color:#FFF">BAD</td>';
				} else if($a > $level[0]->maksimal && $a <= $level[1]->maksimal){
					$e = '<td style="background:yellow;color:#000">POOR</td>';
				} else if($a > 90 && $a <= $level[2]->maksimal){
					$e = '<td style="background:green;color:#FFF">GOOD</td>';		
				}
				$data_report .= ($data[$i]->perspective != $rowbefore) ? '<tr><td colspan="13"><strong>'. $data[$i]->perspective.'</strong></td></tr>' : '';							
				$data_report .= '<tr><td><center>'.($i+1).'</center></td>';
				$data_report .='<td>'.$data[$i]->kpi_name.'</td>'; 
				$data_report .='<td><center>'.$data[$i]->satuan.'</center></td>';
				$data_report .='<td align=right>'.number_format($data[$i]->target,2).'</td>';
				$data_report .='<td><center>'.$data[$i]->bobot.'</center></td>';
				$data_report .='<td align=right>'.number_format($data[$i]->realisasi_org,2).'</td>';
				$data_report .='<td align=right>'.number_format($data[$i]->pencapaian_org,2).'</td>';
				$data_report .='<td align=right>'.number_format($data[$i]->prestasi_org,2).'</td>';
				$data_report .='<td align=right>'.number_format($data[$i]->realisasi_penilai,2).'</td>';
				$data_report .='<td align=right>'.number_format($data[$i]->pencapaian_penilai,2).'</td>';
				$data_report .='<td align=right>'.number_format($data[$i]->prestasi_penilai,2).'</td>';
				$data_report .='<td align=right>'.(($data[$i]->keterangan_penilai) ? ($data[$i]->keterangan_penilai) : '').'</td>'.$e;
				$data_report .='</tr>';	   
			}	
			
			$data_report.= '<tr><td colspan="4">Total</td>';
			$data_report.= '<td><center>'.$t2.'</center></td>';
			$data_report.= '<td colspan="2"></td>'; 
			$data_report.= '<td>'.number_format($t5,2).'</td>';
			$data_report.= '<td colspan="2"></td>';
			$data_report.= '<td>'.number_format($t8,2).'</td>';
			$data_report.= '<td colspan="2"></td>'; 
			$data_report.= '</tr>';
			echo $data_report;  		
	  ?>
	</table>					
	