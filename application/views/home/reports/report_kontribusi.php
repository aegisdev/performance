<?php
 	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment;filename=report_kontribusi_".$periode.".xls"); 
	header("Content-Transfer-Encoding: binary"); 
?>
<h2 align="center">LAPORAN KONTRIBUSI DIREKTORAT <?php echo $periode;?></h2>			
<table border="1" class="table" width="100%" cellpadding="2" cellspacing="0">
	<tr>
			<td rowspan="2" width="1%">NO</td>						
			<td rowspan="2" width="400px">KPI</td>						
			<td colspan="3"><center>DM</center></td>	
			<td colspan="3"><center>DT</center></td>
			<td colspan="3"><center>DS</center></td>
			<td colspan="3"><center>DI</center></td>
			<td colspan="3"><center>DK</center></td>
			<td colspan="3"><center>DP</center></td>
			<td colspan="3"><center>DU</center></td>
			<td colspan="2"><center>PER.</center></td>
		</tr>
		<tr>
			<td width="20px"><center>B</center></td>								
			<td width="20px"><center>R</center></td>								
			<td width="20px"><center>K</center></td>
			<td width="20px"><center>B</center></td>
			<td width="20px"><center>R</center></td>
			<td width="20px"><center>K</center></td>
			<td width="20px"><center>B</center></td>
			<td width="20px"><center>R</center></td>
			<td width="20px"><center>K</center></td>
			<td width="20px"><center>B</center></td>
			<td width="20px"><center>R</center></td>
			<td width="20px"><center>K</center></td>
			<td width="20px"><center>B</center></td>
			<td width="20px"><center>R</center></td>
			<td width="20px"><center>K</center></td>	
			<td width="20px"><center>B</center></td>
			<td width="20px"><center>R</center></td>
			<td width="20px"><center>K</center></td>
			<td width="20px"><center>B</center></td>
			<td width="20px"><center>R</center></td>
			<td width="20px"><center>K</center></td>								
			<td width="20px"><center>R</center></td>
			<td width="20px"><center>P</center></td>						
		</tr>

	  <?php 	
			//$data_report ='';
			for($i=0;$i<count($data);$i++)
			{										
				if($i>0){
					$rowbefore = ($data[$i-1]) ? $data[$i-1]->name : '';
				}else{
					$rowbefore='';
				}
				
				$data_report = ($data[$i]->name != $rowbefore) ? '<tr><td colspan="25"><strong>'. $data[$i]->name.'</strong></td></tr>' : '';							
				$data_report .= '<tr><td><center>'.($i+1).'</center></td>'.
						'<td>'.$data[$i]->kpi.'</td>'. 
						'<td><center>'.bersihkan($data[$i]->dm_bobot).'</center></td>'. 
						'<td><center>'.bersihkan($data[$i]->dm_realisasi).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->dm_kontribusi).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->dt_bobot).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->dt_realisasi).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->dt_kontribusi).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->ds_bobot).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->ds_realisasi).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->ds_kontribusi).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->di_bobot).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->di_realisasi).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->di_kontribusi).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->dk_bobot).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->dk_realisasi).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->dk_kontribusi).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->dp_bobot).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->dp_realisasi).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->dp_kontribusi).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->du_bobot).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->du_realisasi).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->du_kontribusi).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->per_realisasi).'</center></td>'.
						'<td><center>'.bersihkan($data[$i]->per_pencapaian).'</center></td>'.
						'</tr>';	
				echo $data_report;  						
			}	
					
		
		function bersihkan($nilai){
		if($nilai == null || $nilai == "null") 
			return 0;
		else 
			return number_format($nilai,2);
	}		
					
	  ?>
	</table><br/>		
	<div>
			Ket : <br/>
			B = Bobot <br/>
			R = Realisasi <br/>
			K = Kontribusi <br/>
			P = Prestasi <br/>
			PER. = Perusahaan <br/>
		</div>
	