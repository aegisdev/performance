<?php
 	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment;filename=report_aligment_rkm_".$periode.".xls"); 
	header("Content-Transfer-Encoding: binary"); 
?>
<h2 align="center">LAPORAN ALIGMENT RKM <?php echo $periode;?></h2>				
<table border="1" class="table" width="100%" cellpadding="2" cellspacing="0">
	<tr>
		<td width="1px">NO</td>
		<td width="300px">KPI</td>								
		<td width="30px"><center>DU</center></td>								
		<td width="30px"><center>DM</center></td>							
		<td width="30px"><center>DT</center></td>	
		<td width="30px"><center>DS</center></td>
		<td width="30px"><center>DI</center></td>
		<td width="30px"><center>DK</center></td>
		<td width="30px"><center>DP</center></td>
	</tr>	

	  <?php 	
			$data_report='';
			for($i=0;$i<count($data);$i++)
			{										
				if($i>0){
					$rowbefore = ($data[$i-1]) ? $data[$i-1]->perspective : '';
				}else{
					$rowbefore='';
				}
				
				$data_report .= ($data[$i]->perspective != $rowbefore) ? '<tr><td colspan="9"><strong>'. $data[$i]->perspective.'</strong></td></tr>' : '';							
				$data_report .= '<tr><td>'.($i+1).'</td>'.
						'<td>'.$data[$i]->kpi.'</td>'. 
						'<td><center>'.buatCentang($data[$i]->du).'</center></td>'. 
						'<td><center>'.buatCentang($data[$i]->dm).'</center></td>'.
						'<td><center>'.buatCentang($data[$i]->dt).'</center></td>'.
						'<td><center>'.buatCentang($data[$i]->ds).'</center></td>'.
						'<td><center>'.buatCentang($data[$i]->di).'</center></td>'.
						'<td><center>'.buatCentang($data[$i]->dk).'</center></td>'.
						'<td><center>'.buatCentang($data[$i]->dp).'</center></td>'.
						'</tr>';				         
			}	
		echo $data_report;  			
		
		function buatCentang($nilai){
			if($nilai == null || $nilai == 'NULL' || $nilai == 0){
				return "-";
			}
			else if($nilai == 2){
				return "v";
			} else {
				return "o";
			}
		}			
					
	  ?>
	</table>			
	<div class="clearfix"></div>
	<div>
		Ket : <br/>
		v : Pemilik KPI <br/>
		o : Kontributor 
	</div>		
	