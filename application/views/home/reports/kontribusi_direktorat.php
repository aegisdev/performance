<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">					
	<!-- END BEGIN STYLE CUSTOMIZER -->   	
	<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
	<h3 class="page-title">
		Laporan Kontribusi Direktorat			
	</h3>
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo base_url();?>home">Beranda</a> 
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="#">Kontribusi Direktorat</a> </li>				
	</ul>
	<!-- END PAGE TITLE & BREADCRUMB-->		
	<div class="portlet box green">
		<div class="portlet-title">
			<h4>
				<i class="icon-table"></i>Kontribusi Direktorat
			</h4>												
		</div>
		<div class="portlet-body">					
			<table border="0">							
				<tr>
					<td width="100px">Periode</td>
					<td>:
						<select name="year" id="periode">
							<option value="-1">-- Pilih Tahun --</value>
							<?php for($year=2013;$year <=(date('Y')+1);$year++){ ?>
							<option value="<?php echo $year;?>"><?php echo $year;?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
			</table>
			<div class="pull-right">
				<button class="btn btn_report"  data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="1">.PDF</button>
				<button class="btn btn_report"  data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="2">.XLS</button>
				<!--
				<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
				</button>
				<ul class="dropdown-menu">
					 <li><a href="#" data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="1" class="btn_report">Export to PDF</a></li>
					<li><a href="#" data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="2" class="btn_report">Export to Excel</a> </li>
				</ul>
				-->
			</div>
			<table class="table table-bordered table-striped table-hover" id="table_kontribusi_direktorat" style="font-size:9px">
				<thead>
					<tr>
						<td rowspan="2" width="100px">KPI</td>						
						<td colspan="3"><center>DM</center></td>	
						<td colspan="3"><center>DT</center></td>
						<td colspan="3"><center>DS</center></td>
						<td colspan="3"><center>DI</center></td>
						<td colspan="3"><center>DK</center></td>
						<td colspan="3"><center>DP</center></td>
						<td colspan="3"><center>DU</center></td>
						<td colspan="2"><center>PER.</center></td>
					</tr>
					<tr>
						<td width="20px">B</td>								
						<td width="20px">R</td>								
						<td width="20px">K</td>
						<td width="20px">B</td>
						<td width="20px">R</td>
						<td width="20px">K</td>
						<td width="20px">B</td>
						<td width="20px">R</td>
						<td width="20px">K</td>
						<td width="20px">B</td>
						<td width="20px">R</td>
						<td width="20px">K</td>
						<td width="20px">B</td>
						<td width="20px">R</td>
						<td width="20px">K</td>	
						<td width="20px">B</td>
						<td width="20px">R</td>
						<td width="20px">K</td>
						<td width="20px">B</td>
						<td width="20px">R</td>
						<td width="20px">K</td>								
						<td width="20px">R</td>
						<td width="20px">P</td>
						
					</tr>
				</thead>
				<tbody></tbody>
			</table>
			<div class="clearfix"></div>
			<div>
				Ket : <br/>
				B = Bobot <br/>
				R = Realisasi <br/>
				K = Kontribusi <br/>
				P = Prestasi <br/>
				PER. = Perusahaan <br/>
			</div>
		</div>
	</div>	
</div>
<!-- END PAGE HEADER-->
<script type="text/javascript" charset="utf-8">	
(function(){
	var tblReportKontribusiSettings = {
		"bSort": false,
		"bFilter": false,
		"bInfo":false,
		"bPaginate": false,
		"sScrollY": "500px",
		"sScrollX": "100%",
		"sScrollXInner": "150%",
		"bScrollCollapse": true,
	}
	
	var isFixedColumns = false;
		
	var tblReportKontribusi = $('#table_kontribusi_direktorat').dataTable(tblReportKontribusiSettings);

	$('#periode').change(function(){
		var optionSelected = $(this).find("option:selected");
		var periode =optionSelected.val();	
		$('.btn_report').attr('data-periode',optionSelected.val());
		var	href="<?php echo base_url();?>reports/get_report_kontribusi/"+periode; 	
		$('.btn_report').attr('data-href',href);
		loadKontributorDirektorat(optionSelected.val());
	});
	
	function loadKontributorDirektorat(periode){		
		$.ajax({
			type: 'post',
			data: {'periode' : periode},
			url : '<?php echo base_url();?>reports/get_kontribusi_direktorat',
			dataType: 'json',
			beforeSend: function(){
				tblReportKontribusi.fnClearTable();
			}
		})
		.done(function(response, textStatus, jqhr){	
			if(response.length > 0){
				for(var i=0;i<response.length;i++){
					var rowbefore = (response[i-1]) ? response[i-1].name : '';
					// var el = (response[i].name != rowbefore) ? '<tr><td colspan="24"><strong>'+response[i].name+'</strong></td></tr>' : '';
// 					el	+= '<tr>' +
// 							'<td>'+response[i].kpi+'</td>' + 
// 							'<td>'+bersihkan(response[i].dm_bobot)+'</td>' +
// 							'<td>'+bersihkan(response[i].dm_realisasi)+'</td>' +
// 							'<td>'+bersihkan(response[i].dm_kontribusi)+'</td>' +
// 							'<td>'+bersihkan(response[i].dt_bobot)+'</td>' +
// 							'<td>'+bersihkan(response[i].dt_realisasi)+'</td>' +
// 							'<td>'+bersihkan(response[i].dt_kontribusi)+'</td>' +
// 							'<td>'+bersihkan(response[i].ds_bobot)+'</td>' +
// 							'<td>'+bersihkan(response[i].ds_realisasi)+'</td>' +
// 							'<td>'+bersihkan(response[i].ds_kontribusi)+'</td>' +
// 							'<td>'+bersihkan(response[i].di_bobot)+'</td>' +
// 							'<td>'+bersihkan(response[i].di_realisasi)+'</td>' +
// 							'<td>'+bersihkan(response[i].di_kontribusi)+'</td>' +
// 							'<td>'+bersihkan(response[i].dk_bobot)+'</td>' +
// 							'<td>'+bersihkan(response[i].dk_realisasi)+'</td>' +
// 							'<td>'+bersihkan(response[i].dk_kontribusi)+'</td>' +
// 							'<td>'+bersihkan(response[i].dp_bobot)+'</td>' +
// 							'<td>'+bersihkan(response[i].dp_realisasi)+'</td>' +
// 							'<td>'+bersihkan(response[i].dp_kontribusi)+'</td>' +
// 							'<td>'+bersihkan(response[i].du_bobot)+'</td>' +
// 							'<td>'+bersihkan(response[i].du_realisasi)+'</td>' +
// 							'<td>'+bersihkan(response[i].du_kontribusi)+'</td>' +
// 							'<td>'+bersihkan(response[i].per_realisasi)+'</td>' +
// 							'<td>'+bersihkan(response[i].per_pencapaian)+'</td>' +
// 							'</tr>';
// 						$('#table_kontribusi_direktorat > tbody:last').append(el);

						if(response[i].name != rowbefore){
							tblReportKontribusi.fnAddData(['<strong>'  + response[i].name + '</strong>','','','','','','','','','','','','','','','','','','','','','','','']);
						}
						tblReportKontribusi.fnAddData([
							response[i].kpi, 
							bersihkan(response[i].dm_bobot),
							bersihkan(response[i].dm_realisasi),
							bersihkan(response[i].dm_kontribusi),
							bersihkan(response[i].dt_bobot),
							bersihkan(response[i].dt_realisasi),
							bersihkan(response[i].dt_kontribusi),
							bersihkan(response[i].ds_bobot),
							bersihkan(response[i].ds_realisasi),
							bersihkan(response[i].ds_kontribusi),
							bersihkan(response[i].di_bobot),
							bersihkan(response[i].di_realisasi),
							bersihkan(response[i].di_kontribusi),
							bersihkan(response[i].dk_bobot),
							bersihkan(response[i].dk_realisasi),
							bersihkan(response[i].dk_kontribusi),
							bersihkan(response[i].dp_bobot),
							bersihkan(response[i].dp_realisasi),
							bersihkan(response[i].dp_kontribusi),
							bersihkan(response[i].du_bobot),
							bersihkan(response[i].du_realisasi),
							bersihkan(response[i].du_kontribusi),
							bersihkan(response[i].per_realisasi),
							bersihkan(response[i].per_pencapaian)

						]);
				}
			}
			
			if(!isFixedColumns){
				new FixedColumns( tblReportKontribusi );
				isFixedColumns = true;
			}
		})
		.fail(function(){
		
		});
	}

	function bersihkan(nilai){
		if(nilai == undefined || nilai == null || nilai == "null") 
			return 0;
		else 
			return parseFloat(nilai).toFixed(2);
	}
	$(".btn_report").click(function(){
		var periode=$(this).attr('data-periode');
		var file_type=$(this).attr('data-file');
		var href=$(this).attr('data-href');
		if (periode =='-1'){
			alert("Unit dan periode harus dipilih");
		} else{			
			//console.log(href+"/"+file_type);
			window.location.href=href+"/"+file_type;
		}

	});
}());
</script>