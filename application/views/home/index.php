<!DOCTYPE html>
<html lang="en"> 
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Performance | Home</title>
	<?php echo $css;?>
	<style type="text/css">
		#flot-tooltip { font-size: 12px; font-family: Verdana, Arial, sans-serif; position: absolute; display: none; border: 2px solid; padding: 2px; background-color: #FFF; opacity: 0.8; -moz-border-radius: 5px; -webkit-border-radius: 5px; -khtml-border-radius: 5px; border-radius: 5px; }
		.legend > div {
			width: 100px!important;
		}
	</style>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<?php echo $header;?>
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        	
			<?php echo $left;?>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<?php echo $right;?>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
		
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php echo $footer;?>
	</div>
	<?php echo $js;?>
	<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
	<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>			
	<script language="javascript">				
		function routes(id,title) {		
			$("h3").text(title);
			$('#content-right').load('<?php echo base_url();?>'+id+'?t=' + Math.random());
			$('#loader').show();
		}
		//setInterval(function(){setTime()},5000);
		function setTime(){			
			$.ajax({
				type: 'get',
				url: "<?php echo base_url();?>check_session/index",
				dataType: 'json',
				global: false
			})
			.done(function(response, textStatus, jqhr){
				if(response=="0"){
					window.open('<?php echo base_url();?>login','_self');
				}
			})
		}
			
		$(document).ajaxStart(function() {
			$('#ajax-loader').show();
		});	

		$(document).ajaxStop(function() {
			$('#ajax-loader').hide();
		});	

		$(document).ready(function(){	
			$('#select-periode-dashboard').select2();
			$('#pilih-tampilan-kpi').select2();

			var periodeDashboard = <?=date('Y');?>;
			var kpiIdDashboard = 22;

			$('body').delegate('.open-modal','click', function(ev){
				ev.preventDefault();
				$('#myModalLabel').html($(this).attr('data-title'));
				$('#main-modal-body').html('');
				$("#myModal").modal({show:true,backdrop:true});
			});	
			
			function fillMultiSeriesData(){
				$.ajax({
					url: '<?php echo base_url();?>home/get_stat_perspektif_quarter',
					dataType: 'json'
				})
				.done(function(response, textStatus, jqhr){
					if(response){
						
					}
				})	
				.fail(function(){

				});
			}

			function showTooltip(x, y, contents, z) {
		        $('<div id="flot-tooltip">' + contents + '</div>').css({
		            top: y - 20,
		            left: x - 90,
		            'border-color': z,
		        }).appendTo("body").show();
		    }
			
			function tweenNumber(finalNumber, elemetID){
				$({someValue: 0}).animate({someValue: finalNumber}, {
					duration: 1000,
					easing:'swing',
					step: function() { 
					  	$(elemetID).text(parseInt(this.someValue) + "%");
				  	}
				});
			}
	
			var isColumnFix = false;
			var tblKontribusi = $('#table_kontribusi_direktorat').dataTable({
									"bSort": false,
									"bFilter": false,
									"bInfo":false,
									"bPaginate": false,
									"sScrollY": "400px",
									"sScrollX": "100%",
									"sScrollXInner": "150%",
									"bScrollCollapse": true
								});
			
			
			function calculateLevel(a){
				var e = '<span style="background:#0000FF;color:#FFF">EXCELLENT</span>';
				if(a <= 60){
					e = '<span style="background:#FF0000;color:#FFF">BAD</span>';
				} else if(a > 60 && a <= 90){
					e = '<span style="background:yellow;color:#000">POOR</span>';
				} else if(a > 90 && a <= 100){
					e = '<span style="background:#FF0000;color:#FFF">GOOD</span>';		
				}
				
				return e;
			}	

			$('.modal-body').delegate('.btn-detail-gauge-kpi', 'click', function(ev){
				var kpiId = $(this).attr('kpi-id');
				var perspectiveId = $(this).attr('perspective-id');

				$.ajax({
					url: '<?php base_url();?>home/get_detail_gauge_kpi',
					type: 'post',
					dataType: 'json',
					data: {'periode' : periodeDashboard, 'kpi-id': kpiId},
					beforeSend: function(){
						$('#table-kontribusi-direktorat-detail > tbody:last').empty();
					}
				})
				.done(function(response, textStatus, jqhr){
					if(response){
						$('#modal-detail-gauge').modal('hide');

						var el = ''; 
						for(var i=0;i<response.length;i++){
							var ss = '';
							switch(response[i].progress_id){
								case '1':
								ss = 'Not Started';
								break;
								case '2':
								ss = response[i].progress + '%';
								break;
								default:
								ss = 'Done';
								break;
							}

							el +=	'<tr><td>'+response[i].program_kerja+'</td>'+
							'<td>'+response[i].org_name+'</td>'+
							'<td style="text-align:center;">'+ss+'</td>'+
							'<td>'+response[i].ti+'</td>'+
							'</tr>';
						}

						$('#table-kontribusi-direktorat-detail').append(el);
						$('#btn-back-perspektif').attr('perspective-id', perspectiveId);

						$('#modal-detail-gauge-kpi').modal({show: true, backdrop: true});
					}
				})
				.fail(function(){
					alert('Terjadi Kesalahan, Silahkan Refresh Halaman!');
				}) 

				return false;
			});
			
			$('body').delegate('.btn-detail-gauge','click',function(ev){
				showDialogDetail(this);
			
				return false;
			});

			function showDialogDetail(el){
				var id = $(el).attr('perspective-id');
					
					$.ajax({
						url: '<?php echo base_url();?>home/get_detail_gauge',
						type: 'post',
						dataType: 'json',
						data: { 'periode': periodeDashboard, 'perspective-id': id},
						beforeSend: function(){
							$('#tbl-selected-kpi > tbody:last').empty();
						}
					})
					.done(function(response, dataText, jqhr){
						$('#title-detail-gauge').html(response[0].perspective);

						if(response){
							var t1=0, t2=0, t3=0, t4=0, t5=0, t6=0, t7=0, t8=0;
							for(var i=0;i<response.length;i++){
								t1 += parseFloat(response[i].target) * 1;
								t2 += parseFloat(response[i].bobot) * 1;
								t3 += parseFloat(response[i].realisasi_org) * 1;
								t4 += parseFloat(response[i].pencapaian_org) * 1;
								t5 += parseFloat(response[i].prestasi_org) * 1;
								t6 += parseFloat(response[i].realisasi_penilai) * 1;
								t7 += parseFloat(response[i].pencapaian_penilai) * 1;
								t8 += parseFloat(response[i].prestasi_penilai) * 1;
						
								var rowbefore = (response[i-1]) ? response[i-1].perspective : '';
								var el = (response[i].perspective != rowbefore) ? '<tr><td colspan="12"><strong>'+response[i].perspective+'</strong></td></tr>' : '';
								el += '<tr><td>'+response[i].order_no+'</td>' +
										'<td><a href="#" perspective-id="'+id+'" kpi-id="'+response[i].kpi_id+'" class="btn-detail-gauge-kpi" data-dismiss="modal">'+response[i].kpi_name+'</a></td>' + 
										'<td>'+response[i].satuan+'</td>' +
										'<td>'+response[i].target+'</td>' +
										'<td>'+response[i].bobot+'</td>' +
										'<td>'+response[i].realisasi_org+'</td>' +
										'<td>'+parseFloat(response[i].pencapaian_org).toFixed(2)+'</td>' +
										'<td>'+parseFloat(response[i].prestasi_org).toFixed(2)+'</td>' +
										'<td>'+response[i].realisasi_penilai+'</td>' +
										'<td>'+parseFloat(response[i].pencapaian_penilai).toFixed(2)+'</td>' +
										'<td>'+parseFloat(response[i].prestasi_penilai).toFixed(2)+'</td>' +
										'<td>'+((response[i].keterangan_penilai) ? (response[i].keterangan_penilai) : '') +'</td>' +
										//calculateLevel(response[i].pencapaian_penilai) + 
										'</tr>';

									$('#tbl-selected-kpi > tbody:last').append(el);
							}
					
							//add total rows 
							var htmlTemp = '<tr><td colspan="4">Total</td>' + 
											'<td>'+ t2.toFixed(2) +'</td>' +
											'<td colspan="2"></td>' + 
											'<td>'+ t5.toFixed(2) +'</td>' + 
											'<td colspan="2"></td>' + 
											'<td>'+ t8.toFixed(2) +'</td>' + 
											'<td colspan="2"></td>' + 
											'</tr>';
							$('#tbl-selected-kpi > tbody:last').append(htmlTemp);
						}
					})
					.fail(function(){
					
					});
						
					$('#modal-detail-gauge').modal({show: true, backdrop:true});
			}
			
			
			function buildGaugeOpt(curVal, maxVal, realValue){
				var red = parseInt((60/100) * maxVal);
				var yellow = parseInt((90/100) * maxVal);

				return {
				    chart: {
				        type: 'gauge',
				        plotBackgroundColor: null,
				        plotBackgroundImage: null,
				        plotBorderWidth: 0,
				        plotShadow: false
				    },
				    title: {
				        text: 'Nilai ' + parseFloat(realValue).toFixed(2) + ' %'
				    },
				    pane: {
				        startAngle: -150,
				        endAngle: 150,
				        background: [{
				            backgroundColor: {
				                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
				                stops: [
				                    [0, '#FFF'],
				                    [1, '#333']
				                ]
				            },
				            borderWidth: 0,
				            outerRadius: '109%'
				        }, {
				            backgroundColor: {
				                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
				                stops: [
				                    [0, '#333'],
				                    [1, '#FFF']
				                ]
				            },
				            borderWidth: 1,
				            outerRadius: '107%'
				        }, {
				            // default background
				        }, {
				            backgroundColor: '#DDD',
				            borderWidth: 0,
				            outerRadius: '105%',
				            innerRadius: '103%'
				        }]
				    },
				    // the value axis
				    yAxis: {
				        min: 0,
				        max: parseInt(maxVal),
				        
				        minorTickInterval: 'auto',
				        minorTickWidth: 1,
				        minorTickLength: 10,
				        minorTickPosition: 'inside',
				        minorTickColor: '#666',
				
				        tickPixelInterval: 30,
				        tickWidth: 2,
				        tickPosition: 'inside',
				        tickLength: 10,
				        tickColor: '#666',
				        labels: {
				            step: 2,
				            rotation: 'auto'
				        },
				        title: {
				            text: ''
				        },
				        plotBands: [{
				            from: yellow,
				            to: maxVal,
				            color: '#55BF3B' // green
				        }, {
				            from: red,
				            to: yellow,
				            color: '#DDDF0D' // yellow
				        }, {
				            from: 0,
				            to: red,
				            color: '#DF5353' // red
				        }]        
				    },
				    series: [{
				        name: 'Prestasi',
				        data: [parseInt(curVal)],
				        tooltip: {
				            valueSuffix: ''
				        }
				    }]
				}
			}

			function loadGaugePerspective(periode){
				$.ajax({
					url: '<?php echo base_url();?>home/get_gauge_perspective',
					type: 'post',
					data: {'periode' : periode},
					dataType: 'json'
				})
				.done(function(response, textStatus, jqhr){
					if(response){
						for(var i=0;i<response.length;i++){
							var opt = buildGaugeOpt(response[i].persen, response[i].max, response[i].sebelum_persen);
							$('#gauge-' + parseInt(i+1)).highcharts(opt);
							$('#title-gauge-' + parseInt(i+1)).html('<a href="#" style="color:white;" class="btn-detail-gauge" perspective-id="'+response[i].id+'">' + response[i].name.substr(0,33) + '</a> ('+ response[i].jm_bobot+'%)');
						}
					} else {
						for(var i=0;i<7;i++){
							var opt = buildGaugeOpt(0, 100);
							$('#gauge-' + parseInt(i+1)).highcharts(opt);
							$('#title-gauge-' + parseInt(i+1)).html("No Data");
						}
					}
				})	
				.fail(function(){

				});				
			}

			function caseNull(val){
				if(val == null)
					return 0;
				else
					return parseInt(val); 
			}

			function buildStackedBar(arr){

				var progressData = [];

				for(var i=0;i<arr.length;i++){
					progressData[i] = [caseNull(arr[i].DI),caseNull(arr[i].DK),caseNull(arr[i].DM),caseNull(arr[i].DP),caseNull(arr[i].DS),caseNull(arr[i].DT),caseNull(arr[i].DU)];
				}

				return {
				    chart: {
				        type: 'column'
				    },
				    title: {
				        text: ''
				    },
				    xAxis: {
				        categories: ['DI', 'DK', 'DM', 'DP', 'DS', 'DT', 'DU']
				    },
				    yAxis: {
				        min: 0,
				        title: {
				            text: 'Jumlah'
				        },
				        stackLabels: {
				            enabled: true,
				            style: {
				                fontWeight: 'bold',
				                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
				            }
				        }
				    },
				    tooltip: {
				        formatter: function() {
				            return '<b>'+ this.x +'</b><br/>'+
				                this.series.name +': '+ this.y +'<br/>'+
				                'Total: '+ this.point.stackTotal;
				        }
				    },
				    plotOptions: {
				        column: {
				            stacking: 'normal',
				            dataLabels: {
				                enabled: true,
				                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
				            },
				            events: {
				        		click: function(e){
				        			showDetailPelaksanaanRKM(e.point.category);
				        		}
				        	},
				        	cursor: 'pointer'
				        }
				    },
		            colors: [
		                '#35aa47',
		                '#0000ff',
		                '#ff0000'
		            ],
				    series: [  {
				        name: 'Done',
				        data: progressData[2]
				    },{
				        name: 'On Progress',
				        data: progressData[1]
				    },{
				        name: 'Not Started',
				        data: progressData[0]
				    }]
				}
			}
			
			function showDetailPelaksanaanRKM(direktoratName){
				$.ajax({
					url: '<?php echo base_url();?>home/get_detail_pelaksanaan_rkm',
					type: 'post',
					dataType: 'json',
					data: {'periode' : periodeDashboard, 'kode-direktorat': direktoratName},
					beforeSend: function(){
						$('#tabel_program_kerja > tbody:last').empty();
					}
				})
				.done(function(response, textStatus, jqhr){
					if(response.length > 0){
						var el = '';
						for(var i=0;i<response.length;i++){		
							if(response[i].progress_id==1){
								var progress_id=1;
								var check_1='✔';
								var check_2='';
								var check_3='';
							}else if(response[i].progress_id==2) {
								var progress_id=3;
								var check_1='';
								var check_2=response[i].value + '%';
								var check_3='';
							}else if(response[i].progress_id==3){
								var progress_id=3;
								var check_1='';
								var check_2='';
								var check_3='✔';
							}else {
								var progress_id='';
								var check_1='';
								var check_2='';
								var check_3='';
							}

							var status_verifikasi = 'Belum diverifikasi';
							if(response[i].status_verifikasi == 1){
								status_verifikasi = 'Sudah Diverifikasi';
							} else if(response[i].status_verifikasi == 2) {
								status_verifikasi = 'Sudah Diverifikasi Tapi Belum Sesuai';
							}
		
							el+='<tr>' + 
							'<td>'+(i+1)+'<input type="hidden" name="program_kerja_id[]" value="'+response[i].id+'"/></td>' + 
							'<td>'+response[i].program_kerja+'</td>' + 
							'<td class="center-column" width="100px">'+check_1+'</td>' + 
							'<td class="center-column" width="100px">'+check_2+'</td>' + 
							'<td class="center-column" width="70px">'+check_3+'</td>' +
							'<td>'+status_verifikasi+'</td>' + 
							'<td width="200px">'+((response[i].keterangan != null) ? response[i].keterangan : '')+'</td>' + 
							'</tr>';
							//$('#tabel_program_kerja > tbody:last').append(el);				
						}
						$('#tabel_program_kerja > tbody:last').append(el);	
						$('#title-detail-pelaksanaan-rkm').html('Detail Pelaksanaan RKM ' + response[0].org_name);
						
						$('#modal-detail-pelaksanaan-rkm').modal({show:true, backdrop:true});
					} 
				})
				.fail(function(){
				
				})
			}

			function loadStackedRkm(periode){
				$.ajax({
					url: '<?php echo base_url();?>home/get_stacked_rkm',
					type: 'post',
					data: {'periode': periode},
					dataType: 'json'
				})	
				.done(function(response, textStatus, jqhr){
					if(response){
						loadPieRkm(periode, response);
						var opt = buildStackedBar(response);
						$('#rkm-stacked').highcharts(opt);
					}
				})
				.fail({

				});
			}
			

			function buildOptRKM(arr){
				var progressData = [];

				for(var i=0;i<arr.length;i++){
					progressData[i] = [caseNull(arr[i].DI),caseNull(arr[i].DK),caseNull(arr[i].DM),caseNull(arr[i].DP),caseNull(arr[i].DS),caseNull(arr[i].DT),caseNull(arr[i].DU, caseNull(arr[i].PU))];
				}

				return {
		            chart: {
		                type: 'column'
		            },
		            title: {
		                text: ''
		            },
		            xAxis: {
		                categories: ['DI', 'DK', 'DM', 'DP', 'DS', 'DT', 'DU', 'PERUSAHAAN']
		            },
		            yAxis: {
		                min: 0,
		                title: {
		                    text: 'Prestasi'
		                },
		                allowDecimals: false
		            },
		            tooltip: {
		                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		                    '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
		                footerFormat: '</table>',
		                shared: true,
		                useHTML: true
		            },
		            plotOptions: {
		                column: {
		                    pointPadding: 0.2,
		                    borderWidth: 0
		                }
		            },
		            series: [{
		                name: 'Prestasi',
		                data: progressData[0]
		            }]
		        }
			}

			function buildHistoryBar(arr, periode){
				var progressData = [];

				for(var i=0;i<arr.length;i++){
					progressData[i] = [
						caseNull(arr[i][0]), 
						caseNull(arr[i][1]), 
						caseNull(arr[i][2]), 
						caseNull(arr[i][3]), 
						caseNull(arr[i][4]), 
						caseNull(arr[i][5]), 
						caseNull(arr[i][6]), 
						caseNull(arr[i][7]), 
						caseNull(arr[i][8]), 
						caseNull(arr[i][9]), 
						caseNull(arr[i][10]), 
						caseNull(arr[i][11])
					];
				}

				return {
          chart: {
              type: 'column'
          },
          title: {
              text: ''
          },
          xAxis: {
              categories: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des']
          },
          yAxis: {
              min: 0,
              title: {
                  text: 'Jumlah'
              },
              allowDecimals: false
          },
          tooltip: {
              headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
              pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                  '<td style="padding:0"><b>{point.y} </b></td></tr>',
              footerFormat: '</table>',
              shared: true,
              useHTML: true
          },
          plotOptions: {
              column: {
                pointPadding: 0,
                borderWidth: 0,
                events: {
	        				click : function(e){
	        					showDetailRkmPerBulan(e.point.category, periode);
	        				}
	        			},
	        			cursor: 'pointer'
              }
          },
    		  colors: [
  		      '#ff0000',
            '#0000ff',
            '#35aa47'
          ],
          series: [{
              name: 'Not Started',
              data: progressData[0]
          },{
              name: 'On Progress',
              data: progressData[1]
          },{
              name: 'Done',
              data: progressData[2]
          }]
	      }
			}

			function showDetailRkmPerBulan(bln, periode){
				var month;
				var monthLabel;

				switch(bln){
					case 'Jan':
					month = 1;
					monthLabel = 'Januari';
					break;
					case 'Feb':
					month = 2;
					monthLabel = 'Februari';
					break;
					case 'Mar':
					month = 3;
					monthLabel = 'Maret';
					break;
					case 'Apr':
					month = 4;
					monthLabel = 'April';
					break;
					case 'Mei':
					month = 5;
					monthLabel = 'Mei';
					break;
					case 'Jun':
					month = 6;
					monthLabel = 'Juni';
					break;
					case 'Jul':
					month = 7;
					monthLabel = 'Juli';
					break;
					case 'Aug':
					month = 8;
					monthLabel = 'Agustus';
					break;
					case 'Sep':
					month = 9;
					monthLabel = 'September';
					break;
					case 'Okt':
					month = 10;
					monthLabel = 'Oktober';
					break;
					case 'Nov':
					month = 11;
					monthLabel = 'November';
					break;
					default:
					month = 12;
					monthLabel = 'Desember';
					break;
				}

				$.ajax({
					url: '<?php echo base_url();?>home/get_detail_history_rkm',
					type: 'post',
					data: {'periode': periode, 'month' : month},
					dataType: 'json',
					beforeSend: function(){
						$('#table_detail_history_rkm > tbody:last').empty();
					}					
				})
				.done(function(response){
					if(response){
						var response = response.a;

						var el = '';
						for(var i=0;i<response.length;i++){		
							if(response[i].progress_id==1){
								var progress_id=1;
								var check_1='✔';
								var check_2='';
								var check_3='';
							} else if(response[i].progress_id==2) {
								var progress_id=3;
								var check_1='';
								var check_2=response[i].progress + '%';
								var check_3='';
							} else if(response[i].progress_id==3){
								var progress_id=3;
								var check_1='';
								var check_2='';
								var check_3='✔';
							} else {
								var progress_id='';
								var check_1='';
								var check_2='';
								var check_3='';
							}

							var status_verifikasi = 'Belum diverifikasi';
							if(response[i].status_verifikasi == 1){
								status_verifikasi = 'Sudah Diverifikasi';
							} else if(response[i].status_verifikasi == 2) {
								status_verifikasi = 'Sudah Diverifikasi Tapi Belum Sesuai';
							}
		
							el+='<tr>' + 
							'<td>'+(i+1)+'<input type="hidden" name="program_kerja_id[]" value="'+response[i].id+'"/></td>' + 
							'<td>'+response[i].unit_name+'</td>' + 
							'<td>'+response[i].program_kerja+'</td>' + 
							'<td class="center-column" width="100px">'+check_1+'</td>' + 
							'<td class="center-column" width="100px">'+check_2+'</td>' + 
							'<td class="center-column" width="70px">'+check_3+'</td>' +
							// '<td>'+status_verifikasi+'</td>' + 
							// '<td width="200px">'+((response[i].keterangan != null) ? response[i].keterangan : '')+'</td>' + 
							'</tr>';
						}

						$('#table_detail_history_rkm > tbody:last').append(el);	
						$('#title-detail-history-rkm').html('Detail Pelaksanaan RKM pada Bulan ' + monthLabel + ' ' + periode);

						$('#modal-detail-history-rkm').modal({show: true, backdrop: true});
					}
				})
				.fail(function(){

				})
			}

			function loadPrestasiPerusahaan(periode){
				$.ajax({
					url: '<?php echo base_url();?>home/get_prestasi_perusahaan',
					type: 'post',
					data: {'periode': periode},
					dataType: 'json'
				})	
				.done(function(response, textStatus, jqhr){
					if(response){
						var opt = buildOptRKM(response);
						$('#prestasi-perusahaan-bar').highcharts(opt);
					}
				})
				.fail({

				});	
			}
			

			function buildLineOpt(arr, tahun){
				var catTahun = [];
				for(var i=tahun-9;i<=tahun;i++){
					catTahun.push(i);
				} 

				var seriesData = [];
				for(var j=0;j<4;j++){
					var pcb = arr[j][0];
					seriesData[j] = [];
					for(var i=1;i<11;i++){
						seriesData[j].push(caseNull(pcb["tahun_" + i]));
					}
				}

				return {
		            title: {
		                text: '',
		                x: -20 //center
		            },
		            xAxis: {
		                categories: catTahun
		            },
		            yAxis: {
		                title: {
		                    text: 'Realisasi'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }],
		                min: 0
		            },
		            tooltip: {
		                valueSuffix: ''
		            },
		            series: [{
		                name: 'Realisasi',
		                data: seriesData[0]
		            }, {
		                name: 'Best Performance',
		                data: seriesData[1]
		            }, {
		                name: 'Kompetitor',
		                data: seriesData[2]
		            }, {
		                name: 'Benchmark',
		                data: seriesData[3]
		            }]
		        }
			}

			function loadRealisasiKm(periode, kpiId){
				$.ajax({
					url: '<?php echo base_url();?>home/get_realisasi_km',
					type: 'post',
					data: {'periode': periode, 'kpi_id': kpiId},
					dataType: 'json'
				})	
				.done(function(response, textStatus, jqhr){
					if(response){
						var opt = buildLineOpt(response, periode);
						$('#chart-pencapaian-kpi').highcharts(opt);
					}
				})
				.fail({

				});
			}
			
			function loadKontributorDirektorat(periode){		
				$.ajax({
					type: 'post',
					data: {'periode' : periode},
					url : '<?php echo base_url();?>reports/get_kontribusi_direktorat',
					dataType: 'json',
					beforeSend: function(){
						tblKontribusi.fnClearTable();
					}
				})
				.done(function(response, textStatus, jqhr){	
					if(response){
						for(var i=0;i<response.length;i++){
							var rowbefore = (response[i-1]) ? response[i-1].name : '';
							if(response[i].name != rowbefore){
								tblKontribusi.fnAddData(['<strong>'  + response[i].name + '</strong>','','','','','','','','','','','','','','','','','','','','','','','','']);
							}
							tblKontribusi.fnAddData([
								response[i].kpi, 
								bersihkan(response[i].dm_bobot),
								bersihkan(response[i].dm_realisasi),
								bersihkan(response[i].dm_kontribusi),
								bersihkan(response[i].dt_bobot),
								bersihkan(response[i].dt_realisasi),
								bersihkan(response[i].dt_kontribusi),
								bersihkan(response[i].ds_bobot),
								bersihkan(response[i].ds_realisasi),
								bersihkan(response[i].ds_kontribusi),
								bersihkan(response[i].di_bobot),
								bersihkan(response[i].di_realisasi),
								bersihkan(response[i].di_kontribusi),
								bersihkan(response[i].dk_bobot),
								bersihkan(response[i].dk_realisasi),
								bersihkan(response[i].dk_kontribusi),
								bersihkan(response[i].dp_bobot),
								bersihkan(response[i].dp_realisasi),
								bersihkan(response[i].dp_kontribusi),
								bersihkan(response[i].du_bobot),
								bersihkan(response[i].du_realisasi),
								bersihkan(response[i].du_kontribusi),
								bersihkan(response[i].per_bobot),
								bersihkan(response[i].per_realisasi),
								bersihkan(response[i].per_pencapaian)
							]);
						}
				
						if(!isColumnFix){
							new FixedColumns( tblKontribusi );
							isColumnFix = true;
						}
					}
				})
				.fail(function(){
	
				});
			}

			function loadPieRkm(periode, arr){
				var graphData = [];
				var arrNumber = [];
				var isNotNull = 0;

				for(var i=0;i<arr.length;i++){
					number = caseNull(arr[i].DI) + caseNull(arr[i].DK) + caseNull(arr[i].DM) + caseNull(arr[i].DP) + caseNull(arr[i].DS) + caseNull(arr[i].DT) + caseNull(arr[i].DU) * 1;
					graphData[i] = {data : number};
					arrNumber[i] = number;

					if(number > 0){
						isNotNull++;
					}
				}

				graphData[0].label = 'Not Started ('+arrNumber[0]+')';
				graphData[0].color = '#ff0000';
				graphData[1].label = 'On Progress ('+arrNumber[1]+')';
				graphData[1].color = '#0000ff';
				graphData[2].label = 'Done ('+arrNumber[2]+')';
				graphData[2].color = '#35aa47';

				if(isNotNull > 0){
					$.plot($("#pie-rekap-rkm"), graphData, {
	            series: {
	                pie: {
	                    show: true,
	                    radius: 1,
	                    label: {
	                        show: true,
	                        radius: 3 / 4,
	                        formatter: function (label, series) {
	                            return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">' + label + '<br/>' + Math.round(series.percent) + '%</div>';
	                        },
	                        background: {
	                            opacity: 0.5
	                        }
	                    }
	                }
	            },
	            legend: {
	                show: false
	            }
	        });	
				} else {
					$('#pie-rekap-rkm').html('<center><h2>Data belum tersedia.</h2></center>');
				}
			}

			function loadHistoryRKM(periode){
				$.ajax({
					type: 'POST',
					url: '<?=base_url();?>home/get_history_rkm',
					dataType: 'json',
					data: { 'periode' : periode }
				})
				.done(function(response){
					var sin = [],
	            cos = [];
	            tan = [];

					for (var i = 0; i < 12; i++) {
						var abln = (response.a[i]) ? response.a[i].bulan : i;
						var bbln = (response.a[i]) ? response.b[i].bulan : i;
						var cbln = (response.a[i]) ? response.c[i].bulan : i;

						var ajmlbln = (response.a[i]) ? response.a[i].jml : 0;
						var bjmlbln = (response.b[i]) ? response.b[i].jml : 0;
						var cjmlbln = (response.c[i]) ? response.c[i].jml : 0;


            sin.push(parseInt(ajmlbln));
            cos.push(parseInt(bjmlbln));
            tan.push(parseInt(cjmlbln));
					};

					var arr = [sin, cos, tan];
					var opt = buildHistoryBar(arr, periode);
					//console.log(opt);
					$('#chart-history-rkm').highcharts(opt);

					// plot = $.plot($("#chart-history-rkm"), [{
	    //         data: sin,
	    //         label: "Not Started",
	    //         color: "#ff0000"
	    //     }, {
	    //         data: cos,
	    //         label: "On Progress",
	    //         color: "#0000ff"
	    //     }, {
	    //         data: tan,
	    //         label: "Done",
	    //         color: "#35aa47"
	    //     }], {
	    //         series: {
	    //             lines: {
	    //                 show: true,
	    //                 fill: true
	    //             }
	    //         },
	    //         crosshair: {
	    //             mode: "x"
	    //         },
	    //         grid: {
	    //             hoverable: true,
	    //             autoHighlight: false
	    //         },
	    //         yaxis: {
	    //             min: 0,
	    //             tickSize: 1,
	    //             tickDecimals: 0
	    //         },
	    //         xaxis : {
	    //         	categories : ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des']
	    //         }
	    //     });

	    //     var legends = $("#chart-history-rkm .legendLabel");
	    //     legends.each(function () {
	    //         // fix the widths so they don't jump around
	    //         //$(this).css('width', $(this).width());
	    //     });

	    //     var updateLegendTimeout = null;
	    //     var latestPosition = null;

	    //     function updateLegend() {
	    //         updateLegendTimeout = null;

	    //         var pos = latestPosition;

	    //         var axes = plot.getAxes();
	    //         if (pos.x < axes.xaxis.min || pos.x > axes.xaxis.max || pos.y < axes.yaxis.min || pos.y > axes.yaxis.max) return;

	    //         var i, j, dataset = plot.getData();
	    //         for (i = 0; i < dataset.length; ++i) {
	    //             var series = dataset[i];

	    //             // find the nearest points, x-wise
	    //             for (j = 0; j < series.data.length; ++j)
	    //             if (series.data[j][0] > pos.x) break;

	    //             // now interpolate
	    //             var y, p1 = series.data[j - 1],
	    //                 p2 = series.data[j];
	    //             if (p1 == null) y = p2[1];
	    //             else if (p2 == null) y = p1[1];
	    //             else y = p1[1] + (p2[1] - p1[1]) * (pos.x - p1[0]) / (p2[0] - p1[0]);

	    //             legends.eq(i).text(series.label.replace(/=.*/, "= " + y.toFixed(2)));
	    //         }
	    //     }

	    //     $("#chart-history-rkm").bind("plothover", function (event, pos, item) {
	    //         latestPosition = pos;
	    //         if (!updateLegendTimeout) updateLegendTimeout = setTimeout(updateLegend, 50);
	    //     });
				})
				.fail(function(){

				})
			}

			function bersihkan(nilai){
				if(nilai == undefined || nilai == null || nilai == "null") 
					return 0;
				else 
					return parseFloat(nilai).toFixed(2);
			}

			function initDashboard(){
				loadKontributorDirektorat(periodeDashboard);
				loadRealisasiKm(periodeDashboard, kpiIdDashboard);
				loadPrestasiPerusahaan(periodeDashboard);
				loadStackedRkm(periodeDashboard);
				loadGaugePerspective(periodeDashboard);		
				loadHistoryRKM(periodeDashboard);
			}

			initDashboard();

        	$('#pilih-tampilan-kpi').change(function(){
        		var optionSelected = $(this).find("option:selected");
        		kpiIdDashboard = optionSelected.val();
        		loadRealisasiKm(periodeDashboard, kpiIdDashboard);
        	});

        	$('#select-periode-dashboard').change(function(){
        		var optionSelected = $(this).find("option:selected");	
        		periodeDashboard =  optionSelected.val();
        		initDashboard();
        	});

	        var date = new Date();
	        var d = date.getDate();
	        var m = date.getMonth();
	        var y = date.getFullYear();

	        var h = {};

		    if ($('#calendar-history').parents(".portlet").width() <= 720) {
	            $('#calendar-history').addClass("mobile");
	            h = {
	                left: 'title, prev,next',
	                center: '',
	                right: 'today,month,agendaWeek,agendaDay'
	            };
	        } else {
	            $('#calendar-history').removeClass("mobile");
	            h = {
	                left: 'title',
	                center: '',
	                right: 'prev,next,today,month,agendaWeek,agendaDay'
	            };
	        }

	        $('#calendar-history').html("");

	        $.ajax({
	        	url: '<?php echo base_url();?>home/get_calendar_data',
	        	dataType: 'json'
	        })
	        .done(function(response, textStatus, jqhr){
	        	if(response){
	        		dataEvents = [];
	        		for (var i = 0; i < response.length; i++) {
	        			var aktor = (response[i].name == null) ? "Developer" : response[i].name;
	        			// Split timestamp into [ Y, M, D, h, m, s ]
								var t = response[i].tgl_input.split(/[- :]/);

								// Apply each element to the Date function
								var d = new Date(t[0], t[1]-1, t[2]);

	        			dataEvents.push({
	        				title : response[i].aksi + ' Realisasi oleh ' + aktor,
	        				start : d
	        			});
	        		}

        			$('#calendar-history').fullCalendar({
			            disableDragging: true,
			            header: h,
			            editable: true,
			            events: dataEvents
			        });

			        $('#calendar-display').hide();
	        	}
	        })
	        .fail(function(){

	        });
		});

		$('#btn-show-calendar').click(function(){
			$('#calendar-display').toggle();

			return false;
		});
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
