<script>
	
	function update() {
		var str = $("#fform").serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>input_period/update",
			data: str,
			success: function(msg){
				if(msg=='0') {
					new Messi('Proses submit dokumen telah tutup !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ module('input_period','Realization Input Period'); }});
				} else
				if(msg=='1') {
					new Messi('Proses submit dokumen telah dibuka !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ module('input_period','Realization Input Period'); }});
				} 
				else {
					new Messi('Data gagal diupdate !<br />Pesan : '+msg, {title: 'Document Submission Schedule', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				} 
			},
			error: function(fnc,msg){
				new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
			}
		});
	};	
</script>

	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Tingkat Penilaian
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="#" onClick="routes('parameter_level','Tingkat Penilaian')">Tingkat Penilaian</a> 	
				</li>	
							
			</ul>
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-edit"></i>
						<span class="hidden-480">Daftar Tingkat Penilaian</span>
					</h4>
				</div>
				<div class="portlet-body form">					
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form method="post" name="fform" id="fform" class="form-horizontal">
									<table class="table table-bordered table-stripped">		
											<tr>
												<td width="10%"></td>
												<td width="10%">Minimal &nbsp; &nbsp; &nbsp;&nbsp; Maksimal</td>
												
											</tr>
										<?php foreach ($data as $row){?>
											<tr>
												<td><?php echo $row->name;?></td>
												<td><input type="text" name="minimal[]" value="<?php echo $row->minimal;?>" class="span2">
												<input type="text" name="maksimal[]" value="<?php echo $row->maksimal;?>" class="span2">
												<input type="hidden" name="id[]" value="<?php echo $row->id;?>">
												</td>												
											</tr>
										<?php }	?>											
									</div>
									<tr>
										<td colspan="2" class="center-column">
											<button type="button" name="btn_rangking" id="btn_rangking" onClick="save()" class="btn blue"><i class="icon-save"></i> Simpan</button>		
										</td>
									</tr>
									</table>
								 </form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
	
<script language="javascript">		
	function save() {
		var str = $("#fform").serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>parameter_level/update",
			data: str,
			success: function(msg){
				if(msg=='1') {
					new Messi('Data berhasil diupdate !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('parameter_level','Tingkat Penilaian'); }});
				}
				else {
					new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Profile Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				} 
			},
			error: function(fnc,msg){
				new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
			}
		});
	};	
</script>