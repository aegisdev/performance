<!-- BEGIN PAGE HEADER-->
<div class="row-fluid">					
	<!-- END BEGIN STYLE CUSTOMIZER -->   	
	<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
	<h3 class="page-title">
		Monitor Realisasi	KPI		
	</h3>
	<ul class="breadcrumb">
		<li>
			<i class="icon-home"></i>
			<a href="<?php echo base_url();?>home">Beranda</a> 
			<i class="icon-angle-right"></i>
		</li>
		<li><a href="#">Monitor Realisasi KPI</a> </li>				
	</ul>
	<!-- END PAGE TITLE & BREADCRUMB-->		
	<div class="portlet box green">
		<div class="portlet-title">
			<h4>
				<i class="icon-table"></i>Monitor Realisasi KPI
			</h4>												
		</div>
		<div class="portlet-body">							
			<table border="0" id="data_table">				
				<tr>
					<td>Periode</td>
					<td>
						<select name="year" id="periode" class="span2 m-wrap">
							<option value="-1">-- Pilih Tahun --</value>
							<?php for($year=2013;$year <=(date('Y')+1);$year++){ ?>
							<option value="<?php echo $year;?>"><?php echo $year;?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td width="100px">Filter</td>
					<td colspan="2" width="90%">
						<select name="choose_km" id="choose_km" class="span3 m-wrap">										
							<option value="-1">-- Pilih KM --</value>
							<option value="1">KM Perusahaan</value>
							<option value="2">KM Unit </value>
						</select>
					</td>
				</tr>				
				<tr id="show_unit">
					<td width="100px">Unit</td>
					<td colspan="2" width="90%">
						<select name="organization_id" id="organization_id" class="span6 m-wrap">
							<option value="-1">-- Pilih Unit --</value>
							<?php foreach($data as $d){ ?>
							<option value="<?php echo $d->id;?>"><?php echo $d->name;?></option>
							<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Nama KPI</td>
					<td>
						<select name="kpi" id="kpi" class="span3 m-wrap">
							<option value="-1">-- Pilih KPI --</value>

						</select>
					</td>
				</tr>				
				</table><br/>		
				<div>
					<table border="0">						
					</table>
				</div>
				<div class="clearfix"></div>
				<div id="chart_monitor" style="min-width: 310px; height: 350px; margin: 0 auto"></div>

			<div class="clearfix"></div>
		</div>
	</div>	
</div>
<!-- END PAGE HEADER-->	
<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script language="javascript">
$(document).ready(function(){
	$("#periode").select2(); 
	$("#organization_id").select2(); 
	$("#choose_km").select2(); 	
	$("#kpi").select2(); 	
});
	
(function(){
	$("#show_unit").hide();
	$('#periode').change(function(){
		var optionSelected = $(this).find("option:selected");
		//KM
		if($('#choose_km').val()=='1'){
			$("#show_unit").hide();
			//loadMonitor(optionSelected.val(),'',$('#kpi').val());
		}
		//KM Unit
		if($('#choose_km').val()=='2'){
			$("#show_unit").show();
			//loadMonitor(optionSelected.val(), $('#organization_id').val(),$('#kpi').val());
		}
	});

	$('#choose_km').change(function(){
		var optionSelected = $(this).find("option:selected");
		if($('#choose_km').val()=='1'){
			$("#show_unit").hide();
			//loadMonitor($('#periode').val(),'',$('#kpi').val());
			var periode = $('#periode').val();

			$.ajax({
				url: '<?= base_url();?>kpi/get_kpi_periode',
				type: 'POST',
				dataType: 'json',
				data: { 'periode' : periode, 'unit' : -1},
				beforeSend: function(){
					$("#kpi").empty();
				}
			})
			.done(function(response){
				if(response){
					var html = '<option value="-1" selected>-- Pilih Unit --</value>';
					for (var i = 0; i < response.length; i++) {
						html += '<option value="'+response[i].id+'">' + response[i].text + '</option>';
					};
					$("#kpi").append(html);
				}
			})
			.fail(function(){

			})
		}
		//KM Unit
		if($('#choose_km').val()=='2'){
			$("#show_unit").show();
			//loadMonitor(optionSelected.val(), $('#organization_id').val(),$('#kpi').val());
		}
	});

	$('#organization_id').change(function(){
		var optionSelected = $(this).find("option:selected");
		if($('#choose_km').val()=='2'){
			$("#show_unit").show();
			//loadMonitor($('#periode').val(),optionSelected.val(),$('#kpi').val());
		} 
	});

	$('#kpi').change(function(){
		var optionSelected = $(this).find("option:selected");
		if($('#choose_km').val()=='2'){
			$("#show_unit").show();
		}
		loadMonitor($('#periode').val(),$('#choose_km').val(),optionSelected.val(),$("#organization_id").val());
	});

	function loadMonitor(periode,unit_id,kpi_id,org_id){
		$.ajax({
			url: '<?php echo base_url();?>monitor/get_monitor',
			type: 'post',
			data: {'periode': periode,'unit_id':unit_id,'kpi_id':kpi_id,'org_id':org_id},
			dataType: 'json'
		})	
		.done(function(response, textStatus, jqhr){
			if(response){
				var opt = buildLineOpt(response, periode);
				$('#chart_monitor').highcharts(opt);
			}
		})
		.fail({

		});
	}

	function buildLineOpt(arr, tahun){
		var tgl_input = [];
		var seriesDataOrg =[];		
		var seriesDataPenilai =[];
		var k=0;
		var l=0;

		for(var j=0;j<arr.length;j++){

			if(arr[j].org == 'org'){		
				seriesDataOrg[k]=[];	
				tgl_input.push(arr[j].tgl_input);
				seriesDataOrg[k].push(arr[j].tgl_input,parseFloat(arr[j].realisasi));	
				k++;
			} else if(arr[j].org == 'penilai'){		
				seriesDataPenilai[l]=[];
				tgl_input.push(arr[j].tgl_input);
				seriesDataPenilai[l].push(arr[j].tgl_input,parseFloat(arr[j].realisasi));
				l++;
			}


		}

		return {
			title: {
				text: '',
				x: -20 //center
			},
			xAxis: {
				categories: tgl_input,
				title : {
					text : 'Tanggal Realisasi'
				}
			},
			yAxis: {
				title: {
					text: ''
				},
				plotLines: [{
					value: 0,
					width: 1,
					color: '#808080'
				}],
				min: 0
			},
			tooltip: {
				valueSuffix: ''
			},
			series: [{
				name: 'Penilai',
				data: seriesDataPenilai
			}, {
				name: 'Unit',
				data: seriesDataOrg
			}]
		}
	}

	$('#organization_id').change(function(){
		var optionSelected = $(this).find("option:selected");

		var unit_id = optionSelected.val();
		var periode = $('#periode').val();
		
		$.ajax({
			url: '<?= base_url();?>kpi/get_kpi_periode',
			type: 'POST',
			dataType: 'json',
			data: { 'periode' : periode, 'unit' : unit_id},
			beforeSend: function(){
				$("#kpi").empty();
			}
		})
		.done(function(response){
			if(response){
				var html = '<option value="-1" selected>-- Pilih Unit --</value>';
				for (var i = 0; i < response.length; i++) {
					html += '<option value="'+response[i].id+'">' + response[i].text + '</option>';
				};
				$("#kpi").append(html);
			}
		})
		.fail(function(){

		})
	});

	$('#periode').change(function(){
		var optionSelected = $(this).find("option:selected");

		var unit_id = $('#organization_id').val();
		var periode = optionSelected.val();
		
		$.ajax({
			url: '<?= base_url();?>kpi/get_kpi_periode',
			type: 'POST',
			dataType: 'json',
			data: { 'periode' : periode, 'unit' : unit_id},
			beforeSend: function(){
				$("#kpi").empty();
			}
		})
		.done(function(response){
			if(response){
				var html = '<option value="-1" selected>-- Pilih Unit --</value>';
				for (var i = 0; i < response.length; i++) {
					html += '<option value="'+response[i].id+'">' + response[i].text + '</option>';
				};
				$("#kpi").append(html);
			}
		})
		.fail(function(){

		})
	});

}());
</script>