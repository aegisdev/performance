<style>
#container{background:#ebebeb;}
.switch{
    border:none;
    background:left no-repeat;
    width:105px;
    height:46px;
    padding:0;
    margin:0;
}
 
.on, .off{
    width:50px;
    height:40px;
    display:inline-block;
    cursor:pointer;
}
 
.result{display:none;}
</style>
<script>
$(document).ready(function(){
    $('.switch').css('background', 'url("<?php echo base_url();?>assets/img/switch.png")');
    $('.on_off').css('display','none');
    $('.on, .off').css('text-indent','-10000px');
	
	if(<?php echo $data[0]->status?>==0){$('.switch').css('background-position', 'right');}
	if(<?php echo $data[0]->status?>==1){$('.switch').css('background-position', 'left');}
	
    $("input[name=on_off]").change(function() {
      var button = $(this).val();
        if(button == 'off'){ $('.switch').css('background-position', 'right'); }
        if(button == 'on'){ $('.switch').css('background-position', 'left'); }  
               
         $('.result span').html(button);
         $('.result').fadeIn();
 
   });
 
});
	
	function update() {
		var str = $("#fform").serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>input_period/update",
			data: str,
			success: function(msg){
				if(msg=='0') {
					new Messi('Proses submit dokumen telah tutup !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ module('input_period','Realization Input Period'); }});
				} else
				if(msg=='1') {
					new Messi('Proses submit dokumen telah dibuka !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ module('input_period','Realization Input Period'); }});
				} 
				else {
					new Messi('Data gagal diupdate !<br />Pesan : '+msg, {title: 'Document Submission Schedule', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				} 
			},
			error: function(fnc,msg){
				new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
			}
		});
	};	
</script>

	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Periode Input Realisasi
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="#" onClick="routes('input_period','Periode Input Realisasi')">Periode Input Realisasi</a> 	
				</li>	
							
			</ul>
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-edit"></i>
						<span class="hidden-480">Periode Input Realisasi</span>
					</h4>
				</div>
				<div class="portlet-body form">					
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form method="post" name="fform" id="fform" class="form-horizontal">
									<fieldset>
										<div class="control-group">
											<label class="control-label">Status</label>
											<div class="controls switch">
												<?php if($data[0]->status==0){?>
													<label class="off">Off<input type="radio" class="on_off" name="on_off" value="off" checked/></label>
													<label class="on">On<input type="radio" class="on_off" name="on_off" value="on"/></label>	
												<?php } else { ?>
													<label class="off">Off<input type="radio" class="on_off" name="on_off" value="off"/></label>
													<label class="on">On<input type="radio" class="on_off" name="on_off" value="on" checked/></label>	
												<?php } ?>
											</div>
										</div>
										<div class="form-actions">
											<a href="#" class="btn blue" id="btn_update" onClick="javascript:save();"><i class="icon-edit"></i> Ubah</a>		
										</div>
									</fieldset>	
								 </form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
	
<script language="javascript">		
	function save() {
		var str = $("#fform").serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>input_period/update",
			data: str,
			success: function(msg){
				if(msg=='1') {
					new Messi('Data berhasil diupdate !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('input_period','Realization Input Period'); }});
				}
				else {
					new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Profile Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				} 
			},
			error: function(fnc,msg){
				new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
			}
		});
	};	
	
	$('#old_password').change(function(){
		var password=$('#old_password').val();
		$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>users/IsExist",
				data: {password:password},
				success: function(msg){
					if(msg=='0') {
						new Messi('Password tidak terdaftar !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("old_password").focus();$('#old_password').val(''); }});
						return false;
					} 					
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
	
	});
</script>