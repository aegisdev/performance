	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Hak Akses Group			
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Hak Akses Group</a> </li>				
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->		
				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>Daftar Hak Akses Group
						</h4>												
					</div>
					<div class="portlet-body">
						<!--<div class="clearfix">
							<div class="btn-group">
								<button class="btn blue" onClick="javascript:routes('groups/add','Manegemen Role')">
								<i class="icon-plus"></i> Tambah
								</button>
							</div>							
						</div>-->
						<table class="table table-bordered table-striped table-hover" id="data_table">
							<thead>
								<tr>
									<th width="3%">ID#</th>							
									<th>GRUP</th>
									<th>KETERANGAN</th>
									<th width="130px">AKSI</th>	
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data as $row) { ?>
									<tr>
										<td><?php echo $row->id;?></td>
										<td><?php echo $row->name;?></td>
										<td><?php echo $row->keterangan;?></td>
										<td class="center-column">
											<button class="btn blue btn_edit_role" object="<?php echo $row->id;?>"><i class="icon-edit"></i></button>											
										</td>
									</tr>
									<?php } ?>					
							</tbody>
						</table>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->


<div class="modal" id="modal_btn_edit_role">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Edit User Group Role</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="form-edit-user-group-role">
          <div class="control-group">
              <label class="control-label">Name</label>
              <div class="controls">
                  <input type="text" class="span6 " readonly='readonly' id="edit-group-role-name" name="edit-name" />
                  <span class="help-inline"></span>
              </div>
          </div>
          <div class="control-group">
              <label class="control-label">Menu</label>
              <div class="controls">
                <ul style="list-style:none;">
                <?php 
                  foreach ($menu as $row){
                    if($row->parent_id ==0){
                      echo "<li><label class=\"checkbox\">";  
                      echo  "<input id=\"menu-id-".$row->id."\" type=\"checkbox\" value=\"".$row->id."\" name=\"menu-id[]\" />" . $row->name;
                      echo "</label>";      
                   // } else {
                      $this->db->where('parent_id', $row->id);
                      $this->db->order_by('order_no','asc');
                      $query_2 = $this->db->get('menu');
					  if (count($query_2)>0){	
                     /* echo "<li><label class=\"checkbox\">";
                      echo  "<input class=\"check-all\" id=\"menu-id-".$row->id."\" type=\"checkbox\" value=\"".$row->id."\" name=\"menu-id[]\" />" . $row->name;
                      echo "</label>";*/
                      echo "<ul style=\"list-style:none;\">";

                      foreach ($query_2->result() as $row_2) {
                        echo "<li><label class=\"checkbox\">";
                        echo "<input parent-id=\"".$row->id."\" class=\"children-of-". $row->id." children-checkbox\" id=\"menu-id-".$row_2->id."\" type=\"checkbox\" value=\"".$row_2->id."\" name=\"menu-id[]\">". $row_2->name."</label></li>";       
						
                     
					  } echo  "</ul>";

                      echo "</li>";
					  }
                    }
                  }
                ?>  
                </ul>               
              </div>
          </div>
          <input type="hidden" name="id" id="edit-group-role-id" />
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn blue" id="btn_save"><i class="icon-save"></i> Simpan</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" charset="utf-8">	
	$('#data_table').dataTable({
			"sPaginationType": "bootstrap",
			"oLanguage": {
				"sLengthMenu": "_MENU_ baris per halaman",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next"
				}
			},
			"aoColumnDefs": [{
				'bSortable': false,
				'aTargets': [0,2,3]
			}]
		});		


$(function() {
  $('#modal_btn_edit_role').easyModal();
  //$('input[type=radio], input[type=checkbox]').uniform();
});

$('.check-all').click(function(){
  var parentId = $(this).val();

  if(this.checked){
    $('.children-of-' + parentId).each(function(){
      this.checked = true;
    });
  } else {
    $('.children-of-' + parentId).each(function(){
      this.checked = false;
    });
  }
});

$('.children-checkbox').click(function(){
  var parentId = $(this).attr('parent-id');

  if(this.checked){
    $('#menu-id-' + parentId).attr('checked', true);
  }
});

$('.btn_edit_role').click(function(){
  var id = $(this).attr('object');

  $.ajax({
    url: '<?php echo base_url();?>groups/id/' + id,
    dataType: 'json'
  })
  .done(function(response, textStatus, jqhr){

  })
  .fail(function(){

  })
  .always(function(response, textStatus,jqhr){
    if(response.status == "ok"){
      $('#edit-group-role-id').val(response.data.id);
      $('#edit-group-role-name').val(response.data.name);
    }    
  })

  $.ajax({
    url: '<?php echo base_url();?>roles/id/' + id,
    dataType: 'json',
    beforeSend: function(){
      $('input[type=checkbox]').removeAttr('checked');
    }
  })
  .done(function(response, textStatus, jqhr){
    if(response.status == "ok"){
      for(var i=0;i<response.data.length;i++){
        var id = response.data[i].menu_id;
        $('#menu-id-' + id).attr('checked',true);
      }
    }
  })
  .fail({

  });

  $('#modal_btn_edit_role').trigger('openModal');
});

$('#btn_save').click(function(){
  $.ajax({
    url: '<?php base_url();?>roles/edit/',
    type: 'POST',
    dataType: 'json',
    data: $('#form-edit-user-group-role').serialize()
  })
  .done(function(response, textStatus, jqhr){
    //listUserGroupRole();
    $('#modal_btn_edit_role').trigger('closeModal');
  })
  .fail(function(){

  });
  
 
});
</script>