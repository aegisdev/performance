
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Daftar KPI				
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('kpi','Daftar KPI')">Daftar KPI</a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>Daftar KPI
						</h4>
					</div>
					<div class="portlet-body">	
						<table border="0">							
							<tr>
								<td width="100px">Perspektif</td>
								<td>
									<select name="perspective_id" id="perspective_id" class="span6 m-wrap">
										<option value="">--Pilih Perspektif--</option>
										<?php foreach ($perspectives as $p) { ?>
											<option value="<?php echo $p->id;?>"><?php echo $p->name;?></option>
										<?php } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td width="100px">Nama KPI</td>
								<td width="90%">
									<select name="kpi_id" id="kpi_id" class="span6 m-wrap">
										<option value="">--Pilih KPI--</option>
										<?php foreach ($kpi as $k) { ?>
											<option value="<?php echo $k->id;?>"><?php echo $k->kpi;?></option>
										<?php } ?>
									</select>
								</td>
							</tr>
						</table> <br/>
						<div class="clearfix">
							<div class="btn-group">
								<button class="btn blue" onClick="javascript:routes('kpi/add','Tambah KPI')">
								<i class="icon-plus"></i> Tambah
								</button>
							</div>							
						</div>
						<table class="table table-bordered table-striped table-hover" id="data_table">							
							<thead>
								<tr>
									<th width="3%">ID#</th>
									<th>NAMA KPI</th>														
									<th>PERSPEKTIF</th>								
									<th>SASARAN INISIATIF</th>							
									<th>SATUAN</th>								
									<th>FORMULA</th>								
									<th>BATAS PENILAIAN</th>								
									<th width="130px">AKSI</th>									
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data as $row) {
									
								?>
									<tr>
										<td><?php echo $row->id;?></td>
										<td><?php echo $row->kpi;?></td>
										<td><?php echo $row->perspective_name;?></td>
										<td><?php echo $row->inisiative_name;?></td>
										<td class="center-column"><?php echo $row->unit;?></td>
										<td><?php echo $row->formula;?></td>
										<td class="center-column"><?php echo $row->batas_penilaian;?></td>
										<td class="center-column">
											<button class="btn blue" onClick="javascript:routes('kpi/edit/<?php echo $row->id;?>','Edit KPI')"><i class="icon-edit" title="Edit data"></i></button>
											<button class="btn delete_data red" data-id="<?php echo $row->id;?>" data-label="<?php echo $row->kpi;?>"><i class="icon-trash" title="Hapus data"></i></button>
										</td>
									</tr>
									<?php } ?>			
							</tbody>
						</table>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->

<!-- END PAGE CONTAINER-->		
<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script language="javascript">
$(document).ready( function () {	
	$("#perspective_id").select2(); 
	$("#kpi_id").select2();
	
	$('#data_table').dataTable({	
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ baris per halaman",
			"oPaginate": {
				"sPrevious": "Prev",
				"sNext": "Next"
			}
		},
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': [0]
		}]
	});
});
	$('#data_table').delegate('.delete_data','click',function () { 
		var id =$(this).attr('data-id');
		var label = $(this).attr('data-label');
		new Messi('Yakin ingin menghapus data KPI <strong>'+label+'</strong> ?', {
						title: 'Confirmation', 
						titleClass: 'anim info', 
						closeButton: true,
						buttons: [
						{
							id: 0, 
							label: 'YES', 
							val: '1'
						},{
							id: 1, 
							label: 'NO', 
							val: '0'
						}
						], 
						callback:function(val)
						{ 
							if(val == 0) {
								return false;
							} else if(val == 1) {
								$.ajax({
									type: "POST",
									url: "<?php echo base_url();?>kpi/delete",
									data: "id="+id,
									success: function(msg){
										if(msg=='1') {
											new Messi('Data berhasil dihapus !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('kpi','Managemen KPI'); }});
										} else {
											new Messi('Data gagal dihapus !<br />Debug : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
										} 
									},
									error: function(fnc,msg){
										new Messi('Tidak dapat terhubung ke server untuk malakukan proses hapus data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
									}
								});
							} 
						}
					}
				);
	});
	$("#perspective_id").change(function(){
		var optionSelected = $(this).find("option:selected");
		loadKpiForTable(optionSelected.val(), $('#kpi_id').val());
	});
	$("#kpi_id").change(function(){
		var optionSelected = $(this).find("option:selected");
		loadKpiForTable($('#perspective_id').val(), optionSelected.val());
	});
	function loadKpiForTable(perspective_id, kpi_id){
		$.ajax({
			type: 'post',
			data: {'perspective_id' : perspective_id, 'kpi_id' : kpi_id },
			url : '<?php echo base_url();?>kpi/get_kpi',
			dataType: 'json',
			beforeSend: function(){
				$('#data_table > tbody:last').empty();
			}
		})
		.done(function(response, textStatus, jqhr){
			if(response){
				for(var i=0;i<response.length;i++){					
					var el ='<tr><td>'+response[i].id+'</td>' +
							'<td>'+response[i].kpi+'</td>' + 
							'<td>'+response[i].perspective_name+'</td>' +
							'<td>'+response[i].inisiative_name+'</td>' +
							'<td class="center-column">'+response[i].unit+'</td>' +
							'<td>'+response[i].formula+'</td>' +
							'<td class="center-column">'+response[i].batas_penilaian+'</td>' +
							'<td class="center-column">'+
								'<button class="btn blue" onClick=\'javascript:routes("kpi/edit/'+response[i].id+'","Edit KPI")\'><i class="icon-edit" title="Edit data"></i></button>'+
								'&nbsp<button class="btn delete_data red" data-id="'+response[i].id+'" data-label="'+response[i].kpi+'"><i class="icon-trash" title="Hapus data"></i></button>'+
							'</td>'+
							'</tr>';
						$('#data_table > tbody:last').append(el);
				}
			}
		})
		.fail(function(){
		
		});
	}
</script>
	
	