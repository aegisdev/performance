
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Peta KPI				
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">Peta KPI</a> </li>				
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->		
				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-map-marker"></i>Peta KPI
						</h4>												
					</div>
					<div class="portlet-body">					
						<table id="data_table" class="table table-bordered treetable">
							<thead>
								<tr>					
									<th>NAMA</th>
								</tr>
							</thead>
							<tbody>
								<?php  foreach ($data as $row){
									$data2=$this->model->select('kpi',array('perspective_id'=>$row->id));
								?>
									<tr data-tt-id="<?php echo $row->id;?>">
										<td><?php echo $row->name;?></td>								
									</tr>	
								<?php 
								$no=0;
								foreach ($data2 as $row2){
								?>
									<tr data-tt-parent-id="<?php echo $row->id;?>" data-tt-id="<?php echo $row->id."-".$row2->id;?>">
										<td><?php echo $row2->kpi;?></td>								
									</tr>	
								<?php	}
								} ?>								
								
								
							</tbody>
						</table>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->
	
<script type="text/javascript" charset="utf-8">	
	$('#data_table').treetable({ expandable: true });
	// Highlight selected row
$("#data_table tbody").on("mousedown", "tr", function() {
  $(".selected").not(this).removeClass("selected");
  $(this).toggleClass("selected");
});

// Drag & Drop Example Code
$("#data_table .file, #example-advanced .folder").draggable({
  helper: "clone",
  opacity: .75,
  refreshPositions: true,
  revert: "invalid",
  revertDuration: 300,
  scroll: true
});

$("#data_table .folder").each(function() {
  $(this).parents("#example-advanced tr").droppable({
    accept: ".file, .folder",
    drop: function(e, ui) {
      var droppedEl = ui.draggable.parents("tr");
      $("#data_table").treetable("move", droppedEl.data("ttId"), $(this).data("ttId"));
    },
    hoverClass: "accept",
    over: function(e, ui) {
      var droppedEl = ui.draggable.parents("tr");
      if(this != droppedEl[0] && !$(this).is(".expanded")) {
        $("#data_table").treetable("expandNode", $(this).data("ttId"));
      }
    }
  });
});
</script>