<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Managemen KPI		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>		
				<li><a href="#" onClick="routes('kpi','KPI Management')">Daftar KPI </a> 	
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('kpi/add','Tambah KPI ')"> Tambah</a> </li>						
			</ul>
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-edit"></i>
						<span class="hidden-480">Tambah KPI</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">
									<div class="control-group">
										  <label class="control-label">Nama KPI</label>
										  <div class="controls">
											 <input name="kpi" id="kpi" type="text" value="<?php echo $data[0]->kpi;?>" class="span6 m-wrap"/>
										  </div>
									 </div>
									  <div class="control-group">
										  <label class="control-label">Batas Penilaian</label>
										  <div class="controls">
											  <select name="batas_penilaian_id" id="batas_penilaian_id" class="span6 m-wrap">
												<option value=""></option>
												<?php foreach ($batas_penilaian as $bs) { 
													$selected=($bs->id==$data[0]->batas_penilaian_id) ? "selected":"";
												?>
													<option value="<?php echo $bs->id;?>" <?php echo $selected;?>><?php echo $bs->minimal."-".$bs->maksimal;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									 <div class="control-group">
										  <label class="control-label">Perspektif PKPU</label>
										  <div class="controls">										  
											 <select name="perspective_id" id="perspective_id" class="span6 m-wrap">
												<option value=""></option>
												<?php foreach ($perspectives as $perspective) { 
													$selected=($perspective->id==$data[0]->perspective_id) ? "selected":"";
												?>
													<option value="<?php echo $perspective->id;?>" <?php echo $selected;?>><?php echo $perspective->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									 
									  <div class="control-group">
										  <label class="control-label">Strategis Inisiatif</label>
										  <div class="controls">										  
											  <select name="inisiative_id" id="inisiative_id" class="span6 m-wrap">
												<option value=""></option>
												<?php foreach ($inisiatives as $inisiative) { 
													$selected=($inisiative->id==$data[0]->inisiative_id)?"selected":"";
												?>
													<option value="<?php echo $inisiative->id;?>" <?php echo $selected;?>><?php echo $inisiative->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>								 									  
									  <div class="control-group">
										  <label class="control-label">Satuan</label>
										  <div class="controls">										  
											 <select name="unit_id" id="unit_id" class="span6 m-wrap">
												<option value="">Pilih satuan</option>
												<?php foreach ($units as $unit) { 
													$selected=($unit->id==$data[0]->unit_id) ? "selected":"";
												?>												
													<option value="<?php echo $unit->id;?>" <?php echo $selected;?>><?php echo $unit->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									  <div class="control-group">
										  <label class="control-label">Formula</label>
										  <div class="controls">										  
											 <select name="formula_id" id="formula_id" class="span4 m-wrap">
												<option value=""></option>
												<?php foreach ($formulas as $formula) { 
													$selected=($formula->id==$data[0]->formula_id) ? "selected":"";
												?>
													<option value="<?php echo $formula->id;?>" <?php echo $selected;?>><?php echo $formula->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>									
									
									 <div class="control-group">
										<div class="controls">									
											<input name="id" id="id" type="hidden" value="<?php echo $data[0]->id;?>" class="span6 m-wrap"/>
											<button class="btn blue" type="button" onClick="save()"><i class="icon-save"> Ubah</i></button>
											<button class="btn red" type="button" onClick="routes('kpi','List KPI')"> <i class="icon-share"> Kembali</i></button>
										</div>
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->	
<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script language="javascript">
$(document).ready(function(){
		$("#batas_penilaian_id").select2(); 
		$("#perspective_id").select2(); 
		$("#inisiative_id").select2();
		$("#formula_id").select2(); 
		$("#unit_id").select2(); 
	
});
	function validate() {
		if ($('#kpi').val() =='') {
			new Messi('Unit masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("kpi").focus(); }});
			return false;
		}
		if ($('#type_id').val() =='') {
			new Messi('Type masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("type_id").focus(); }});
			return false;
		}
		if ($('#inisiative_id').val() =='') {
			new Messi('Strategi inisiatif masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("inisiative_id").focus(); }});
			return false;
		}
	
		if ($('#formula_id').val() =='') {
			new Messi('Formula masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("formula_id").focus(); }});
			return false;
		}		
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>kpi/update",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil disimpan !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('kpi','Managemen KPI'); }});
					}
					else if(msg=='2') {
						new Messi('Total bobot tidak boleh lebih dari 100 !', {title: 'Message', titleClass: 'anim warning', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 					
					else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
</script>