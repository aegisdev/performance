<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Managemen KPI		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('kpi','Managemen KPI')">Daftar KPI </a> 
					
				</li>					
			</ul>
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-plus"></i>
						<span class="hidden-480">Tambah KPI</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">
									<div class="control-group">
										  <label class="control-label">Nama KPI</label>
										  <div class="controls">
											 <input name="kpi" id="kpi" type="text" class="span6 m-wrap"/>
										  </div>
									 </div>	
									 <div class="control-group">
										  <label class="control-label">Batas Penilaian</label>
										  <div class="controls">
											  <select name="batas_penilaian_id" id="batas_penilaian_id" class="span6 m-wrap">
												<option value=""></option>
												<?php foreach ($batas_penilaian as $bs) { ?>
													<option value="<?php echo $bs->id;?>"><?php echo $bs->minimal."-".$bs->maksimal;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>					
									 <div class="control-group">
										  <label class="control-label">Perspektif KPKU</label>
										  <div class="controls">										  
											 <select name="perspective_id" id="perspective_id" class="span6 m-wrap">
												<option value=""></option>
												<?php foreach ($perspectives as $perspective) { ?>
													<option value="<?php echo $perspective->id;?>"><?php echo $perspective->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	 
									 <div class="control-group">
										  <label class="control-label">Inisiatif Strategis</label>
										  <div class="controls">										  
											 <select name="inisiative_id" id="inisiative_id" class="span6 m-wrap">
												<option value=""></option>
												<?php foreach ($inisiatives as $inisiative_id) { ?>
													<option value="<?php echo $inisiative_id->id;?>"><?php echo $inisiative_id->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>																			 
									  <div class="control-group">
										  <label class="control-label">Satuan</label>
										  <div class="controls">										  
											 <select name="unit_id" id="unit_id" class="span6 m-wrap">
												<option value=""></option>
												<?php foreach ($units as $unit) { ?>
													<option value="<?php echo $unit->id;?>"><?php echo $unit->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									  <div class="control-group">
										  <label class="control-label">Formula</label>
										  <div class="controls">										  
											 <select name="formula_id" id="formula_id" class="span6 m-wrap">
												<option value=""></option>
												<?php foreach ($formulas as $formula) { ?>
													<option value="<?php echo $formula->id;?>"><?php echo $formula->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>														
									 <div class="control-group">
										<div class="controls">		
											<button class="btn blue" type="button" onClick="save()"><i class="icon-save"> Simpan</i></button>
											<button class="btn red" type="button" onClick="routes('kpi','List KPI')"> <i class="icon-share"> Kembali</i></button>
										</div>
									 </div>							 
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->	
<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script language="javascript">
$(document).ready(function(){
	$("#batas_penilaian_id").select2(); 
	$("#perspective_id").select2(); 
	$("#inisiative_id").select2(); 
	$("#formula_id").select2(); 
	$("#unit_id").select2(); 
});
	function validate() {
		if ($('#kpi').val() =='') {
			new Messi('Unit masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("kpi").focus(); }});
			return false;
		}
		if ($('#type_id').val() =='') {
			new Messi('Type masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("type_id").focus(); }});
			return false;
		}
		if ($('#inisiative_id').val() =='') {
			new Messi('Strategi inisiatif masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("inisiative_id").focus(); }});
			return false;
		}
		if ($('#formula_id').val() =='') {
			new Messi('Formula masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("formula_id").focus(); }});
			return false;
		}		
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>kpi/save",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil disimpan !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('kpi','Manajemen KPI'); }});
					} 
					else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
</script>