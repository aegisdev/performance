<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				User Management		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" data-toggle="tooltip" rel="tooltip" data-placement="top" title="Klik untuk melihat menu management" onClick="routes('users','List Users')">Users </a> </li>		
				<i class="icon-angle-right"></i>	
				<li><a href="#" data-toggle="tooltip" rel="tooltip" data-placement="top" title="Klik untuk melihat menu management"> Edit</a> </li>						
			</ul>
			<div class="portlet box red tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-reorder"></i>
						<span class="hidden-480">Edit User</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">
									<div class="control-group">
										  <label class="control-label">Username</label>
										  <div class="controls">
											 <input name="username" id="username" type="text" value="<?php echo $user[0]->username;?>" class="span6 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="Username" disabled />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Name</label>
										  <div class="controls">
											 <input name="name" id="name" type="text" value="<?php echo $user[0]->name;?>" class="span6 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="Nama" />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Old Password</label>
										  <div class="controls">
											 <input name="old_password" id="old_password" type="password" class="span6 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="Password" />
										  </div>
									 </div> 
									 <div class="control-group">
										  <label class="control-label">New Password</label>
										  <div class="controls">
											 <input name="new_password" id="new_password" type="password"  class="span6 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="Password" />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Re New Password</label>
										  <div class="controls">
											 <input name="re_new_password" id="re_new_password" type="password"  class="span6 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="Password" />
										  </div>
									 </div>
									  <div class="control-group">
										  <label class="control-label">User Group</label>
										  <div class="controls">
											<input name="group_name" id="group_name" type="text" value="<?php echo $user[0]->group_name;?>"  class="span6 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="group_name" disabled />
																					
										  </div>
									 </div>
									 <div class="control-group">
										<div class="controls">	
											<input name="id" id="id" type="hidden" value="<?php echo $user[0]->id;?>">
											<input name="id" id="user_group_id" type="hidden" value="<?php echo $user[0]->user_group_id;?>">
											<button class="btn red" type="button" onClick="save()">Update</button>
											<!-- <button class="btn green" type="button" onClick="routes('users','List Users')">Back</button> -->
										</div>
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
<script language="javascript">
	function validate() {
		if ($('#username').val() =='') {
			new Messi('Nik masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("username").focus(); }});
			return false;
		}
		if ($('#name').val() =='') {
			new Messi('Name masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("name").focus(); }});
			return false;
		}
		if ($('#old_password').val() =='') {
			new Messi('Old password masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("old_password").focus();  }});
			return false;
		}
		if ($('#new_password').val() =='') {
			new Messi('New Password masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("new_password").focus();  }});
			return false;
		}
		if ($('#re_new_password').val() =='') {
			new Messi('Re New Password masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("re_new_password").focus();  }});
			return false;
		}
		if($('#new_password').val() !=$('#re_new_password').val())
		{
			new Messi('New Password dan Re New Password tidak cocok !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("new_password").focus();  }});
			return false;
		}
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>users/update",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil diupdate !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					}
					else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Profile Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
	
	$('#old_password').change(function(){
		var password=$('#old_password').val();
		$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>users/IsExist",
				data: {password:password},
				success: function(msg){
					if(msg=='0') {
						new Messi('Password tidak terdaftar !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("old_password").focus();$('#old_password').val(''); }});
						return false;
					} 					
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
	
	});
</script>