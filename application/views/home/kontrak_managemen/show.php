<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				User Management		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="#" data-toggle="tooltip" rel="tooltip" data-placement="top" title="Klik untuk melihat menu management" onClick="routes('users','List Users')">Users </a> 
					<i class="icon-angle-right"></i>
				</li>		
				
				<li><a href="#" data-toggle="tooltip" rel="tooltip" data-placement="top" title="Klik untuk melihat menu management"> Reset Password </a> </li>						
			</ul>
			<div class="portlet box red tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-reorder"></i>
						<span class="hidden-480">Reset Password</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">
									<div class="control-group">
										  <label class="control-label">Username</label>
										  <div class="controls">
											 <input type="text" value="<?php echo $user[0]->username;?>" class="span6 m-wrap" disabled />
											 <input name="username" id="username" type="hidden" value="<?php echo $user[0]->username;?>" class="span6 m-wrap" />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Name</label>
										  <div class="controls">
											 <input type="text" value="<?php echo $user[0]->name;?>" class="span6 m-wrap" disabled />
											 <input name="name" id="name" type="hidden" value="<?php echo $user[0]->name;?>" class="span6 m-wrap" />
										  </div>
									 </div> 
									 <div class="control-group">
										  <label class="control-label">Email</label>
										  <div class="controls">
												<input type="text" value="<?php echo $user[0]->email;?>" class="span6 m-wrap" disabled />
												<input name="email" id="email" type="hidden" value="<?php echo $user[0]->email;?>" class="span6 m-wrap" />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Password</label>
										  <div class="controls">
											 <input name="password" id="password" type="text" class="span6 m-wrap" value ="airnav" />
										  </div>
									 </div>									 
									  <div class="control-group">
										  <label class="control-label">User Group</label>
										  <div class="controls">
											<input name="group_name" id="group_name" type="text" value="<?php echo $user[0]->group_name;?>"  class="span6 m-wrap" disabled />												
										  </div>
									 </div>
									 <div class="control-group">
										<div class="controls">	
											<input name="id" id="id" type="hidden" value="<?php echo $user[0]->id;?>">										
											<button class="btn red" type="button" onClick="ResetPassword()">Reset</button>											
										</div>
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
<script language="javascript">
	function validate() {
		if ($('password').val() =='') {
			new Messi('Password masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("password").focus();  }});
			return false;
		}		
	}
			
	function ResetPassword() {	
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>users/reset",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Password berhasil direset !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					}
					else {
						new Messi('Password gagal direset !<br />Pesan : '+msg, {title: 'Profile Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
</script>