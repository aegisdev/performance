<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Batas Penilaian	
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>					
				<li>
					<a href="#" onClick="routes('batas_penilaian','Batas Penilaian')">Batas Penilaian</a> 	
					<i class="icon-angle-right"></i>	
				</li>	
				<li><a href="#"> Ubah</a> </li>						
			</ul>
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-edit"></i>
						<span class="hidden-480">Ubah Batas Penilaian</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">
									<div class="control-group">
										  <label class="control-label">Minimal</label>
										  <div class="controls">
											 <input name="minimal" id="minimal" type="text" value="<?php echo $data[0]->minimal;?>" class="span6 m-wrap" />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Maksimal</label>
										  <div class="controls">
											 <input name="maksimal" id="maksimal" type="text" value="<?php echo $data[0]->maksimal;?>" class="span6 m-wrap"/>
										  </div>
									 </div>									 
									 <div class="control-group">
										<div class="controls">	
											<input name="id" id="id" type="hidden" value="<?php echo $data[0]->id;?>">											
											<button class="btn blue" type="button" onClick="save()"><i class="icon-save"></i> Ubah</button>	
											<button class="btn blue" type="button" onClick="routes('batas_penilaian','Batas Penilaian')"><i class="icon-share"></i> Kembali</button>											
										</div>
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->			
<script language="javascript">
	function validate() {
		if ($('#minimal').val() =='') {
			new Messi('Minimal masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("minimal").focus(); }});
			return false;
		}
		if ($('#maksimal').val() =='') {
			new Messi('Maksimal masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("maksimal").focus(); }});
			return false;
		}		
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>batas_penilaian/update",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil disimpan !', {title: 'Message', titleClass: 'anim info', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('batas_penilaian','Batas Penilaian'); }});
					} 
					else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
</script>