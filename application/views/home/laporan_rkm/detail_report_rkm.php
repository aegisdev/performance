<?php 
	$mulai = @(count($a[0]->date) > 0) ? $a[0]->date : '01-01-1970';
	$berlangsung = @(count($b[0]->date) > 0) ? $b[0]->date : '-';
	$done = @(count($c[0]->date) > 0) ? $c[0]->date : '-';
	$tglVerifikasi = @($a[0]->tgl_verifikasi) ? $a[0]->tgl_verifikasi : '-';
	$statusVerifikasi = @($a[0]->status_verifikasi == 0) ? 'Belum diverifikasi' : (($a[0]->status_verifikasi == 1) ? 'Sesuai' : 'Belum Sesuai');
	$keteranganVerifikasi = @($a[0]->keterangan_verifikasi) ? $a[0]->keterangan_verifikasi : '-';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Report</title>
	<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css" rel="stylesheet">	
</head>
<body>
	<h3 align="center">LAPORAN PROGRESS RKM <?= $periode;?></h3>
	<div class="row">
		<div class="span12">
			<table border="0" cellpadding="0px" cellspacing="5px" width="100%" style="margin-left: 10px;">
				<tr valign="top">
					<td>
						<div>
							Judul RKM : <strong><?= $title[0]->program_kerja; ?></strong><br/>
							Tanggal Input RKM : <span id="tgl-input-rkm"><?= $mulai;?></span><br/>
							Tanggal Mulai Pelaksanaan : <span id="tgl-mulai-pelaksanaan"><?= $berlangsung;?></span><br/>
							Tanggal Selesai : <span id="tgl-selesai"><?= $done;?></span><br/>
							Tanggal Verifikasi : <?= $tglVerifikasi;?> <br/>
							Status Verifikasi : <strong><?= $statusVerifikasi;?></strong><br/>
							Keterangan : <?= $keteranganVerifikasi;?> <br/>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div id="tbl-referensi-tanggal">
							<table border="1" width="100%" cellpadding="2px" cellspacing="0">
								<tr style="text-align:center;">
									<th>No</th>
									<th>Tanggal</th>
									<th>Progress</th>
									<th>Keterangan</th>
								</tr>
								<?php 
									for ($i=0; $i < count($data); $i++) { 
										echo '<tr>';
										echo '<td style="text-align:center;">' .($i+1). '. </td>';
										echo '<td>' .$data[$i]->date. '</td>';
										echo '<td style="text-align:center;">' .$data[$i]->value. '%</td>';
										echo '<td>' .$data[$i]->keterangan. '</td>';
										echo '</tr>'; 
									}
								?>
							</table>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
		</div>
	</div>
</body>
</html>