	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Laporan RKM		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('penyusunan_rkm/index','Laporan RKM')">Laporan RKM</a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>Laporan RKM
						</h4>
					</div>
					<div class="portlet-body">						
						<table border="0">
							<tr>
								<td width="100px">Unit</td>
								<td colspan="2" width="90%">
									<select name="organization_id" id="organization_id" class="span6 m-wrap">										
										<option value="-1">-- Pilih Unit --</value>
										<?php foreach($data as $row){ ?>
										<option value="<?php echo $row->id;?>"><?php echo $row->name;?></value>
										<?php } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td width="100px">Periode</td>
								<td>
									<select name="year" id="year" class="span2 m-wrap">
										<option value="-1">-- Pilih Tahun --</value>
										<?php for($year=2013;$year <=(date('Y')+1);$year++){ ?>
										<option value="<?php echo $year;?>"><?php echo $year;?></option>
										<?php } ?>
									</select>
								</td>
								<td>
								</td>
							</tr>
						</table><br/>	
						<div class="pull-right">
							<button class="btn btn_report"  data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="1">.PDF</button>
							<button class="btn btn_report"  data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="2">.XLS</button>
							<!--
							<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="icon-angle-down"></i>
							</button>
							<ul class="dropdown-menu">
								 <li><a href="#" data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="1" class="btn_report">Export to PDF</a></li>
								<li><a href="#" data-href="" data-unit="-1" data-title="-1" data-periode="-1" data-file="2" class="btn_report">Export to Excel</a> </li>
							</ul>
							-->
						</div>					 						
						<form id="form_update_program_kerja">
						<input type="hidden" id="choose_unit" name="choose_unit" />
						<input type="hidden" id="choose_periode" name="choose_periode" />
						<table class="table table-bordered table-striped table-hover" id="tabel_program_kerja">
							<thead>
								<tr>
									<th width="5px">NO</th>
									<th>PROGRAM KERJA</th>													
									<th width="50px">NOT STARTED</th>													
									<th width="50px">IN PROGRESS</th>													
									<th width="50px">DONE</th>		
									<th>STATUS VERIFIKASI</th>									
									<th width="50px">DETAIL</th>													
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						</form>
						
						<div class="clearfix"></div> 
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->

<div class="modal" id="modal_tampilkan_keterangan" style="width:900px!important;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Detail Progress</h4>
      </div>
      <div class="modal-body">
				<div class="portlet box red">
					<div class="portlet-title">
						<h4><i class="icon-reorder"></i>Progress</h4>
					</div>
					<div class="portlet-body">
						<div id="chart_2" class="chart"></div>
					</div>
				</div>
				<div>
					<table border="0" cellpadding="0px" cellspacing="5px">
						<tr valign="top">
							<td>
								<div id="tbl-referensi-tanggal">
									
								</div>
							</td>
							<td width="50%">
								<div style="margin-left: 10px;">
									Tanggal Input RKM : <span id="tgl-input-rkm"></span> <br/>
									Tanggal Mulai Pelaksanaan : <span id="tgl-mulai-pelaksanaan"></span><br/>
									Tanggal Selesai : <span id="tgl-selesai"></span><br/>
									Tanggal Verifikasi : <span id="tgl-verifikasi"></span><br/> 
									Status Verifikasi : <span id="status-verifikasi"></span><br/>
									Keterangan : <span id="keterangan-verifikasi"></span><br/>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="2">&nbsp;</td>
						</tr>
					</table>
				</div>
      </div>
      <div class="modal-footer">
				<button class="btn green btn_report_detail" data-href="-1" data-unit="-1" data-title="-1" data-periode="-1" data-id="-1">Ekspor ke PDF</button>
        <button type="button" class="btn blue" data-dismiss="modal" aria-hidden="true" onclick="$('#modal_tampilkan_keterangan').trigger('closeModal');">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script src="<?php echo base_url();?>assets/flot/jquery.flot.js"></script>
<script src="<?php echo base_url();?>assets/flot/jquery.flot.resize.js"></script>
<script src="<?php echo base_url();?>assets/flot/jquery.flot.crosshair.js"></script>
<script language="javascript">
	$(document).ready(function(){
		$('#modal_tampilkan_keterangan').easyModal();
		$("#organization_id").select2(); 
		$("#year").select2(); 
		$('#tabel_program_kerja').dataTable({		
			"sPaginationType": "bootstrap",
			"oLanguage": {
				"sLengthMenu": "_MENU_ baris per halaman",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next"
				}
			},
			"bSort": false,
			"bFilter": false,
			"bInfo": false,
			"bPaginate": false
		});

		//Interactive Chart
  	function chart2(data) {
      function randValue() {
          return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
      }

      var pageviews = data;
      // console.log(data);
      var plot = $.plot($("#chart_2"), [{
          data: pageviews,
          label: "Progress"
      }], {
          series: {
              lines: {
                show: true,
                lineWidth: 2,
                fill: true,
                fillColor: {
                    colors: [{
                        opacity: 0.05
                    }, {
                        opacity: 0.01
                    }]
                }
              },
              points: {
                 show: true
              },
              shadowSize: 2
          },
          grid: {
              hoverable: true,
              clickable: true,
              tickColor: "#eee",
              borderWidth: 0
          },
          colors: ["#d12610", "#37b7f3", "#52e136"],
          xaxis: {
              ticks: 10,
              tickDecimals: 0
          },
          yaxis: {
              ticks: 10,
              tickDecimals: 0
          }
      });

      function showTooltip(x, y, contents) {
          $('<div id="tooltip">' + contents + '</div>').css({
              position: 'absolute',
              display: 'none',
              top: y + 5,
              left: x + 15,
              border: '1px solid #333',
              padding: '4px',
              color: '#fff',
              'border-radius': '3px',
              'background-color': '#333',
              opacity: 0.80
          }).appendTo("body").fadeIn(200);
      }

      var previousPoint = null;
      $("#chart_2").bind("plothover", function (event, pos, item) {
          $("#x").text(pos.x.toFixed(2));
          $("#y").text(pos.y.toFixed(2));

          if (item) {
              if (previousPoint != item.dataIndex) {
                  previousPoint = item.dataIndex;

                  $("#tooltip").remove();
                  var x = item.datapoint[0].toFixed(2),
                      y = item.datapoint[1].toFixed(2);

                  showTooltip(item.pageX, item.pageY, item.series.label + " of " + x + " = " + y);
              }
          } else {
              $("#tooltip").remove();
              previousPoint = null;
          }
      });
  	}

  	//chart2();

		$('#tabel_program_kerja').delegate('.btn-detail-rkm', 'click', function(){
		  var unit = $('#organization_id').val();
		  var periode = $('#year').val(); 
			var program_kerja_id = $(this).attr('data-id-rkm');
			var title = $(this).attr('data-title-rkm');
			var href = '<?= base_url();?>laporan_rkm/report_detail_rkm/' +unit+ '/' +periode+ '/' +program_kerja_id+ '/';

			$('#modal_tampilkan_keterangan').trigger('openModal');

			$.ajax({
				url: '<?= base_url();?>laporan_rkm/get_progress',
				type: 'POST',
				dataType: 'json',
				data: { 'periode' : periode, 'id-rkm' : program_kerja_id, 'unit' : unit } 
			})
			.done(function(res){
				var response = res.data;
				var mulai = (res.a.length > 0) ? res.a[0].date : '01-01-1970';
				var berlangsung = (res.b.length > 0) ? res.b[0].date : '-';
				var done = (res.c.length > 0) ? res.c[0].date : '-';
				var tglVerikasi = (res.a[0].tanggal_verifikasi != null) ? res.a[0].tanggal_verifikasi : '-';
				var keteranganVerifikasi = (res.a[0].keterangan_verifikasi != null) ? res.a[0].keterangan_verifikasi : '-';
				var data = [];
				var labelStatusVerifikasi = 'Belum diverifikasi';

				if(res.a[0].status_verifikasi == 1){
					labelStatusVerifikasi = 'Sesuai';
				} else if(res.a[0].status_verifikasi == 2){
					labelStatusVerifikasi = 'Belum Sesuai';
				}

				var referensi = '<table width="100%" border="1" cellpadding="5px" cellspacing="5px"><tr><td>No</td><td>Tanggal</td><td>Progress</td><td>Keterangan</td></tr>';

				for (var i = 0; i < response.length; i++) {
					data.push([i+1, parseInt(response[i].value)]);
					referensi += '<tr><td>'+(i+1)+'</td><td>'+response[i].date+'</td><td align="center">'+response[i].value+'%</td><td>'+response[i].keterangan +'</td></tr>';
				};

				referensi += '</table>';

				$('#tgl-input-rkm').text(mulai);
				$('#tgl-mulai-pelaksanaan').text(berlangsung);
				$('#tgl-selesai').text(done);
				$('#status-verifikasi').text(labelStatusVerifikasi);
				$('#tgl-verifikasi').text(tglVerikasi)
				$('#keterangan-verifikasi').text(keteranganVerifikasi);
				$('#tbl-referensi-tanggal').html(referensi);

				$('.btn_report_detail').attr('data-href', href);
				chart2(data);
			})
			.fail(function(e){

			});
		});
	});

	(function(){
		function loadRkm(unit, periode){
			$.ajax({
				type: 'post',
				data: {'unit' : unit, 'periode' : periode},
				url : '<?php echo base_url();?>laporan_rkm/get_progaram_kerja',
				dataType: 'json',
				beforeSend: function(){
					$('#tabel_program_kerja > tbody:last').empty();
					//$('#btn-save-realization-unit').hide();
				}
			})
			.done(function(response, textStatus, jqhr){
				if(response){	
					var  el='';
					el+='<tr><th width="5px">NO</th><th>PROGRAM KERJA</th><th width="50px">NOT STARTED</th><th width="50px">IN PROGRESS</th><th width="50px">DONE</th><th>STATUS VERIFIKASI</th><th width="50px">DETAIL</th></tr>';
					for(var i=0;i<response.length;i++){		
						if(response[i].progress_id==1){
							var progress_id=1;
							var check_1='✔';
							var check_2='';
							var check_3='';
						}else if(response[i].progress_id==2) {
							var progress_id=3;
							var check_1='';
							var check_2=response[i].value + '%';
							var check_3='';
						}else if(response[i].progress_id==3){
							var progress_id=3;
							var check_1='';
							var check_2='';
							var check_3='✔';
						}else {
							var progress_id='';
							var check_1='';
							var check_2='';
							var check_3='';
						}

						var status_verifikasi = 'Belum diverifikasi';
						if(response[i].status_verifikasi == 1){
							status_verifikasi = 'Sudah Diverifikasi';
						} else if(response[i].status_verifikasi == 2) {
							status_verifikasi = 'Sudah Diverifikasi Tapi Belum Sesuai';
						}
						
						el+='<tr>' + 
						'<td>'+(i+1)+'<input type="hidden" name="program_kerja_id[]" value="'+response[i].id+'"/></td>' + 
						'<td>'+response[i].program_kerja+'</td>' + 
						'<td class="center-column" width="100px" > '+check_1+'</td>' + 
						'<td class="center-column" width="100px">'+check_2+'</td>' + 
						'<td class="center-column" width="70px">'+check_3+'</td>' + 
						'<td>'+status_verifikasi+'</td>' + 
						'<td width="50px" class="center-column"><button class="btn blue btn-detail-rkm" type="button" data-id-rkm="'+response[i].id+'" data-title-rkm="'+response[i].program_kerja+'"><i class=" icon-search"></i></button></td>' + 
						'</tr>';
						//$('#tabel_program_kerja > tbody:last').append(el);				
					}
					$('#tabel_program_kerja').html(el);	
				}
			})
			.fail(function(){
			
			});
		}
		
		$('#btn_program_kerja').click(function(){
		  var unit = $('#organization_id').val();
		  var periode = $('#year').val();
		  
		  $.ajax({
			url: '<?php echo base_url();?>pelaksanaan_rkm/get_progaram_kerja',
			dataType: 'json',
			data: {'unit' : unit, 'periode': periode},
			type: 'post',
			beforeSend: function(){
				listTabelProgramKerja.fnClearTable();
			}
		  })
		  .done(function(response, textStatus, jqhr){
			if(response){
				$('#choose_program_kerja_unit').val(unit);
				$('#choose_program_kerja_periode').val(periode);
				
				for(var i=0;i<response.length;i++){
					listTabelProgramKerja.fnAddData([response[i].keterangan, '<input type="checkbox" name="program_kerja_selected[]" value="'+ response[i].id +'" />']);
				}
				$('#modal_program_kerja').trigger('openModal');
			}		
		  })
		  .fail(function(){
		  });
		});
		
		$('#btn_save').click(function(){		
			$.ajax({
				type: 'post',
				url: '<?php echo base_url();?>pelaksanaan_rkm/update_rencana_kerja',
				data: $('#form_update_program_kerja').serialize(),
				dataType: 'json'
			})
			.done(function(response, textStatus, jqhr){
				if(response.status == 'ok'){
					$('#modal_porgam_kerja').trigger('closeModal');
				}
				var unit=$('#choose_unit').val();
				var periode=$('#choose_periode').val();
				loadRkm(unit, periode);
			})
			.fail(function(){
			})
		});

		$('#organization_id').change(function(){
			var optionSelected = $(this).find("option:selected");
			var unit = optionSelected.val();
			var title = $('option:selected').html();
			
			$('.btn_report').attr('data-title',title);
			$('.btn_report').attr('data-unit',unit);	
			
			$('#choose_unit').val(optionSelected.val());		
			if($('#year').val() !=-1){
				var	href= "<?php echo base_url();?>laporan_rkm/get_report_rkm/"+unit+"/"+$('#year').val(); 	
				$('.btn_report').attr('data-href',href);
				loadRkm(optionSelected.val(), $('#year').val());
			}
		});

		$('#year').change(function(){
			var optionSelected = $(this).find("option:selected");	
			var periode =optionSelected.val();	
			$('.btn_report').attr('data-periode',optionSelected.val());
			$('#choose_periode').val(optionSelected.val());	
			if($('#organization_id').val() !=-1){		
				var unit=$('#organization_id').find("option:selected");
				var title = $('#organization_id').find("option:selected").html();
				var	href="<?php echo base_url();?>laporan_rkm/get_report_rkm/"+$('#organization_id').val()+"/"+periode; 	
				$('.btn_report').attr('data-href',href);
				loadRkm($('#organization_id').val(), optionSelected.val());
			}
		});

		$(".btn_report").click(function(){
			var title = $(this).attr('data-title');
			var unit = $(this).attr('data-unit');
			var periode = $(this).attr('data-periode');
			var file_type = $(this).attr('data-file');
			var href = $(this).attr('data-href');
			
			if (unit =='-1' || periode =='-1'){
				alert("Unit dan periode harus dipilih");
			} else{
				//console.log(href+"/"+file_type);
				window.location.href = href+"/"+file_type;
			}
		});

		$('.btn_report_detail').click(function(){
			window.location.href = $(this).attr('data-href');
		});

	}());
</script>
	
	