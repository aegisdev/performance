<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Managemen KPI		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">KPI </a> 	
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('kpi','KPI Management')">Daftar KPI </a> 	
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('kpi/add','Tambah KPI ')"> Tambah</a> </li>						
			</ul>
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-edit"></i>
						<span class="hidden-480">Tambah KPI</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">
									<div class="control-group">
										  <label class="control-label">Nama KPI</label>
										  <div class="controls">
											 <input name="kpi" id="kpi" type="text" value="<?php echo $data[0]->kpi;?>" class="span6 m-wrap"/>
										  </div>
									 </div>
									  <div class="control-group">
										  <label class="control-label">Batas Penilaian</label>
										  <div class="controls">
											  <select name="batas_penilaian_id" id="batas_penilaian_id" class="span3 m-wrap">
												<option value="">Pilih Batas Penilaian</option>
												<?php foreach ($batas_penilaian as $bs) { 
													$selected=($bs->id==$data[0]->batas_penilaian_id) ? "selected":"";
												?>
													<option value="<?php echo $bs->id;?>" <?php echo $selected;?>><?php echo $bs->minimal."-".$bs->maksimal;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									  <div class="control-group">
										  <label class="control-label">Strategi Perspektif</label>
										  <div class="controls">										  
											 <select name="perspective_id" id="perspective_id" class="span4 m-wrap">
												<option value="">Pilih Strategi Perspektif</option>
												<?php foreach ($perspectives as $perspective) { 
													$selected=($perspective->id==$data[0]->perspective_id) ? "selected":"";
												?>
													<option value="<?php echo $perspective->id;?>" <?php echo $selected;?>><?php echo $perspective->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	 						
									 <div class="control-group">
										  <label class="control-label">Strategic Objective</label>
										  <div class="controls">										  
											 <select name="objective_id" id="objective_id" class="span4 m-wrap">
												<option value="">Pilih Strategic Objective</option>
												<?php foreach ($objectives as $objective) { 
													$selected=($objective->id==$data[0]->objective_id) ? "selected":"";
												?>
													<option value="<?php echo $objective->id;?>" <?php echo $selected;?>><?php echo $objective->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									  <div class="control-group">
										  <label class="control-label">Pemilik KPI</label>
										  <div class="controls">										  
											 <select name="organization_id" id="organization_id" class="span6 m-wrap">
												<option value="">Pilih Pemilik KPI</option>
												<?php foreach ($organizations as $organization) { 
													$selected=($organization->id==$data[0]->organization_id) ? "selected":"";
												?>
													<option value="<?php echo $organization->id;?>" <?php echo $selected;?>><?php echo $organization->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>										  
									  <div class="control-group">
										  <label class="control-label">Satuan</label>
										  <div class="controls">										  
											 <select name="unit_id" id="unit_id" class="span3 m-wrap">
												<option value="">Pilih satuan</option>
												<?php foreach ($units as $unit) { 
													$selected=($unit->id==$data[0]->unit_id) ? "selected":"";
												?>												
													<option value="<?php echo $unit->id;?>" <?php echo $selected;?>><?php echo $unit->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									  <div class="control-group">
										  <label class="control-label">Formula</label>
										  <div class="controls">										  
											 <select name="formula_id" id="formula_id" class="span4 m-wrap">
												<option value="">Pilih Formula</option>
												<?php foreach ($formulas as $formula) { 
													$selected=($formula->id==$data[0]->formula_id) ? "selected":"";
												?>
													<option value="<?php echo $formula->id;?>" <?php echo $selected;?>><?php echo $formula->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									
									  <div class="control-group">
										  <label class="control-label">Unit Pendukung</label>
										  <div class="controls">										  
											 <select name="support_id" id="support_id" class="span6 m-wrap">
												 <option value="">Pilih Unit Pendukung</option>		
												<?php foreach ($organizations as $organization) { ?>
													<option value="<?php echo $organization->id."-".$organization->name;?>"><?php echo $organization->name;?></option>
												<?php } ?>										
											 </select>
											 
											<!-- <button type="button" class="btn" id="id"><i class="icon-plus"></i> Tambah</button>-->
											<button type="button" class="btn blue" id="addUnitPendukung"><i class="icon-plus"></i> Tambah</button>
										  </div>									  									  
									 </div>	
									 <div class="control-group" id="UnitPendukungBobot">									  									  
											<?php 
											if (count($kpi_support)>0){
												$i=0;
												foreach ($kpi_support as $ks) {
												$i++;	
												?>	
														<div class="controls" id="unit_member_<?php echo $i;?>">	
														<input type="hidden" name="support_id[]" id="support_id[]" value="<?php echo $ks->organization_id;?>">
														<input type="text" value="<?php echo $ks->name;?>" class="span3 m-wrap" name="unit_support[]"> 
														<input type="text" value="<?php echo $ks->bobot;?>" name="bobot[]" class="span1 m-wrap input_bobot"> 
														<a class="btn btn-medium" onClick="javascript:deleteUnit('<?php echo $i;?>');" href="#deleteUnit" id="deleteUnit" title="Click to delete this delegate member data"><i class="icon-trash"></i></a>
														</div>
													<?php }
											}
											?>	
										  	
									 </div>	
									 <div class="control-group">
										<div class="controls">		
											<input type="hidden" name="count_support" id="count_support" value="<?php echo count($kpi_support);?>">
											<input name="id" id="id" type="hidden" value="<?php echo $data[0]->id;?>" class="span6 m-wrap"/>
											<button class="btn blue" type="button" onClick="save()"><i class="icon-save"> Ubah</i></button>
											<button class="btn blue" type="button" onClick="routes('kpi','List KPI')"> <i class="icon-share"> Kembali</i></button>
										</div>
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->	
<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script language="javascript">
$(document).ready(function(){
		$("#UnitPendukungBobot").show('');
		$("#organization_id").select2(); 
		$("#batas_penilaian_id").select2(); 
		$("#perspective_id").select2(); 
		$("#objective_id").select2(); 
		$("#unit_id").select2(); 
		$("#formula_id").select2(); 
		$("#support_id").select2(); 
});
	function validate() {
		if ($('#kpi').val() =='') {
			new Messi('Unit masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("kpi").focus(); }});
			return false;
		}
		if ($('#type_id').val() =='') {
			new Messi('Type masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("type_id").focus(); }});
			return false;
		}
		if ($('#objective_id').val() =='') {
			new Messi('Objective masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("objective_id").focus(); }});
			return false;
		}
		if ($('#organization_id').val() =='') {
			new Messi('Pemilik KPI masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("organization_id").focus(); }});
			return false;
		}	
		if ($('#unit_id').val() =='') {
			new Messi('Unit masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("unit_id").focus(); }});
			return false;
		}	
		if ($('#formula_id').val() =='') {
			new Messi('Formula masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("formula_id").focus(); }});
			return false;
		}		
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>kpi/update",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil disimpan !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('kpi','Managemen KPI'); }});
					}
					else if(msg=='2') {
						new Messi('Total bobot tidak boleh lebih dari 100 !', {title: 'Message', titleClass: 'anim warning', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 					
					else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
	

	$("#organization_id").change(function(){
		//alert($("#organization_id").val());	
	});
	
	var i = parseInt($("#count_support").val())+1;
	$('#addUnitPendukung').click(function() {		
		var str=$("#support_id").val();
		if(str=="")
		{
			new Messi('Unit pendukung harus dipilih !', {title: 'Message', titleClass: 'anim warning', buttons: [{id: 0, label: 'Close', val: 'X'}]});
			return false;
		}else{
			$("#UnitPendukungBobot").show();
			var spl=str.split("-");
			var org=$("#organization_id").val();
			$("#count_support").val(i);
			var fieldParam = '<div id="unit_member_'+i+'">';
			fieldParam += '<div class="controls">';
			fieldParam += '<input type="hidden" value="'+spl[0]+'" class="span3 m-wrap" name="support_id[]">';
			fieldParam += '<input type="text" value="'+spl[1]+'" class="span3 m-wrap" name="unit_support[]"> <input type="text" class="span1 m-wrap input_bobot" data-bobot="'+spl[1]+'" name="bobot[]"> <a class="btn btn-medium" onClick="javascript:deleteUnit('+i+');" href="#deleteUnit" id="deleteUnit" title="Click to delete this delegate member data"><i class="icon-trash"></i></a><br />';
			fieldParam += '</div>';	
			$(fieldParam).fadeIn('slow').appendTo('#UnitPendukungBobot');		
			i++;	
		}
	});
	function deleteUnit(j) {
		$("#unit_member_"+j).fadeOut(100).remove();
	}
	$(".unit_score").change(function(){
	})
	var total_bobot=0;
	$("#fform").delegate('.input_bobot','keyup',function(){
		if($(this).val()=="")
			total_bobot=0;			
			total_bobot=total_bobot+parseInt($(this).val());		
			/*if(total_bobot>100){
				alert("Tidak boleh lebih dari 100%");
				return false;
			}*/
		$("#total_bobot").val(total_bobot);		
	});
	
</script>