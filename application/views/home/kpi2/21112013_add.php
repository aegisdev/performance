<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Managemen KPI		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#">KPI </a> 	
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('kpi','Managemen KPI')">Daftar KPI </a> 	
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('kpi/add','Tambah KPI ')"> Tambah</a> </li>						
			</ul>
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-plus"></i>
						<span class="hidden-480">Tambah KPI</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">
									<div class="control-group">
										  <label class="control-label">KPI</label>
										  <div class="controls">
											 <input name="kpi" id="kpi" type="text" class="span6 m-wrap"/>
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Tipe</label>
										  <div class="controls">										  
											 <select name="type_id" id="type_id">
												<option value="">Pilih Tipe</option>
												<?php foreach ($types as $type) { ?>
													<option value="<?php echo $type->id;?>"><?php echo $type->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									 <div class="control-group">
										  <label class="control-label">Strategic Objective</label>
										  <div class="controls">										  
											 <select name="objective_id" id="objective_id">
												<option value="">Pilih Strategic Objective</option>
												<?php foreach ($objectives as $objective) { ?>
													<option value="<?php echo $objective->id;?>"><?php echo $objective->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									  <div class="control-group">
										  <label class="control-label">Owner</label>
										  <div class="controls">										  
											 <select name="owner_id" id="owner_id">
												<option value="">Pilih Owner</option>
												<?php foreach ($objectives as $objective) { ?>
													<option value="<?php echo $objective->id;?>"><?php echo $objective->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									  <div class="control-group">
										  <label class="control-label">Result Collector</label>
										  <div class="controls">										  
											 <select name="collector_id" id="collector_id">
												<option value="">Pilih Result Collector</option>
												<?php foreach ($objectives as $objective) { ?>
													<option value="<?php echo $objective->id;?>"><?php echo $objective->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									  <div class="control-group">
										  <label class="control-label">Unit</label>
										  <div class="controls">										  
											 <select name="unit_id" id="unit_id">
												<option value="">Pilih Unit</option>
												<?php foreach ($units as $unit) { ?>
													<option value="<?php echo $unit->id;?>"><?php echo $unit->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									  <div class="control-group">
										  <label class="control-label">Tipe Skor</label>
										  <div class="controls">										  
											 <select name="score_id" id="score_id">
												<option value="">Pilih Tipe Skor</option>
												<?php foreach ($scores as $score) { ?>
													<option value="<?php echo $score->id;?>"><?php echo $score->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									 <div class="control-group">
										  <label class="control-label">Formula</label>
										  <div class="controls">										  
											 <select name="formula_id" id="formula_id">
												<option value="">Pilih Formula</option>
												<?php foreach ($formulas as $formula) { ?>
													<option value="<?php echo $formula->id;?>"><?php echo $formula->name;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	
									 <div class="control-group">
										<div class="controls">							
											<button class="btn blue" type="button" onClick="save()"><i class="icon-save"> Simpan</i></button>
											<button class="btn blue" type="button" onClick="routes('kpi','List KPI')"> <i class="icon-share"> Kembali</i></button>
										</div>
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
<script language="javascript">
	function validate() {
		if ($('#kpi').val() =='') {
			new Messi('Unit masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("kpi").focus(); }});
			return false;
		}
		if ($('#type_id').val() =='') {
			new Messi('Type masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("type_id").focus(); }});
			return false;
		}
		if ($('#objective_id').val() =='') {
			new Messi('Objective masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("objective_id").focus(); }});
			return false;
		}
		if ($('#owner_id').val() =='') {
			new Messi('Owner masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("owner_id").focus(); }});
			return false;
		}	
		if ($('#collector_id').val() =='') {
			new Messi('Collector masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("collector_id").focus(); }});
			return false;
		}	
		if ($('#unit_id').val() =='') {
			new Messi('Unit masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("unit_id").focus(); }});
			return false;
		}	
		if ($('#score_id').val() =='') {
			new Messi('Score masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("score_id").focus(); }});
			return false;
		}	
		if ($('#formula_id').val() =='') {
			new Messi('Formula masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("formula_id").focus(); }});
			return false;
		}		
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>kpi/save",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil disimpan !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('kpi','KPI Management'); }});
					} 
					else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
</script>