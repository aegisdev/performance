	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				User Guide			
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="javascript:routes('help/guide','Guide')">User Guide</a> </li>				
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			
				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-book"></i>
							User Guide
						</h4>
					</div>
					<div class="portlet-body">	
						<div class="about-us">
							<div class="text-right">
								 <a href="<?php echo base_url();?>user_guide/USER_GUIDE.pdf" target="_blank" class="btn blue">Download User Guide</a>
							</div>
							Patunjuk:<br/>
							Klik pada menu/submenu untuk melihat isi user guide yang dinginkan.<br/><br/>
                                 <div id="accordion" class="accordion in collapse" style="height: auto;">									 
									  <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseA" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												 A. Login
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseA">
											 <div class="accordion-inner">
											 <p>Untuk dapat mengakses aplikasi EPD, user harus melakukan:</p>

												<ol>
													<li>Bukalah aplikasi EPD menggunakan <em>browser</em> (Internet explorer atau Mozilla firefox atau lainnya) dengan alamat url <a href="http://162.243.253.41/performance/">http://162.243.253.41/performance/</a>, sistem akan menampilkan form login seperti pada gambar A.1.</li>
													<image src="<?php echo base_url();?>user_guide/images/login.jpg"></image>
													<li>Masukan <strong>Username</strong></li>
													<li>Masukan <strong>Password</strong></li>
													<li>Pilih tombol <strong>Login</strong></li>
												</ol>

												<p>Jika login berhasil, user akan dibawa ke halaman utama seperti pada gambar B.1</p>						

												<p>Keterangan:</p>

												<p>Jika belum memiliki account untuk login ke aplikasi EPD, silakan menghubungi administrator</p>

												
											 </div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												 B. Halaman Utama
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseTwo">
											 <div class="accordion-inner">
											  <p>Untuk dapat mengakses aplikasi EPD, user harus melakukan:</p>
												<image src="<?php echo base_url();?>user_guide/images/halaman_utama.jpg"></image>
												<ol type="a">
													<li><strong>Menu</strong> user berdasarkan hak akses user</li>
													<li>Halaman Dashboard</li>
													<li>Menu identitas user yang terdiri dari <strong>My</strong> <strong>Profile </strong>dan <strong>Log Out</strong></li>
												</ol>
											 </div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseThree" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												 C. Menu
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseThree">
											 <div class="accordion-inner">
												Menu aplikasi EPD terdiri dari <strong>Dashboard, Laporan, Referensi Strategis, Kontrak, Manajemen, KPI dan Program Kerja, Parameter, Pengaturan Pengguna, dan Help</strong> seperti yang diperlihatkan pada gambar C.1<br/>
												<image src="<?php echo base_url();?>user_guide/images/menu.jpg"></image>
											 </div>
										 </div>
									 </div>									
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseFour" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.1. Referensi Strategi
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseFour">
											 <div class="accordion-inner">
											 <p>Menu aplikasi EPD terdiri dari <strong>Dashboard</strong>, <strong>Laporan</strong>, <strong>Referensi</strong> <strong>Strategis</strong>, <strong>Kontrak</strong>, <strong>Manajemen</strong>, <strong>KPI dan Program Kerja</strong>, <strong>Parameter</strong>, <strong>Pengaturan Pengguna</strong>, dan <strong>Help</strong> seperti yang diperlihatkan pada gambar C.1</p>
												<image src="<?php echo base_url();?>user_guide/images/referensi_strategi.jpg"></image>
											 </div>									 
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseFive" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.1.1 Submenu Sasaran Strategi
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseFive">
											 <div class="accordion-inner">
												<p>Submenu ini untuk mengelola data sasaran strategis yang berupa menampilkan list data, Tambah, Edit, dan Delete.</p>
												<p><strong><u>Menampilkan Daftar Sasaran Strategis</u></strong></p>													
												<ol>
													<li>Pilih submenu <strong>Sasaran Strategis</strong>, <strong>Daftar Sasaran Strategis</strong> akan ditampilkan&nbsp; seperti pada gambar C.1.1.1</li>
												</ol>
												<image src="<?php echo base_url();?>user_guide/images/sasaran_strategis.jpg"></image>
												<p>Keterangan:</p>

												<ol>
													<li><strong>Tambah</strong>, untuk membuka form <strong>Tambah Sasaran Strategis</strong></li>
													<li><strong>Edit</strong>, untuk menampilkan form Ubah Sasaran Strategis yang dipilih</li>
													<li><strong>Hapus</strong>, untuk menghapus data Sasaran Strategis yang dipilih</li>
												</ol>
												<p><strong><u>Menampilkan Daftar Sasaran Strategis</u></strong></p>

												<ol>
													<li>Pilih submenu <strong>Sasaran Strategis</strong>, <strong>Daftar Sasaran Strategis</strong> akan ditampilkan&nbsp; seperti pada gambar C.1.1.1</li>
												</ol>
													
												<p>	<image src="<?php echo base_url();?>user_guide/images/sasaran_strategis_list.jpg"></image>
												<p>Keterangan:</p>
												</p>
												<ol type="a">
													<li><strong>Tambah</strong>, untuk membuka form <strong>Tambah Sasaran Strategis</strong></li>
													<li><strong>Edit</strong>, untuk menampilkan form Ubah Sasaran Strategis yang dipilih</li>
													<li><strong>Hapus</strong>, untuk menghapus data Sasaran Strategis yang dipilih</li>
												</ol>
										
										<p><strong><u>Menambah Data Sasaran Strategis</u></strong></p>
											<ol>
												<li>Pilih tombol <strong>Tambah</strong>, form <strong>Tambah Sasaran Strategis</strong> akan ditampilkan seperti pada gambar C.1.1.2</li>
												<li>Isi data <strong>Sasaran</strong> *)</li>
												<li>Pilih tombol <strong>Simpan </strong>untuk menyimpan data sasaran strategis yang baru</li>
											</ol>
											<image src="<?php echo base_url();?>user_guide/images/sasaran_strategis_add.jpg"></image>
											<p>Keterangan :</p>

											<ol type="a">
												<li><strong>Kembali</strong>, tombol untuk menutup form <strong>Tambah Sasaran Strategis</strong> dan kembali ke <strong>Daftar Sasaran Strategis</strong> seperti yang ditampilkan pada gambar C.1.1.1</li>
											</ol>
											<p><strong><u>Mengubah Data Sasaran Strategis</u></strong></p>
											<ol>
												<li>Pilih tombol <strong>Ubah</strong> pada data sasaran strategis yang ingin diubah, form <strong>Ubah Sasaran Strategis</strong> akan ditampilkan seperti pada gambar C.1.1.3</li>
													<image src="<?php echo base_url();?>user_guide/images/sasaran_strategis_edit.jpg"></image>
												<li>Isi nama sasaran dengan data yang baru *)</li>
												<li>Pilih tombol <strong>Ubah </strong>untuk menyimpan perubahan data sasaran strategis</li>
											</ol>

											<p>Keterangan :</p>
											<ol type="a">
												<li><strong>Kembali</strong>, tombol untuk menutup form <strong>Tambah Sasaran Strategis</strong> dan kembali ke <strong>Daftar Sasaran Strategis</strong> seperti yang ditampilkan pada gambar C.1.1.1</li>
											</ol>
											<p><strong><u>Hapus Data Sasaran Strategis</u></strong></p>

											<ol>
												<li>Pilih tombol <strong>Hapus </strong>pada sata sasaran strategis yang ingin dihapus, dialog konfirmasi penghapusan data sasaran strategis akan ditampilkan seperti pada gambar C.1.1.4</li>
												<li>Pilih tombol <strong>Yes </strong>utuk menghapus data sasaran strategis</li>
											</ol>
											<image src="<?php echo base_url();?>user_guide/images/sasaran_strategis_delete.jpg"></image>
											<p>Keterangan :</p>
											<ol type="a">
												<li><strong>No</strong>, tombol untuk menutup dialog penghapusan data sasaran strategis dan kembali ke <strong>Daftar Sasaran Strategis</strong> seperti yang ditampilkan pada gambar C.1.1.1</li>
											</ol>
											 </div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseSix" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												 C.1.2. Submenu Tema Strategis
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseSix">
											 <div class="accordion-inner">
												<p>Submenu ini untuk mengelolah data tema strategis yang berupa menampilkan list data, <strong>Tambah, Edit</strong>, dan <strong>Delete.</strong></p>
												<image src="<?php echo base_url();?>user_guide/images/tema_strategis.jpg"></image>
												
												<p><strong><u>Menampilkan Daftar Tema Strategis</u></strong></p>
												<ol>
													<li>Pilih submenu Tema Strategis, <strong>Daftar Tema Strategis</strong> akan ditampilkan seperti pada gambar C.1.2.1</li>
												</ol>
												<image src="<?php echo base_url();?>user_guide/images/tema_strategis_list.jpg"></image>
												<p>Keterangan:</p>
												<ol style="list-style-type:lower-alpha">
													<li><strong>Tambah</strong>, untuk membuka form <strong>Tambah Tema Strategis</strong></li>
													<li><strong>Edit</strong>, untuk menampilkan form <strong>Ubah Tema Strategis</strong> yang dipilih</li>
													<li><strong>Hapus</strong>, utuk menghapus data tema strategis yang dipilih</li>
												</ol>
												<p><strong><u>Menambah Data Tema Strategis</u></strong></p>
												<ol>
													<li>Pilih tombol <strong>Tambah</strong>, form <strong>Tambah Tema Strategis</strong> akan ditampilkan seperti pada gambar C.1.2.3</li>
												
												<image src="<?php echo base_url();?>user_guide/images/tema_strategis_add.jpg"></image>
												<li>Isi data <strong>Tema*)</strong></li>
												<li>Pilih <strong>Sasaran</strong> tema *)</li>
												<li>Isi <strong>Tujuan</strong> tema (opsional)</li>
												<li>Pilih tombol <strong>Simpan </strong>untuk menyimpan data tema strategis yang baru</li>
											</ol>

											<p>Keterangan :</p>

											<ol style="list-style-type:lower-alpha">
												<li><strong>Kembali</strong>, tombol untuk menutup form <strong>Tambah Tema Strategis</strong> dan kembali ke <strong>Daftar Tema Strategis</strong> seperti yang ditampilkan pada gambar C.1.2.1</li>
											</ol>
											<p><strong><u>Mengubah Data Tema Strategis</u></strong></p>
											<ol>
												<li>Pilih tombol <strong>Ubah </strong>pada data tema strategis yang ingin diubah, form <strong>Ubah Tema Strategis</strong>&nbsp; akan ditampilkan seperti pada gambar C.1.2.4</li>											

												<image src="<?php echo base_url();?>user_guide/images/tema_strategis_edit.jpg"></image>
												
												<li>Isi <strong>Tema</strong> dengan data yang baru *)</li>
												<li>Pilih <strong>Sasaran</strong> dengan data yang baru *)</li>
												<li>Isi <strong>Tujuan</strong> dengan data yang baru (opsional)</li>
												<li>Pilih tombol <strong>Ubah </strong>untuk menyimpan perubahan data tema strategis</li>
											</ol>

											<p>Keterangan :</p>

											<ol style="list-style-type:lower-alpha">
												<li><strong>Kembali</strong>, tombol untuk menutup form <strong>Ubah Tema Strategis</strong> dan kembali ke <strong>Daftar Tema Strategis</strong> seperti yang ditampilkan pada gambar C.1.2.1</li>
											</ol>
											<p><strong><u>Hapus Data Tema Strategis</u></strong></p>
												<ol>
													<li>Pilih tombol <strong>Hapus </strong>pada data tema strategis yang ingin dihapus, dialog konfirmasi penghapusan data tema strategis akan ditampilkan seperti pada gambar C.1.2.5</li>
													<image src="<?php echo base_url();?>user_guide/images/tema_strategis_delete.jpg"></image>
												<li>Pilih tombol <strong>Yes </strong>untuk menghapus data tema strategis</li>
											</ol>

											<p>Keterangan :</p>
											<ol style="list-style-type:lower-alpha">
												<li><strong>No</strong>, tombol untuk menutup dialog penghapusan data tema strategis dan kembali ke <strong>Daftar Tema Strategis</strong> seperti yang ditampilkan pada gambar C.1.2.1</li>
											</ol>												
											 </div>
										 </div>
									 </div>									
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC13" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												 C.1.3. Submenu Inisiatif Strategis
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC13">
											 <div class="accordion-inner">
												<p>Submenu ini untuk mengelola data inisiatif strategis yang berupa menampilkan list data, <strong>Tambah, Edit</strong>, dan <strong>Delete.</strong></p>
												<image src="<?php echo base_url();?>user_guide/images/inisiatif_strategis.jpg"></image>
												<p><strong><u>Menampilkan Daftar Inisiatif Strategis</u></strong></p>
												<ol>
													<li>Klik submenu <strong>Inisiatif Strategis</strong>, <strong>Daftar Inisiatif Strategis</strong> akan ditampilkan seperti pada gambar C.1.3.1</li>
												</ol>
												<p>Keterangan:</p>
												<ol style="list-style-type:lower-alpha">
													<li><strong>Tambah</strong>, untuk membuka form <strong>Tambah Inisiatif Strategis</strong></li>
													<li><strong>Edit</strong>, utnuk menampilkan form Ubah Inisiaatif Strategis yang dipilih</li>
													<li><strong>Hapus</strong>, untuk menghapus data inisiatif strategis yang dipilih</li>
												</ol>
												<p><strong><u>Menampilkan Daftar Inisiatif Strategis</u></strong></p>
												<ol>
													<li>Klik submenu <strong>Inisiatif Strategis</strong>, <strong>Daftar Inisiatif Strategis</strong> akan ditampilkan seperti pada gambar C.1.3.1</li>
												</ol>												
												<image src="<?php echo base_url();?>user_guide/images/inisiatif_strategis_list.jpg"></image>
												<p>Keterangan:</p>
												<ol style="list-style-type:lower-alpha">
													<li><strong>Tambah</strong>, untuk membuka form <strong>Tambah Inisiatif Strategis</strong></li>
													<li><strong>Edit</strong>, utnuk menampilkan form Ubah Inisiaatif Strategis yang dipilih</li>
													<li><strong>Hapus</strong>, untuk menghapus data inisiatif strategis yang dipilih</li>
												</ol>
											<p><strong><u>Menambah Data Inisiatif Strategis</u></strong></p>
												<ol>
													<li>Pilih tombol <strong>Tambah</strong>, form <strong>Tambah Inisiatif Strategis</strong> akan ditampilkan seperti pada gambar C.1.3.2</li>		
												<image src="<?php echo base_url();?>user_guide/images/inisiatif_strategis_add.jpg"></image>
													<li>Isi <strong>Inisiatif</strong></li>
													<li>Pilih <strong>Tema</strong></li>
													<li>Pilih tombol <strong>Simpan </strong>data inisiatif strategis yang baru</li>
												</ol>
												<p>Keterangan :</p>
												<ol style="list-style-type:lower-alpha">
													<li><strong>Kembali</strong>, tombol untuk menutup form <strong>Tambah Inisiatif Strategis</strong> dan kembali ke <strong>Daftar Inisiatif Strategis</strong> seperti yang ditampilkan pada gambar C.1.3.1</li>
												</ol>
												<p><strong><u>Mengubah Data Inisiatif Strategis</u></strong></p>
												<ol>
													<li>Pilih tombol <strong>Ubah </strong>pada data inisiatif strategis yang ingin diubah, form Ubah Inisiatif Strategis akan ditampilkan seperti pada gambar C.1.3.3</li>
												<image src="<?php echo base_url();?>user_guide/images/inisiatif_strategis_edit.jpg"></image>
													<li>Isi <strong>Inisiatif</strong> dengan data yang baru *)</li>
													<li>Pilih <strong>Tema</strong> dengan data yang baru *)</li>
													<li>Pilih tombol <strong>Ubah</strong></li>
												</ol>
												<p>Keterangan :</p>
												<ol style="list-style-type:lower-alpha">
													<li><strong>Kembali</strong>, tombol kembali ke gambar C.1.3.1</li>
												</ol>
													<p><strong><u>Hapus Data Inisiatif Strategis</u></strong></p>
												<ol>
														<li>Pilih tombol <strong>Hapus</strong>, form konfirmasi penghapusan data inisiatif strategis akan ditampilkan&nbsp; seperti pada gambar C.1.3.4</li>						
														<li>Pilih tombol <strong>Yes </strong>untuk menghapus data inisiatif strategis</li>
												</ol>
												<image src="<?php echo base_url();?>user_guide/images/inisiatif_strategis_delete.jpg"></image>		
												<p>Keterangan :</p>
												<ol style="list-style-type:lower-alpha">
													<li><strong>No</strong>, tombol untuk menutup dialog penghapusan data inisiatif strategis dan kembali ke <strong>Daftar Inisiatif Strategis</strong> seperti yang ditampilkan pada gambar C.1.3.1</li>
												</ol>
											 </div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapse9" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												 C.2. Parameter
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapse9">
											 <div class="accordion-inner">
												<p>Menu Parameter terdiri dari submenu <strong>Struktur Organisasi, Satuan, Jenis Formula, Batas Penilaian, Tingkat Penilaian, </strong>dan <strong>Periode Input Realisasi</strong> seperti yang ditampikan pada gambar C.2</p>

												<image src="<?php echo base_url();?>user_guide/images/parameter.jpg"></image>												
											 </div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapse10" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.2.1. Submenu Struktur Organisasi
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapse10">
											 <div class="accordion-inner">
												<p>Langkah-langkah mengelolah data pada submenu Struktur Organiasi :</p>
												<ol>
													<li>Pilih <strong>Parameter </strong><strong>&agrave;</strong><strong> Struktur Organisasi</strong> seperti pada gambar C.2.1, data struktur organisasi akan ditampilkan seperti pada gambar C.2.1.1</li>
												</ol>

												<image src="<?php echo base_url();?>user_guide/images/struktur_organisasi.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/struktur_organisasi_1.jpg"></image>
												<p>Keterangan:</p>
												<ol style="list-style-type:lower-alpha">
													<li><strong>Tambah</strong>, untuk menampilkan form <strong>Tambah Organisasi</strong></li>
													<li>Klik pada nama organisasi/unit untuk menampilkan form <strong>Ubah</strong> <strong>Organisasi</strong> yang dipilih.</li>
													<li><em>Scroll</em> untuk menggeser <strong>Struktur Organisasi</strong> ke kiri/kanan</li>
												</ol>
												<p><strong><u>Menambah Data Organisasi</u></strong></p>
												<ol>
													<li>Pilih tombol <strong>Tambah</strong>, form <strong>Tambah Organisasi</strong> akan ditampilkan seperti pada gambar C.2.1.2</li>
													<image src="<?php echo base_url();?>user_guide/images/struktur_organisasi_add.jpg"></image>				
													<li>Isi <strong>Nama</strong> *)</li>
													<li>Isi <strong>Pimpinan</strong>, <strong>NIK</strong>, dan <strong>Jabatan</strong> (optional)</li>
													<li>Pilih <strong>Parent Name </strong>*)</li>
												</ol>
												<p>Keterangan :</p>
												<ol style="list-style-type:lower-alpha">
													<li><strong>Close (x)</strong>, tombol untuk menutup form Tambah Organisasi dan kembali Struktur Organisasi seperti yang ditampilkan pada ke gambar C.2.1.1</li>
												</ol>
												<p><strong><u>Mengubah/Menghapus Data Group</u></strong></p>
												<ol>
													<li>Klik pada nama organisasi/unit di Struktur Organisasi, form <strong>Ubah Struktur Organisasi</strong> akan ditampilkan seperti pada gambar C.2.1.3</li>
																							
												<image src="<?php echo base_url();?>user_guide/images/struktur_organisasi_edit.jpg"></image>	
													<li>Isi <strong>Nama </strong>dengan data yang baru *)</li>
													<li>Isi <strong>Pimpinan</strong>, <strong>NIK,</strong> dan <strong>Jabatan</strong> dengan data yang baru (optional)</li>
													<li>Pilih <strong>Parent Name </strong>dengan pilihan yang baru *)</li>
													<li>Pilih tombol<strong> Simpan </strong>untuk menyimpan perubahan data organisasi</li>
													<li>Pilih <strong>Hapus</strong> untuk menghapus data organisasi</li>
												</ol>

												<p>Keterangan :</p>

												<ol style="list-style-type:lower-alpha">
													<li><strong>Close (x)</strong>, tombol untuk menutup form Ubah Struktur Organisasi dan kembali Struktur Organisasi seperti yang ditampilkan pada ke gambar C.2.1.1</li>
												</ol>
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapse11" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.2.2. Submenu Satuan
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapse11">
											 <div class="accordion-inner">
												<p>Langkah-langkah mengelolah data pada submenu Satuan :</p>

												<ol>
													<li>Pilih <strong>Parameter </strong><strong>&agrave;</strong><strong> Satuan</strong> seperti pada gambar C.2.2, data <strong>Daftar Satuan</strong> akan ditampilkan seperti pada gambar C.2.1.1</li>
												</ol>
												<image src="<?php echo base_url();?>user_guide/images/satuan.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/satuan_list.jpg"></image>	
												<ol style="list-style-type:lower-alpha">
												<li><strong>Tambah</strong>, untuk menampilkan form <strong>Tambah Satuan.</strong></li>
													<li>Mengelolah daftar satuan seperti mengkopi (Copy), mengeksport daftar ke dalam format <strong>CSV</strong>, <strong>Excel</strong>, dan <strong>PDF</strong>, serta mencetak daftar satuan.</li>
													<li><strong>Edit</strong>, untuk menampilkan form <strong>Ubah Satuan</strong> yang dipilih.</li>
													<li><strong>Hapus</strong>, untuk menghapus data satuan yang dipilih.</li>
												</ol>
												<p><strong><u>Menambah Data Organisasi</u></strong></p>
													<ol>
														<li>Pilih tombol <strong>Tambah</strong>, form <strong>Tambah Satuan</strong> akan ditampilkan seperti pada gambar C.2.2.2</li>
												<image src="<?php echo base_url();?>user_guide/images/satuan_add.jpg"></image>	
													<li>Isi <strong>Satuan *)</strong></li>
													<li>Isi <strong>Keterangan</strong> (optional)</li>
													<li>Pilih tombol<strong> Simpan </strong>untuk menyimpan data satuan yang baru</li>
												</ol>
												<p>Keterangan :</p>
												<ol style="list-style-type:lower-alpha">
													<li><strong>Kembali</strong>, tombol untuk menutup form <strong>Tambah Satuan</strong> dan kembali ke <strong>Daftar Satuan</strong> seperti yang ditampilkan pada gambar C.2.2.1</li>
												</ol>
												<p<strong><u>Mengubah Data Satuan</u></strong></p>
												<ol>
													<li>Klik tombol <strong>Ubah</strong> pada data satuan yang ingin diubah, form <strong>Ubah Satuan </strong>akan ditampilkan seperti pada gambar C.2.1.3</li>
													<image src="<?php echo base_url();?>user_guide/images/satuan_edit.jpg"></image>	
													<li>Isi <strong>Satuan </strong>dengan data yang baru *)</li>
													<li>Isi <strong>Keterangan</strong> dengan data yang baru<strong> (</strong>optional)</li>
													<li>Pilih tombol<strong> Ubah </strong>untuk menyimpan perubahan data satuan</li>
												</ol>
												<p>Keterangan :</p>
												<ol style="list-style-type:lower-alpha">
													<li><strong>Kembali</strong>, tombol untuk menutup form Ubah Satuan dan kembali <strong>ke Daftar Satuan</strong> seperti yang ditampilkan pada gambar C.2.2.1</li>
												</ol>

											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapse12" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.2.3. Submenu Jenis Formula
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapse12">
											 <div class="accordion-inner">
												<p>Langkah-langkah mengelolah data pada submenu Jenis Formula :</p>
												<ol>
													<li>Pilih <strong>Parameter </strong><strong>&agrave;</strong><strong> Jenis Formula</strong> seperti pada gambar C.2.3, data <strong>Daftar Jenis Formula</strong> akan ditampilkan seperti pada gambar C.2.3.1</li>
												</ol>
												<image src="<?php echo base_url();?>user_guide/images/jenis_formula.jpg"></image>	
												<p>Keterangan :</p>
												<ol style="list-style-type:lower-alpha">
													<li>Mengelolah <strong>Daftar Jenis Laporan</strong> seperti mengkopi (Copy), mengeksport ke dalam format <strong>CSV</strong>, <strong>Excel</strong>, dan <strong>PDF</strong>, serta mencetak <strong>Daftar Jenis Formula</strong>.</li>
												</ol>
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapse13" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.2.4. Submenu Batas Penilaian
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapse13">
											 <div class="accordion-inner">
												<p>Langkah-langkah mengelolah data pada submenu Batas Penilaian :</p>
												<ol>
													<li>Pilih <strong>Parameter </strong><strong>&agrave;</strong><strong> Batas Penilaian</strong> seperti pada gambar C.2.4, data <strong>Daftar Batas Penilaian</strong> akan ditampilkan seperti pada gambar C.2.4.1</li>
												</ol>

												<image src="<?php echo base_url();?>user_guide/images/batas_penilaian.jpg"></image>		
												<p>Keterangan:</p>
												<ol style="list-style-type:lower-alpha">
													<li><strong>Tambah</strong>, untuk menampilkan form <strong>Tambah Batas Penilaian.</strong></li>
													<li>Mengelolah <strong>Daftar Batas Penilaian</strong> seperti mengkopi (Copy), mengekspor ke dalam format <strong>CSV</strong>, <strong>Excel</strong>, dan <strong>PDF</strong>, serta mencetak <strong>daftar batas penilaian</strong>.</li>
													<li><strong>Edit</strong>, untuk menampilkan form <strong>Ubah Batas Penilaian</strong> yang dipilih</li>
													<li><strong>Hapus</strong>, untuk menghapus Batas Penilaian yang dipilih.</li>
												</ol>
												<p><strong><u>Menambah Data Batas Penilaian</u></strong></p>
													<ol>
														<li>Pilih tombol <strong>Tambah</strong>, form <strong>Tambah Batas Penilaian</strong> akan ditampilkan seperti pada gambar C.2.4.2</li>							
												<image src="<?php echo base_url();?>user_guide/images/batas_penilaian_add.jpg"></image>	
													<li>Isi nilai <strong>Minimal *)</strong></li>
													<li>Isi nilai <strong>Maksimal</strong> *)</li>
													<li>Pilih tombol<strong> Simpan </strong>untuk menyimpan data batas penilaian baru</li>
												</ol>

												<p>Keterangan :</p>

												<ol style="list-style-type:lower-alpha">
													<li><strong>Kembali</strong>, tombol untuk menutup form <strong>Tambah Batas Penilaian</strong> dan kembali ke <strong>Daftar Batas Penilaian</strong> seperti yang ditampilkan pada gambar C.2.4.1</li>
												</ol>
											<p><strong><u>Mengubah Data Batas Penilaian</u></strong></p>
											<ol>
												<li>Klik tombol <strong>Ubah</strong> pada data batas penilaian yang ingin diubah, form <strong>Ubah Batas Penilaian </strong>akan ditampilkan seperti pada gambar C.2.4.4</li>											
												<image src="<?php echo base_url();?>user_guide/images/batas_penilaian_edit.jpg"></image>	<li>Isi nilai <strong>Minimal </strong>dengan data yang baru *)</li>
												<li>Isi nilai <strong>Maksimal</strong>, dengan data yang baru<strong> *</strong>)</li>
												<li>Pilih tombol<strong> Simpan </strong>untuk menyimpan perubahan data batas penilaian</li>
											</ol>
											<p>Keterangan :</p>
											<ol style="list-style-type:lower-alpha">
												<li><strong>Kembali</strong>, tombol untuk menutup form <strong>Ubah Batas Penilaian</strong> dan kembali ke <strong>Daftar Bata Penilaian</strong> seperti yang ditampilkan pada gambar C.2.4.1</li>
											</ol>	
											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapse14" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.2.5. Submenu Tingkat Penilaian
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapse14">
											 <div class="accordion-inner">	
											 <ol>
												<li>Pilih <strong>Parameter </strong><strong>&agrave;</strong><strong> Tingkat Penilaian</strong> seperti pada gambar C.2.5, data <strong>Daftar Tingkat Penilaian</strong> akan ditampilkan seperti pada gambar C.2.5.1</li> 		
												<image src="<?php echo base_url();?>user_guide/images/tingkat_penilaian.jpg"></image>
												<li>Isi nilai <strong>Minimal *)</strong></li>
												<li>Isi nilai <strong>Maksimal</strong> *)</li>
												<li>Pilih tombol <strong>Simpan</strong> untuk menyimpan data tingkat penilaian</li>
											</ol>							

											<p>Keterangan :</p>
											<ol style="list-style-type:lower-alpha">
												<li>Tingkat penilaian</li>
												<li>Nilai minimal dan maksimal sesuai tingkat penilaian</li>
											</ol>												
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapse15" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.2.5. Submenu Input Realisasi
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapse15">
											 <div class="accordion-inner">
												<p>Langkah-langkah mengelolah data pada submenu Periode Input Realisasi :</p>
												<ol>
													<li>Pilih <strong>Parameter </strong><strong>&agrave;</strong><strong> Periode Input Realisasi</strong> seperti pada gambar C.2.6, form <strong>Periode Input Realisasi</strong> akan ditampilkan seperti pada gambar C.2.6.1</li>
												<image src="<?php echo base_url();?>user_guide/images/input_realisasi.jpg"></image>			
													<li>Pilih <strong>OFF</strong> untuk menutup periode input realisasi</li>
													<li>Pilih <strong>ON</strong> untuk membuka periode realisasi</li>
													<li>Pilih tombol Ubah untuk menyimpan perubahan periode input realisasi</li>
												</ol>

												<p>Keterangan :</p>

												<ol style="list-style-type:lower-alpha">
													<li><strong>OFF</strong>, input realisasi tidak diizinkan</li>
													<li><strong>ON, </strong>input realisasi diizinkan</li>
												</ol>

											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC3" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.3. Pengaturan Pengguna
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC3">
											 <div class="accordion-inner">
												<p>Menu ini terdiri dari submenu <strong>Hak Akses Kelompok </strong>dan <strong>Pengguna</strong> seperti yang ditampikan pada gambar C.3 dan hanya dapat diakses oleh user Administrator.</p>

												<image src="<?php echo base_url();?>user_guide/images/pengaturan_pengguna.jpg"></image>			
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC31" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.3.1. Submenu Hak Akses Kelompok
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC31">
											 <div class="accordion-inner">
												<p>Langkah-langkah menampilkan submenu Kelompok Pengguna :</p>
												<ol>
													<li>Pilih <strong>Pengaturan Pengguna </strong><strong>&agrave;</strong><strong> Hak Akses Kelompok </strong>seperti pada gambar C.3.1, <strong>Daftar Hak Akses Kelompok</strong> akan ditampilkan seperti pada gambar C.3.1.1</li>
												</ol>
												<image src="<?php echo base_url();?>user_guide/images/hak_akses_kelompok.jpg"></image>		
												<image src="<?php echo base_url();?>user_guide/images/hak_akses_kelompok_list.jpg"></image>	<li>Keterangan :
													<ol style="list-style-type:lower-alpha">
														<li><strong>Edit</strong>, untuk menampilkan form <strong>Ubah Hak Akses Kelompok</strong></li>
														<li>Mengelolah Daftar Hak Akses Kelompok seperti mengkopi (Copy), mengeksport ke dalam format <strong>CSV</strong>, <strong>Excel</strong>, dan <strong>PDF</strong>, serta mencetak Daftar Hak Akses Kelompok.</li>
													</ol>
												</li>	
												<p><strong><u>Mengubah Hak Akses Kelompok</u></strong></p>
												<ol>
													<li>Pilih tombol <strong>Ubah </strong>pada data hak akses kelompok yang ingin diubah, form <strong>Ubah Hak Akses Kelompok</strong> akan ditampilkan seperti pada gambar C.3.1.2</li>			
												<image src="<?php echo base_url();?>user_guide/images/hak_akses_kelompok_edit.jpg"></image><li><em>Check list</em> nama menu/sub menu untuk group yang bersangkutan</li>
												<li>Pilih tombol<strong> Simpan </strong>untuk menyimpan hak akses pengguna</li>
											</ol>
											<p>Keterangan :</p>
											<ol style="list-style-type:lower-alpha">
												<li><strong>Close</strong>, tombol untuk menutup form <strong>Edit User Group Role </strong>dan menampilkan Daftar Hak Akses Kelompok seperti pada gambar C.3.1.1</li>
											</ol>			
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC32" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.3.2. Submenu Pengguna
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC32">
											 <div class="accordion-inner">
												<ol>
													<li>Langkah-langkah mengelolah data pada submenu Pengguna :</li>
													<li>Pilih <strong>Pengaturan Pengguna </strong><strong>&agrave;</strong><strong> Pengguna</strong> seperti pada gambar C.3.2, <strong>Daftar Pengguna</strong> akan ditampilkan seperti pada gambar C.3.2.1</li>
												</ol>
												<image src="<?php echo base_url();?>user_guide/images/pengguna.jpg"></image>
												<p>Keterangan :</p>
											<ol style="list-style-type:lower-alpha">
												<li><strong>Tambah</strong>, akan menampilkan form <strong>Tambah Pengguna</strong></li>
												<li>Mengelolah <strong>Daftar Pengguna</strong> seperti mengkopi (Copy), mengeksport ke dalam format <strong>CSV</strong>, <strong>Excel</strong>, dan <strong>PDF</strong>, serta mencetak Daftar Pengguna.</li>
												<li><strong>Edit</strong>, akan menampilkan form <strong>Ubah Data Pengguna </strong>yang dipilih</li>
												<li><strong>Hapus</strong>, akan menghapus data pengguna yang dipilih</li>
												<li>Mereset password pengguna</li>
											</ol>
												<p><strong><u>Menambah Data Pengguna</u></strong></p>
												<ol>
													<li>Pilih tombol <strong>Tambah</strong>, form <strong>Tambah Pengguna</strong> akan ditampilkan seperti pada gambar C.3.2.2</li>
													<image src="<?php echo base_url();?>user_guide/images/pengguna_add.jpg"></image>
												<li>Ketikan <strong>Username, Nama, Password, Ulangi Password</strong>, dan<strong> Email</strong> *)</li>
												<li>Pilih <strong>Group User *)</strong></li>
												<li>Pilih <strong>Unit</strong> *)</li>
												<li>Pilih tombol <strong>Simpan </strong>untuk menyimpan data pengguna yang baru</li>
											</ol>

											<p>Keterangan :</p>

											<ol style="list-style-type:lower-alpha">
												<li><strong>Kembali</strong>, tombol untuk menutup form <strong>Tambah Pengguna</strong> dan kembali ke <strong>Daftar Penguna</strong> seperti yang ditampilkan pada gambar C.3.2.1</li>
											</ol>
											<p><strong><u>Mengubah Data Pengguna</u></strong></p>
												<ol>
													<li>Pilih tombol <strong>Ubah</strong>, form <strong>Ubah Data Pengguna</strong> akan ditampilkan seperti pada gambar C.3.2.3</li>
																							
													<image src="<?php echo base_url();?>user_guide/images/pengguna_edit.jpg"></image>		<li>Ketikan <strong>Username </strong>dan<strong> Nama </strong>yang baru *)</li>
													<li>Pilih <strong>User Group *)</strong></li>
													<li>Pilih <strong>Unit *)</strong></li>
													<li>Pilih tombol<strong> Ubah </strong>untuk menyimpan perubahan data pengguna</li>
												</ol>
											<p>Keterangan :</p>
											<ol style="list-style-type:lower-alpha">
												<li><strong>Kembali</strong>, tombol untuk menutup form Ubah Data Pengguna dan kembali ke Daftar Pengguna seperti yang ditampilkan pada gambar C.3.2.1</li>
											</ol>
											<p><strong><u>Hapus Data Pengguna</u></strong></p>
											<ol>
												<li>Pilih tombol <strong>Hapus </strong>pada data pengguna yang ingin dihapus, dialog konfirmasi penghapusan data pengguna akan ditampilkan&nbsp; seperti pada gambar C.3.2.4</li>									
												<image src="<?php echo base_url();?>user_guide/images/pengguna_delete.jpg"></image>		<li>Pilih tombol <strong>Yes </strong>untuk menghapus data pengguna</li>
											</ol>
												<p>Keterangan :</p>
												<ol style="list-style-type:lower-alpha">
													<li><strong>No</strong>, tombol untuk menutup dialog konfirmasi penghapusan data pengguna dan kembali ke <strong>Daftar Pengguna</strong> seperti yang diperlihatkan pada gambar C.3.3.1</li>
												</ol>
												<p><strong><u>Reset Password</u></strong></p>
												<ol>
													<li>Pilih tombol <strong>Reset Password </strong>pada data pengguna yang passwordnya ingin direset, form <strong>Reset Password</strong> akan ditampilkan seperti pada gambar C.3.3.5</li>
																					
												<image src="<?php echo base_url();?>user_guide/images/reset_password.jpg"></image>	
												<li>Isi <strong>Password </strong>baru atau gunakan password default<strong> *)</strong></li>
												<li>Pilih tombol <strong>Yes </strong>untuk menyimpan perubahan reset password</li>
												</ol>
												<p>Keterangan :</p>
												<ol style="list-style-type:lower-alpha">
													<li>Hanya <em>input text</em> ini yang bisa diubah</li>
													<li><strong>Kembali</strong>, tombol untuk menutup form <strong>Reset Password</strong> dan kembali Daftar Pengguna seperti yang ditampilkan pada gambar C.3.2.1</li>
												</ol>
			
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC4" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.4. KPI dan Program Kerja
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC4">
											 <div class="accordion-inner">
												<p>Menu ini terdiri dari sub menu <strong>Daftar KPI</strong> dan <strong>Daftar Program Kerja </strong>seperti yang diperlihatkan pada gambar C.4</p>
												<image src="<?php echo base_url();?>user_guide/images/menu_kpi.jpg"></image>				
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC41" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.4.1. Submenu Daftar KPI
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC41">
											 <div class="accordion-inner">
												<p>Submenu ini untuk mengelolah data KPI yang berupa menampilkan list data, <strong>Tambah, Edit</strong>, dan <strong>Delete.</strong></p>
												<image src="<?php echo base_url();?>user_guide/images/kpi.jpg"></image>	
												<p><strong><u>Menampilkan Daftar KPI</u></strong></p>
												<ol>
													<li>Pilih submenu <strong>Daftar KPI</strong>, <strong>Daftar KPI</strong> akan ditampilkan seperti pada gambar C.4.1.2</li>
												</ol>												
												<image src="<?php echo base_url();?>user_guide/images/kpi_list.jpg"></image><p>Keterangan:</p>
												<ol style="list-style-type:lower-alpha">
													<li><strong>Tambah</strong>, untuk membuka form <strong>Tambah KPI</strong></li>
													<li><strong>Edit</strong>, untuk menampilkan form <strong>Ubah KPI</strong> yang dipilih
													<ol style="list-style-type:lower-alpha">
														<li><strong>Hapus</strong>, untuk menghapus data KPI yang dipilih</li>
													</ol>
													</li>
												</ol>
												<p><strong><u>Menambah Data KPI</u></strong></p>
												<ol>
													<li>Pilih <strong>Tambah</strong>, form <strong>Tambah KPI</strong>&nbsp; akan ditampilkan seperti pada gambar C.4.1.2</li>
												</ol>
												<image src="<?php echo base_url();?>user_guide/images/kpi_add.jpg"></image>	
												<li>Ketikan <strong>Nama KPI</strong> *)</li>
												<li>Pilih <strong>Batas Penilaian </strong>*)</li>
												<li>Pilih <strong>Perspektif KPKU </strong>*)</li>
												<li>Pilih <strong>Inisiatif Strategis </strong>*)</li>
												<li>Pilih<strong> Satuan </strong>*)</li>
												<li>Pilih<strong> Formula </strong>*)</li>
												<li>Pilih tombol<strong> Simpan </strong>untuk menyimpan data KPI yang baru</li>
											</ol>

											<p>Keterangan :</p>
											<ol style="list-style-type:lower-alpha">
												<li><strong>Kembali</strong>, tombol untuk menutup form <strong>Tambah KPI</strong> dan kembali <strong>Daftar KPI</strong> seperti yang ditampilkan pada gambar C.4.1.1</li>
											</ol>
											<p><strong><u>Mengubah Data KPI</u></strong></p>
											<ol>
												<li>Pilih tombol <strong>Ubah</strong>, form <strong>Ubah KPI</strong> akan ditampilkan seperti pada gambar C.4.1.3</li>
											</ol>
												<image src="<?php echo base_url();?>user_guide/images/kpi_edit.jpg"></image>		<li>Ketikan <strong>Nama KPI</strong> *)</li>
												<li>Pilih <strong>Batas Penilaian </strong>*)</li>
												<li>Pilih <strong>Perspektif KPKU </strong>*)</li>
												<li>Pilih <strong>Inisiatif Strategis </strong>*)</li>
												<li>Pilih<strong> Satuan </strong>*)</li>
												<li>Pilih<strong> Formula </strong>*)</li>
												<li>Pilih tombol<strong> Ubah </strong>untuk menyimpan perubahan data KPI</li>
											</ol>

											<p>Keterangan :</p>

											<ol style="list-style-type:lower-alpha">
												<li><strong>Kembali</strong>, tombol untuk menutup form <strong>Tambah KPI</strong> dan kembali <strong>Daftar KPI</strong> seperti yang ditampilkan pada gambar C.4.1.1</li>
											</ol>
											<p><strong><u>Hapus Data KPI</u></strong></p>
											<ol>
												<li>Pilih tombol <strong>Hapus</strong>, dialog konfirmasi penghapusan data KPI akan ditampilkan&nbsp; seperti pada gambar C.4.1.4</li>
												<image src="<?php echo base_url();?>user_guide/images/kpi_delete.jpg"></image>	
												<li>Pilih tombol <strong>Yes </strong>untuk menghapus data KPI</li>										
											</ol>
											<p>Keterangan :</p>
											<ol style="list-style-type:lower-alpha">
												<li><strong>No</strong>, tombol untuk menutup dialog Konfirmasi dan kembali <strong>Daftar KPI</strong> seperti yang ditampilkan pada gambar C.4.1.1</li>
											</ol>					
											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC42" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.4.2. Submenu Daftar Program Kerja
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC42">
											 <div class="accordion-inner">
												<p>Submenu ini untuk mengelolah data program kerja yang berupa menampilkan list data, <strong>Tambah, Edit</strong>, dan <strong>Delete.</strong></p>
												<p><strong><u>Menampilkan Daftar KPI</u></strong></p>
												<ol>
													<li>Klik submenu <strong>Daftar Program Kerja</strong>, <strong>Daftar Program Kerja</strong> akan ditampilkan&nbsp; seperti pada gambar C.4.2.1</li>
														<image src="<?php echo base_url();?>user_guide/images/program_kerja.jpg"></image>
												</ol>												
												<image src="<?php echo base_url();?>user_guide/images/program_kerja_list.jpg"></image><p>Keterangan:</p>

												<ol style="list-style-type:lower-alpha">
													<li><strong>Tambah,</strong> untuk membuka form <strong>Tambah Program Kerja</strong></li>
													<li><strong>View</strong>, utnuk membuka file program kerja jika tersedia</li>
													<li><strong>Edit</strong>, untuk menampilkan form <strong>Ubah Program Kerja</strong>
													<ol style="list-style-type:lower-alpha">
														<li><strong>Hapus</strong>, untuk menghapus Data Program Kerja</li>
													</ol>
													</li>
												</ol>
												<p><strong><u>Menambah Program Kerja</u></strong></p>
												<ol>
													<li>Pilih tombol <strong>Tambah</strong>, form <strong>Tambah Program Kerja</strong> akan ditampilkan seperti pada gambar C.4.2.2</li>
												
												<image src="<?php echo base_url();?>user_guide/images/program_kerja_add.jpg"></image>
												<li>Pilih <strong>Nama KPI</strong> (opsional)</li>
												<li>Pilih <strong>Organisasi *)</strong></li>
												<li>Ketika <strong>Program Kerja *)</strong></li>
												<li>Ketikan <strong>Keterangan *)</strong></li>
												<li>Upload<strong> File </strong>(opsional)</li>
												<li>Pilih tombol<strong> Simpan </strong>untuk menyimpan data program kerja yang baru</li>
											</ol>

											<p>Keterangan :</p>

											<ol style="list-style-type:lower-alpha">
												<li><strong>Kembali</strong>, tombol untuk menutup form <strong>Tambah Program Kerja</strong> dan kembali ke <strong>Daftr Program Kerja</strong> seprti yang ditampilkan paa gambar C.4.2.1</li>
											</ol>
											<p><strong><u>Mengubah Program Kerja</u></strong></p>
											<ol>
												<li>Pilih tombol <strong>Ubah</strong> pada data program kerja yang ingin diubah, form <strong>Ubah</strong> <strong>Program Kerja</strong> akan ditampilkan seperti pada gambar C.4.2.3</li>										
												<image src="<?php echo base_url();?>user_guide/images/program_kerja_edit.jpg"></image>
												<li>Isi <strong>Nama KPI</strong> dengan data yang baru (opsional)</li>
												<li>Pilih <strong>Organisasi </strong>dengan data yang baru *)</li>
												<li>Isi <strong>Program Kerja </strong>dengan data yang baru *)</li>
												<li>Isi <strong>Keterangan </strong>dengan data yang baru *)</li>
												<li>Upload<strong> File </strong>dengan data yang baru (opsional)</li>
												<li>Pilih tombol<strong> Ubah </strong>untuk menyimpan perubahan data program kerja</li>
											</ol>
											<p>Keterangan :</p>
											<ol style="list-style-type:lower-alpha">
												<li><strong>Kembali</strong>, tombol untuk menutup form <strong>Tambah Program Kerja</strong> dan kembali ke <strong>Daftr Program Kerja</strong> seprti yang ditampilkan paa gambar C.4.2.1</li>
											</ol>
											<p><strong><u>Hapus Data Program Kerja</u></strong></p>
											<ol>
												<li>Pilih tombol <strong>Hapus </strong>pada program kerja yang ingin dihapus, dialog konfirmasi penghapusan data program kerja akan ditampilkan&nbsp; seperti pada gambar C.4.2.4</li>
										

												<image src="<?php echo base_url();?>user_guide/images/program_kerja_delete.jpg"></image>	
												
												<li>Pilih tombol <strong>Yes </strong>untuk menghapus data program kerja</li>
											</ol>

											<p>Keterangan :</p>

											<ol style="list-style-type:lower-alpha">
												<li><strong>No</strong>, tombol untuk menutup dialog konfirmasi penghapusan data program kerja dan kembali ke Daftar Pogram Kerja seperti yang ditampilkan pada gambar C.4.2.4</li>
											</ol>
		
											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC5" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5. Kontrak Manajemen
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC5">
											 <div class="accordion-inner">
												<p>Menu ini terdiri dari submenu <strong>KM Perusahaan, Distribusi KM, KM Unit, Realisasi Oleh Unit, Realisasi Oleh Penilai, Finalisasi Penilai, Penyusunan RKM, Pelaksanaan RKM, </strong>dan<strong> Data Pembanding </strong>seperti yang diperlihatkan pada gambar C.5.</p>
												<image src="<?php echo base_url();?>user_guide/images/kontrak_manajemen.jpg"></image>				
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC51" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.1. Submenu KM Perusahaan
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC51">
											 <div class="accordion-inner">
												<p>Langkah-langkah mengelolah data pada submenu <strong>KM Perusahaan</strong> :</p>
													<ol>
														<li>Pilih <strong>Kontrak Manajemen </strong><strong>&agrave;</strong><strong> KM Perusahaan</strong> seperti pada gambar C.5.1, <strong>Daftar</strong> <strong>KM Perusahaan</strong> akan ditampilkan seperti pada gambar C.5.1.1</li>													

												<image src="<?php echo base_url();?>user_guide/images/km_perusahaan.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/km_perusahaan_1.jpg"></image>		<li>Pilih <strong>Periode</strong> KM Perusahaan</li>
													<li>Pilih tombol <strong>Pilih KPI </strong>akan menampilkan pilihan KPI seperti pada gambar</li>
												<image src="<?php echo base_url();?>user_guide/images/km_perusahaan_2.jpg"></image>		<li><em>Check list</em> KPI yang dipilih</li>
												<li>Pilih tombol <strong>Simpan</strong> pada <strong>Daftar KPI</strong> untuk menyimpan data KPI yang dipilih menutup form <strong>Daftar Pilihan KPI</strong> dan kembali ke <strong>Daftar KM Perusahaan</strong> seperti yang ditampilkan pada gambar C.5.1.1</li>
												<li>Tentukan <strong>Order No </strong>(c) indikator</li>
												<li>Isi <strong>Boot</strong> (d) indikator</li>
												<li>Isi <strong>Target</strong> (e) indikator</li>
												<li>Pilih tombol <strong>Simpan</strong> pada KM Perusahaan untuk menyimpan data KM Perusahaan</li>
											</ol>
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC52" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.2. Submenu Distribusi KM
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC52">
											 <div class="accordion-inner">
												<p>Langkah-langkah mengelolah data pada submenu <strong>Distribusi KM</strong> :</p>
												<ol>
													<li>Pilih <strong>Kontrak Manajemen </strong><strong>&agrave;</strong><strong> Distribusi KM </strong>seperti pada gambar C.5.2, <strong>Daftar</strong> <strong>Distribusi KM</strong> akan ditampilkan seperti pada gambar C.5.2.1</li>
											
												<image src="<?php echo base_url();?>user_guide/images/distribusi_km.jpg"></image>		
												<image src="<?php echo base_url();?>user_guide/images/distribusi_km_list.jpg"></image>	<li>Pilih <strong>Periode</strong> Direktorat KM (a)</li>
													<li>Pilih tombol <strong>Detail </strong>untuk menampilkan form <strong>Detail RKM</strong> seperti pada gambar C.5.2.2</li>
																
												<image src="<?php echo base_url();?>user_guide/images/distribusi_km_detail.jpg"></image>	
													<li>Pilih tombol <strong>Unit </strong>(c) untuk menambahkan unit</li>
													<li>Pilih tombol <strong>Sub Unit </strong>(d) untuk menambahkan sub unit</li>
													<li>Pilih tombol Simpan (f) untuk menyimpan data detail RKM, menutup form <strong>Detail RKM</strong> dan kembali ke <strong>Daftar Distribusi KM</strong> seperti yang ditampilkan pada gambar C.5.2.1</li>
												</ol>
												<p>Keterangan:</p>

												<ol style="list-style-type:lower-alpha">
													<li><strong>Hapus</strong>, tombol untuk menghapus sub unit</li>
													<li><strong>Close (x)</strong>, tombol untuk menutup form Detail RKM dan kembali ke <strong>Daftar Distribusi KM</strong> seperti yang ditampilkan pada gambar C.5.2.1</li>
												</ol>
												
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC53" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.3. Submenu KM Unit
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC53">
											 <div class="accordion-inner">
												<p>Langkah-langkah mengelolah data pada submenu <strong>KM</strong> <strong>Unit</strong>:</p>
												<ol>
													<li>Pilih <strong>Kontrak Manajemen </strong><strong>&agrave;</strong><strong> KM Unit </strong>seperti pada gambar C.5.3, <strong>Daftar</strong> <strong>KM Unit</strong> akan ditampilkan seperti pada gambar C.5.3.1</li>	
													<image src="<?php echo base_url();?>user_guide/images/km_unit.jpg"></image>				
													<image src="<?php echo base_url();?>user_guide/images/km_unit_list.jpg"></image>	<li>Pilih Unit (a)</li>
													<li>Pilih<strong> Periode</strong> (b)</li>
													<li>Tentukan <strong>Order No </strong>(c) indikator</li>
													<li>Isi <strong>Boot</strong> (d) indikator</li>
													<li>Isi <strong>Target</strong> (e) indikator</li>
													<li>Pilih tombol <strong>Simpan </strong>(f) untuk menyimpan data KM Unit</li>
												</ol>
												<p>Keterangan:</p>
												<ol style="list-style-type:lower-alpha">
													<li><strong>Hapus</strong>, tombol untuk menghapus data KM Unit yang dipilih</li>
												</ol>
											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC54" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.4. Submenu Realisasi Oleh Unit
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC54">
											 <div class="accordion-inner">
												<p>Langkah-langkah mengelolah data pada submenu <strong>Realisasi Oleh Unit</strong>:</p>
												<ol>
													<li>Pilih <strong>Kontrak Manajemen </strong><strong>&agrave;</strong><strong> Realisasi Oleh Unit</strong> seperti pada gambar C.5.4, <strong>Daftar</strong> <strong>Realisasi Oleh Unit</strong> akan ditampilkan seperti pada gambar C.5.4.1</li>
												<image src="<?php echo base_url();?>user_guide/images/realisasi_oleh_unit.jpg"></image><br/>				
												<image src="<?php echo base_url();?>user_guide/images/realisasi_oleh_unit_list.jpg"></image>	
													<li>Pilih <strong>Unit</strong> (a)</li>
													<li>Pilih<strong> Periode</strong> (b)</li>
													<li>Isi <strong>Realisasi</strong> (d) indicator</li>
													<li>Pilih tombol <strong>Kalkulasi</strong> (e) untuk menghitung realisasi oleh unit</li>
													<li>Pilih tombol <strong>Simpan </strong>(f) untuk menyimpan data <strong>Realisasi Oleh Unit</strong></li>
												</ol>
											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC55" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.5. Submenu Realisasi Oleh Penilai
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC55">
											 <div class="accordion-inner">
												<p>Langkah-langkah mengelolah data pada submenu <strong>Realisasi Oleh Penilai</strong>:</p>
												<ol>
													<li>Pilih <strong>Kontrak Manajemen </strong><strong>&agrave;</strong><strong> Realisasi Oleh Penilai</strong> seperti pada gambar C.5.5, <strong>Daftar</strong> <strong>Realisasi Oleh Penilai</strong> akan ditampilkan seperti pada gambar C.5.5.1</li>				<image src="<?php echo base_url();?>user_guide/images/realisasi_oleh_penilai.jpg"></image><br/>				
												<image src="<?php echo base_url();?>user_guide/images/realisasi_oleh_penilai_list.jpg"></image>	
													<li>Pilih <strong>Unit</strong> (a)</li>
													<li>Pilih<strong> Periode</strong> (b)</li>
													<li>Isi <strong>Realisasi</strong> (d) indicator</li>
													<li>Isi <strong>Keterangan </strong>(e)</li>
													<li>Pilih tombol <strong>Kalkulasi</strong> (f) untuk menghitung realisasi oleh penilai</li>
													<li>Pilih tombol <strong>Simpan </strong>(g) untuk menyimpan data <strong>Realisasi Oleh Penilai</strong></li>
												</ol>
											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC56" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.6. Submenu Finalisasi Penilai
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC56">
											 <div class="accordion-inner">
												<p>Langkah-langkah mengelolah data pada submenu <strong>Finalisasi Penilai</strong>:</p>
												<ol>
													<li>Pilih <strong>Kontrak Manajemen </strong><strong>&agrave;</strong><strong> Finalisasi Penilai</strong> seperti pada gambar C.5.6, <strong>Daftar</strong> <strong>Finalisasi Penilai</strong> akan ditampilkan seperti pada gambar C.5.6.1</li>
													<image src="<?php echo base_url();?>user_guide/images/finalisasi_penilai.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/finalisasi_penilai_list.jpg"></image>	
													<li>Pilih<strong> Periode</strong> (b)</li>
													<li>Isi <strong>Realisasi Unit</strong> (d)</li>
													<li>Isi <strong>Realisasi Penilai</strong> (d)</li>
													<li>Pilih tombol <strong>Kalkulasi</strong> (e) untuk menghitung finalisasi sebelum disimpan</li>
													<li>Pilih tombol <strong>Simpan </strong>(f) untuk menyimpan data <strong>Finalisasi</strong></li>
													<li>Pilih tombol <strong>Finalisasi</strong> untuk melakukan finalisasi penilaian.</li>
												</ol>
												<p>Keterangan:</p>
												<p>Setelah tombol <strong>Finalisasi </strong>(f) tipilih, tombol <strong>Kalkulasi </strong>(e) dan <strong>Simpan </strong>(f) di-disabled</p>
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC57" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.7. Submenu Penyusunan RKM
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC57">
											 <div class="accordion-inner">
												<p>Langkah-langkah mengelolah data pada submenu <strong>Penyusunan RKM</strong>:</p>
												<ol>
													<li>Pilih <strong>Kontrak Manajemen </strong><strong>&agrave;</strong><strong> Penyusunan RKM</strong> seperti pada gambar C.5.7, <strong>Daftar</strong> <strong>Penyusunan RKM</strong> akan ditampilkan seperti pada gambar C.5.7.1</li>								
													<image src="<?php echo base_url();?>user_guide/images/penyusunan_rkm.jpg"></image>		
													<image src="<?php echo base_url();?>user_guide/images/penyusunan_rkm_list.jpg"></image>	<li>Pilih <strong>Unit</strong> (a)</li>
														<li>Pilih<strong> Periode</strong> (b)</li>
												
													<image src="<?php echo base_url();?>user_guide/images/penyusunan_rkm_list_2.jpg"></image>		
														<li><em>Check list</em> program kerja (e) yang dipilih. Program kerja yang sudah ada di <strong>Daftar Penyusunan RKM</strong> tidak akan muncul di form <strong>Pilihan Program Kerja</strong>.</li>
														<li>Pilih tombol <strong>Simpan</strong> (f) untuk menyimpan data program kerja yang dipilih, menutup form Pilihan Program Kerja dan kembali ke <strong>Daftar Penyusunan RKM</strong> seperti yang ditampilkan pada gamabr C.5.7.1</li>
													</ol>

												<p>Keterangan:</p>

												<ol style="list-style-type:lower-alpha">
													<li><strong>Close (x),</strong> tombol untuk menutup form <strong>Pilihan Program Kerja</strong> dan kembali ke <strong>Daftar Penyusunan RKM</strong> seperti yang ditampilkan pada gamabr C.5.7.1</li>
												</ol>

											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC58" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.8. Submenu Pelaksanaan RKM
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC58">
											 <div class="accordion-inner">
												<p>Langkah-langkah mengelolah data pada submenu <strong>Pelaksanaan RKM</strong>:</p>
												<ol>
													<li>Pilih <strong>Kontrak Manajemen </strong><strong>&agrave;</strong><strong> Pelaksanaan RKM</strong> seperti pada gambar C.5.8, <strong>Daftar</strong> <strong>Pelaksanaan RKM</strong> akan ditampilkan seperti pada gambar C.5.8.1</li>
												
												<image src="<?php echo base_url();?>user_guide/images/pelaksanaan_rkm.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/pelaksanaan_rkm_list.jpg"></image>	
												
													<li>Pilihan <strong>Tahun</strong></li>
													<li><strong>Tambah</strong>, untuk menampilkan form <strong>Tambah Data Pembanding</strong></li>
													<li><strong>Edit, </strong>tombol untuk mengubah data pembanding yang dipilih</li>
													<li><strong>Hapus, </strong>tombol untuk menghapus data pembanding yang dipilih</li>
												</ol>
												
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC59" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.9. Submenu Data Pembanding
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC59">
											 <div class="accordion-inner">
												<p><strong><u>Menambah Data Pembanding</u></strong></p>
												<ol>
													<li>Pilih tombol <strong>Tambah</strong>, form <strong>Tambah Data Pembanding</strong> akan ditampilkan seperti pada gambar C.5.9.2</li>		
													<image src="<?php echo base_url();?>user_guide/images/data_pembanding.jpg"></image>
													<image src="<?php echo base_url();?>user_guide/images/data_pembanding_list.jpg"></image>
													<li>Pilih Nama KPI *)</li>
													<li>Isi nilai <strong>Kompetitor</strong> *)</li>
													<li>Isi nilai <strong>Best Performance</strong> *)</li>
													<li>Isi nilai <strong>Benchmark</strong> *)</li>
													<li>Pilih tombol <strong>Simpan </strong>untuk menyimpan data pembanding yang baru</li>
												</ol>
												<p>Keterangan :</p>
												<ol style="list-style-type:lower-alpha">
													<li><strong>Tahun</strong>, ditampilkan secara otomatis ketika tombol Tambah pada Daftar Pembanding dipilih</li>
													<li><strong>Kembali, </strong>tombol untuk menutup form <strong>Tambah Data Pembanding</strong> dan kembali ke <strong>Daftar Data Pembanding</strong> seperti yang ditampilkan pada gambar C.5.9.1</li>
												</ol>
											<p><strong><u>Mengubah Data Pembanding</u></strong></p>
												<ol>
													<li>Pilih tombol <strong>Edit </strong>pada data pembanding yang ingin diubah, form <strong>Ubah Data Pembanding</strong> akan ditampilkan seperti pada gambar C.5.9.3</li>				
													<image src="<?php echo base_url();?>user_guide/images/data_pembanding_add.jpg"></image>		
													<image src="<?php echo base_url();?>user_guide/images/data_pembanding_edit.jpg"></image>	
													<li>Pilih Nama KPI dengan data yang baru *)</li>
													<li>Isi nilai <strong>Kompetitor</strong> dengan data yang baru *)</li>
													<li>Isi nilai <strong>Best Performance </strong>dengan data yang baru *)</li>
													<li>Isi nilai <strong>Benchmark</strong> dengan data yang baru *)</li>
													<li>Pilih tombol <strong>Ubah </strong>untuk menyimpan perubahan data pembanding</li>
												</ol>

												<p>Keterangan :</p>

												<ol style="list-style-type:lower-alpha">
													<li><strong>Tahun</strong>, tidak dapat diubah</li>
													<li><strong>Kembali, </strong>tombol untuk menutup form <strong>Ubah Data Pembanding</strong> dan kembali ke <strong>Daftar Data Pembanding</strong> seperti yang ditampilkan pada gambar C.5.9.1</li>
												</ol>

											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC6" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6. Laporan
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC6">
											 <div class="accordion-inner">
												<p>Menu ini terdiri dari submenu <strong>Peta Strategi, Peta KPI, Aligment RKM, Laporan KM Perusahaan, Laporan KM Unit, Kontribusi Direktorat, Monitor Realisasi, </strong>dan<strong> Laporan RKM </strong>seperti yang diperlihatkan pada gambar C.6.</p>
												<image src="<?php echo base_url();?>user_guide/images/laporan.jpg"></image>					
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC61" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.1. Submenu Peta Strategi
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC61">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/peta_strategi_list.jpg"></image>					
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC62" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.2. Submenu Peta KPI
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC62">
											 <div class="accordion-inner">
												<p>Langkah-langkah menampilkan data pada submenu <strong>Peta KPI</strong> :</p>
												<ol>
													<li>Pilih <strong>Laporan </strong><strong>&agrave;</strong><strong> Peta KPI</strong> seperti pada gambar C.6.2, <strong>Daftar</strong> <strong>Peta KPI</strong> akan ditampilkan seperti pada gambar C.6.2.1</li>
												</ol>
												<image src="<?php echo base_url();?>user_guide/images/peta_kpi.jpg"></image><br/>		
												<image src="<?php echo base_url();?>user_guide/images/peta_kpi_list.jpg"></image>		
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC63" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.3. Submenu Aligment RKM
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC63">
											 <div class="accordion-inner">
												<p>Langkah-langkah menampilkan data submenu <strong>Aligment RKM</strong>:</p>
												<ol>
													<li>Pilih <strong>Laporan </strong><strong>&agrave;</strong><strong> Aligment RKM </strong>seperti pada gambar C.6.3, <strong>Daftar</strong> <strong>Aligment RKM</strong> akan ditampilkan seperti pada gambar C.6.3.1</li>	
												<image src="<?php echo base_url();?>user_guide/images/aligment_rkm.jpg"></image>	<li>Pilih <strong>Periode</strong> (a) Aligment RKM</li>
													<li>Pilih <strong>Tools </strong>(b) untuk export laporan Aligment RKM ke dalam format PDF (<strong>Export to PDF) </strong>atau excel<strong> (Export to Excel</strong>)</li>
												</ol>												
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC64" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.4. Submenu Laporan RKM Perusahaan
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC64">
											 <div class="accordion-inner">
												<p>Langkah-langkah menampilkan data pada submenu Laporan RKM Perusahaan:</p>
												<ol>
													<li>Pilih <strong>Laporan </strong><strong>&agrave;</strong><strong> Laporan RKM Perusahaan </strong>seperti pada gambar C.6.4, <strong>Daftar</strong> <strong>Laporan RKM Perusahaan</strong> akan ditampilkan seperti pada gambar C.6.4.1</li>
												</ol>
												<image src="<?php echo base_url();?>user_guide/images/laporan_rkm_perusahaan.jpg"></image>					
												<image src="<?php echo base_url();?>user_guide/images/laporan_rkm_perusahaan_list.jpg"></image>		
												<p>Keterangan :</p>
												<ol type="a">
													<li>Pilih <strong>Unit </strong>(a) Laporan RKM yang ingin diatampilkan</li>
													<li>Pilih <strong>Periode </strong>(b) Laporan RKM yang ingin diatampilkan</li>
													<li>Pilih <strong>Tools </strong>(c) untuk export laporan Laporan RKM ke dalam format PDF (<strong>Export to PDF) </strong>atau excel<strong> (Export to Excel</strong>)</li>
												</ol>

											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC65" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.5. Submenu Laporan KM Unit
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC65">
											 <div class="accordion-inner">
												<p>Langkah-langkah menampilkan data pada submenu Laporan KM Unit:</p>
												<ol>
													<li>Pilih <strong>Laporan </strong><strong>&agrave;</strong><strong> Laporan KM Unit </strong>seperti pada gambar C.6.5, <strong>Daftar</strong> <strong>Laporan KM Unit</strong> akan ditampilkan seperti pada gambar C.6.5.1</li>
												</ol>

												<image src="<?php echo base_url();?>user_guide/images/laporan_km_unit.jpg"></image>					
												<image src="<?php echo base_url();?>user_guide/images/laporan_km_unit_list.jpg"></image>	
												<p>Keterangan :</p>
												<ol type="a">
													<li>Pilih <strong>Unit </strong>(a) Laporan KM Unit yang ingin diatampilkan</li>
													<li>Pilih <strong>Periode </strong>(b) Laporan KM Unit yang ingin diatampilkan</li>
													<li>Pilih <strong>Tools </strong>(c) untuk export laporan Laporan KM Unit ke dalam format PDF (<strong>Export to PDF) </strong>atau excel<strong> (Export to Excel</strong>)</li>
												</ol>

											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC66" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.6. Submenu Kontribusi Direktorat
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC66">
											 <div class="accordion-inner">
												<p>Langkah-langkah menampilkan data pada submenu Laporan Kontributsi Direktorat:</p>
												<ol>
													<li>Pilih <strong>Laporan </strong><strong>&agrave;</strong><strong> Kontribusi Direktorat </strong>seperti pada gambar C.6.6, Daftar <strong>Kontribusi Direktorat </strong>akan ditampilkan seperti pada gambar C.6.6.1</li>
												</ol>												
												<image src="<?php echo base_url();?>user_guide/images/kontribusi_direktorat.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/kontribusi_direktorat_list.jpg"></image>		
												<p>Keterangan :</p>
												<ol type="a">
													<li>Pilih <strong>Periode </strong>(a) Kontribusi Direkktorat yang ingin diatampilkan</li>
													<li>Pilih <strong>Tools </strong>(b) untuk export Kontribusi Direkktorat ke dalam format PDF (<strong>Export to PDF) </strong>atau excel<strong> (Export to Excel</strong>)</li>
												</ol>

											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC67" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.7. Submenu Monitor Realisasi
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC67">
											 <div class="accordion-inner">
												<p>Langkah-langkah menampilkan data pada submenu <strong>Monitor Realiasi</strong>:</p>
												<ol>
													<li>Pilih <strong>Laporan </strong><strong>&agrave;</strong><strong> Monitor Realisasi </strong>seperti pada gambar C.6.7, data <strong>Grafik</strong> <strong>Monitor Realisasi KM Perusahaan </strong>akan ditampilkan seperti pada gambar C.6.7.1</li>
												
												<image src="<?php echo base_url();?>user_guide/images/monitor_realisasi.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/monitor_realisasi_grafik_1.jpg"></image>		
													<li>Pilih <strong>Periode </strong>(a) Monitor Realisasi yang ingin diatampilkan</li>
													<li>Pilih <strong>Filter </strong>(b). Jika <strong>Filter</strong> yang dipilih adalah <strong>KM Unit,</strong> maka akan muncul pilihan <strong>Unit</strong> seperti ditampilkan pada gambar C.6.7.2</li>
												</ol>
												<image src="<?php echo base_url();?>user_guide/images/monitor_realisasi_grafik_2.jpg"></image>		
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC68" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.8. Submenu Laporan RKM
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC68">
											 <div class="accordion-inner">
												<p>Langkah-langkah menampilkan submenu Laporan RKM:</p>
													<ol>
														<li>Pilih <strong>Laporan </strong><strong>&agrave;</strong><strong> Laporan RKM </strong>seperti pada gambar C.6.8, <strong>Daftar</strong> <strong>Laporan RKM </strong>akan ditampilkan seperti pada gambar C.6.8.1</li>
													</ol>

												<image src="<?php echo base_url();?>user_guide/images/laporan_rkm.jpg"></image>	<br/>		
												<image src="<?php echo base_url();?>user_guide/images/laporan_rkm_list.jpg"></image>
												<p>Keterangan :</p>
												<ol>
													<li>Pilih <strong>Unit </strong>(a) RKM yang ingin diatampilkan</li>
													<li>Pilih <strong>Periode </strong>(b) RKM yang ingin diatampilkan</li>
													<li>Pilih <strong>Tools </strong>(c) untuk export Daftar RKM ke dalam format PDF (<strong>Export to PDF) </strong>atau excel<strong> (Export to Excel</strong>)</li>
												</ol>
												
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC7" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.7. Dashboard
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC7">
											 <div class="accordion-inner">
												<p>Menu dashboard menampilkan data aplikasi EPD dalam bentuk grafik yang terdiri dari <strong>Tema Strategis</strong>, <strong>Keuangan dan Pasar</strong>, <strong>Pelanggan/Customer</strong>, <strong>Efektivitas Produk dan Proses</strong>, <strong>Fokus Tenaga Kerja</strong>, <strong>Kepeimimpinan, Tata Kelola</strong>, dan, <strong>Perusahaan, Grakfik Pelaksanaan RKM, Gerafik Realisasi Per-KPI, Grafik Prestasi Perusahaan, Kontribusi Direktorat,</strong> dan <strong>Calender History</strong>.</p>
												<image src="<?php echo base_url();?>user_guide/images/dashboard.jpg"></image><p>Keterangan:</p>
												<ol style="list-style-type:lower-alpha">
													<li>Pilih<strong> Periode</strong> dasboard yang ingin ditampilkan</li>
													<li>Tanggal saat ini</li>
												</ol>						
												<image src="<?php echo base_url();?>user_guide/images/dashboard_1.jpg"></image>	<p>Keterangan C.7b:</p>
												<ol style="list-style-type:lower-alpha">
													<li>Gauge <strong>Keungan dan Pasar</strong> dengan bobot 20 % dan nilai prestasi 103.95 %</li>
													<li>Gauge <strong>Pelanggan/Customer</strong> dengan bobot 24 % dan nilai prestasi 100 %</li>
													<li>Gauge <strong>Efektivitas Produk dan Proses</strong> dengan bobot 20 % dan nilai prestasi 101.10 %</li>
												</ol>												
												<image src="<?php echo base_url();?>user_guide/images/dashboard_2.jpg"></image><p>Keterangan C.7c:</p>
												<ol style="list-style-type:lower-alpha">
													<li>Gauge <strong>Keungan dan Pasar</strong> dengan bobot 18 % dan nilai prestasi 9983 %</li>
													<li>Gauge <strong>Pelanggan/Customer</strong> dengan bobot 18 % dan nilai prestasi 102.61 %</li>
													<li>Gauge <strong>Efektivitas Produk dan Proses</strong> dengan bobot 100 % dan nilai prestasi 101.45 %</li>
												</ol>												
												<image src="<?php echo base_url();?>user_guide/images/dashboard_3.jpg"></image>	<p>Keterangan C.7d:</p>
												<ol style="list-style-type:lower-alpha">
													<li>Total RKM</li>
													<li>Jumlah RKM yang telah selesai dilaksanakan (<em>Done</em>)</li>
													<li>Jumlah RKM yang sedang berlangsung (<em>On Progress</em>)</li>
													<li>Jumlah RKM yang belum dilaksanakan (<em>Not Srated</em>)</li>
												</ol>					
												<image src="<?php echo base_url();?>user_guide/images/dashboard_4.jpg"></image><p>Keterangan C.7e:</p>
												<ol style="list-style-type:lower-alpha">
													<li>Menampilkan pilihan KPI pada Grafik Realisasi Per-KPI yang terdiri dari kategori grafik Realisasi, Best Performance, Kompetitor, dan Benchmark.</li>
													<li>Grafik Prestasi Perusahaan</li>
												</ol>
												
												<image src="<?php echo base_url();?>user_guide/images/dashboard_5.jpg"></image><p>Keterangan C.7f:</p>
												<ol style="list-style-type:lower-alpha">
													<li>Kolom nama KPI</li>
													<li>Terdiri dari kolom :</li>
												</ol>
												<ol style="list-style-type:lower-alpha">
													<li>DM&nbsp;&nbsp; : AIR TRAFFIC MANAGEMENT DIRECTOR</li>
													<li>DT &nbsp;&nbsp;&nbsp; : AIR NAVIGATION FACILITIES DIRECTOR</li>
													<li>DS&nbsp;&nbsp;&nbsp;&nbsp; : SAFETY &amp; STANDARD DIRECTOR</li>
													<li>DI &nbsp;&nbsp;&nbsp; : SERVICE DEVELOPMENT &amp; IT DIRECTOR</li>
													<li>DK &nbsp;&nbsp; : FINANCE DIRECTOR</li>
													<li>DP&nbsp;&nbsp;&nbsp;&nbsp; : PERSONNEL &amp; GENERAL AFFAIRS DIRECTOR</li>
													<li>DU&nbsp;&nbsp;&nbsp; : PRESIDEN DIRECTOR</li>
													<li>PER&nbsp; : Perushaan</li>
												</ol>
												<ol style="list-style-type:lower-alpha">
													<li>Terdiri dari kolom</li>
												</ol>
												<ol style="list-style-type:lower-alpha">
													<li>B&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Bobot</li>
													<li>R&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Realisasi</li>
													<li>K&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Kontribusi</li>
													<li>P&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Prestasi</li>
												</ol>
												<image src="<?php echo base_url();?>user_guide/images/dashboard_6.jpg"></image>
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC8" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.8. Help
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC8">
											 <div class="accordion-inner">
												<p>Menu <strong>Help</strong> terdiri dari dua submenu yaitu <strong>User Guide</strong> dan <strong>FAQ. </strong>Sub menu<strong> User Guide</strong> berisi petunjuk penggunaan aplikasi EPD sedangkan submenu <strong>FAQ</strong> berisi pertanayaan-pertanyaan yang sering muncul seputar penggunaan aplikasi EPD.</p>

												<image src="<?php echo base_url();?>user_guide/images/help.jpg"></image>						
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseD" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												D. My Profile
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseD">
											 <div class="accordion-inner">
												<p>Menu ini berisi informasi data user. Untuk mengetahui/mengubah profile user dapat dilakukan dengan langkah-langkah sebagai berikut:</p>
												<ol>
												<li>Pilih menu <strong>My</strong> <strong>Profile, </strong>data edit user akan ditampilkan seperti padagambar D.2.</li>
										
												<image src="<?php echo base_url();?>user_guide/images/my_profile.jpg"></image>	<li>Isi <strong>Nama</strong> dengan data yang baru *)</li>
												<li>Isi <strong>Old Password</strong> sesuai data password yang sedang aktif *)</li>
												<li>Isi <strong>New Password</strong> dengan data yang baru *)</li>
												<li>Isi <strong>Re New Password</strong> sama dengan New Password *)</li>
												<li>Pilih tombol <strong>Ubah</strong> untuk menyimpan perubahan data useru</li>
											</ol>
											<p>Keterangan :</p>
											<ol style="list-style-type:lower-alpha">
												<li><strong>Username</strong>, tidak dapat diubah</li>
											</ol>					
											</div>
										 </div>
									 </div>  
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseE" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												E. Log Out
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseE">
											 <div class="accordion-inner">
												<p>Menu ini untuk keluar dari aplikasi EPD dengan cara memilih menu Log out</p>
												<image src="<?php echo base_url();?>user_guide/images/logout.jpg"></image>						
											</div>
										 </div>
									 </div> 							

								 </div>
							</div>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->	
	