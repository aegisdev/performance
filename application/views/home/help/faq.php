<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				FAQ			
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="javascript:routes('help/faq','FAQ')">FAQ</a> </li>				
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-tint"></i>
							FAQ
						</h4>
					</div>
					<div class="portlet-body">	
							
                                     <h4>General Questions</h4>
                                     <div id="accordion" class="accordion in collapse" style="height: auto;">

                                         <div class="accordion-group">
                                             <div class="accordion-heading">
                                                 <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
                                                     Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus ?
                                                 </a>
                                             </div>
                                             <div class="accordion-body collapse in" id="collapseOne">
                                                 <div class="accordion-inner">
                                                     Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                 </div>
                                             </div>

                                         </div>
                                         <div class="accordion-group">
                                             <div class="accordion-heading">
                                                 <a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
                                                     Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid ?
                                                 </a>
                                             </div>
                                             <div class="accordion-body collapse" id="collapseTwo">
                                                 <div class="accordion-inner">
                                                     Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                 </div>
                                             </div>
                                         </div>
                                         <div class="accordion-group">
                                             <div class="accordion-heading">
                                                 <a href="#collapseThree" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
                                                     Nihil anim keffiyeh helvetica, craft beer labore wes ?
                                                 </a>
                                             </div>
                                             <div class="accordion-body collapse" id="collapseThree">
                                                 <div class="accordion-inner">
                                                     Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                 </div>
                                             </div>
                                         </div>
                                         <div class="accordion-group">
                                             <div class="accordion-heading">
                                                 <a href="#collapseFour" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
                                                     Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't ?
                                                 </a>
                                             </div>
                                             <div class="accordion-body collapse" id="collapseFour">
                                                 <div class="accordion-inner">
                                                     Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                 </div>
                                             </div>
                                         </div>
                                         <div class="accordion-group">
                                             <div class="accordion-heading">
                                                 <a href="#collapseFive" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
                                                     Nihil anim keffiyeh helvetica, craft beer labore wes ?
                                                 </a>
                                             </div>
                                             <div class="accordion-body collapse" id="collapseFive">
                                                 <div class="accordion-inner">
                                                     Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                 </div>
                                             </div>
                                         </div>
                                         <div class="accordion-group">
                                             <div class="accordion-heading">
                                                 <a href="#collapseSix" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
                                                     Nihil anim keffiyeh helvetica, craft beer labore wes ?
                                                 </a>
                                             </div>
                                             <div class="accordion-body collapse" id="collapseSix">
                                                 <div class="accordion-inner">
                                                     Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                 </div>
                                             </div>
                                         </div>
                                         <div class="accordion-group">
                                             <div class="accordion-heading">
                                                 <a href="#collapseSeven" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
                                                     Ad vegan excepteur butcher vice lomo ?
                                                 </a>
                                             </div>
                                             <div class="accordion-body collapse" id="collapseSeven">
                                                 <div class="accordion-inner">
                                                     Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                 </div>
                                             </div>
                                         </div>
                                         <div class="accordion-group">
                                             <div class="accordion-heading">
                                                 <a href="#collapse8" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
                                                     Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ?
                                                 </a>
                                             </div>
                                             <div class="accordion-body collapse" id="collapse8">
                                                 <div class="accordion-inner">
                                                     Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                 </div>
                                             </div>
                                         </div>

                                     </div>
                               
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
<script type="text/javascript" charset="utf-8">			
	$('#data_table').dataTable({
		"sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ baris per halaman",
			"oPaginate": {
				"sPrevious": "Prev",
				"sNext": "Next"
			}
		},
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': [0]
		}]
	});
	
	$('.delete_data').click(function () { 
		var id =$(this).attr('data-id');
		new Messi('Yakin ingin menghapus data dengan ID '+id+' ?', {
						title: 'Confirmation', 
						titleClass: 'anim info', 
						closeButton: true,
						buttons: [
						{
							id: 0, 
							label: 'YES', 
							val: '1'
						},{
							id: 1, 
							label: 'NO', 
							val: '0'
						}
						], 
						callback:function(val)
						{ 
							if(val == 0) {
								return false;
							} else if(val == 1) {
								$.ajax({
									type: "POST",
									url: "<?php echo base_url();?>help/delete",
									data: "id="+id,
									success: function(msg){
										if(msg=='1') {
											new Messi('Data berhasil dihapus !', {title: 'Menu Message', titleClass: 'anim info', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('help','Help Management'); }});
										} else {
											new Messi('Data gagal dihapus !<br />Debug : '+msg, {title: 'Menu Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
										} 
									},
									error: function(fnc,msg){
										new Messi('Tidak dapat terhubung ke server untuk malakukan proses hapus data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
									}
								});
							} 
						}
					}
				);
	});
</script>
	
	