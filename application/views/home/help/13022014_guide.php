<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				User Guide			
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="javascript:routes('help/guide','Guide')">User Guide</a> </li>				
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-book"></i>
							User Guide
						</h4>
					</div>
					<div class="portlet-body">	
						<div class="about-us">
							<div class="text-right">
								 <a href="<?php echo base_url();?>user_guide/USER_GUIDE.pdf" target="_blank" class="btn blue">Download User Guide</a>
							</div>
							Patunjuk:<br/>
							Klik pada menu/submenu untuk melihat isi user guide yang dinginkan.<br/><br/>
                                 <div id="accordion" class="accordion in collapse" style="height: auto;">									 
									  <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseA" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												 A. Login
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseA">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/login.jpg"></image>
											 </div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												 B. Halaman Utama
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseTwo">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/halaman_utama.jpg"></image>
											 </div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseThree" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												 C. Menu
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseThree">
											 <div class="accordion-inner">
												Menu aplikasi EPD terdiri dari Dashboard, Laporan, Referensi Strategis, Kontrak, Manajemen, KPI dan Program Kerja, Parameter, Pengaturan Pengguna, dan Help seperti yang diperlihatkan pada gambar C.1<br/>
												<image src="<?php echo base_url();?>user_guide/images/menu.jpg"></image>
											 </div>
										 </div>
									 </div>									
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseFour" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.1. Referensi Strategi
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseFour">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/referensi_strategi.jpg"></image>
											 </div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseFive" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.1.1 Submenu Sasaran Strategi
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseFive">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/sasaran_strategis.jpg"></image><br/><br/>
												<image src="<?php echo base_url();?>user_guide/images/sasaran_strategis_list.jpg"></image><br/><br/>
												<image src="<?php echo base_url();?>user_guide/images/sasaran_strategis_add.jpg"></image><br/><br/>
												<image src="<?php echo base_url();?>user_guide/images/sasaran_strategis_edit.jpg"></image><br/><br/>
												<image src="<?php echo base_url();?>user_guide/images/sasaran_strategis_delete.jpg"></image><br/><br/>
											 </div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseSix" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												 C.1.2. Submenu Tema Strategis
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseSix">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/tema_strategis.jpg"></image>
												<image src="<?php echo base_url();?>user_guide/images/tema_strategis_list.jpg"></image>
												<image src="<?php echo base_url();?>user_guide/images/tema_strategis_add.jpg"></image>
												<image src="<?php echo base_url();?>user_guide/images/tema_strategis_edit.jpg"></image>
												<image src="<?php echo base_url();?>user_guide/images/tema_strategis_delete.jpg"></image>
											 </div>
										 </div>
									 </div>									
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC13" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												 C.1.3. Submenu Inisiatif Strategis
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC13">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/inisiatif_strategis.jpg"></image>
												<image src="<?php echo base_url();?>user_guide/images/inisiatif_strategis_list.jpg"></image>
												<image src="<?php echo base_url();?>user_guide/images/inisiatif_strategis_add.jpg"></image>
												<image src="<?php echo base_url();?>user_guide/images/inisiatif_strategis_edit.jpg"></image>
												<image src="<?php echo base_url();?>user_guide/images/inisiatif_strategis_delete.jpg"></image>
											 </div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapse9" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												 C.2. Parameter
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapse9">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/parameter.jpg"></image>												
											 </div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapse10" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.2.1. Submenu Struktur Organisasi
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapse10">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/struktur_organisasi.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/struktur_organisasi_1.jpg"></image>		
												<image src="<?php echo base_url();?>user_guide/images/struktur_organisasi_add.jpg"></image>		
												<image src="<?php echo base_url();?>user_guide/images/struktur_organisasi_edit.jpg"></image>	
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapse11" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.2.2. Submenu Satuan
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapse11">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/satuan.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/satuan_list.jpg"></image>		
												<image src="<?php echo base_url();?>user_guide/images/satuan_add.jpg"></image>		
												<image src="<?php echo base_url();?>user_guide/images/satuan_edit.jpg"></image>	
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapse12" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.2.3. Submenu Jenis Formula
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapse12">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/jenis_formula.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/jenis_formula_list.jpg"></image>	
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapse13" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.2.4. Submenu Batas Penilaian
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapse13">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/batas_penilaian.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/batas_penilaian_list.jpg"></image>	
												<image src="<?php echo base_url();?>user_guide/images/batas_penilaian_add.jpg"></image>	
											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapse14" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.2.5. Submenu Batas Penilaian
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapse14">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/tingkat_penilaian.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/tingkat_penilaian_list.jpg"></image>	
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapse15" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.2.5. Submenu Input Realisasi
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapse15">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/input_realisasi.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/input_realisasi_list.jpg"></image>	
											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC3" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.3. Pengaturan Pengguna
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC3">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/pengaturan_pengguna.jpg"></image>			
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC31" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.3.1. Submenu Hak Akses Kelompok
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC31">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/hak_akses_kelompok.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/hak_akses_kelompok_list.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/hak_akses_kelompok_add.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/hak_akses_kelompok_edit.jpg"></image>			
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC32" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.3.2. Submenu Pengguna
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC32">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/pengguna.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/pengguna_list.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/pengguna_add.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/pengguna_edit.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/pengguna_delete.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/reset_password.jpg"></image>			
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC4" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.4. KPI dan Program Kerja
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC4">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/menu_kpi.jpg"></image>				
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC41" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.4.1. Submenu Daftar KPI
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC41">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/kpi.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/kpi_list.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/kpi_add.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/kpi_edit.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/kpi_delete.jpg"></image>				
											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC42" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.4.2. Submenu Daftar Program Kerja
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC42">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/program_kerja.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/program_kerja_list.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/program_kerja_add.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/program_kerja_edit.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/program_kerja_delete.jpg"></image>				
											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC5" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5. Kontrak Manajemen
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC5">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/kontrak_manajemen.jpg"></image>				
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC51" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.1. Submenu KM Perusahaan
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC51">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/km_perusahaan.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/km_perusahaan_1.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/km_perusahaan_2.jpg"></image>					
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC52" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.2. Submenu Distribusi KM
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC52">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/distribusi_km.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/distribusi_km_list.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/distribusi_km_detail.jpg"></image>			
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC53" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.3. Submenu KM Unit
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC53">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/km_unit.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/km_unit_list.jpg"></image>					
											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC54" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.4. Submenu Realisasi Oleh Unit
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC54">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/realisasi_oleh_unit.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/realisasi_oleh_unit_list.jpg"></image>					
											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC55" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.5. Submenu Realisasi Oleh Penilai
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC55">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/realisasi_oleh_penilai.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/realisasi_oleh_penilai_list.jpg"></image>					
											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC56" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.6. Submenu Finalisasi Penilai
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC56">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/finalisasi_penilai.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/finalisasi_penilai_list.jpg"></image>					
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC57" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.7. Submenu Penyusunan RKM
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC57">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/penyusunan_rkm.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/penyusunan_rkm_list.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/penyusunan_rkm_list_2.jpg"></image>		
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC58" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.8. Submenu Pelaksanaan RKM
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC58">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/pelaksanaan_rkm.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/pelaksanaan_rkm_list.jpg"></image>		
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC59" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.5.9. Submenu Data Pembanding
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC59">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/data_pembanding.jpg"></image>				
												<image src="<?php echo base_url();?>user_guide/images/data_pembanding_list.jpg"></image>		
												<image src="<?php echo base_url();?>user_guide/images/data_pembanding_add.jpg"></image>		
												<image src="<?php echo base_url();?>user_guide/images/data_pembanding_edit.jpg"></image>		
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC6" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6. Laporan
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC6">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/laporan.jpg"></image>					
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC61" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.1. Submenu Peta Strategi
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC61">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/peta_strategi_list.jpg"></image>					
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC62" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.2. Submenu Peta KPI
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC62">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/peta_kpi_list.jpg"></image>					
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC63" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.3. Submenu Aligment RKM
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC63">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/aligment_rkm.jpg"></image>					
												<image src="<?php echo base_url();?>user_guide/images/aligment_rkm_list.jpg"></image>					
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC64" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.4. Submenu Laporan RKM Perusahaan
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC64">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/laporan_rkm_perusahaan.jpg"></image>					
												<image src="<?php echo base_url();?>user_guide/images/laporan_rkm_perusahaan_list.jpg"></image>					
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC65" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.5. Submenu Laporan KM Unit
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC65">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/laporan_km_unit.jpg"></image>					
												<image src="<?php echo base_url();?>user_guide/images/laporan_km_unit_list.jpg"></image>					
											</div>
										 </div>
									 </div>
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC66" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.6. Submenu Kontribusi Direktorat
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC66">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/kontribusi_direktorat.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/kontribusi_direktorat_list.jpg"></image>		
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC67" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.7. Submenu Monitor Realisasi
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC67">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/monitor_realisasi.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/monitor_realisasi_grafik_1.jpg"></image>		
												<image src="<?php echo base_url();?>user_guide/images/monitor_realisasi_grafik_2.jpg"></image>		
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC68" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.6.8. Submenu Laporan RKM
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC68">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/laporan_rkm.jpg"></image>			
												<image src="<?php echo base_url();?>user_guide/images/laporan_rkm_list.jpg"></image>				
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC7" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.7. Dashboard
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC7">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/dashboard.jpg"></image>						
												<image src="<?php echo base_url();?>user_guide/images/dashboard_1.jpg"></image>						
												<image src="<?php echo base_url();?>user_guide/images/dashboard_2.jpg"></image>						
												<image src="<?php echo base_url();?>user_guide/images/dashboard_3.jpg"></image>						
												<image src="<?php echo base_url();?>user_guide/images/dashboard_4.jpg"></image>						
												<image src="<?php echo base_url();?>user_guide/images/dashboard_5.jpg"></image>						
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseC8" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												C.8. Help
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseC8">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/help.jpg"></image>						
											</div>
										 </div>
									 </div> 
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseD" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												D. My Profile
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseD">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/my_profile.jpg"></image>					
												<image src="<?php echo base_url();?>user_guide/images/my_profile_edit.jpg"></image>					
											</div>
										 </div>
									 </div>  
									 <div class="accordion-group">
										 <div class="accordion-heading">
											 <a href="#collapseE" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle">
												E. Log Out
											 </a>
										 </div>
										 <div class="accordion-body collapse" id="collapseE">
											 <div class="accordion-inner">
												<image src="<?php echo base_url();?>user_guide/images/logout.jpg"></image>						
											</div>
										 </div>
									 </div> 							

								 </div>
							</div>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
	