	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Data Pembanding	
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('measurements/kompetitor','Data Pembanding')">Data Pembanding</a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>
							Data Pembanding
						</h4>
					</div>
					<div class="portlet-body">		
						<table border="0">
							<tr>
								<td width="100px">Periode</td>
								<td>
									<select name="year" id="tahun">
										<option value="-1">-- Pilih Tahun --</value>
										<?php for($year=2013;$year <=(date('Y')+1);$year++){ ?>
										<option value="<?php echo $year;?>"><?php echo $year;?></option>
										<?php } ?>
									</select>
								</td>
								<td>
								</td>
							</tr>
						</table>
						<br/>
						<div class="btn-group">
							<button class="btn blue" id="btn_tahun" onClick="javascript:routes('kompetitor/add/2013','Kompetitor')">
							<i class="icon-plus"></i> Tambah
							</button>
						</div>
						<br/>							
						<table class="table table-bordered table-striped table-hover" id="data_table">
							<thead>
								<tr>
									<th width="3%">ID#</th>
									<th>TAHUN</th>
									<th>KPI</th>											
									<th>BEST PERFORMANCE</th>											
								<!-- 	<th>KOMPETITOR</th>											
									<th>BENCHMARK</th>	 -->										
									<th width="130px">AKSI</th>											
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data as $row) { ?>
									<tr>
										<td><?php echo $row->id;?></td>
										<td><?php echo $row->tahun;?></td>
										<td><?php echo $row->kpi;?></td>
										<td class="center-column"><?php echo $row->best_performance;?></td>				
										<!-- <td class="center-column"><?php echo $row->kompetitor;?></td>				
										<td class="center-column"><?php echo $row->benchmark;?></td>		 -->		
										<td class="center-column">
											<button class="btn blue" onClick="javascript:routes('kompetitor/edit/<?php echo $row->id;?>','Kompetitor')"><i class="icon-edit" title="Edit data"></i></button>
											<button class="btn delete_data blue" data-id="<?php echo $row->id;?>" data-label="<?php echo $row->kpi;?>"><i class="icon-trash" title="Hapus data"></i></button>
										</td>				
									</tr>
									<?php } ?>			
							</tbody>
						</table>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->

<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>
<script type="text/javascript" charset="utf-8">		
 	$(document).ready(function(){
 		$('#tahun').select2();
		$('#data_table').dataTable({
			"sPaginationType": "bootstrap",
			"oLanguage": {
				"sLengthMenu": "_MENU_ baris per halaman",
				"oPaginate": {
					"sPrevious": "Prev",
					"sNext": "Next"
				}
			},
			"aoColumnDefs": [{
				'bSortable': false,
				'aTargets': [0,-1]
			}]
		});

		$('#data_table').delegate('.delete_data', 'click', function () { 
			var id =$(this).attr('data-id');
			var label = $(this).attr('data-label');
			new Messi('Yakin ingin menghapus data pembanding KPI <strong>'+label+'</strong>?', {
				title: 'Confirmation', 
				titleClass: 'anim info', 
				closeButton: true,
				buttons: [
				{
					id: 0, 
					label: 'YES', 
					val: '1'
				},{
					id: 1, 
					label: 'NO', 
					val: '0'
				}
				], 
				callback:function(val)
				{ 
					if(val == 0) {
						return false;
					} else if(val == 1) {
						$.ajax({
							type: "POST",
							url: "<?php echo base_url();?>kompetitor/delete",
							data: "id="+id,
							success: function(msg){
								if(msg=='1') {
									new Messi('Data berhasil dihapus !', {title: 'Menu Message', titleClass: 'anim info', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('kompetitor','Kompetitor'); }});
								} else {
									new Messi('Data gagal dihapus !<br />Debug : '+msg, {title: 'Menu Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
								} 
							},
							error: function(fnc,msg){
								new Messi('Tidak dapat terhubung ke server untuk malakukan proses hapus data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
							}
						});
					} 
				}
			});
		});
		
		$("#tahun").change(function(){	
			var tahun=$("#tahun").val();
			$("#btn_tahun").attr('onClick',"javascript:routes('kompetitor/add/"+tahun+"','Kompetitor')");	
			$.ajax({
				type: 'post',
				url : '<?php echo base_url();?>kompetitor/get_pembanding/'+tahun,
				dataType: 'json',	
				beforeSend: function(){
					$('#data_table > tbody:last').empty();
					//$('#btn-save-realization-unit').hide();
				}			
			})
			.done(function(response, textStatus, jqhr){
				if(response){	
					var el='';
					for(var i=0;i<response.length;i++){	
						el+='<tr>';
						el+='<td width="30px" class="center-column">'+(i+1)+'</td>';
						el+='<td class="center-column">'+response[i].tahun+'</td>';
						el+='<td>'+response[i].kpi+'</td>';
						el+='<td class="center-column">'+response[i].best_performance+'</td>';					
						// el+='<td class="center-column">'+response[i].kompetitor+'</td>';					
						// el+='<td class="center-column">'+response[i].benchmark+'</td>';
						el+='<td class="center-column">';
						el+='<button class="btn blue" onclick="javascript:routes(\'kompetitor/edit/'+response[i].id+'\',\'Kompetitor\')"><i class="icon-edit" title="Edit data"></i></button>';
						el+='&nbsp;<button class="btn delete_data blue" data-id="'+response[i].id+'" data-label="'+response[i].kpi+'"><i class="icon-trash" title="Hapus data"></i></button>';
						el+='</td>';						
						el+='</tr>';			
					}				
					$('#data_table > tbody:last').append(el);	
				}
			})
			.fail(function(){
			
			});
		});

		function del_item(id){
			new Messi('Yakin ingin menghapus data dengan ID '+id+' ?', {
				title: 'Confirmation', 
				titleClass: 'anim info', 
				closeButton: true,
				buttons: [
				{
					id: 0, 
					label: 'YES', 
					val: '1'
				},{
					id: 1, 
					label: 'NO', 
					val: '0'
				}
				], 
				callback:function(val)
				{ 
					if(val == 0) {
						return false;
					} else if(val == 1) {
						$.ajax({
							type: "POST",
							url: "<?php echo base_url();?>kompetitor/delete",
							data: "id="+id,
							success: function(msg){
								if(msg=='1') {
									new Messi('Data berhasil dihapus !', {title: 'Menu Message', titleClass: 'anim info', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('kompetitor','Kompetitor'); }});
								} else {
									new Messi('Data gagal dihapus !<br />Debug : '+msg, {title: 'Menu Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
								} 
							},
							error: function(fnc,msg){
								new Messi('Tidak dapat terhubung ke server untuk malakukan proses hapus data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
							}
						});
					} 
				}
			});
		}
	});
</script>
	
	