<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Data Pembanding	
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" data-toggle="tooltip" rel="tooltip" data-placement="top" onClick="routes('kompetitor','Kompetitor')">Kompetitor </a> </li>		
				<i class="icon-angle-right"></i>	
				<li><a href="#" data-toggle="tooltip" rel="tooltip" data-placement="top"> Tambah</a> </li>						
			</ul>
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-plus"></i>
						<span class="hidden-480">Tambah Data Pembanding</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">								
									 <div class="control-group">
										  <label class="control-label">Tahun</label>
										  <div class="controls">
											<input type="text" name="tahun" id="tahun" value="<?php echo $tahun;?>" readonly="1" class="span2 m-wrap">
																			
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Nama KPI</label>
										  <div class="controls">
											  <select name="kpi_id" id="kpi_id" class="span4 m-wrap">
												<option value="">Pilih nama KPI</option>
												<?php foreach ($data as $row) { ?>
													<option value="<?php echo $row->id;?>"><?php echo $row->kpi;?></option>
												<?php } ?>
											 </select>
										  </div>
									 </div>	 
									<!--<div class="control-group">
										  <label class="control-label">Perusahaan Terbaik</label>
										  <div class="controls">
											 <textarea id="perusahaan_terbaik" name="perusahaan_terbaik" class="span4 m-wrap" rows="3"></textarea>																																													 											 
										  </div>
									 </div>	-->	
<!-- 									 <div class="control-group">
										  <label class="control-label">Kompetitor</label>
										  <div class="controls">
											 <input name="kompetitor" id="kompetitor" type="text" value="" class="span2 m-wrap"/>
										  </div>
									 </div>	  -->
									 	<input name="kompetitor" id="kompetitor" type="hidden" value="0" class="span2 m-wrap"/>
									  <input name="benchmark" id="benchmark" type="hidden" value="0" class="span2 m-wrap"/>

									  <div class="control-group">
										  <label class="control-label">Best Performance</label>
										  <div class="controls">
											 <input name="best_performance" id="best_performance" type="text" value="" class="span2 m-wrap"/>
										  </div>
									 </div>	 
<!-- 									 <div class="control-group">
										  <label class="control-label">Benchmark</label>
										  <div class="controls">
											 <input name="benchmark" id="benchmark" type="text" value="" class="span2 m-wrap"/>
										  </div>
									 </div>	  -->
									 <div class="control-group">
										<div class="controls">					
											<button class="btn blue" type="button" onClick="save()"><i class="icon-save"></i> Simpan</button>
											<button class="btn red" type="button" onClick="routes('kompetitor','Kompetitor')"><i class="icon-share"></i> Kembali</button>
											
										</div>
									 </div>									
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>	
<script language="javascript">
$(document).ready(function(){
	$("#kpi_id").select2(); 
	//$("#tahun").select2(); 	
});

	function validate() {
		if ($('#tahun').val() =='') {
			new Messi('Tahun masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("tahun").focus(); }});
			return false;
		}
		if ($('#kpi_id').val() =='') {
			new Messi('KPI masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("kpi_id").focus();  }});
			return false;
		}
		if ($('#best_performance').val() =='') {
			new Messi('Best performance terbaik masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("best_performance").focus();  }});
			return false;
		}
		if ($('#kompetitor').val() =='') {
			new Messi('Kompetitor masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("nilai").focus();  }});
			return false;
		}
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>kompetitor/save",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil disimpan !', {title: 'Message', titleClass: 'anim success', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('kompetitor','Kompetitor'); }});
					} else if (msg=='2'){
						new Messi('Group sudah terdaftar!<br />Pesan : '+msg, {title: 'Message', titleClass: 'anim warning', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
	/*$("#tahun").change(function(){
		var tahun=$('#tahun').val();
		$.ajax({
			type: 'post',
			data: {tahun:tahun},
			url: "<?php echo base_url();?>kpi/get_kpi_by_tahun",
			dataType: 'json'
		})
		.done(function(response, textStatus, jqhr){
			if(response){			
				var str='';
				str+='<option value=""></option>';
				for(var i=0;i<response.length;i++){					
					str+='<option value="'+response[i].id+'">'+response[i].kpi+'</option>';					
				}			
				$('#kpi_id').html(str);				
			}
		})
		.fail(function(){
		
		});
	
	});*/
</script>