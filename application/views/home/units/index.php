
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Satuan			
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('units','Satuan')">Satuan </a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>
							Daftar Satuan
						</h4>
					</div>
					<div class="portlet-body">	
						<div class="clearfix">
							<div class="btn-group">
								<button class="btn blue" onClick="javascript:routes('units/add','Tambah Unit')">
								<i class="icon-plus"></i> Tambah
								</button>
							</div>						
						</div>
						<table class="table table-bordered table-striped table-hover" id="data_table">
							<thead>
								<tr>
									<th width="3%">ID#</th>
									<th>SATUAN</th>
									<th>KETERANGAN</th>								
									<th width="130px">AKSI</th>									
								</tr>
							</thead>
							<tbody>
								<?php foreach ($data as $row) { ?>
									<tr>
										<td><?php echo $row->id;?></td>
										<td><?php echo $row->name;?></td>
										<td><?php echo $row->description;?></td>
										<td class="center-column">
											<button class="btn blue" onClick="javascript:routes('units/edit/<?php echo $row->id;?>','Edit Unit')"><i class="icon-pencil"></i></button>
											<button class="btn delete_data blue" data-id="<?php echo $row->id;?>"><i class="icon-trash "></i></button>
										</td>
									</tr>
									<?php } ?>			
							</tbody>
						</table>
						<div class="clearfix"></div>
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->
	
<script type="text/javascript" charset="utf-8">			
	$('#data_table').dataTable({
		"sDom": 'T<"clear">lfrtip',
			"oTableTools": {
				"sSwfPath": "<?php echo base_url();?>assets/img/swf/copy_csv_xls_pdf.swf"
			},
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ baris per halaman",
			"oPaginate": {
				"sPrevious": "Prev",
				"sNext": "Next"
			}
		},
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': [0,2,3]
		}]
	});
	
	$('#data_table').delegate('.delete_data','click',function () { 
		var id =$(this).attr('data-id');
		new Messi('Yakin ingin menghapus data dengan ID '+id+' ?', {
						title: 'Confirmation', 
						titleClass: 'anim info', 
						closeButton: true,
						buttons: [
						{
							id: 0, 
							label: 'YES', 
							val: '1'
						},{
							id: 1, 
							label: 'NO', 
							val: '0'
						}
						], 
						callback:function(val)
						{ 
							if(val == 0) {
								return false;
							} else if(val == 1) {
								$.ajax({
									type: "POST",
									url: "<?php echo base_url();?>units/delete",
									data: "id="+id,
									success: function(msg){
										if(msg=='1') {
											new Messi('Data berhasil dihapus !', {title: 'Menu Message', titleClass: 'anim info', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('units','Unit Management'); }});
										} else {
											new Messi('Data gagal dihapus !<br />Debug : '+msg, {title: 'Menu Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
										} 
									},
									error: function(fnc,msg){
										new Messi('Tidak dapat terhubung ke server untuk malakukan proses hapus data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
									}
								});
							} 
						}
					}
				);
	});
</script>
	
	