	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">					
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Verifikasi RKM		
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" onClick="routes('verifikasi_rkm/index','Pelaksanaan RKM')">Pelaksanaan RKM</a>					
				</li>			
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->			

				<div class="portlet box green">
					<div class="portlet-title">
						<h4>
							<i class="icon-table"></i>Verifikasi RKM
						</h4>
					</div>
					<div class="portlet-body">						
						<table border="0">
							<tr>
								<td width="100px">Unit</td>
								<td colspan="2" width="90%">
									<select name="organization_id" id="organization_id" class="span6 m-wrap">										
										<option value="-1">-- Pilih Unit --</value>
										<?php foreach($data as $row){ ?>
										<option value="<?php echo $row->id;?>"><?php echo $row->name;?></value>
										<?php } ?>
									</select>
								</td>
							</tr>
							<tr>
								<td>Periode</td>
								<td>
									<select name="year" id="year" class="span2 m-wrap">
										<option value="-1">-- Pilih Tahun --</value>
										<?php for($year=2013;$year <=(date('Y')+1);$year++){ ?>
										<option value="<?php echo $year;?>"><?php echo $year;?></option>
										<?php } ?>
									</select>
								</td>
								<td>
								</td>
							</tr>
						</table>
						<br/>
						<div class="clearfix">						
							<div class="pull-left">
								<p>
									<button class="btn blue" type="button" id="btn_save"><i class="icon-save"></i> Simpan</button>
								</p> 
							</div>
						</div>
						<form id="form_update_program_kerja">
						<input type="hidden" id="choose_unit" name="choose_unit" />
						<input type="hidden" id="choose_periode" name="choose_periode" />
						<table class="table table-bordered table-striped table-hover" id="tabel_program_kerja">
							<thead>
								<tr>
									<th width="5px">NO</th>
									<th width="300px">PROGRAM KERJA</th>													
									<th width="100px">STATUS</th>
									<th>KETERANGAN</th>
									<th width="300px">VERIFIKASI</th>													
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						</form>
						<center>
						<button class="btn blue" id="btn-save-realization-unit" style="display:none"><i class="icon-save"></i> Simpan</button>
						</center>
						<div class="clearfix"></div> 
					</div>
				</div>	
	</div>
	<!-- END PAGE HEADER-->

<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$("#organization_id").select2(); 
		$("#year").select2(); 	
	});

	(function(){
		function loadRkm(unit, periode){
			$.ajax({
				type: 'post',
				data: {'unit' : unit, 'periode' : periode},
				url : '<?php echo base_url();?>pelaksanaan_rkm/get_progaram_kerja',
				dataType: 'json',
				beforeSend: function(){
					$('#tabel_program_kerja > tbody:last').empty();
					//$('#btn-save-realization-unit').hide();
				}
			})
			.done(function(response, textStatus, jqhr){
				if(response){			
					for(var i=0;i<response.length;i++){
						var status = 'Done'; 		
						if(response[i].status_tobe_verify == 1){
							status = 'Not Started';
						} else if(response[i].status_tobe_verify == 2) {
							status = response[i].value_tobe_verify + '%';
						}

						var selectedSatu = '';
						var selectedDua = '';
						var display = '';

						if(response[i].status_verifikasi == 1 || response[i].status_verifikasi == 0){
							selectedSatu = 'selected';
						} else if(response[i].status_verifikasi == 2){
							selectedDua = 'selected';
							display = '';
						} 

						var el = '<tr valign="middle">' + 
							'<td width="30px">'+(i+1)+'<input type="hidden" name="program_kerja_id[]" value="'+response[i].id+'"/></td>' + 
							'<td><input type="hidden" value="'+response[i].program_kerja+'" name="program_kerja[]" />'+response[i].program_kerja+'</td>' + 
							'<td class="center-column" width="100px">'+status+'</td>' + 
							'<td>'+((response[i].keterangan_tobe_verify) ?  response[i].keterangan_tobe_verify : '')+'</td>'+ 
						  '<td>' + 
						  	'<select name="status_verifikasi[]" style="width:160px" class="status_verifikasi" id-text-area="dispute_rkm_'+i+'" >' + 
						  		'<option value="1" '+selectedSatu+'>Sesuai</option>' + 
						  		'<option value="2" '+selectedDua+'>Belum Sesuai</option>' + 
						  	'</select>' +
						  	'<div id="dispute_rkm_'+i+'" '+display+'>' + 
						  		'<textarea name="keterangan_verifikasi[]" rows="4" style="width:280px;">'+ ((response[i].keterangan_verifikasi) ?  response[i].keterangan_verifikasi : '') +'</textarea>' + 
						  	'</div>'+ 
						  '</td>'+
						  '<input type="hidden" value="'+response[i].value_tobe_verify+'" name="value_tobe_verify[]" />' +
						  '<input type="hidden" value="'+response[i].status_tobe_verify+'" name="status_tobe_verify[]" />' +
						  '<input type="hidden" value="'+response[i].value+'" name="value[]" />' +
						  '<input type="hidden" value="'+response[i].progress_id+'" name="progress_id[]" />' +
						  '<input type="hidden" value="'+response[i].program_kerja_id+'" name="id_rkm[]" />' +
						 	'<input type="hidden" value="'+response[i].keterangan_rencana+'" name="keterangan[]" />' +
						 	'<input type="hidden" value="'+response[i].keterangan_tobe_verify+'" name="keterangan_tobe_verify[]" />' +
						  '</tr>';
						$('#tabel_program_kerja > tbody:last').append(el);				
					}
				}
			})
			.fail(function(){
			
			});
		}
	
		$('#btn_save').click(function(e){		
			new Messi('Apakah anda yakin data yang anda masukkan sudah benar dan akan melakukan verifikasi?', {
				title: 'Confirmation', 
				titleClass: 'anim info', 
				closeButton: true,
				buttons: [
				{
					id: 0, 
					label: 'YES', 
					val: '1'
				},{
					id: 1, 
					label: 'NO', 
					val: '0'
				}], 
				callback:function(val){ 
					if(val == 0) {
						return false;
					} else if(val == 1) {
						$.ajax({
							type: 'post',
							url: '<?php echo base_url();?>verifikasi_rkm/verifikasi',
							data: $('#form_update_program_kerja').serialize(),
							dataType: 'json'
						})
						.done(function(response, textStatus, jqhr){
							if(response.status == 'ok'){
								$.growl.notice({ title: "Informasi", message: "Data berhasil disimpan ke database." });
								$('#modal_porgam_kerja').trigger('closeModal');
							} else {
								$.growl.error({ title: "Informasi", message: "Terjadi kesalahan, silahkan kontak admin!" });
							}
							var unit=$('#choose_unit').val();
							var periode=$('#choose_periode').val();
							loadRkm(unit, periode);
						})
						.fail(function(){
						})
					} 
				}
			});

			e.preventDefault();
		});

		$('#organization_id').change(function(){
			var optionSelected = $(this).find("option:selected");
			$('#choose_unit').val(optionSelected.val());		
			if($('#year').val() !=-1)
				loadRkm(optionSelected.val(), $('#year').val());
		});

		$('#year').change(function(){
			var optionSelected = $(this).find("option:selected");
			$('#choose_periode').val(optionSelected.val());	
			if($('#organization_id').val() !=-1)
				loadRkm($('#organization_id').val(), optionSelected.val());
		});

		$('#tabel_program_kerja').delegate('.status_verifikasi', 'change', function(a){
			var id = $(this).attr('id-text-area');
			// if(a.currentTarget.value == 2){
			// 	$('#' + id).show();
			// } else {
			// 	$('#' + id).hide();
			// }
		});

	}());

</script>	