<form style="margin: 10px;">
	<div class="span5">
		<fieldset>        
			<?php
				echo $menus;			
				/*foreach($menus as $menu){
					 echo "<label class=\"checkbox\"><input class=\"checkbox\" type=\"checkbox\" data-menu-id=\"".$menu->id."\" data-menu=\"".$menu->name."\" value=\"".$menu->id."\" name=\"menu[]\" id=\"menu_id-" .$menu->id. "\">" . $menu->name . "</label>";
				} */
			?>         
        </fieldset>
	</div>
</form>
<script type="text/javascript">
	$("#btn_save_list_group").click(function(){
		var menu_name='';
		var menu_id='';
			$(':checked.checkbox').each(function(i, el){
				menu_name+=$(this).attr("data-menu")+";";			
				menu_id+=$(this).attr("data-menu-id")+";";
			});  					
			if(menu_id.length>0){
				$('#menu_name').val(menu_name);
				$('#menu_id').val(menu_id);
				return true;
			}else
			{
				alert("Harus pilih salah satu");
				return false;
			}
	
	  }); 
	 
		$(".select_all").click(function () {
			var current=$(this).attr('data-menu-id');
			$('.items'+current).attr('checked', this.checked);
		});
		$(".select_item").click(function(){			
		if($(".select_item").length == $(".select_item:checked").length) {
			$(".select_all").attr("checked", "checked");	
		} else {
			$(".select_all").removeAttr("checked");	
			
		}
 
    });
	
	

$(function() {
  $('#modal-edit-user-group-role').easyModal();
  //$('input[type=radio], input[type=checkbox]').uniform();
});

$('.check-all').click(function(){
  var parentId = $(this).val();

  if(this.checked){
    $('.children-of-' + parentId).each(function(){
      this.checked = true;
    });
  } else {
    $('.children-of-' + parentId).each(function(){
      this.checked = false;
    });
  }
});

$('.children-checkbox').click(function(){
  var parentId = $(this).attr('parent-id');

  if(this.checked){
    $('#menu-id-' + parentId).attr('checked', true);
  }
});

$('.btn-edit-user-group-role').click(function(){
  var id = $(this).attr('object');

  $.ajax({
    url: '<?php echo base_url();?>user_group/id/' + id,
    dataType: 'json'
  })
  .done(function(response, textStatus, jqhr){

  })
  .fail(function(){

  })
  .always(function(response, textStatus,jqhr){
    if(response.status == "ok"){
      $('#edit-group-role-id').val(response.data.id);
      $('#edit-group-role-name').val(response.data.name);
    }    
  })

  $.ajax({
    url: '<?php echo base_url();?>user_group_role/id/' + id,
    dataType: 'json',
    beforeSend: function(){
      $('input[type=checkbox]').removeAttr('checked');
    }
  })
  .done(function(response, textStatus, jqhr){
    if(response.status == "ok"){
      for(var i=0;i<response.data.length;i++){
        var id = response.data[i].menu_id;
        $('#menu-id-' + id).attr('checked',true);
      }
    }
  })
  .fail({

  });

  $('#modal-edit-user-group-role').trigger('openModal');
});

$('#btn-edit-changes-user-group-role').click(function(){
  $.ajax({
    url: '<?php base_url();?>user_group_role/edit/',
    type: 'POST',
    dataType: 'json',
    data: $('#form-edit-user-group-role').serialize()
  })
  .done(function(response, textStatus, jqhr){
    listUserGroupRole();
    $('#modal-edit-user-group-role').trigger('closeModal');
  })
  .fail(function(){

  });
});
</script>