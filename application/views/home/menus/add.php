<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid" id="content-right">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">						
			<!-- END BEGIN STYLE CUSTOMIZER -->   	
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->			
			<h3 class="page-title">
				Managemen Menu			
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="<?php echo base_url();?>home">Beranda</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#" data-toggle="tooltip" rel="tooltip" data-placement="top" title="Klik untuk melihat menu management" onClick="routes('menus','List Menu')">Menu </a>	
				<i class="icon-angle-right"></i>	
				 </li>	
				<li><a href="#" data-toggle="tooltip" rel="tooltip" data-placement="top" title="Klik untuk melihat menu management"> Add</a> </li>						
			</ul>			
			<div class="portlet box green tabbable">
				<div class="portlet-title">
					<h4>
						<i class="icon-plus"></i>
						<span class="hidden-480">Tambah Menu</span>
					</h4>
				</div>
				<div class="portlet-body form">
					<div class="tabbable portlet-tabs">
						<p>
						<div class="tab-content">
							<div id="portlet_tab1" class="tab-pane active">
								<form name="fform" id="fform" class="form-horizontal">
									<div class="control-group">
										  <label class="control-label">Name</label>
										  <div class="controls">
											 <input name="name" id="name" type="text" value="" class="span6 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="Name" />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">URL</label>
										  <div class="controls">
											 <input name="url" id="url" type="text" value="" class="span6 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="URL" />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Order No</label>
										  <div class="controls">
											 <input name="order_no" id="order_no" type="text" value="" class="span2 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="order_no" />
										  </div>
									 </div>
									  <div class="control-group">
										  <label class="control-label">Icon</label>
										  <div class="controls">
											 <input name="icon" id="icon" type="text" value="" class="span2 m-wrap popovers" data-trigger="hover" data-content="Harus diisi" data-original-title="order_no" />
										  </div>
									 </div>
									 <div class="control-group">
										  <label class="control-label">Parent ID</label>
										  <div class="controls">
											  <select class="m-wrap span6" tabindex="1" name="parent_id" id="parent_id">
													<option value="0">Root</option>
													<?php foreach($menus as $menu){?>
														<option value="<?php echo $menu->id;?>"><?php echo $menu->name?></option>
													<?php } ?>
											  </select>
										  </div>
									 </div>				  
									 <div class="control-group">
										<div class="controls">							
											<button class="btn blue" type="button" onClick="save()"><i class="icon-save"></i> Simpan</button>
											<button class="btn blue" type="button" onClick="routes('menus','Managemen Menu')"><i class="icon-share"></i> Kembali</button>
											
										</div>
									 </div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
	</div>
	<!-- END PAGE HEADER-->
</div>
<!-- END PAGE CONTAINER-->		
<link href="<?php echo base_url();?>assets/autocomplete/select2.css" rel="stylesheet"/>
<script src="<?php echo base_url();?>assets/autocomplete/select2.js"></script>
<script language="javascript">
	$(document).ready(function() { $("#parent_id").select2(); });	
	function validate() {
		if ($('#name').val() =='') {
			new Messi('Name masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("name").focus(); }});
			return false;
		}
		if ($('#url').val() =='') {
			new Messi('URL masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("url").focus(); }});
			return false;
		}
		if ($('#order_no').val() =='') {
			new Messi('Order no masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("order_no").focus(); }});
			return false;
		}
		if ($('#parent_id').val() =='') {
			new Messi('Parent ID masih kosong, mohon diisi !', {title: 'Warning', titleClass: 'anim warning', buttons: [{id: 0, label: 'OK', val: 'X'}], callback:function(){ document.getElementById("parent_id").focus();  }});
			return false;
		}
	}
			
	function save() {
		if (validate()==false) {
			return false;
		} else {
			var str = $("#fform").serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>menus/save",
				data: str,
				success: function(msg){
					if(msg=='1') {
						new Messi('Data berhasil disimpan !', {title: 'Menu Message', titleClass: 'anim info', buttons: [{id: 0, label: 'Close', val: 'X'}], callback:function(){ routes('menus','Menu Management'); }});
					}else if (msg=='2') {
						new Messi('Menu sudah terdaftar !<br />Pesan : '+msg, {title: 'Menu Message', titleClass: 'anim warning', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					}  else {
						new Messi('Data gagal disimpan !<br />Pesan : '+msg, {title: 'Menu Message', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
					} 
				},
				error: function(fnc,msg){
					new Messi('Tidak dapat terhubung ke server untuk malakukan proses update data !', {title: 'Error', titleClass: 'anim error', buttons: [{id: 0, label: 'Close', val: 'X'}]});
				}
			});
		}	
	};	
</script>