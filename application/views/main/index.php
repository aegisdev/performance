<!DOCTYPE html>
<html lang="en"> 
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>Performance | Home</title>
	
	<?php echo $css;?>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="fixed-top">
	<!-- BEGIN HEADER -->
	<div class="header navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container-fluid">
				<!-- BEGIN LOGO -->
				<a class="brand" href="<?php echo base_url();?>home">
				<img src="#" alt="logo" width="133px" />
				</a>
				<!-- END LOGO -->
				<!-- BEGIN RESPONSIVE MENU TOGGLER -->
				<a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
				<img src="<?php echo base_url();?>assets/img/menu-toggler.png" alt="" />
				</a>          
				<!-- END RESPONSIVE MENU TOGGLER -->				
				<!-- BEGIN TOP NAVIGATION MENU -->					
				<ul class="nav pull-right">					
					
					<!-- BEGIN USER LOGIN DROPDOWN -->
					<li class="dropdown user">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img alt="" src="<?php echo base_url();?>assets/img/avatar1_small.jpg" />
						<span class="username"><?php echo $this->session->userdata('name');?></span>
						<i class="icon-angle-down"></i>
						</a>
						<ul class="dropdown-menu">											
							<li><a href="<?php echo base_url();?>login/logout"><i class="icon-key"></i> Log Out</a></li>
						</ul>
					</li>
					<!-- END USER LOGIN DROPDOWN -->
				</ul>
				<!-- END TOP NAVIGATION MENU -->	
			</div>
		</div>
		<!-- END TOP NAVIGATION BAR -->
	</div>
	<!-- END HEADER -->
	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        	
			<ul>
				<li class="start">
					<a href="<?php echo base_url();?>main">
						<i class="icon-dashboard"></i> 
						<span class="title">Dashboard</span>
						<span class="selected"></span>
					</a>
				</li>	
				<li class="start">
					<a href="#" onClick="routes('menus','Menu Management')">
						<i class="icon-cogs"></i> 
						<span class="title">Menu Management</span>
						<span class="selected"></span>
					</a>
				</li>		
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<!-- BEGIN PAGE CONTAINER-->
			<div class="container-fluid" id="content-right">
				<?php echo $right;?>
			</div>
			<!-- END PAGE CONTAINER-->		
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
		
	<!-- BEGIN FOOTER -->
	<div class="footer">
		<?php echo $footer;?>
	</div>
	<?php echo $js;?>			
	<script language="javascript">		
		function routes(id,title) {
			$("h3").text(title);
			$('#content-right').load('<?php echo base_url();?>'+id,{noncache:Math.random()*99999});
			$('#loader').show();
		}
		$(document).ready(function() {		
			$('body').delegate('.open-modal','click', function(ev){
				ev.preventDefault();
				$('#myModalLabel').html($(this).attr('data-title'));
				$('#main-modal-body').html('');
				$("#myModal").modal({show:true,backdrop:true});
			});					
		});
		function loadRightContent(ajaxUrl){
			$.ajax({
				url: ajaxUrl,
				success: function(response){
					$('#content-right').html(response);
				},
				error: function(e){

				},
				complete: function(){

				}
			});
	}	
	</script>
	<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
